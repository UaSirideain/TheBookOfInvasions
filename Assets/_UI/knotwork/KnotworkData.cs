﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Knotwork Border", menuName = "Knotwork/Border", order = 1)]
public class KnotworkData : ScriptableObject
{
	[SerializeField] public Sprite horizontalSection;
	[SerializeField] public Sprite verticalSection;
	[SerializeField] public Sprite topLeft;
	[SerializeField] public Sprite cornerBottomLeft;
	[SerializeField] public Sprite cornerTopRight;
	[SerializeField] public Sprite cornerBottomRight;
	[SerializeField] public Sprite endLeft;
	[SerializeField] public Sprite endTop;

	[SerializeField] public Color colour;
}