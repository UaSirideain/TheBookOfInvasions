﻿using UnityEngine;
using System.Collections;

public class RotateSprite : MonoBehaviour
{

	[SerializeField] float rotateSpeed;
	void
	Update ()
	{
		transform.Rotate (Vector3.forward * rotateSpeed);
	}
}
