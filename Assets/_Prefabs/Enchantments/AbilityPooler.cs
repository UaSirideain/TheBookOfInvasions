﻿using System.Collections.Generic;
using UnityEngine;

public enum PoolTag { LightStone, FireSpray }

public class AbilityPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public PoolTag tag;
        public GameObject prefab;
        public int size;
    }

    #region Singleton

    public static AbilityPooler instance;
    private void 
    Awake()
    {
        instance = this;
    }
    
    #endregion

    public List<Pool> pools;
    public Dictionary<PoolTag, Queue<GameObject>> poolDictionary;

    private void
    Start()
    {
        poolDictionary = new Dictionary<PoolTag, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> abilityPool = new Queue<GameObject>();
            for (int index = 0; index < pool.size; index++)
            {
                GameObject gameObj = Instantiate(pool.prefab);
                gameObj.SetActive(false);
                gameObj.transform.SetParent(transform, false);
                abilityPool.Enqueue(gameObj);
            }
            poolDictionary.Add(pool.tag, abilityPool);
        }
    }

    public GameObject
    Create(PoolTag tag, Vector3 position, Quaternion rotation)
    {
        if (poolDictionary.ContainsKey(tag))
        {
            GameObject abilityToSpawn = poolDictionary[tag].Dequeue();
            abilityToSpawn.SetActive(true);
            abilityToSpawn.transform.position = position;
            abilityToSpawn.transform.rotation = rotation;

            poolDictionary[tag].Enqueue(abilityToSpawn);
            return abilityToSpawn;
        }
        Debug.Log("Pool with tag " + tag + " doesn't exist");
        return null;
    }
}