﻿using System.Collections.Generic;
using UnityEngine;

public class PathGuide : MonoBehaviour
{
    private List<Direction> paths = new List<Direction>();

    public void
    AddDirections(List<Direction> directions)
    {
        foreach (Direction direction in directions)
        {
            if (!paths.Contains(direction))
            {
                paths.Add(direction);
            }
        }
    }

    public void
    RemoveDirections()
    {
        paths.Clear();
    }

    public List<Direction>
    GetDirections()
    {
        return paths;
    }

    public Vector3
    CalculatePath(Vector3 moveDirection)
    {
        if (moveDirection != Vector3.zero)
        {
            moveDirection = FindNearestDirection(moveDirection).direction;
            moveDirection.y = 0f;
        }
        return moveDirection;
    }

    private Direction
    FindNearestDirection(Vector3 moveDirection)
    {
        Direction nearestDirection = new Direction();
        float difference = 100;
        foreach (Direction path in paths)
        {
            float distance = Vector3.Distance(path.direction, moveDirection);
            if (distance < difference)
            {
                difference = distance;
                nearestDirection = path;
            }
        }

        return nearestDirection;
    }
}