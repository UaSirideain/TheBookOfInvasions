﻿using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{
    PathBuilder builder { get { return GetComponentInChildren<PathBuilder>(); } }
    PathGuide guide { get { return GetComponentInChildren<PathGuide>(); } }

    [SerializeField] private List<Transform> pointList = new List<Transform>();

    List<Direction> avatarToPointDirections = new List<Direction>();

    public void
    SetupAvatarLink(AvatarGuideLink avatarLink)
    {
        avatarToPointDirections = builder.SetListAndBuild(new List<Transform>() { avatarLink.transform.parent, ClosestPoint(avatarLink.transform.parent)});
        guide.AddDirections(avatarToPointDirections);
        avatarLink.SetGuide(guide);
    }

    public void
    DisableAvatarLink(AvatarGuideLink avatarLink)
    {
        avatarLink.SetGuide(null);
        guide.RemoveDirections();
    }

    public void
    RemoveInitialiserDirectionsFromGuide()
    {
        guide.RemoveDirections();
        guide.AddDirections(builder.SetListAndBuild(pointList));

    }
    
    private Transform
    ClosestPoint(Transform avatar)
    {
        Transform closestPoint = null;
        float distance = 100f;
        foreach (Transform point in pointList)
        {
            if (distance > Vector3.Distance(avatar.position, point.position))
            {
                distance = Vector3.Distance(avatar.position, point.position);
                closestPoint = point;
            }
        }
        return closestPoint;
    }
}
