﻿using System.Collections.Generic;
using UnityEngine;

public class PathBuilder : MonoBehaviour
{
    public List<Direction>
    SetListAndBuild(List<Transform> newPointList)
    {
        return BuildDirections(newPointList);
    }

    private List<Direction>
    BuildDirections(List<Transform> pointList)
    {
        List<Direction> availableDirections = new List<Direction>();
        for (int index = 0; index < pointList.Count; index++)
        {
            if ((index + 1) < pointList.Count)
            {
                Direction direction = new Direction();
                direction = direction.SetPath(pointList[index], pointList[index + 1]);
                Direction oppositeDirection = direction.SetPath(pointList[index + 1], pointList[index]);
                availableDirections.Add(direction);
                availableDirections.Add(oppositeDirection);
            }
        }
        return availableDirections;
    }
}