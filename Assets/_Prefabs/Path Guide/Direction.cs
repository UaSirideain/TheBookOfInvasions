﻿using UnityEngine;

public class Direction
{
    public Direction(Vector3 direction = new Vector3())
    {
        currentDirection = direction;
    }

    private Vector3 currentDirection;
    public Vector3 direction { get { return currentDirection; } }

    public Direction
    SetPath(Transform start, Transform end)
    {
        return new Direction((end.position - start.position).normalized);
    }

    public override bool
    Equals(object pathObject)
    {
        Direction direction = (Direction)pathObject;
        return currentDirection == direction.currentDirection;
    }

    public override int 
    GetHashCode()
    {
        return base.GetHashCode();
    }
}