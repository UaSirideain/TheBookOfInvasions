﻿using UnityEngine;

public class PathTrigger : MonoBehaviour
{
    PathManager pathManager { get { return GetComponentInParent<PathManager>(); } }

    private void
    OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AvatarGuideLink>() || other.GetComponentInChildren<AvatarGuideLink>())
        {
            pathManager.RemoveInitialiserDirectionsFromGuide();
        }
    }
}
