﻿using UnityEngine;

public class PathCollider : MonoBehaviour
{
    PathManager pathManager { get { return GetComponentInParent<PathManager>(); } }

    private void 
    OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AvatarGuideLink>() || other.GetComponentInChildren<AvatarGuideLink>())
        {
            AvatarGuideLink avatarLink = other.GetComponent<AvatarGuideLink>() ? other.GetComponent<AvatarGuideLink>() : other.GetComponentInChildren<AvatarGuideLink>();
            pathManager.SetupAvatarLink(avatarLink);
        }
    }

    private void 
    OnTriggerExit(Collider other)
    {
        if (other.GetComponent<AvatarGuideLink>() || other.GetComponentInChildren<AvatarGuideLink>())
        {
            AvatarGuideLink avatarLink = other.GetComponent<AvatarGuideLink>() ? other.GetComponent<AvatarGuideLink>() : other.GetComponentInChildren<AvatarGuideLink>();
            pathManager.DisableAvatarLink(avatarLink);
        }
    }
}