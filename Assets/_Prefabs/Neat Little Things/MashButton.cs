﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MashButton : MonoBehaviour
{
	Color backgroundFlashColour = new Color(1, 1, 1, 1);
	Color backgroundRegularColour;

	[SerializeField] Image background;
	[SerializeField] Image fill;
	RectTransform fillRT;
	RectTransform backgroundRT;

	float percentageFilled = 0f;
	const float percentageFilledOnPress = .4f;

	void
	Start()
	{
		backgroundRegularColour = background.color;
		fillRT = fill.GetComponent<RectTransform> ();
		backgroundRT = background.GetComponent<RectTransform> ();
		targetFill = fillRT.sizeDelta;
	}

    void
	Update()
    {
		if (Input.GetKeyDown("e"))
		{
			StartCoroutine(Fill ());
		}
    }

	int lerpTicket = 0;

	Vector2 startFill;
	Vector2 targetFill;

	IEnumerator
	Fill()
	{
		percentageFilled = Mathf.Clamp01(percentageFilled + percentageFilledOnPress);

		startFill = fillRT.sizeDelta;
		targetFill = new Vector2(backgroundRT.sizeDelta.x * percentageFilled, fillRT.sizeDelta.y);

		lerpTicket++;
		int myLerpTicket = lerpTicket;
	
		background.color = backgroundFlashColour;

		float t = 0f;

		while (myLerpTicket == lerpTicket && t < 1f)
		{
			background.color = Mathfx.Sinerp (backgroundFlashColour, backgroundRegularColour, t);
			fillRT.sizeDelta = Mathfx.Sinerp (startFill, targetFill, t);

			t += Time.deltaTime / .5f;

			yield return null;
		}

		if (myLerpTicket == lerpTicket)
		{
			background.color = backgroundRegularColour;
		}
	}
}