﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Battle;

public class Injections : MonoBehaviour
{
    public static Injections references;
    [SerializeField] public Host allyHostPrefab;
    [SerializeField] public Host enemyHostPrefab;
    [SerializeField] public List<Persistency> defaultBattleEndPersistencies;

    void
    Awake()
    {
        references = this;
    }
}