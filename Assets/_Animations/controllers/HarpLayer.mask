%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: HarpLayer
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: _body
    m_Weight: 1
  - m_Path: _cape
    m_Weight: 1
  - m_Path: _hair
    m_Weight: 1
  - m_Path: Reference
    m_Weight: 1
  - m_Path: Reference/Hips
    m_Weight: 1
  - m_Path: Reference/Hips/LeftUpLeg
    m_Weight: 1
  - m_Path: Reference/Hips/LeftUpLeg/LeftLeg
    m_Weight: 1
  - m_Path: Reference/Hips/LeftUpLeg/LeftLeg/LeftFoot
    m_Weight: 1
  - m_Path: Reference/Hips/LeftUpLeg/LeftLeg/LeftFoot/LeftFootNub
    m_Weight: 1
  - m_Path: Reference/Hips/RightUpLeg
    m_Weight: 1
  - m_Path: Reference/Hips/RightUpLeg/RightLeg
    m_Weight: 1
  - m_Path: Reference/Hips/RightUpLeg/RightLeg/RightFoot
    m_Weight: 1
  - m_Path: Reference/Hips/RightUpLeg/RightLeg/RightFoot/RightFootNub
    m_Weight: 1
  - m_Path: Reference/Hips/Spine
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/LeftShoulder
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/LeftShoulder/LeftArm
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/LeftShoulder/LeftArm/LeftForeArm
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/LeftShoulder/LeftArm/LeftForeArm/LeftHand
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/LeftShoulder/LeftArm/LeftForeArm/LeftHand/LeftHandNub
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/Neck
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/Neck/Head
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/Neck/Head/HeadNub
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/RightShoulder
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/RightShoulder/RightArm
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/RightShoulder/RightArm/RightForeArm
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/RightShoulder/RightArm/RightForeArm/RightHand
    m_Weight: 1
  - m_Path: Reference/Hips/Spine/RightShoulder/RightArm/RightForeArm/RightHand/RightHandNub
    m_Weight: 1
