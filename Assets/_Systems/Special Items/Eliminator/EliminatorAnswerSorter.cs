﻿using System.Collections.Generic;

public class EliminatorAnswerSorter
{
    Dictionary<AnswerCategory, List<EliminatorAnswer>> sortedAnswers = new Dictionary<AnswerCategory, List<EliminatorAnswer>>();

    public Dictionary<AnswerCategory, List<EliminatorAnswer>>
    SortAnswersIntoCategories(List<EliminatorAnswer> unsortedAnswers)
    {
        sortedAnswers.Clear();

        foreach (EliminatorAnswer answer in unsortedAnswers)
        {
            SortAnswer(answer);
        }

        return sortedAnswers;
    }

    private void
    SortAnswer(EliminatorAnswer answer)
    {
        AnswerCategory answerType = answer.GetAnswerType();
        if (sortedAnswers.ContainsKey(answerType))
        {
            sortedAnswers[answerType].Add(answer);
        }
        else
        {
            sortedAnswers.Add(answerType, new List<EliminatorAnswer>() { answer });
        }
    }
}
