﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EliminatorUICategory : MonoBehaviour
{
    RectTransform rectTransform { get { return GetComponent<RectTransform>(); } }

    [SerializeField] private Text title;
    [SerializeField] private PotentialAnswerButton uiAnswerPrefab;
    [SerializeField] private AnswerCategory categoryType;

    List<EliminatorAnswer> activeAnswers = new List<EliminatorAnswer>();
    Dictionary<EliminatorAnswer, PotentialAnswerButton> uiAnswers = new Dictionary<EliminatorAnswer, PotentialAnswerButton>();

    public AnswerCategory
    GetCategoryType()
    {
        return categoryType;
    }

    public void
    Initialise(AnswerCategory newCategory)
    {
        categoryType = newCategory;
        title.text = categoryType.ToString().ToUpper();
    }

    public void
    MergeAnswers(List<EliminatorAnswer> pendingAnswers)
    {
        bool answersAdded;
        bool answersDeleted;

        AddAnswers(pendingAnswers, out answersAdded);
        DeleteAnswers(pendingAnswers, out answersDeleted);

        if (answersAdded || answersDeleted)
        {
            RenderCategory(GetUIAnswers());
        }
    }

    public void
    AnswerSelected()
    {
        foreach (PotentialAnswerButton button in GetUIAnswers())
        {
            button.SetUpOnClick(FindObjectOfType<EliminatorUI>());
        }
    }

    private List<PotentialAnswerButton>
    GetUIAnswers()
    {
        List<PotentialAnswerButton> answerButtons = new List<PotentialAnswerButton>();

        foreach (EliminatorAnswer answer in activeAnswers)
        {
            answerButtons.Add(uiAnswers[answer]);
        }

        return answerButtons;
    }

    private void
    AddAnswers(List<EliminatorAnswer> pendingAnswers, out bool answersAdded)
    {
        answersAdded = false;

        foreach (EliminatorAnswer pendingAnswer in pendingAnswers)
        {
            if (!pendingAnswer.IsInList(activeAnswers))
            {
                activeAnswers.Add(pendingAnswer);
                uiAnswers.Add(pendingAnswer, CreateUIAnswer(pendingAnswer));
                answersAdded = true;
            }
        }
    }

    private void
    DeleteAnswers(List<EliminatorAnswer> pendingAnswers, out bool answersDeleted)
    {
        answersDeleted = false;

        for (int i = Mathf.Clamp(activeAnswers.Count - 1, 0, 1000); i >= 0; i--)
        {
            if (!activeAnswers[i].IsInList(pendingAnswers))
            {
                uiAnswers.Remove(activeAnswers[i]);
                activeAnswers.RemoveAt(i);
                answersDeleted = true;
            }
        }
    }

    private PotentialAnswerButton
    CreateUIAnswer(EliminatorAnswer plainAnswer)
    {
        PotentialAnswerButton uiAnswer = Instantiate(uiAnswerPrefab);
        uiAnswer.Initialise(plainAnswer);
        return uiAnswer;
    }

    private void
    RenderCategory(List<PotentialAnswerButton> answerButtons)
    {
        for (int index = 0; index < answerButtons.Count; index++)
        {
            RectTransform rt = answerButtons[index].GetComponent<RectTransform>();
            rt.SetAsChildOf(rectTransform);
            rt.anchoredPosition = new Vector3(0, rt.sizeDelta.y * -(index + 1), 0);
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, uiAnswerPrefab.GetComponent<RectTransform>().sizeDelta.y * transform.childCount - 1);
        }
    }
}