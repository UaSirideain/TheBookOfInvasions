﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliminatorMaterial : MonoBehaviour
{
	[SerializeField] Texture orangeEmission;
	[SerializeField] Texture blueEmission;

	[SerializeField] MeshRenderer materialToChange;
	[SerializeField] float duration;
	Color currentColour;

	public void
	SetOrangeAnswer()
	{
		materialToChange.material.SetTexture ("_EmissionMap", orangeEmission);
		StartCoroutine (LerpEmission (Color.black, Color.white));
	}

	public void
	SetBlueAnswer()
	{
		materialToChange.material.SetTexture ("_EmissionMap", blueEmission);
		StartCoroutine (LerpEmission (Color.black, Color.white));
	}

	public void
	SetRandomAnswer(int answer)
	{
		if (answer == 0)
		{
			SetOrangeAnswer ();
		}
		else if (answer == 1)
		{
			SetBlueAnswer ();
		}
	}

	public void
	SetNoAnswer()
	{
		if (materialToChange.material.GetColor("_EmissionColor") == Color.white)
		{
			StartCoroutine (LerpEmission (Color.white, Color.black));
		}
	}

	public void
	ResetMaterial()
	{
		materialToChange.material.SetColor ("_EmissionColor", Color.black);
	}


	IEnumerator
	LerpEmission(Color start, Color end)
	{
		materialToChange.material.SetColor ("_EmissionColor", start);

		yield return new WaitForSeconds (.5f);

		float t = 0f;
	
		while (t < 1.2f)
		{
			materialToChange.material.SetColor ("_EmissionColor", Color.Lerp(start, end, t));
			t += Time.deltaTime / duration;
			yield return null;
		}
	}
}