﻿using System.Collections.Generic;
using UnityEngine;

public class EliminatorUIBuilder : MonoBehaviour
{
    [SerializeField] RectTransform eliminatorPanel;
    [SerializeField] EliminatorUICategory categoryPrefab;

    EliminatorAnswerSorter sorter = new EliminatorAnswerSorter();

    [SerializeField] List<EliminatorUICategory> activeCategories = new List<EliminatorUICategory>();

    public List<EliminatorUICategory>
    BuildUI()
    {
        BuildCategoryColumns();
        BuildCategoryButtons();
        return activeCategories;
    }

    private void
    BuildCategoryColumns()
    {
        Dictionary<AnswerCategory, List<EliminatorAnswer>> categorisedAnswers = sorter.SortAnswersIntoCategories(FindObjectOfType<EliminatorAnswerContainer>().GetAnswers());

        CreateSpecificCategory(categorisedAnswers, AnswerCategory.TheDoors);
        CreateSpecificCategory(categorisedAnswers, AnswerCategory.RitualOffice);
        CreateSpecificCategory(categorisedAnswers, AnswerCategory.General);

        RenderCategories();
    }

    private void
    BuildCategoryButtons()
    {
        foreach (EliminatorUICategory category in activeCategories)
        {
            category.AnswerSelected();
        }
    }

    private void
    CreateSpecificCategory(Dictionary<AnswerCategory, List<EliminatorAnswer>> categorisedAnswers, AnswerCategory targetType)
    {
        if (categorisedAnswers.ContainsKey(targetType))
        {
            GetAppropriateCategory(targetType).MergeAnswers(categorisedAnswers[targetType]);
        }
        else
        {
            DestroyAppropriateCategory(targetType);
        }
    }

    private EliminatorUICategory
    GetAppropriateCategory(AnswerCategory targetType)
    {
        foreach (EliminatorUICategory oldCategory in activeCategories)
        {
            if (oldCategory.GetCategoryType() == targetType)
            {
                return oldCategory;
            }
        }

        EliminatorUICategory newCategory = Instantiate(categoryPrefab);
        newCategory.Initialise(targetType);
        activeCategories.Add(newCategory);
        return newCategory;
    }

    private void
    DestroyAppropriateCategory(AnswerCategory targetType)
    {
        if (activeCategories.Count > 0)
        {
            for (int index = Mathf.Clamp(activeCategories.Count - 1, 0, 1000); index >= 0; index--)
            {
                if (activeCategories[index].GetCategoryType() == targetType)
                {
                    GameObject emptyCategory = activeCategories[index].gameObject;
                    activeCategories.RemoveAt(index);
                    Destroy(emptyCategory);
                }
            }
        }
    }

    private void
    RenderCategories()
    {
        float nextCategoryYPosition = 0f;

        for (int index = 0; index < activeCategories.Count; index++)
        {
            RectTransform category_t = activeCategories[index].GetComponent<RectTransform>();
            category_t.SetAsChildOf(eliminatorPanel);
            category_t.anchoredPosition = new Vector2(0, nextCategoryYPosition);
            nextCategoryYPosition -= (category_t.sizeDelta.y + 50);
        }
    }
}