﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The eliminator.
/// </summary>
public class EliminatorAnswerContainer : MonoBehaviour
{	
	[SerializeField] List<EliminatorAnswer> eliminatorAnswers = new List<EliminatorAnswer>();

	public List<EliminatorAnswer>
	GetAnswers()
	{
		return eliminatorAnswers;
	}

	public void
	AddAnswers(List<EliminatorAnswer> newAnswers)
	{
		foreach (EliminatorAnswer newAnswer in newAnswers)
		{
			if (!eliminatorAnswers.Contains (newAnswer))
			{
				eliminatorAnswers.Add (newAnswer);
			}
		}
	}

    /// <summary>
    /// Deletes the specified answers from the full list of eliminator answers
    /// </summary>
    /// <param name="oldAnswers"></param>
    
    public void
    DeleteAnswers(List<EliminatorAnswer> oldAnswers)
    {
        foreach (EliminatorAnswer oldAnswer in oldAnswers)
        {
            if (eliminatorAnswers.Contains(oldAnswer))
            {
                eliminatorAnswers.Remove(oldAnswer);
            }
        }
    }

    /// <summary>
    /// Removes specified answers from a specified category
    /// </summary>
    /// <param name="oldAnswers"></param>
    /// <param name="category"></param>

    public void
    DeleteAnswers(List<EliminatorAnswer> oldAnswers, AnswerCategory category)
    {
        foreach (EliminatorAnswer oldAnswer in oldAnswers)
        {
            if (eliminatorAnswers.Contains(oldAnswer) && oldAnswer.GetAnswerType().Equals(category))
            {
                eliminatorAnswers.Remove(oldAnswer);
            }
        }
    }

    /// <summary>
    /// Clears the list of answers stored in the specified category
    /// </summary>
    /// <param name="category"></param>

    public void
    ClearAnswers(AnswerCategory category)
    {
        if (eliminatorAnswers.Count > 0)
        {
            for (int answerIndex = Mathf.Clamp(eliminatorAnswers.Count - 1, 0, 1000); answerIndex >= 0; answerIndex--)
            {
                if (eliminatorAnswers[answerIndex].GetAnswerType().Equals(category))
                {
                    eliminatorAnswers.RemoveAt(answerIndex);
                }
            }
        }
    }

    /// <summary>
    /// Clears the list of ALL answers stored by the eliminator
    /// </summary>

    public void
    ClearAnswers()
    {
        eliminatorAnswers.Clear();
    }
}