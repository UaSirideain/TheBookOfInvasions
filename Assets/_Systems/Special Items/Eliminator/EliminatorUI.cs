﻿using GUIManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Field.Abilities;

public class EliminatorUI : MonoBehaviour, IInputtable, IGUI
{
	[SerializeField] PotentialAnswerButton answerListEntryPrefab;
	List<PotentialAnswerButton> answersList = new List<PotentialAnswerButton>();

	[SerializeField] Transform background;

	[SerializeField] GameObject checkButton;

	[SerializeField] Transform orangeSlot;
	[SerializeField] Transform blueSlot;

	bool orangeSlotFull = false;
	bool blueSlotFull = false;

	EliminatorAnswer orangeSlotAnswer;
	EliminatorAnswer blueSlotAnswer;

	[SerializeField] EliminatorMaterial eliminator;

	EliminatorEffect fieldItem;

	bool active = false;

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (){InputState.Battle}, ExitUI_Input);
	}

	GUIController controller;

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
		transform.SetParent (controller.transform);
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.FullscreenUI;
	}

	void
	ExitUI_Input()
	{
		if (active)
		{
			Deactivate ();
		}
	}

    List<EliminatorUICategory> categories = new List<EliminatorUICategory>();

    public void
	Activate(EliminatorEffect newFieldItem)
	{
		fieldItem = newFieldItem;

		ResetUI ();

        categories = GetComponent<EliminatorUIBuilder>().BuildUI();

		GetComponentInChildren<BackgroundDimmer> ().DimBackground ();

		active = true;
		controller.RegisterActivatedUI (this);
	}

	public void
	Deactivate()
	{
		foreach (PotentialAnswerButton button in answersList)
		{
			Destroy (button.gameObject);
		}
        ResetUI();
		fieldItem.Disable (null);
		active = false;

		controller.RegisterDeactivatedGUI (this);
	}

	void
	ResetUI()
	{
        RemoveAnswerFromOrangeSlot();
        RemoveAnswerFromBlueSlot();
        answersList.Clear();
        eliminator.ResetMaterial ();
	}

	public void
	ClickAnswerButton(PotentialAnswerButton answer)
	{
		if (!blueSlotFull || !orangeSlotFull)
		{
			answer.Deactivate ();
			AddAnswerToSlot (answer.GetAnswer ());
		}
        DeactivateOtherCategories(answer.GetAnswer().GetAnswerType());
	}

    private void
    DeactivateOtherCategories(AnswerCategory answerType)
    {
        foreach (EliminatorUICategory category in categories)
        {
            if (!category.GetCategoryType().Equals(answerType))
            {
                foreach (PotentialAnswerButton button in category.transform.GetComponentsInChildren<PotentialAnswerButton>())
                {
                    button.Deactivate();
                }
            }
        }
    }

	void
	AddAnswerToSlot(EliminatorAnswer answer)
	{
		if (orangeSlotFull)
		{
			blueSlotFull = true;
			blueSlotAnswer = answer;
			blueSlot.GetComponentInChildren<Text> ().text = answer.GetName();
		}
		else
		{
			orangeSlotFull = true;
			orangeSlotAnswer = answer;
			orangeSlot.GetComponentInChildren<Text> ().text = answer.GetName();
		}
			
		SetCheckButtonActive ();
	}

	public void
	RemoveAnswerFromOrangeSlot()
	{
		string answerName = "";

		if (orangeSlotFull)
		{
			orangeSlot.GetComponentInChildren<Text> ().text = "";
			answerName = orangeSlotAnswer.GetName();
			orangeSlotAnswer = null;
			orangeSlotFull = false;
		}

        ActivateButtons(answerName);
	}

    public void
    RemoveAnswerFromBlueSlot()
    {
        string answerName = "";

        if (blueSlotFull)
        {
            blueSlot.GetComponentInChildren<Text>().text = "";
            answerName = blueSlotAnswer.GetName();
            blueSlotAnswer = null;
            blueSlotFull = false;
        }

        ActivateButtons(answerName);
    }

    private void
    ActivateButtons(string answerName)
    {
        if (!orangeSlotFull && !blueSlotFull)
        {
            foreach (EliminatorUICategory category in categories)
            {
                foreach (PotentialAnswerButton button in category.transform.GetComponentsInChildren<PotentialAnswerButton>())
                {
                    if (!button.GetAnswer().HasBeenAsked())
                    {
                        button.Activate();
                    }
                }
            }
        }
        else
        {
            foreach (EliminatorUICategory category in categories)
            {
                foreach (PotentialAnswerButton button in category.transform.GetComponentsInChildren<PotentialAnswerButton>())
                {
                    if (!button.GetAnswer().HasBeenAsked())
                    {
                        if (button.GetAnswer().GetName() == answerName)
                        {
                            button.Activate();
                        }
                    }
                }
            }
        }

        SetCheckButtonActive();
    }

	void
	SetCheckButtonActive()
	{
		checkButton.SetActive (orangeSlotFull && blueSlotFull);
	}

	public void
	CheckAnswers()
	{
		if (orangeSlotAnswer.IsDangerous() && blueSlotAnswer.IsDangerous())
		{
			eliminator.SetRandomAnswer(Random.Range (0, 2));
		}
		else if (orangeSlotAnswer.IsDangerous())
		{
			eliminator.SetOrangeAnswer ();
		}
		else
		{
			eliminator.SetBlueAnswer ();
		}

        MarkAnswersAsAsked();
	}

    private void 
    MarkAnswersAsAsked()
    {
        orangeSlotAnswer.SetAsAsked();
        blueSlotAnswer.SetAsAsked();
    }
}