﻿using System.Collections.Generic;
using UnityEngine;

public enum AnswerCategory { None, TheDoors, RitualOffice, General }

[System.Serializable]
public class EliminatorAnswer : System.Object
{
    [SerializeField] private string answerName = "";
    [SerializeField] private AnswerCategory answerType = AnswerCategory.General;
    [SerializeField] private bool isDangerous = true;
    [SerializeField] private bool hasBeenAsked = false;

    public bool
    IsInList(List<EliminatorAnswer> answers)
    {
        foreach (EliminatorAnswer answer in answers)
        {
            if (answer.GetName().Equals(answerName) && answer.HasBeenAsked().Equals(HasBeenAsked()))
            {
                return true;
            }
        }
        return false;
    }

    public string
    GetName()
    {
        return answerName;
    }

    public void
    SetName(string newName)
    {
        answerName = newName;
    }

    public void
    SetAnswerType(AnswerCategory answerCategory)
    {
        answerType = answerCategory;
    }

    public AnswerCategory
    GetAnswerType()
    {
        return answerType;
    }

    public bool
    IsDangerous()
    {
        return isDangerous;
    }

    public void
    SetAsDangerous()
    {
        isDangerous = true;
    }

    public void
    SetAsSafe()
    {
        isDangerous = false;
    }

    public bool
    HasBeenAsked()
    {
        return hasBeenAsked;
    }

    public void
    SetAsAsked()
    {
        hasBeenAsked = true;
    }
}