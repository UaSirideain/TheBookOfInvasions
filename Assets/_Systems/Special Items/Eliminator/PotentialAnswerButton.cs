﻿using UnityEngine;
using UnityEngine.UI;

public class PotentialAnswerButton : MonoBehaviour
{
	EliminatorAnswer answerInfo;
	[SerializeField] Button button;
	[SerializeField] Text text;

	public void
	Initialise(EliminatorAnswer newAnswer)
	{
		answerInfo = newAnswer;
		if (answerInfo.HasBeenAsked())
		{
			Deactivate ();
		}

		text.text = answerInfo.GetName();
	}

	public void
	Activate()
	{
		button.enabled = true;
		text.color = new Color (1, 1, 1, 1);
	}

	public void
	Deactivate()
	{
		button.enabled = false;
		text.color = new Color (1, 1, 1, .35f);
    }

    public void
    SetUpOnClick(EliminatorUI ui)
    {
        transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { ui.ClickAnswerButton(this); });
    }

	public EliminatorAnswer
	GetAnswer()
	{
		return answerInfo;
	}
}