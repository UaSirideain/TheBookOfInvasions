﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFollow : MonoBehaviour
{
	[SerializeField] List<Transform> waypoints = new List<Transform>();
	Vector3[] points;

	Transform listener;
	Transform tr;

	void
	Awake ()
	{
		if (waypoints.Count < 2 || waypoints.Contains (null))
		{
			print (transform.parent.name + ": Not enough waypoints for audio source to form a line.");
			this.enabled = false;
		}
		else
		{
			points = new Vector3[waypoints.Count];
			for (int i = 0; i < waypoints.Count; i++)
			{
				points [i] = waypoints [i].position;
			}

			listener = GameObject.FindObjectOfType<AudioListener>().transform;
			tr = transform;
		}
	}
	
	void
	Update ()
	{
		System.Array.Sort<Vector3> (points, delegate(Vector3 way1, Vector3 way2){return Vector3.Distance (way1, listener.position).CompareTo (Vector3.Distance (way2, listener.position));});

		tr.position = Vector3.Lerp (tr.position, ClosestPointOnLine (points [0], points [1], listener.position), Time.deltaTime * 10);
	}

	Vector3
	ClosestPointOnLine(Vector3 vA, Vector3 vB, Vector3 vPoint)
	{
		var vVector1 = vPoint - vA;
		var vVector2 = (vB - vA).normalized;

		var d = Vector3.Distance (vA, vB);
		var t = Vector3.Dot (vVector2, vVector1);

		if (t <= 0)
			return vA;
		if (t >= d)
			return vB;

		var vVector3 = vVector2 * t;

		var vClosestPoint = vA + vVector3;

		return vClosestPoint;
	}
}