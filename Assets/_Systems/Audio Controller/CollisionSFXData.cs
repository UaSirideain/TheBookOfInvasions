﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Collision SFX Data", menuName = "SFX", order = 1)]
public class CollisionSFXData : ScriptableObject
{
	[SerializeField] List<AudioClip> lightCollisionSFX;
	[SerializeField] List<AudioClip> mediumCollisionSFX;
	[SerializeField] List<AudioClip> heavyCollisionSFX;

	public AudioClip
	GetLightSFX()
	{
		return GetSFXFrom (lightCollisionSFX);
	}

	public AudioClip
	GetMediumSFX()
	{
		return GetSFXFrom (mediumCollisionSFX);
	}

	public AudioClip
	GetHeavySFX()
	{
		return GetSFXFrom (heavyCollisionSFX);
	}

	AudioClip
	GetSFXFrom(List<AudioClip> targetList)
	{
		int i = Random.Range (0, targetList.Count - 1);
		return targetList [i];
	}
}