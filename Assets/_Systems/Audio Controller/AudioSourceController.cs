﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PauseCompanion))]
public class AudioSourceController : MonoBehaviour, IPausable
{
	public void
	Pause()
	{
		GetComponent<AudioSource> ().Pause ();
	}

	public void
	Unpause()
	{
		GetComponent<AudioSource> ().UnPause ();
	}
}