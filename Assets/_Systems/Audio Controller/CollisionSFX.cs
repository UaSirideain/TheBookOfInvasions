﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CollisionSFX : MonoBehaviour
{
	AudioSource audioSource { get { return GetComponent<AudioSource> (); } }
	[SerializeField] CollisionSFXData chainSFX;

	void
	Awake()
	{
		audioSource.clip = chainSFX.GetLightSFX ();
	}

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<AvatarFacade> ())
		{
			audioSource.PlayOneShot (chainSFX.GetLightSFX ());
		}
	}
}