﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public delegate void StringDelegate();
public delegate void NumericDelegate(string numeric);
public delegate void AxesDelegate(float x, float y);

public enum InputState{UI=0, FullscreenUI=1, PausingUI=2, Walking=3, Running=4, WeaponDrawn = 11, Sneaking=10, PlayingMusic=5, ReadyingAbilities=6, BlockingInteractions=7, SettingsUI=8, Battle=9}

namespace PlayerInput{

[System.Serializable]
public class InputRequest
{
	public readonly bool sendAxes;
	public readonly bool sendNumeric;
	public readonly bool sendString;
	public readonly bool sendMouseAxes;

	public readonly List<InputState> conflicts;

	public readonly StringDelegate stringDelegate;
	public readonly AxesDelegate axesDelegate;
	public readonly NumericDelegate numericDelegate;
	public readonly InputButton inputButton;

	public readonly InputMode mode;

	public InputRequest(InputMode newMode, InputButton newButton, List<InputState> newConflicts, StringDelegate newStringDelegate)
	{
		mode = newMode;
		inputButton = newButton;
		conflicts = newConflicts;
		stringDelegate = newStringDelegate;

		sendString = true;
		sendAxes = false;
		sendNumeric = false;
		sendMouseAxes = false;
	}

	public InputRequest(List<InputState> newConflicts, AxesDelegate newAxesDelegate)
	{
		conflicts = newConflicts;
		axesDelegate = newAxesDelegate;

		sendString = false;
		sendAxes = true;
		sendNumeric = false;
		sendMouseAxes = false;
	}

	public InputRequest(List<InputState> newConflicts, AxesDelegate newAxesDelegate, bool mouseAxis)
	{
		conflicts = newConflicts;
		axesDelegate = newAxesDelegate;

		sendString = false;
		sendAxes = true;
		sendNumeric = false;
		sendMouseAxes = true;
	}

	public InputRequest(InputMode newMode, List<InputState> newConflicts, NumericDelegate newNumericDelegate)
	{
		mode = newMode;
		conflicts = newConflicts;
		numericDelegate = newNumericDelegate;

		sendString = false;
		sendAxes = false;
		sendNumeric = true;
		sendMouseAxes = false;
	}

	public bool
	IsValid()
	{
		if (sendAxes)
		{
			return !axesDelegate.Target.Equals (null);
		}
		else if (sendString)
		{
			return !stringDelegate.Target.Equals (null);
		}
		else
		{
			return !numericDelegate.Target.Equals (null);
		}
	}

	public bool
	MatchesInputObject(InputObject input)
	{
		if (input.type == InputType.Axes)
		{
			return sendAxes;
		}
		else if (input.type == InputType.MouseAxes)
		{
			return sendMouseAxes;
		}
		else if (input.type == InputType.String && input.state == mode)
		{
			return inputButton == input.inputButton;
		}
		else if (input.type == InputType.Numeric && input.state == mode)
		{
			return sendNumeric;
		}
		return false;
	}

	public bool
	CanBeFoundIn(List<InputRequest> others)
	{
		foreach (InputRequest other in others)
		{
			
			if (IdenticalTo (other))
			{
				return true;
			}
		}
		return false;
	}
		
	bool
	IdenticalTo(InputRequest other)
	{
		if(other.conflicts.SequenceEqual(conflicts))
		{
			if (sendAxes && other.sendAxes)
			{
				if (axesDelegate.Target.Equals (other.axesDelegate.Target) && mode == other.mode)
				{
					return true;
				}
			}
			else if (sendNumeric && other.sendNumeric)
			{
				if (numericDelegate.Target.Equals (other.numericDelegate.Target) && mode == other.mode)
				{
					return true;
				}
			}
			else if (inputButton == other.inputButton)
			{
				if (stringDelegate.Target.Equals (other.stringDelegate.Target) && mode == other.mode)
				{
					return true;
				}
			}
		}
		return false;
	}
}
}