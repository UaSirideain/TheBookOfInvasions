﻿using UnityEngine;
using PlayerInput;

[RequireComponent(typeof(InputValidator))]
public class InputListener : MonoBehaviour
{
	IInputSubject subject;
	InputValidator validator { get { return GetComponent<InputValidator> (); } }

	public void
	SetListening()
	{
		if (subject == null)
		{
			subject = PlayerInput.FieldInput.manager.GetComponent<IInputSubject> ();
		}
		subject.RegisterListener (this);
	}

	public void
	SetNotListening()
	{
		if (subject == null)
		{
			subject = PlayerInput.FieldInput.manager.GetComponent<IInputSubject> ();
		}
		subject.RemoveListener (this);
	}

	public void
	RegisterInput(InputObject input)
	{
		validator.RegisterInput (input);
	}
}