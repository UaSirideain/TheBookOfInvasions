﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputtable
{
	void
	SubscribeTo(InputValidator newInput);
}