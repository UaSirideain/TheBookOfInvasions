﻿using System.Collections.Generic;
using UnityEngine;

using PlayerInput;

public class InputValidator : MonoBehaviour, IPausable
{
	List<InputRequest> axesRequests = new List<InputRequest>();
	List<InputRequest> mouseAxesRequests = new List<InputRequest>();
	List<InputRequest> stringRequests = new List<InputRequest>();
	List<InputRequest> numericRequests = new List<InputRequest> ();

	StoredInput inputStore = new StoredInput();

	public void
	SubscribeToStringInput(InputMode mode, InputButton inputString, List<InputState> conflicts, StringDelegate action)
	{
		InputRequest newRequest = new InputRequest (mode, inputString, conflicts, action);

		if (newRequest.CanBeFoundIn (stringRequests) == false)
		{
			stringRequests.Add (newRequest);
		}
	}

	public void
	SubscribeToNumericInput(InputMode mode, List<InputState> conflicts, NumericDelegate action)
	{
		InputRequest newRequest = new InputRequest (mode, conflicts, action);

		if (newRequest.CanBeFoundIn (numericRequests) == false)
		{
			numericRequests.Add (newRequest);
		}
	}

	public void
	SubscribeToAxesInput(List<InputState> conflicts, AxesDelegate action)
	{
		InputRequest newRequest = new InputRequest (conflicts, action);

		if (newRequest.CanBeFoundIn (axesRequests) == false)
		{
			axesRequests.Add (newRequest);
		}
	}

	public void
	SubscribeToMouseAxesInput(List<InputState> conflicts, AxesDelegate action)
	{
		InputRequest newRequest = new InputRequest (conflicts, action, true);

		if (newRequest.CanBeFoundIn (mouseAxesRequests) == false)
		{
			mouseAxesRequests.Add (newRequest);
		}
	}

	bool paused = false;

	public void
	Pause()
	{
		paused = true;
	}

	public void
	Unpause()
	{
		paused = false;

		List<InputObject> storedInput = inputStore.GetStoredInput ();

		foreach (InputObject input in storedInput)
		{
			RegisterInput (input);
		}

		inputStore.ClearStoredInput();
	}

	public void
	RegisterInput(InputObject input)
	{
		if (paused == false || input.inputButton == InputButton.ExitUI)
		{
			if (input.type == InputType.Axes)
			{
				DeleteInvalidRequests (axesRequests);

				foreach (InputRequest request in axesRequests)
				{
					if (VerifyNoConflicts (request.conflicts))
					{
						request.axesDelegate (input.MoveAxes.x, input.MoveAxes.y);
					}
				}
			}
			else if (input.type == InputType.String)
			{
				DeleteInvalidRequests (stringRequests);

				foreach (InputRequest request in stringRequests)
				{
					if (request.MatchesInputObject (input))
					{
						if (VerifyNoConflicts (request.conflicts))
						{
							request.stringDelegate ();
						}
					}
				}
			}
			else if (input.type == InputType.Numeric)
			{
				DeleteInvalidRequests (numericRequests);

				foreach (InputRequest request in numericRequests)
				{
					if (request.MatchesInputObject (input))
					{
						if (VerifyNoConflicts (request.conflicts))
						{
							request.numericDelegate (input.myNumeric);
						}
					}
				}
			}
			else if (input.type == InputType.MouseAxes)
			{
				DeleteInvalidRequests (mouseAxesRequests);

				foreach (InputRequest request in mouseAxesRequests)
				{
					if (VerifyNoConflicts (request.conflicts))
					{
						request.axesDelegate (input.MouseAxes.x, input.MouseAxes.y);
					}
				}
			}
		}
		else
		{
			inputStore.ParseInput (input);
		}
	}

	public void
	ValidateSubscriptions()
	{
		GatherSubscriptions ();
		DeleteInvalidRequests (axesRequests);
		DeleteInvalidRequests (stringRequests);
		DeleteInvalidRequests (numericRequests);
	}

	void
	GatherSubscriptions()
	{
		foreach (Transform child in transform.GetComponentsInChildren<Transform>())
		{
			if (child.GetComponent<InputValidator> () && child != this.transform)
			{
				break;
			}
            if (child.GetComponent<IInputtable>() != null)
            {
                foreach (IInputtable inputtable in child.GetComponents<IInputtable>())
                {
                    inputtable.SubscribeTo(this);
                }
            }
		}
	}

	bool
	VerifyNoConflicts(List<InputState> conflicts)
	{
		foreach (InputState state in conflicts)
		{
			if (FindObjectOfType<InputStates> ().GetActiveStates ().Contains (state))
			{
				return false;
			}
		}
		return true;
	}

	void
	DeleteInvalidRequests(List<InputRequest> requests)
	{
		for (int i = 0; i < requests.Count; i++)
		{
			if (requests [i].IsValid() == false)
			{
				requests [i] = null;
			}
		}
		while (requests.Contains (null))
		{
			requests.Remove (null);
		}
	}

	void
	OnEnable()
	{
		ValidateSubscriptions ();
	}
}