﻿using System.Collections.Generic;
using UnityEngine;

public enum InputType { Axes, MouseAxes, String, Numeric }
public enum InputMode { Down, Up }
public enum InputButton
{
	LeftMouse,
	RightMouse,

	DrawWeapon,
	Attack,

	OpenQuickItemsUI,
	ChangeMusicalStrain,
	Run,
	Sneak,
	SwitchCharacter,
	Pause,
	ToggleNumericMap,

	OpenFormationUI,
	OpenSkillSelectionUI,
	OpenInventoryUI,
	OpenAlchemyUI,
	HelpUI,
	ExitUI,

	ToggleFirstPersonCamera,
	Interact,

	RaiseOctave,
	LowerOctave,

	ResetFloraPosition,

	HorizontalMovement,
	VerticalMovement,

	Nothing
}

namespace PlayerInput{

public class InputObject : System.Object
{
    public InputType type;
	public InputMode state;

	public InputButton inputButton;

    private Vector2 mouseAxes;
    public Vector2 MouseAxes
    {
        get { return mouseAxes; }
        set { mouseAxes = value; }
    }

    private Vector2 moveAxes;
    public Vector2 MoveAxes
    {
        get { return moveAxes; }
        set { moveAxes = value; }
    }
		
    public InputObject(string inputString = null, Vector2 newCoordinates = new Vector2(), Vector2 newAxes = new Vector2())
    {
        MouseAxes = newCoordinates;
        MoveAxes = newAxes;
    }

	public void
	RegisterState(InputMode newState)
	{
		state = newState;
	}

	public string myNumeric;

	public void
	RegisterNumeric (string numeric)
	{
		myNumeric = numeric;
		type = InputType.Numeric;
	}

	public void
	RegisterButton (InputButton newButton)
	{
		inputButton = newButton;
		type = InputType.String;
	}

    public void
    RegisterAxes(float x, float y)
    {
        moveAxes.x = x;
        moveAxes.y = y;
		type = InputType.Axes;
    }

    public void
    RegisterMouseAxes(float x, float y)
    {
        mouseAxes.x = x;
        mouseAxes.y = y;
		type = InputType.MouseAxes;
    }

    public void
    ResetInput()
    {
        ResetMouseAxes();
        ResetMoveAxes();
		inputButton = InputButton.Nothing;
    }

    public void
    ResetMoveAxes()
    {
        MoveAxes = Vector2.zero;
    }

    public void
    ResetMouseAxes()
    {
        MouseAxes = Vector2.zero;
    }   
}
}