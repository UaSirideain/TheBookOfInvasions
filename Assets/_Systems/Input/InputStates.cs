﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GUIManagement;
using Battle;

namespace PlayerInput{

public class InputStates : MonoBehaviour
{
	public List<InputState>
	GetActiveStates()
	{
		List<InputState> states = new List<InputState>();

		if (FindObjectOfType<AvatarController> ())
		{
			states.AddRange (FindObjectOfType<AvatarController> ().GetActiveCharacter ().GetComponent<ActiveStates> ().GetActiveStates ());
		}

		states.AddRange(FindObjectOfType<GUIController>().GetActiveStates());
		states.AddRange (FindObjectOfType<InputRouter> ().GetComponent<ActiveStates> ().GetActiveStates ());

		return states;
	}
}
}