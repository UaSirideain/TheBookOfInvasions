﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerInput;

public class ActiveStates : MonoBehaviour
{
	List<InputState> activeStates = new List<InputState>();

	void
	OnEnable()
	{
		foreach (IStateChanger stateChanger in GetComponentsInChildren<IStateChanger>())
		{
			stateChanger.SetStateMachine (this);
		}
	}

	public List<InputState>
	GetActiveStates()
	{
		return activeStates;
	}

	public void
	WriteState(InputState state)
	{
		if (activeStates.Contains (state) == false)
		{
			activeStates.Add (state);
		}
	}

	public void
	RemoveState(InputState state)
	{
		if (activeStates.Contains (state))
		{
			activeStates.Remove (state);
		}
	}

	public void
	Clear()
	{
		activeStates.Clear ();
	}
}