﻿using System.Collections.Generic;
using UnityEngine;

namespace PlayerInput{

public class FieldInput : MonoBehaviour, IInputSubject
{
	Dictionary<string, List<InputButton>> inputMap = new Dictionary<string, List<InputButton>> ()
	{
		{"backspace", new List<InputButton>(){InputButton.OpenQuickItemsUI, InputButton.ChangeMusicalStrain}},
		{"space", new List<InputButton>(){InputButton.SwitchCharacter}},
		{"left shift", new List<InputButton>(){InputButton.Run}},
		{"left ctrl", new List<InputButton>(){InputButton.Sneak}},
		{"h", new List<InputButton>(){InputButton.ToggleNumericMap}},

		{"x", new List<InputButton>(){InputButton.DrawWeapon}},

		{"f", new List<InputButton>(){InputButton.OpenFormationUI}},
		{"p", new List<InputButton>(){InputButton.OpenSkillSelectionUI}},
		{"o", new List<InputButton>(){InputButton.OpenAlchemyUI}},
		{"u", new List<InputButton>(){InputButton.OpenInventoryUI}},
		{"escape", new List<InputButton>(){InputButton.ExitUI, InputButton.Pause}},

		{"l", new List<InputButton>(){InputButton.ToggleFirstPersonCamera}},
		{"e", new List<InputButton>(){InputButton.Interact}},
		{"c", new List<InputButton>(){InputButton.HelpUI}},

		{"controller_circle", new List<InputButton>(){InputButton.Interact}},
		{"controller_x", new List<InputButton>(){InputButton.Run}},
		{"controller_options", new List<InputButton>(){InputButton.ExitUI, InputButton.Pause}},
		{"controller_r3", new List<InputButton>(){InputButton.ToggleFirstPersonCamera}},
		{"controller_touchpad", new List<InputButton>(){InputButton.OpenInventoryUI}},
		{"controller_share", new List<InputButton>(){InputButton.SwitchCharacter}}
	};

	Dictionary<int, InputButton> mouseButtonToInputButton = new Dictionary<int, InputButton>()
	{
		{0,		InputButton.LeftMouse},
		{1, 	InputButton.RightMouse}
	};

//	Dictionary<string, List<InputButton>> axesMaps = new Dictionary<string, List<InputButton>> ()
//	{
//		{"A/D", new List<InputButton>(){InputButton.HorizontalMovement}},
//		{"W/S", new List<InputButton>(){InputButton.VerticalMovement}}
//	};

	public static FieldInput manager;
	InputMessenger messenger = new InputMessenger();
	List<InputListener> inputListeners = new List<InputListener>();

	List<int> mouseButtonInputs = new List<int>(){0, 1};
	List<string> numericInputs = new List<string>(){"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "="};
	List<string> regularInputs = new List<string>();

	InputObject stringInput = new InputObject();
	InputObject axesInput = new InputObject();
	InputObject mouseAxesInput = new InputObject();
	InputObject numericInput = new InputObject();

	List<InputButton> blockedInputs = new List<InputButton>();

	public void
	BlockInput(InputButton input)
	{
		if (blockedInputs.Contains (input) == false)
		{
			blockedInputs.Add (input);
		}
	}

	public void
	UnblockInput(InputButton input)
	{
		if (blockedInputs.Contains (input) == true)
		{
			blockedInputs.Remove (input);
		}
	}

	public List<InputButton>
	GetBlockedInputs()
	{
		return blockedInputs;
	}

	void
	RefreshInputsToCheck()
	{
		foreach (KeyValuePair<string, List<InputButton>> pair in inputMap)
		{
			regularInputs.Add (pair.Key);
		}
	}

	void
	OnEnable()
	{
		RefreshInputsToCheck ();
	}

	void
	CheckKeyMap()
	{
		foreach (string key in regularInputs)
		{
			if (Input.GetButtonDown (key))
			{
				Checker (InputMode.Down, key);
			}

			if (Input.GetButtonUp (key))
			{
				Checker (InputMode.Up, key);
			}
		}
	}

	void
	Checker(InputMode mode, string key)
	{
		List<InputButton> buttons;
		if (inputMap.TryGetValue (key, out buttons))
		{
			foreach (InputButton button in buttons)
			{
				SendString (stringInput, button, mode);
			}
		}
	}

	public void
	RegisterListener(InputListener listener)
	{
		if (!inputListeners.Contains(listener))
		{
			inputListeners.Add(listener);
		}
	}

	public void
	RemoveListener(InputListener listener)
	{
		if (inputListeners.Contains(listener))
		{
			inputListeners.Remove(listener);
		}
	}

    void
    Awake()
    {
		InstanceSelfAsSingleton ();
    }

	void
	InstanceSelfAsSingleton()
	{
		if (manager == null)
		{
			manager = this;
		}
		else if (manager != this)
		{
			Destroy(gameObject);
		}
	}

	void
	Update()
	{
		CalculateAxes ();
		CheckKeyMap ();
		MouseButtonInput ();
		NumericInput ();
		SendAxes ();
		SendMouseAxes ();
	}

	void
	NumericInput()
	{
		foreach (string key in numericInputs)
		{
			if (Input.GetKeyDown (key))
			{
				SendNumeric (numericInput, key, InputMode.Down);
			}
			else if (Input.GetKeyUp(key))
			{
				SendNumeric (numericInput, key, InputMode.Up);
			}
		}
	}

	void
	MouseButtonInput()
	{
		foreach (int mouseButton in mouseButtonInputs)
		{
			if (Input.GetMouseButtonDown (mouseButton))
			{
				SendString (stringInput, mouseButtonToInputButton[mouseButton], InputMode.Down);
			}
			else if (Input.GetMouseButtonUp (mouseButton))
			{
				SendString (stringInput, mouseButtonToInputButton[mouseButton], InputMode.Up);
			}
		}
	}

	void
	SendString(InputObject input, InputButton inputButton, InputMode mode)
	{
		input.ResetInput ();
		input.RegisterState (mode);
		input.RegisterButton (inputButton);
		NotifyListeners (input);
	}

	void
	SendNumeric(InputObject input, string numeric, InputMode mode)
	{
		input.ResetInput ();
		input.RegisterState (mode);
		input.RegisterNumeric (numeric);
		NotifyListeners (input);
	}

	float horizontalAxis = 0f;
	float verticalAxis = 0f;

	void
	CalculateAxes()
	{
		horizontalAxis = Input.GetAxisRaw ("A/D");
		verticalAxis = Input.GetAxisRaw ("W/S");

		if (blockedInputs.Contains (InputButton.HorizontalMovement))
		{
			horizontalAxis = 0f;
		}
		if (blockedInputs.Contains (InputButton.VerticalMovement))
		{
			verticalAxis = 0f;
		}
	}

	void
	SendAxes()
	{
		axesInput.ResetInput ();
		axesInput.RegisterAxes (horizontalAxis, verticalAxis);
		NotifyListeners (axesInput);
	}
    
	void
	SendMouseAxes()
	{
		mouseAxesInput.ResetInput();
		mouseAxesInput.RegisterMouseAxes(Input.GetAxis("mouseX"), Input.GetAxis("mouseY"));
		NotifyListeners(mouseAxesInput);
	}

    void 
    NotifyListeners(InputObject input)
    {
		if (blockedInputs.Contains (input.inputButton) == false)
		{
			messenger.SendMessage (input, inputListeners);
		}
    }
}
}