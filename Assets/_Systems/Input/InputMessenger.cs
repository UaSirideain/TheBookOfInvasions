﻿using System.Collections.Generic;
using UnityEngine;

namespace PlayerInput{

public class InputMessenger
{
	private List<InputListener> messageList = new List<InputListener>();

    public void
	SendMessage(InputObject input, List<InputListener> inputListeners)
    {
        messageList.Clear();
        messageList.AddRange(inputListeners);

		foreach (InputListener listener in messageList)
        {
            listener.RegisterInput(input);
        }
    }
}
}