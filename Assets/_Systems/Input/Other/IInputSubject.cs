﻿namespace PlayerInput{
	
public interface IInputSubject
{
	void RegisterListener(InputListener listener);
	void RemoveListener(InputListener listener);
}
}