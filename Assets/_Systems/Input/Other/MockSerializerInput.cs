﻿using System.Collections.Generic;
using UnityEngine;

public class MockSerializerInput : MonoBehaviour 
{
	void
	OnEnable()
	{
		AcquiredCharacters acquiredCharacters = FindObjectOfType<AcquiredCharacters> ();
		List<AvatarInfo> deserializedCharacterData = acquiredCharacters.GetActiveAvatars ();

		List<AvatarInitialiser> avatars = GetComponent<AvatarInstantiator> ().CreateFieldAvatars (deserializedCharacterData);
		List<AvatarFacade> avatarso = new List<AvatarFacade> ();

		foreach (AvatarInitialiser avatar in avatars)
        {
			avatarso.Add (avatar.GetComponent<AvatarFacade> ());
        }

        SetStartPosition(avatarso);
        SetLightingLayer(avatarso);

        GetComponent<AvatarController> ().SetAsActiveCharacters (avatarso);
	}

    private void
    SetStartPosition(List<AvatarFacade> avatars)
    {
        GameObject spawner = GameObject.Find("Stations");
        for (int index = 0; index < avatars.Count; index++)
        {
            avatars[index].GetTransform().position = spawner.transform.GetChild(index).position;
        }
    }

    private void
    SetLightingLayer(List<AvatarFacade> avatars)
    {
        foreach (AvatarFacade avatar in avatars)
        {
            foreach (SkinnedMeshRenderer mesh in avatar.GetTransform().GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                Transform model = mesh.transform;
                model.gameObject.layer = 10;
            }
            foreach (Animator anim in avatar.GetTransform().GetComponentsInChildren<Animator>())
            {
                Transform model = anim.transform;
                avatar.name = model.name;
            }
        }
    }
}