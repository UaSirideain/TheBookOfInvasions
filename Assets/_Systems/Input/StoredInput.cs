﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerInput{

public class StoredInput
{
	List<InputObject> storedInput = new List<InputObject> ();

	public void
	ParseInput(InputObject input)
	{
		if (input.type == InputType.Numeric || input.type == InputType.String)
		{
			if (input.state == InputMode.Up)
			{
				CheckUpstrokes (input);
			}
			else
			{
				CheckDownstrokes (input);
			}
		}
	}

	public List<InputObject>
	GetStoredInput()
	{
		return storedInput;
	}

	public void
	ClearStoredInput()
	{
		storedInput.Clear ();
	}

	void
	CheckUpstrokes(InputObject input)
	{
		if(InputIsAlreadyStored(input) == false)
		{
			InputObject newInput = new InputObject ();
			newInput.RegisterButton (input.inputButton);
			newInput.state = input.state;

			storedInput.Add (newInput);
		}
	}

	bool
	InputIsAlreadyStored(InputObject input)
	{
		foreach (InputObject storedInput2 in storedInput)
		{
			if (storedInput2.inputButton.Equals (input.inputButton))
			{
				return true;
			}
		}
		return false;
	}

	void
	CheckDownstrokes(InputObject input)
	{
		List<InputObject> storedInputToDestroy = new List<InputObject> ();

		foreach (InputObject storedInput2 in storedInput)
		{
			if (storedInput2.inputButton == input.inputButton)
			{
				storedInputToDestroy.Add (storedInput2);
			}
		}

		foreach (InputObject destroyed in storedInputToDestroy)
		{
			storedInput.Remove (destroyed);
		}

		storedInputToDestroy.Clear ();
	}
}
}