﻿using UnityEngine;
using System.Collections.Generic;

namespace Field.Commands{

public interface ICommandable
{
	List<Command>
	GetCommands();
}
}