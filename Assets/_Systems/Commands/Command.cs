﻿using UnityEngine;

namespace Field.Commands{

[System.Serializable]
public class Command : System.Object
{
	public bool labelOnly = false;

	public delegate void CommandKey();
	public CommandKey myCommand;

	public string label;
	public bool active;
	public bool heading;
}
}
//it would be nice if I could add some value to denote the last command in a set. Then I could categorise things for the player in the context menu
//headings could have null delegates need to disable button, though and maybe change font