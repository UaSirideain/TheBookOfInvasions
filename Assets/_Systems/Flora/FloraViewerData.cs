﻿using System.Collections.Generic;
using UnityEngine;

namespace Field.Flora
{
    public class FloraViewerData : MonoBehaviour
    {
        private int pickableFlora = 0;

        public List<FloraPart>
        GetFloraParts()
        {
            List<FloraPart> floraParts = new List<FloraPart>();
            foreach (Transform child in transform)
            {
                if (child.GetComponent<FloraPart>())
                {
                    floraParts.Add(child.GetComponent<FloraPart>());
                }
            }
            return floraParts;
        }

        public int
        GetPickableFloraCount()
        {
            return pickableFlora;
        }

        public void
        DecrementPickableFlora()
        {
            pickableFlora--;
        }

        public void
        Deactivate()
        {
            Destroy(gameObject);
        }

        private void 
        Awake()
        {
            pickableFlora = transform.childCount;
        }
    }
}