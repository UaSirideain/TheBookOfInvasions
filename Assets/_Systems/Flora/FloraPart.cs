﻿using UnityEngine;

namespace Field.Flora{

public class FloraPart : MonoBehaviour
{
    FloraUI ui;
    FloraViewer viewer;

	[SerializeField] AbilityData item;
    [SerializeField] private string floraPartName;
    [SerializeField] private string description;

    [SerializeField] private bool isLethal;        
    int amount = 1;

    public void
    Initialise(FloraViewer viewer, FloraUI ui)
    {
        this.ui = ui;
        this.viewer = viewer;
    }

    public void
    Deactivate()
    {
        ui.SetText("", "");
        Destroy(gameObject);
    }

    private void 
    OnMouseOver()
    {
        RenderTooltip(true);
    }
    
    private void 
    OnMouseExit()
    {
        RenderTooltip(false);
    }

    private void
    OnMouseDown()
    {
        transform.parent.GetComponent<FloraViewerData>().DecrementPickableFlora();
        viewer.AddToInventory(amount, item);
        Deactivate();
    }

    private void
    RenderTooltip(bool status)
    {
        if (status)
        {
            ui.SetText(floraPartName, description);
        }
        else
        {
            ui.SetText("", "");
        }
    }
}
}