﻿using UnityEngine;

namespace Field.Flora
{
    public class FloraUIData : MonoBehaviour
    {
        [SerializeField] private GameObject floraModel;

        public FloraViewerData
        GetViewerData()
        {
            return floraModel.GetComponent<FloraViewerData>();
        }
    }
}