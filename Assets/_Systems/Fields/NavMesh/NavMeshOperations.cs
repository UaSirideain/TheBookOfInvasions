﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public static class NavMeshOperations
{

	public static float
	CalculatePathDistance(Vector3 pos1, Vector3 pos2)
	{
		NavMeshPath path = new NavMeshPath ();
		NavMesh.CalculatePath (pos1, pos2, NavMesh.AllAreas, path);

		float distance = 0f;
		Vector3 lastPoint = pos1;

		foreach (Vector3 corner in path.corners)
		{
			distance += Vector3.Distance (corner, lastPoint);
			lastPoint = corner;
		}

		return distance;
	}
}