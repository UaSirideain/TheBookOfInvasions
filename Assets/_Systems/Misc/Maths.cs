﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Maths
{
	public static readonly int Infinity = 10000;

	public static int
	ClampPositive(int variable)
	{
		if (variable < 0)
		{
			variable = 0;
		}
		return variable;
	}
}