﻿using UnityEngine;

[RequireComponent(typeof(PauseCompanion))]
public class WaterFlow : MonoBehaviour, IPausable
{
	[SerializeField] Material material;

	[Range(0.01f, 1f)][SerializeField] float flowSpeed;
	[Range(0.04f, 0.1f)][SerializeField] float underFlowSpeed;

	[SerializeField] float flowX;
	[SerializeField] float flowY;

	[SerializeField] Vector2 scale = new Vector2(1, 1);//what does this do exactly?

	[Range(0f, 1f)][SerializeField] float opacity = 0.8f;//what does this do exactly?

	bool paused = false;

    public void
	SetFlowSpeed(float newFlowSpeed)
    {
		flowSpeed = newFlowSpeed;
    }

	public void
	Unpause()
	{
		paused = false;
	}

	public void
	Pause()
	{
		paused = true;
	}

	void
	OnEnable()
	{
		InitialiseMaterial ();
	}

	void
	InitialiseMaterial()
	{
		material = GetComponent<MeshRenderer>().material;
		material.EnableKeyword ("_DETAIL_MULX2");
		material.SetTextureScale ("_MainTex", scale);

		if (opacity == 1f)
		{
			material.SetFloat ("_Mode", 0);
		}
		else
		{
			material.color = new Color (material.color.r, material.color.g, material.color.b, opacity);
		}
	}
		
	void
	Update ()
	{
		if (paused == false)
		{
			material.SetTextureOffset ("_MainTex", CreateNewOffset ("_MainTex", flowSpeed));
			material.SetTextureOffset ("_DetailAlbedoMap", CreateNewOffset ("_DetailAlbedoMap", underFlowSpeed));
		}
	}
		
	Vector2
	CreateNewOffset(string map, float speed)
	{
		Vector2 offset = material.GetTextureOffset(map);
		float newX = offset.x + flowX * Time.deltaTime * speed;
		float newY = offset.y + flowY * Time.deltaTime * speed;
		offset = new Vector2 (newX, newY);

		return offset;
	}
}