﻿using UnityEngine;
using System.Collections;

public class TextureAnimation : MonoBehaviour
{
	[SerializeField] int columns;
	[SerializeField] int rows;
	public float framesPerSecond = 10f;

	private int index = 0;

	void
	Start()
	{
		StartCoroutine (UpdateTiling ());

		Vector2 size = new Vector2 (1f / columns, 1f / rows);
		GetComponent<Renderer>().sharedMaterial.SetTextureScale ("_MainTex", size);
	}

	private IEnumerator
	UpdateTiling()
	{
		while(true)
		{
			index++;
			if (index >= rows * columns)
			{
				index = 0;
			}

			Vector2 offset = new Vector2((float)index / columns - (index / columns), //x
				(index / columns) / (float)rows); //y

			GetComponent<Renderer>().sharedMaterial.SetTextureOffset ("_MainTex", offset);

			yield return new WaitForSeconds (1f / framesPerSecond);
		}
	}
}