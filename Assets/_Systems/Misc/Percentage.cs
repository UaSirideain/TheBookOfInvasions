﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Percentage
{
	public static bool
	Get(int successRate)
	{
		int target = Random.Range (0, 100);

		return target <= successRate;
	}
}