﻿using Battle;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	[SerializeField] NamedCharacter characterName;

	public GameObject battleAvatarPrefab;

	public AIData aIData;

	[SerializeField] List<Skill> skills;
	[SerializeField] List<BattleSkill> equippedBattleSkills;
	[SerializeField] List<BattleSkill> unequippedBattleSkills;
	[SerializeField] List<Stance> equippedStances;

	[SerializeField] Sprite formationPortrait;
	[SerializeField] Sprite turnPortrait;
	[SerializeField] Color thematicColour;

	public NamedCharacter
	GetCharacterName()
	{
		return characterName;
	}

	public List<BattleSkill>
	GetEquippedBattleSkills()
	{
		return equippedBattleSkills;
	}

	public List<BattleSkill>
	GetUnequippedBattleSkills()
	{
		return unequippedBattleSkills;
	}

	public Sprite
	GetTurnPortrait()
	{
		return turnPortrait;
	}

	public void
	ToggleSkillEquipped(BattleSkill skill)
	{
		if (equippedBattleSkills.Contains (skill))
		{
			equippedBattleSkills.Remove(skill);
			unequippedBattleSkills.Add (skill);
		}
		else if (unequippedBattleSkills.Contains(skill))
		{
			unequippedBattleSkills.Remove(skill);
			equippedBattleSkills.Add (skill);
		}
	}

	public void
	AddSkill(List<Skill> newSkills)
	{
		foreach(Skill newSkill in newSkills)
		if (!skills.Contains (newSkill))
		{
			skills.Add (newSkill);
		}
	}

	public List<Stance>
	GetEquippedStances()
	{
		return equippedStances;
	}


	[Header("Battle Stats")]
	public int maxHealth;
	public bool wounded;
	public int woundedThreshold;

	public int attackDamage;
	public int enchantmentDamage;

	public GameObject mesh;
	public Sprite battlePortrait;

	public List<InventoryEntry>
	GetAllFieldAbilities()
	{
		List<InventoryEntry> inventoryEntries = new List<InventoryEntry> ();

		inventoryEntries.AddRange (FindObjectOfType<Inventory> ().GetEntries ());
		inventoryEntries.AddRange (GetFieldSkills ());

		return inventoryEntries;
	}
		
	public Sprite
	GetFormationPortrait()
	{
		return formationPortrait;
	}

	public Color
	GetThematicColour()
	{
		return thematicColour;
	}

	public void
	RemoveInventoryEntry(InventoryEntry entry)
	{
		if (entry.GetAbility ().GetUICategory () != InventoryCategory.Skill)
		{
			FindObjectOfType<Inventory> ().RemoveInventoryEntry (entry);
		}
	}

	List<InventoryEntry>
	GetFieldSkills()
	{
		List<InventoryEntry> fieldSkills = new List<InventoryEntry> ();

		foreach (Skill skill in skills)
		{
			if (skill.GetInventoryEntry () != null)
			{
				fieldSkills.Add (skill.GetInventoryEntry ());
			}
		}
		return fieldSkills;
	}
}