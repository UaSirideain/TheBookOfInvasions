﻿using Battle;
using System.Collections.Generic;
using UnityEngine;
using Field.Abilities;

public class Skill : MonoBehaviour
{
	[SerializeField] FieldAbility ability;
	[SerializeField] BattleSkill battleSkill;
	[SerializeField] AbilityData fieldAbility;

	public FieldAbility
	GetFieldEnchantment()
	{
        return ability;
	}

	public InventoryEntry
	GetInventoryEntry()
	{
		return new InventoryEntry (fieldAbility);
	}
}