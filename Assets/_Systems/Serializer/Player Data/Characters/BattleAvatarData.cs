﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "BattleAvatarData", menuName = "Battle System/BattleAvatarData", order = 1)]
public class BattleAvatarData : ScriptableObject
{
	public string characterName;
	public GameObject mesh;
	public int health;
	public int maxHealth;
	public int woundedThreshold;
	public Sprite portrait;
}