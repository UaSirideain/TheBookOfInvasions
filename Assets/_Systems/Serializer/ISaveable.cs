﻿using System.Collections.Generic;

public interface ISaveable
{
	void
	Save ();

	void
	Load (List<SaveDataBlock> savedData);
}