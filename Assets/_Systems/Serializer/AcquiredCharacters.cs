﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class AcquiredCharacters : MonoBehaviour
{
	[SerializeField] List<AvatarInfo> avatars = new List<AvatarInfo>();

	public List<AvatarInfo>
	GetActiveAvatars()
	{
		return avatars;
	}
}