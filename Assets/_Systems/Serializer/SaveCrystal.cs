﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

public class SaveCrystal : MonoBehaviour
{
	public static SaveCrystal crystal;

	public float health = 24f;
	public float experience = 134f;


	public Vector3 cameraPosition;
	//public Quaternion cameraRotation;

	//public List<GameObject> activeHost;
	//public List<Vector3> hostPositions;

	//wont actually be a list
	//public List<bool> missionsDone;

	//public SkillContainer skillC;




	void
	Awake()
	{
		if (crystal == null)
		{
			DontDestroyOnLoad (gameObject);
			crystal = this;
		}
		else
		if (crystal != this)
		{
			Destroy (gameObject);
		}
	}

	public void
	Save()
	{
		/*
		XmlSerializer serializer = new XmlSerializer(typeof(PlayerData));
		//FileStream stream = new FileStream(Application.persistentDataPath + "/SaveGame.dat", FileMode.Create);
		FileStream stream = File.Create(Application.persistentDataPath + "/SaveGame.dat");
		serializer.Serialize(stream, this);
		stream.Close();


		var serializer = new XmlSerializer(typeof(PlayerData));
		var stream = new FileStream(Application.persistentDataPath + "/SaveGame.dat", FileMode.Create);
		serializer.Serialize(stream, this);
		stream.Close();






		/**/

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/SaveGame.dat");

		PlayerData data = new PlayerData ();
		data.health = health;
		data.experience = experience;

		//data.activeHost = activeHost;
		//data.skillC = skillC;

		bf.Serialize (file, data);
		file.Close ();


	}

	public void
	Load()
	{
		if (File.Exists (Application.persistentDataPath + "/SaveGame.dat"))
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/SaveGame.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize (file);
			file.Close ();

			//skillC = data.skillC;

			health = data.health;
			experience = data.experience;

			//activeHost = data.activeHost;
		}
	}

	[Serializable]
	public class PlayerData
	{
		public float health;
		public float experience;

		public Vector3 cameraPosition;
		//public Quaternion cameraRotation;

		//public List<GameObject> activeHost;
		//public List<Vector3> hostPositions;

		//wont actually be a list
		//public List<bool> missionsDone;

		//public SkillContainer skillC;


	}

	void
	OnGUI()
	{
		if (GUI.Button (new Rect (10, 300, 100, 30), "Load"))
		{
			SaveCrystal.crystal.Load ();
		}

		if (GUI.Button (new Rect (310, 300, 100, 30), "Save"))
		{
			SaveCrystal.crystal.Save ();
		}
	}
}
