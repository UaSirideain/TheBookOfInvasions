﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public class SaveGameController : MonoBehaviour
{
	List<SaveDataBlock> recordedSaveData = new List<SaveDataBlock>();

	public void
	RecordSaveData(SaveDataBlock newSaveData)
	{
		bool found = false;

		foreach (SaveDataBlock block in recordedSaveData)
		{
			if (block.GetHeading () == newSaveData.GetHeading ())
			{
				block.MergeBlock (newSaveData);
				found = true;
			}
		}

		if (found == false)
		{
			recordedSaveData.Add (newSaveData);
		}
	}

	void
	Update()
	{
		if (Input.GetButtonDown ("."))
		{
			Retrieve ();
			WriteRecordedSaveData();
			print ("All Progress has been saved to " + Application.persistentDataPath + "/savefile.txt");
		}
	}
		
	void
	WriteRecordedSaveData()
	{
		StreamWriter outStream = System.IO.File.CreateText (Application.persistentDataPath + "/savefile.txt");

		foreach (SaveDataBlock block in recordedSaveData)
		{
			outStream.WriteLine (block.GetHeading ().ToString());

			foreach (KeyValuePair<string, string> pair in block.GetData())
			{
				string line = "";
				line += pair.Key;
				line += ":";
				line += pair.Value;
				line += ";";
				outStream.WriteLine (line);
			}
			outStream.WriteLine (outStream.NewLine);
		}

		outStream.Close ();
	}

	List<SaveDataBlock>
	ReadSaveFile()
	{
		List<SaveDataBlock> loadedBlocks = new List<SaveDataBlock> ();

		if (File.Exists (Application.persistentDataPath + "/savefile.txt"))
		{
			StreamReader inStream = new StreamReader (Application.persistentDataPath + "/savefile.txt");

			SaveDataHeading currentHeading = SaveDataHeading.Invalid;
			Dictionary<string, string> currentData = new Dictionary<string, string>();

			while (!inStream.EndOfStream)
			{
				string currentLine = inStream.ReadLine ();

				if (currentLine != "")
				{
					if (currentLine.Contains (":") == false)
					{
						SaveDataHeading newHeading = SaveDataHeading.Invalid;
						if(newHeading.TryParse(currentLine, out newHeading))
						{
							if (currentData.Count > 0)
							{
								loadedBlocks.Add (new SaveDataBlock (currentHeading, currentData));
								currentData = new Dictionary<string, string> ();
							}
							currentHeading = newHeading;
						}
					}
					else if (currentLine.Contains (":") == true)
					{
						string[] keyValuePair = currentLine.Split (":" [0]);
						string newKey = keyValuePair [0];
						string newValue = keyValuePair [1].Replace (";", "");

						currentData.Add (newKey, newValue);
					}
				}
			}

			if (currentData.Count > 0)
			{
				loadedBlocks.Add (new SaveDataBlock (currentHeading, currentData));
			}

			inStream.Close ();
		}

		return loadedBlocks;
	}

	void
	Awake()
	{
		SendLoadDataToGameObjects ();
	}

	List<SaveDataBlock>
	FilterBlocks(List<SaveDataBlock> blocks)
	{
		List<SaveDataBlock> newBlocks = new List<SaveDataBlock> ();

		foreach (SaveDataBlock block in blocks)
		{
			if (block.GetHeading () == SaveDataHeading.Settings || block.GetHeading () == SaveDataHeading.LanguageSettings || block.GetHeading () == SaveDataHeading.Inventory)
			{
				newBlocks.Add (block);
			}
		}
		return newBlocks;
	}

	void
	SendLoadDataToGameObjects()
	{
		recordedSaveData = FilterBlocks(ReadSaveFile ());

		foreach (SaveCompanion saveable in FindObjectsOfType<SaveCompanion>())
		{
			saveable.Load (recordedSaveData);
		}
	}

	void
	Retrieve()
	{
		foreach (SaveCompanion saveable in FindObjectsOfType<SaveCompanion>())
		{
			saveable.Save();
		}
	}
}