﻿using UnityEngine;
using System.Collections;

public class SaveMenu : MonoBehaviour
{
	[SerializeField] float savesAcross;
	[SerializeField] float savesDown;
	[SerializeField] GameObject saveSlot;

	void
	OnValidate()
	{
//		for (int i = 0; i < savesAcross * savesDown; i++)
//		{
//			GameObject newSlot = Instantiate (saveSlot);
//			newSlot.transform.SetParent (transform);
//			newSlot.GetComponent<RectTransform>().sizeDelta = new Vector2 (GetComponent<RectTransform>().sizeDelta.x / savesAcross, GetComponent<RectTransform>().sizeDelta.y / savesDown);
//			newSlot.name = "Save Field " + i.ToString ();
//		}
		float x = 0;
		float y = 0;

		for (int i = 0; i < transform.childCount; i++)//RectTransform child in transform)
		{
			RectTransform child = transform.GetChild (i).GetComponent<RectTransform> ();
			child.sizeDelta = new Vector2 (GetComponent<RectTransform>().sizeDelta.x / savesAcross, GetComponent<RectTransform>().sizeDelta.y / savesDown);
			child.anchoredPosition = new Vector2 (x * child.sizeDelta.x, y * child.sizeDelta.y);
			child.name = "save_slot_" + i.ToString ();
			child.GetChild (0).GetComponent<RectTransform> ().sizeDelta = child.sizeDelta;
			child.GetChild (0).GetComponent<UnityEngine.UI.Text> ().text = "Empty Save Slot";

			x++;
			if (x >= savesAcross)
			{
				x -= savesAcross;
				y--;
			}
		}
	}

	public void
	SaveGame(GameObject button)
	{
		print ("Game saved in " + button.name);
	}
}
