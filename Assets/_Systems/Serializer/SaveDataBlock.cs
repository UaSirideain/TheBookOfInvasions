﻿using System.Collections.Generic;

public enum SaveDataHeading{Settings, LanguageSettings, Inventory, Scene, Invalid}

public class SaveDataBlock
{
	SaveDataHeading heading;
	Dictionary<string, string> data;

	public SaveDataBlock(SaveDataHeading newHeading, Dictionary<string, string> newData)
	{
		heading = newHeading;
		data = newData;
	}

	public SaveDataHeading
	GetHeading()
	{
		return heading;
	}

	public Dictionary<string, string>
	GetData()
	{
		return data;
	}

	public void
	MergeBlock(SaveDataBlock newBlock)
	{
		foreach (KeyValuePair<string, string> pair in newBlock.GetData())
		{
			if (data.ContainsKey (pair.Key))
			{
				data [pair.Key] = pair.Value;
			}
			else
			{
				data.Add (pair.Key, pair.Value);
			}
		}
	}
}