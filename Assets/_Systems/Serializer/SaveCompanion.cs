﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveCompanion : MonoBehaviour
{
	public void
	Save()
	{
		GetComponent<ISaveable> ().Save ();
	}

	public void
	Load(List<SaveDataBlock> saveData)
	{
		GetComponent<ISaveable> ().Load (saveData);
	}
}