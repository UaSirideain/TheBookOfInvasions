﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formation{

[System.Serializable]
public class Formation : System.Object
{
	public int tilesAcross = 7;
	public int tilesBelow = 3;
	public List<FormationEntry> entries = new List<FormationEntry>();

	public void
	CloneFrom(Formation master)
	{
		entries.Clear ();

		foreach (FormationEntry entry in master.entries)
		{
			FormationEntry newEntry = new FormationEntry ();
			newEntry.character = entry.character;
			newEntry.position = entry.position;
			entries.Add (newEntry);
                Debug.Log(newEntry.character);
		}
	}
}
}