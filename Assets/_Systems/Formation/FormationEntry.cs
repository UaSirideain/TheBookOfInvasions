﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formation{

[System.Serializable]
public class FormationEntry
{
	public Character character;
	public Vector2 position;
}
}