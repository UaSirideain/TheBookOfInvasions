﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formation{

public class ActiveFormation : MonoBehaviour
{
	[SerializeField] Formation activeFormation;
	[SerializeField] ActiveFormationUI ui {get{return GetComponent<ActiveFormationUI>();}}

	public Formation
	GetFormation()
	{
		WriteDefaultFormation ();
		return activeFormation;
	}

	public void
	Load(Formation newFormation)
	{
		activeFormation.CloneFrom (newFormation);
		ui.RenderOccupiedCells ();
	}

	public void
	RemovePositionOf(Character character)
	{
		FormationEntry targetEntry = null;

		foreach (FormationEntry entry in activeFormation.entries)
		{
			if (entry.character == character)
			{
				targetEntry = entry;
			}
		}

		activeFormation.entries.Remove (targetEntry);
	}

	void
	WriteDefaultFormation()
	{
		if (activeFormation.entries.Count < 1)
		{
			List<Character> characters = ui.GetCharacters ();

			for (int i = 0; i < characters.Count; i++)
			{
				AddPosition (characters [i], new Vector2 (i, 0));
			}
		}
	}

	public void
	AddPosition(Character character, Vector2 position)
	{
		FormationEntry entry = new FormationEntry();
		entry.character = character;
		entry.position = position;
		activeFormation.entries.Add (entry);
	}
}
}