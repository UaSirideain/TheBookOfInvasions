﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EntryPoint : System.Object
{
	[HideInInspector] public string name;
	public Transform entryPoint;

	[HideInInspector] public List<Transform> stations;

	public void
	OnValidate()
	{
		if(entryPoint != null)
		name = entryPoint.name;

		stations.Clear ();
		foreach (Transform child in entryPoint)
		{
			stations.Add (child);
		}
	}
}