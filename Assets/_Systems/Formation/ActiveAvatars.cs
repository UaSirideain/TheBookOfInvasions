﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formation{

public class ActiveAvatars : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }
	[SerializeField] ActiveFormationUI activeFormation;

	[SerializeField] FormationPortrait portraitPrefab;

	const float padding = 20;

	public void
	Initialise(Transform characters)
	{
		BuildPortraits (characters);
	}

	void
	BuildPortraits(Transform characters)
	{
		float panelWidth = 0;

		for(int i = 0; i < characters.GetComponentsInChildren<Character>().Length; i++)
		{
			FormationPortrait portrait = Instantiate (portraitPrefab);
			portrait.transform.SetParent (rt);
			portrait.transform.localScale = new Vector3 (1, 1, 1);

			portrait.Initialise (characters.GetComponentsInChildren<Character> () [i], activeFormation);
			portrait.RenderPosition (i, padding, ref panelWidth);
		}

		rt.sizeDelta = new Vector2 (panelWidth, rt.sizeDelta.y);
	}
}
}