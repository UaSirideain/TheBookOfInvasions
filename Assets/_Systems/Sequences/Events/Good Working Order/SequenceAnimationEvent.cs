﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationType{PickUpItemFloor, PickUpItemSurface, PlaceItemFloor, PlaceItemSurface, FiddleWithItemFloor, LookAround}

namespace Field.Sequences{

public class SequenceAnimationEvent : FieldEvent
{
	[SerializeField] SceneCharacterObject avatar;

	[HideInInspector][SerializeField] AnimationType animationType;
	[HideInInspector][SerializeField] bool waitForEndOfAnimation = true;

	[SerializeField] FieldStage pointOfContactStage;

	bool animationEnded;

	public override IEnumerator
	LocalRun()
	{
		animationEnded = false;

		StartCoroutine (GetAppropriateAvatar().GetFieldAnimations ().PlayEventAnimation (animationType, this));

		while (animationEnded == false && waitForEndOfAnimation == true)
		{
			yield return null;
		}
	}
		
	public void
	RegisterPointOfContact()
	{
		if (pointOfContactStage)
		{
			StartCoroutine (pointOfContactStage.Run (sequenceInfo));
		}
	}

	public void
	RegisterEndOfAnimation()
	{
		animationEnded = true;
	}
		
	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}