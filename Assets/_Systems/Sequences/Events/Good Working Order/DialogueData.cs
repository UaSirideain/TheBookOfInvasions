﻿using UnityEngine;

namespace Field.Dialogue{

[System.Serializable]
public class DialogueData : System.Object
{
	[SerializeField] public string nameText;
	[SerializeField] public string dialogueText;
	[SerializeField] public AudioClip voiceOver;
}
}