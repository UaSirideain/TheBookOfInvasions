﻿using Notifications;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Party Possessions/Add Inventory Item")]
public class AddInventoryItemEvent : FieldEvent
{
    Inventory inventory { get { return FindObjectOfType<Inventory>(); } }
    NotificationUI notificationUI { get { return FindObjectOfType<NotificationUI>(); } }

	[SerializeField] List<InventoryEntry> itemsToGive = new List<InventoryEntry>();
	[SerializeField] string notification;

    public override IEnumerator 
    LocalRun()
    {
        AddItemsToInventory();
        yield return null;
    }

    void
    AddItemsToInventory()
    {
		foreach (InventoryEntry item in itemsToGive)
        {
			inventory.AddInventoryEntry(item.GetAbility(), item.GetAmount());
        }

		notificationUI.DisplayNotification(notification);
    }

	public List<InventoryEntry>
	GetItems()
	{
		return itemsToGive;
	}
}
}