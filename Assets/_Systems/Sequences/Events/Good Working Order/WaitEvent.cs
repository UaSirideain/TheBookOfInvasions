﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class WaitEvent : FieldEvent
{
	[SerializeField] float duration = 0f;
	PauseController pauseController;

	float timer = 0f;
	bool timerRunning = false;

	public override IEnumerator
	LocalRun()
	{
		timer = duration;
		timerRunning = true;
		while (timer > 0f)
		{
			yield return null;
		}
		yield return new WaitForSeconds (duration);
    }

	void
	FixedUpdate()
	{
		if (timerRunning)
		{
			if (!pauseController.IsPaused ())
			{
				timer -= Time.deltaTime;
			}
		}
	}

	void
	OnEnable()
	{
		pauseController = FindObjectOfType<PauseController> ();
	}
}
}