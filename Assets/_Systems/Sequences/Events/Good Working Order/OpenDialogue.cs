﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Dialogue;

public enum ScreenSectionPos{TopRight, Top, TopLeft, MidRight, Mid, MidLeft, BottomRight, Bottom, BottomLeft, Custom}

namespace Field.Sequences{

[AddComponentMenu("Sequences/Dialogue/Open Dialogue Box")]
public class OpenDialogue : FieldEvent
{
	DialogueUI dialogueUI { get { return FindObjectOfType<DialogueUI> (); } }

	[SerializeField] DialogueData dialogueData;

	[SerializeField] ScreenSectionPos boxPosition;
	[SerializeField] Vector2 boxCoordinates;
	[SerializeField] int layer;

	[SerializeField] CloseDialogue closeEvent;

	public override IEnumerator
	LocalRun()
	{
		DialogueBox dialogueBox = dialogueUI.BuildDialogueBox (dialogueData, layer);

		dialogueBox.WriteText (dialogueData.nameText, dialogueData.dialogueText);

		dialogueBox.SetAudioClip (dialogueData.voiceOver);

		yield return new WaitForSeconds (.1f);

		dialogueBox.Build ();

		dialogueBox.SetFollowTarget (boxCoordinates, layer);

		closeEvent.RegisterBox(dialogueBox);
	}

	public DialogueData
	GetDialogueData()
	{
		return dialogueData;
	}

	void
	OnValidate()
	{
		if (boxPosition != ScreenSectionPos.Custom)
		{
			boxCoordinates = ScreenSection.GetScreenCoordinates (boxPosition);
		}
	}
}
}

public static class ScreenSection
{
	const float W = 1920;
	const float H = -1080;

	public static Vector2
	GetScreenCoordinates(ScreenSectionPos pos)
	{
		float x = 0;
		float y = 0;

		if (pos.ToString ().Contains ("Bottom"))
		{
			y = H - (H / 6);
		}
		else if (pos.ToString ().Contains ("Mid"))
		{
			y = H / 2;
		}
		else if (pos.ToString ().Contains ("Top"))
		{
			y = 0 + H / 6;
		}

		if (pos.ToString ().Contains ("Right"))
		{
			x = W - (W / 6);
		}
		else if (pos.ToString ().Contains ("Left"))
		{
			x = 0 + (W / 6);
		}
		else
		{
			x = W / 2;
		}

		return new Vector2(x, y);
	}
}