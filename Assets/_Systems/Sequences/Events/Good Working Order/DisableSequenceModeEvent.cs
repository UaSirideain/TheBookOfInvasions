﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Avatar/End Sequence Mode", 2)]
public class DisableSequenceModeEvent : FieldEvent
{
	[SerializeField] SceneCharacterObject avatar;

	public override IEnumerator
	LocalRun()
	{
		GetAppropriateAvatar().EndSequenceMode();

		yield return null;
	}
		
	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}