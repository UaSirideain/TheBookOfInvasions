﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerInput;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Controls/Disable Input")]
public class DisableInput : FieldEvent
{
	[SerializeField] List<InputButton> targetInputs;

	public override IEnumerator
	LocalRun()
	{
		foreach (InputButton input in targetInputs)
		{
			FindObjectOfType<PlayerInput.FieldInput>().BlockInput (input);
		}

		OtherEffects ();

		yield return null;
	}

	void
	OtherEffects()
	{
		if (targetInputs.Contains(InputButton.Interact))
		{
			sequenceInfo.triggeringAvatar.BlockInteractions ();
		}
	}
}
}