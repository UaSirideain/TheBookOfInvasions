﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Materials/Water Flow")]
public class WaterFlowEvent : FieldEvent
{
    [SerializeField] WaterFlow waterToAffect;
    [SerializeField] float flowSpeed;

    public override IEnumerator LocalRun()
    {
        waterToAffect.SetFlowSpeed(flowSpeed);
        yield return null;
    }
}
}