﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Avatars/Teleport")]
public class Teleport : FieldEvent
{
    [SerializeField] SceneCharacterObject avatar;
	[SerializeField] Transform destination;

	public override IEnumerator
	LocalRun()
	{
		GetAppropriateAvatar().transform.position = destination.position;
		yield return null;
	}

	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}