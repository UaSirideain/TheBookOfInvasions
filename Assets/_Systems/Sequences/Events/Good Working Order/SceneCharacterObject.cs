﻿using UnityEngine;

namespace Field.Sequences{

[System.Serializable]
public class SceneCharacterObject : System.Object
{
    [SerializeField] NamedCharacter namedAvatar;
	[SerializeField] AvatarFacade inspectorAvatar;

	[SerializeField] bool useNamedAvatar = false;

    public AvatarFacade
    GetAvatar()
    {
		if (useNamedAvatar)
		{
			return AvatarOperations.FindCharacter (namedAvatar);
		}
		else
		{
			return inspectorAvatar;
		}
    }
}
}