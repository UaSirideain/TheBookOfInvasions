﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Camera/Fade Out")]
public class FadeOutEvent : FieldEvent
{
	CameraManager cameras { get { return GameObject.FindObjectOfType<CameraManager> (); } }

	const float defaultFadeOutTime = 2.5f;
	const float defaultScreenBlackTime = .06f;

	public override IEnumerator
	LocalRun()
	{
		yield return new WaitForSeconds (defaultScreenBlackTime);
		yield return StartCoroutine (cameras.FadeOut (defaultFadeOutTime));

	}
}

}