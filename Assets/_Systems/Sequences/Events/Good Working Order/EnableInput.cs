﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerInput;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Controls/Enable Input")]
public class EnableInput : FieldEvent
{
	[SerializeField] List<InputButton> targetInputs;

	public override IEnumerator
	LocalRun()
	{
		foreach (InputButton input in targetInputs)
		{
			FindObjectOfType<PlayerInput.FieldInput>().UnblockInput (input);
		}

		OtherEffects ();

		yield return null;
	}

	void
	OtherEffects()
	{
		if (targetInputs.Contains(InputButton.Interact))
		{
			sequenceInfo.triggeringAvatar.UnblockInteractions ();
		}
	}
}
}