﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Dialogue;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Dialogue/Close Dialogue Box")]
public class CloseDialogue : FieldEvent
{
	List<DialogueBox> openBoxes = new List<DialogueBox>();

	[SerializeField] WaitForSeconds timer;
	[SerializeField] bool waitForClick;
	bool clicked = false;

	public override IEnumerator
	LocalRun()
	{
		yield return timer;

		while (clicked == false && waitForClick == true)
		{
			if (Input.GetMouseButtonDown (0))
			{
				clicked = true;
			}
			yield return null;
		}

		foreach (DialogueBox box in openBoxes)
		{
			Destroy (box.gameObject);
		}

		openBoxes.Clear ();
	}

	public void
	RegisterBox(DialogueBox newBox)
	{
		openBoxes.Add (newBox);
	}
}
}