﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/SFX/Play Sound")]
public class SFXEvent : FieldEvent
{
	[SerializeField] bool stop;
	[SerializeField] bool loop;
	[SerializeField] AudioClip audioClip;
    [SerializeField] float volume = 1f;
	[SerializeField] AudioSource audioSource;

	public override IEnumerator
	LocalRun()
	{
		if (stop)
		{
			audioSource.Stop ();
		}
        else
		{
			if (audioClip)
            {
                audioSource.clip = audioClip;
            }
            if (volume != 0f)
            {
                audioSource.volume = volume;
            }
            if (loop)
            {
                audioSource.loop = true;
            }
            else
            {
                audioSource.loop = false;
            }

			audioSource.Play ();
		}
		yield return null;
	}
}
}