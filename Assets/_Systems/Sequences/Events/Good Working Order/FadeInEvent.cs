﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Camera/Fade In")]
public class FadeInEvent : FieldEvent
{
	CameraManager cameras { get { return GameObject.FindObjectOfType<CameraManager> (); } }

	const float defaultFadeInTime = 2.5f;
	const float defaultScreenBlackTime = .06f;

	public override IEnumerator
	LocalRun()
	{
		yield return StartCoroutine (cameras.FadeIn(defaultFadeInTime));
		yield return new WaitForSeconds (defaultScreenBlackTime);
	}
}
}