﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Avatar/Enable Sequence Mode")]
public class EnableSequenceModeEvent : FieldEvent
{
	[SerializeField] SceneCharacterObject avatar;

	public override IEnumerator
	LocalRun()
	{
		GetAppropriateAvatar().EnableSequenceMode ();

		yield return null; 
	}
		
	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}