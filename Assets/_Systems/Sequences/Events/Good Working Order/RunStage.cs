﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class RunStage : FieldEvent
{
	public override IEnumerator
	LocalRun()
	{
		StartCoroutine(GetComponent<FieldStage> ().Run (sequenceInfo));
		yield return null;
	}
}
}