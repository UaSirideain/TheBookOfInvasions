﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/GameObjects/Enable GameObject")]
public class EnableGameObjectEvent : FieldEvent
{
	[SerializeField] List<GameObject> objectsToEnable;

	public override IEnumerator
	LocalRun()
	{
		foreach (GameObject go in objectsToEnable)
		{
			go.SetActive (true);
		}

		yield return null;
	}
}
}