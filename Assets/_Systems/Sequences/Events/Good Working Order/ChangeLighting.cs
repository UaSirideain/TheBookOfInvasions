﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class ChangeLighting : FieldEvent
{
	[SerializeField] Light targetLight;
	[SerializeField] float targetShadowStrength;


	public override IEnumerator
	LocalRun()
	{
		targetLight.shadowStrength = targetShadowStrength;

		yield return null;
	}
}
}