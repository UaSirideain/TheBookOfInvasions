﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/GameObjects/Disable GameObject")]
public class DisableGameObjectEvent : FieldEvent
{
	[SerializeField] List<GameObject> objectsToDisable;

	public override IEnumerator
	LocalRun()
	{
		foreach (GameObject go in objectsToDisable)
		{
			go.SetActive (false);
		}
		yield return null;
	}
}
}