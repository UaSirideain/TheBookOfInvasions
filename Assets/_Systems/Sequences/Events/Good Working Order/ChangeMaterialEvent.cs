﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences
{
	public class ChangeMaterialEvent : FieldEvent
	{
		[SerializeField] MeshRenderer targetMesh;
		[SerializeField] Material newMaterial;

		public override IEnumerator
		LocalRun()
		{
			targetMesh.material = newMaterial;

			yield return null;
		}
	}
}