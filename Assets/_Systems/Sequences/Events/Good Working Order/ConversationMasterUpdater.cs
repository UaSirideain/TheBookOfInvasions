﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class ConversationMasterUpdater : FieldEvent
{
	public override IEnumerator
	LocalRun()
	{
		transform.parent.GetComponent<ConversationMaster> ().WriteDialogue ();

		yield return null;
	}
}
}