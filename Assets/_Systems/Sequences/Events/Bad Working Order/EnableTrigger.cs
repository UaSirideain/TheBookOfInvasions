﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class EnableTrigger : FieldEvent
{
	[SerializeField] List<SequenceTrigger> triggersToEnable;
	[SerializeField] List<SequenceTrigger> triggersToDisable;

	public override IEnumerator
	LocalRun()
	{
		foreach (SequenceTrigger trigger in triggersToEnable)
		{
			trigger.Enable ();
		}

		foreach (SequenceTrigger trigger in triggersToDisable)
		{
			trigger.Disable ();
		}

		yield return null;
	}
}
}