﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences
{
	public class CreateDestroyEvent_Legacy : FieldEvent
	{
		[SerializeField] List<GameObject> instantiate;
		[SerializeField] List<GameObject> destroy;

		public override IEnumerator
		LocalRun()
		{
			foreach (GameObject go in instantiate)
			{
				Instantiate (go);
			}

			foreach (GameObject go in destroy)
			{
				Destroy (go);
			}

			yield return null;
		}
	}
}