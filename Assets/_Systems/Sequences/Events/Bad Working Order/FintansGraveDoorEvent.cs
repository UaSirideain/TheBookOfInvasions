﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Scene Specific/Fintan's Grave Door")]
public class FintansGraveDoorEvent : FieldEvent
{
	[SerializeField] List<Transform> colliderPositions = new List<Transform>();

	[SerializeField] List<Transform> emblemPositions = new List<Transform>();

	[SerializeField] GameObject failureTriggerPrefab;
	[SerializeField] GameObject successTriggerPrefab;

	[SerializeField] List<GameObject> emblems;
	[SerializeField] List<bool> emblemAnswers;

	[SerializeField] List<GameObject> currentEmblems = new List<GameObject>();
	[SerializeField] List<bool> currentEmblemAnswers = new List<bool>();

	[SerializeField] AddEliminatorAnswerEvent answerGiver;

	List<GameObject> thingsToCleanUp = new List<GameObject> ();


	public override IEnumerator
	LocalRun()
	{
		MixUpEmblems ();
		yield return null;
	}


	private void
	MixUpEmblems()
	{
		CleanUpOldEmblems ();

		GenerateNewEmblem (GetRandomEmblemIndex (true));
		GenerateNewEmblem (GetRandomEmblemIndex (false));
		GenerateNewEmblem (GetRandomEmblemIndex (false));

		ShuffleEmblemArray ();

		for (int emblemIndex = 0; emblemIndex < emblemPositions.Count; emblemIndex++)
		{
			InstantiateEmblems(emblemPositions[emblemIndex], emblemIndex);
		}

		for (int colliderIndex = 0; colliderIndex < colliderPositions.Count; colliderIndex++)
		{
			InstantiateColliders(colliderPositions[colliderIndex], colliderIndex);
		}
		//CreateNewEliminatorAnswers ();

		ResetArrays ();
	}

	void
	ResetArrays()
	{
		emblems.AddRange (currentEmblems);
		emblemAnswers.AddRange (currentEmblemAnswers);
		currentEmblems.Clear ();
		currentEmblemAnswers.Clear ();
	}

	/*void
	CreateNewEliminatorAnswers()
	{
		List<EliminatorAnswer> newAnswers = new List<EliminatorAnswer> ();

		foreach (GameObject emblem in currentEmblems)
		{
			EliminatorAnswer newAnswer = new EliminatorAnswer ();
			newAnswer.SetName(emblem.name);
			newAnswer.SetAsDangerous(!currentEmblemAnswers [currentEmblems.IndexOf (emblem)]);
			newAnswers.Add (newAnswer);
		}

		answerGiver.SetAnswers (newAnswers, true);
	}*/

	private void
	InstantiateEmblems(Transform emblemPosition, int index)
	{
		thingsToCleanUp.Add(Instantiate(currentEmblems[index], emblemPosition.position, emblemPosition.rotation, transform));
	}

	private void
	InstantiateColliders(Transform colliderPosition, int index)
	{
		if (currentEmblemAnswers [index] == true)
		{
			thingsToCleanUp.Add(Instantiate(successTriggerPrefab, colliderPosition.position, colliderPosition.rotation, transform)); //Instantiate (successTriggerPrefab, colliderPosition1.position, colliderPosition1.rotation, transform);
		}
		else
		{
			thingsToCleanUp.Add(Instantiate(failureTriggerPrefab, colliderPosition.position, colliderPosition.rotation, transform));
		}
	}

	void
	ShuffleEmblemArray()
	{
		for (int i = currentEmblems.Count - 1; i > 0; i--)
		{
			int r = Random.Range (0, i);

			GameObject tempEmblem = currentEmblems [i];
			bool tempEmblemAnswer = currentEmblemAnswers [i];

			currentEmblems [i] = currentEmblems [r];
			currentEmblemAnswers [i] = currentEmblemAnswers [r];

			currentEmblems [r] = tempEmblem;
			currentEmblemAnswers [r] = tempEmblemAnswer;
		}
	}

	void
	CleanUpOldEmblems()
	{
		foreach (GameObject thing in thingsToCleanUp)
		{
			Destroy (thing);
		}
		thingsToCleanUp.Clear ();
	}

	void
	GenerateNewEmblem(int index)
	{
		currentEmblems.Add (emblems[index]);
		currentEmblemAnswers.Add (emblemAnswers [index]);
		emblems.Remove (emblems [index]);
		emblemAnswers.Remove (emblemAnswers [index]);
	}

	int
	GetRandomEmblemIndex(bool desiredEmblemAnswer)
	{
		List<GameObject> desiredEmblems = new List<GameObject> ();
		foreach (GameObject emblem in emblems)
		{
			if (emblemAnswers [emblems.IndexOf (emblem)] == desiredEmblemAnswer)
			{
				desiredEmblems.Add (emblem);
			}
		}

		return Random.Range (0, desiredEmblems.Count);
	}
}
}