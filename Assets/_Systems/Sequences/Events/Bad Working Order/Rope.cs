﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
	[SerializeField] Transform startOfRope;
	[SerializeField] Transform endOfRope;

	public Vector3
	GetStartPosition()
	{
		return startOfRope.position;
	}

	public Vector3
	GetEndPosition()
	{
		return endOfRope.position;
	}

	public float
	GetLengthOfRope()
	{
		return Mathf.Abs(Vector3.Distance (startOfRope.position, endOfRope.position));
	}
}