﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class TriggerRename : FieldEvent
{
    [SerializeField] private InteractionTrigger trigger;
    [SerializeField] private string newTriggerName = "";

    public override IEnumerator 
    LocalRun()
    {
        trigger.SetTriggerName(newTriggerName);
        yield return null;
    }
}
}