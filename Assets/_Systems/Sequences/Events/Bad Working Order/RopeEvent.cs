﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class RopeEvent : FieldEvent
{
	[SerializeField] bool startOfRope;
	[SerializeField] Rope rope;

	[SerializeField] bool leaveRope;
	[SerializeField] Transform exitPlace;

	public override IEnumerator
	LocalRun()
	{
		if (!leaveRope)
		{
			//action.avatar.GetFieldMovement ().GetComponent<RopeMovement>().SetRope (rope, startOfRope);  //TODO: Assign GetFieldMovement() to a variable
		}
		else
		{
			//action.avatar.GetFieldMovement ().GetComponent<RopeMovement>().LeaveRope (exitPlace);
		}
		yield return null;
	}
}
}