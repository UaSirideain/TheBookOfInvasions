﻿using UnityEngine;

public class Fireflies : MonoBehaviour
{
	[SerializeField] Light firefly;
	string instanceName = "Firefly";

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<AvatarFacade> ())
		{
			print (instanceName);

			if (!other.transform.Find (instanceName))
			{
				Light newFirefly = Instantiate(firefly);
				newFirefly.name = instanceName;
				newFirefly.transform.SetParent (other.transform, true);
				newFirefly.transform.localPosition = new Vector3 (0, 2.5f, 0);
			}
		}
	}
}