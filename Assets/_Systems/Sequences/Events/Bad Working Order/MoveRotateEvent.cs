﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences
{
public class MoveRotateEvent : FieldEvent
{
	[HideInInspector][SerializeField] private Transform objectToMove;
	[HideInInspector][SerializeField] private float durationOfMovement;
	bool instant;

	[HideInInspector][SerializeField] private bool move;
	[HideInInspector][SerializeField] private Vector3 startPosition;
	[HideInInspector][SerializeField] private Vector3 endPosition;

	[HideInInspector][SerializeField] private bool rotate;
	[HideInInspector][SerializeField] private Quaternion startRotation;
	[HideInInspector][SerializeField] private Quaternion endRotation;

	PauseController pauseController;

    public override IEnumerator
	LocalRun()
    {
		if (durationOfMovement <= 0f)
		{
			InstantMovement ();
		}
		else
		{
			yield return StartCoroutine (TimedMovement ());
		}
		yield return null;
    }

	void
	OnEnable()
	{
		pauseController = FindObjectOfType<PauseController> ();
	}

	void
	InstantMovement()
	{
		if (move)
		{
			GoToEndPosition ();
		}
		if (rotate)
		{
			GoToEndRotation ();
		}
	}

	IEnumerator
	TimedMovement()
	{
		float t = 0f;

		while (t < 1.1f)
		{
			if (!pauseController.IsPaused ())
			{
				if (move)
				{
					objectToMove.position = Vector3.Lerp (startPosition, endPosition, t);
				}
				if (rotate)
				{
					objectToMove.rotation = Quaternion.Lerp (startRotation, endRotation, t);
				}

				t += Time.deltaTime / durationOfMovement;
			}
			yield return null;
		}
	}



	[ExecuteInEditMode]
	public void
	SetStartPosition()
	{
		startPosition = objectToMove.position;
	}

	[ExecuteInEditMode]
	public void
	SetEndPosition()
	{
		endPosition = objectToMove.position;
	}

	[ExecuteInEditMode]
	public void
	GoToStartPosition()
	{
		objectToMove.position = startPosition;
	}

	[ExecuteInEditMode]
	public void
	GoToEndPosition()
	{
		objectToMove.position = endPosition;
	}

	[ExecuteInEditMode]
	public void
	SetStartRotation()
	{
		startRotation = objectToMove.rotation;
	}

	[ExecuteInEditMode]
	public void
	SetEndRotation()
	{
		endRotation = objectToMove.rotation;
	}

	[ExecuteInEditMode]
	public void
	GoToStartRotation()
	{
		objectToMove.rotation = startRotation;
	}

	[ExecuteInEditMode]
	public void
	GoToEndRotation()
	{
		objectToMove.rotation = endRotation;
	}
}
}