﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class FadeScreenEvent_Legacy : FieldEvent
{
	[SerializeField] bool fadeFromBlack = false;
	[SerializeField] bool fadeToBlack = false;
	[SerializeField] float fadeTime = 2.5f;
	[SerializeField] float screenBlackTime = 0.1f;

	CameraManager cameras { get { return GameObject.FindObjectOfType<CameraManager> (); } }

	public override IEnumerator
	LocalRun()
	{
		if (fadeFromBlack)
		{
			yield return StartCoroutine (cameras.FadeIn (fadeTime));
		}

		yield return new WaitForSeconds (screenBlackTime);

		if (fadeToBlack)
		{
			yield return StartCoroutine (cameras.FadeOut (fadeTime));
		}
	}
}
}