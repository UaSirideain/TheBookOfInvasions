﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class AddSkillEvent : FieldEvent
{
	[SerializeField] List<Skill> skills;

	public override IEnumerator
	LocalRun()
	{
		sequenceInfo.triggeringAvatar.GetCharacter().AddSkill(skills);

		yield return null;
	}
}
}