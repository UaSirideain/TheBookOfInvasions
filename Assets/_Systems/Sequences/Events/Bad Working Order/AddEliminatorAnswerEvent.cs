﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Party Possessions/Add Eliminator Answer")]
public class AddEliminatorAnswerEvent : FieldEvent
{
	[SerializeField] List<EliminatorAnswer> answers;

	EliminatorAnswerContainer eliminator{ get { return GameObject.FindObjectOfType<EliminatorAnswerContainer> (); } }


	public override IEnumerator
	LocalRun()
	{
		eliminator.AddAnswers(answers);

		yield return null;
	}

	public void
	SetAnswers(List<EliminatorAnswer> newAnswers, bool overwrite)
	{
		if (overwrite)
		{
			answers.Clear ();
		}

		answers.AddRange (newAnswers);
	}

    public void
    ClearOldAnswers(AnswerCategory category)
    {
        eliminator.ClearAnswers(category);
    }
}
}