﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class JumpPoint : FieldEvent
{
    [SerializeField] SceneCharacterObject avatar;   //TODO: Rename
	[SerializeField] Transform jumpTo;  //TODO: Convert to Vector3
	[SerializeField] bool exactPoint = true;
	[SerializeField] Color inColour;
	Color outColour;
		
	public override IEnumerator
	LocalRun()
	{
		if (exactPoint)
		{
			StartCoroutine(MoveAlongArc(GetAppropriateAvatar().transform, jumpTo.position));    //TODO: Assign avatar.GetAvatar(action).transform to a variable
        }
		else
		{
            //TODO: Extract to a Method
			float x = jumpTo.position.x - (transform.position.x - GetAppropriateAvatar().transform.position.x);   //TODO: Assign avatar.GetAvatar(action).transform to a variable
			float y = jumpTo.position.y;
			float z = jumpTo.position.z - (transform.position.z - GetAppropriateAvatar().transform.position.z);   //TODO: Assign avatar.GetAvatar(action).transform to a variable

            Vector3 position = new Vector3 (x, y, z);

			StartCoroutine (MoveAlongArc (GetAppropriateAvatar().transform, position));
		}
		yield return null;
	}

	[SerializeField] AnimationCurve jumpTime = new AnimationCurve (new Keyframe (-10f, 0.85f), new Keyframe (0, 0.55f), new Keyframe (10f, 0.85f)); //TODO: Create Vector2 variable to store each Keyframe in animation curve - Increase readability
	[SerializeField] AnimationCurve jumpHeight = new AnimationCurve (new Keyframe (-4.5f, 2f), new Keyframe (0, 0.25f), new Keyframe (5f, 2f));

	IEnumerator
	MoveAlongArc(Transform avatar, Vector3 position)    //TODO: Rename avatar to avatarTransform
	{
		Vector3 start = avatar.position;
		Vector3 end = position;
		float maxHeight = 1f + Mathf.Abs(start.y - end.y);  //TODO: Extract to a Method
		//float heightDif = Mathf.Abs(start.y - end.y);
		maxHeight = 2f + jumpHeight.Evaluate(Mathf.Abs(start.y - end.y));

		float t = 0f;

		while (t <= 1f)
		{
			float height = Mathf.Sin (Mathf.PI * t) * maxHeight;    //TODO: Extract to a Method
			avatar.position = Vector3.Lerp (start, end, t) + Vector3.up * height;   //TODO: Extract to a Method

			t += Time.deltaTime / jumpTime.Evaluate(Mathf.Abs(start.y - end.y));    //TODO: Extract to a Method

			yield return null;
		}
	}

	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}