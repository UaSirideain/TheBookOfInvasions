﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Avatars/Look At")]
public class LookAtEvent : FieldEvent
{
	[SerializeField] SceneCharacterObject avatar;

	[HideInInspector] [SerializeField] Transform target;


	public override IEnumerator
	LocalRun()
	{
		GetAppropriateAvatar ().LookAt (target);

		yield return null;
	}

	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}