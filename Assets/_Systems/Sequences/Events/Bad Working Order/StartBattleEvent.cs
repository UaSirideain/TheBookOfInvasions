﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Battle;
using Formation;

namespace Field.Sequences
{
	public class StartBattleEvent : FieldEvent
	{
		PauseController pauseController { get { return FindObjectOfType<PauseController>(); } }

		[SerializeField] Formation.Formation enemyFormation;

		CameraManager cameraManager { get { return GameObject.FindObjectOfType<CameraManager>(); } }


		public override IEnumerator
		LocalRun()
		{
			Host enemy = Instantiate(Injections.references.enemyHostPrefab);
			enemy.formation = enemyFormation;
			enemy.advantage = sequenceInfo.enemyAdvantage;

			Host ally = Instantiate(Injections.references.allyHostPrefab);
			ally.formation = FindObjectOfType<ActiveFormation>().GetFormation();
			ally.advantage = sequenceInfo.playerAdvantage;

			StartCoroutine(BattleLogic.manager.InitialiseBattle(ally, enemy, Injections.references.defaultBattleEndPersistencies));

			cameraManager.ActivateBattleCamera();

			pauseController.Pause();

			yield return null;
		}
	}
}