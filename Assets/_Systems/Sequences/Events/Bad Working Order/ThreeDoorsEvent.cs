﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class ThreeDoorsEvent : FieldEvent
{
    const int NUMBER_OF_DOORS = 3;

    [SerializeField] private List<GameObject> allEmblems = new List<GameObject>();
    [SerializeField] private DoorOfThree[] activeDoors = new DoorOfThree[NUMBER_OF_DOORS];
    [SerializeField] private AddEliminatorAnswerEvent eliminator; 

    List<GameObject> activeEmblems = new List<GameObject>();

    public override IEnumerator 
    LocalRun()
    {
        ResetDoors();
        PopulateEmblems();
        AssignEmblemsToDoors();
        AssignSafeDoorAtRandom();
        CreateAndAddAnswers();
        yield return null;
    }

    void 
    ResetDoors()
    {
        foreach (DoorOfThree door in activeDoors)
        {
            door.Clear();
        }
    }

    void
    PopulateEmblems()
    {
        activeEmblems.Clear();
        for (int index = 0; index < NUMBER_OF_DOORS;)
        {
            int emblemIndex = Random.Range(0, allEmblems.Count - 1);
            if (!activeEmblems.Contains(allEmblems[emblemIndex]))
            {
                activeEmblems.Add(allEmblems[emblemIndex]);
                index++;
            }
        }
    }

    void
    AssignEmblemsToDoors()
    {
        for (int index = 0; index < activeDoors.Length; index++)
        {
            activeDoors[index].AssignAsEmblem(activeEmblems[index]);
        }
    }

    void
    AssignSafeDoorAtRandom()
    {
        foreach (DoorOfThree door in activeDoors)
        {
            door.SetAsDangerous();
        }
        int randomInt = Random.Range(0, activeDoors.Length - 1);
        activeDoors[randomInt].SetAsSafe();
    }

    void
    CreateAndAddAnswers()
    {
        List<EliminatorAnswer> newAnswers = new List<EliminatorAnswer>();

        foreach (DoorOfThree door in activeDoors)
        {
            EliminatorAnswer newAnswer = new EliminatorAnswer();
            newAnswer.SetName(door.GetName());
            if (door.IsSafe())
            {
                newAnswer.SetAsSafe();
            }
            else
            {
                newAnswer.SetAsDangerous();
            }
            newAnswer.SetAnswerType(AnswerCategory.TheDoors);
            newAnswers.Add(newAnswer);
        }
        eliminator.ClearOldAnswers(AnswerCategory.TheDoors);
        eliminator.SetAnswers(newAnswers, true);
    }
}
}