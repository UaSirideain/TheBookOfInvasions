﻿using Field.Flora;
using System.Collections;
using UnityEngine;
using Field.Abilities;

namespace Field.Sequences{

public class FloraUIEvent : FieldEvent
{
    FloraViewer viewer { get { return FindObjectOfType<FloraViewer>(); } }
	[SerializeField] FieldAbility itemToGive;

    public override IEnumerator
    LocalRun()
    {
        itemToGive.gameObject.SetActive(true);
        viewer.Initialise(itemToGive.GetComponent<FloraUIData>().GetViewerData());
        yield return null;
    }
}
}