﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

[AddComponentMenu("Sequences/Avatars/End Look At", 2)]
public class EndLookAtEvent : FieldEvent
{
	[SerializeField] SceneCharacterObject avatar;

	[HideInInspector] [SerializeField] Transform target;

	public override IEnumerator
	LocalRun()
	{
		GetAppropriateAvatar ().EndLookAt ();

		yield return null;
	}

	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}