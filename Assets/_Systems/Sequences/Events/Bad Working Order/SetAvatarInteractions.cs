﻿//using UnityEngine;
//using System.Collections;
//
//namespace Field.Sequences{
//
//public enum AvatarToAffect{TriggeringAvatar, AllAvatars, ActiveAvatar, SpecificAvatar}
//
//public class SetAvatarInteractions : FieldEvent
//{
//	[SerializeField] AvatarToAffect avatarToAffect;
//
//	enum BlockOrUnblock{Block, Unblock}
//	[SerializeField] BlockOrUnblock blockOrUnblock;
//
//	public override IEnumerator
//	LocalRun()
//	{
//		if (blockOrUnblock == BlockOrUnblock.Block && avatarToAffect == AvatarToAffect.TriggeringAvatar)
//		{
//			sequenceInfo.triggeringAvatar.BlockInteractions ();
//		}
//		else if (blockOrUnblock == BlockOrUnblock.Unblock && avatarToAffect == AvatarToAffect.TriggeringAvatar)
//		{
//			sequenceInfo.triggeringAvatar.UnblockInteractions ();
//		}
//
//		yield return null;
//	}
//}
//}