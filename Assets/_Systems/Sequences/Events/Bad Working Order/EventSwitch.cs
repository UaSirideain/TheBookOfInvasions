﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class EventSwitch : FieldEvent
{
    [SerializeField] private bool triggerAlternateSequence = false;
    [SerializeField] private FieldEvent normalSequence;
    [SerializeField] private FieldEvent alternateSequence;
	[SerializeField] FieldStage stage1;
	[SerializeField] FieldStage stage2;

    public override IEnumerator 
    LocalRun()
    {
		//alternateSequence.Initialise (sequenceInfo);

        if (triggerAlternateSequence)
        {
            //yield return StartCoroutine(alternateSequence.Run());
			StartCoroutine(stage1.Run(sequenceInfo));
        }
        else
        {
            //yield return StartCoroutine(normalSequence.Run());
			StartCoroutine(stage2.Run(sequenceInfo));
        }

        triggerAlternateSequence = !triggerAlternateSequence;

		yield return null;
    }
}
}