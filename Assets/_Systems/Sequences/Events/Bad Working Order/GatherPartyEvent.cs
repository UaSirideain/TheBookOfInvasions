﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences
{
    public class GatherPartyEvent : FieldEvent
{
	[SerializeField] List<CharacterPosition> positions;
	[SerializeField] bool forceTeleport;

	public override IEnumerator
	LocalRun()
	{
		if (GetTooFar() || forceTeleport)
		{
			yield return StartCoroutine (TeleportCharacters ());
		}
		else
		{
			yield return StartCoroutine(MoveCharacters ());
		}
	}

	bool
	GetTooFar()
	{
		foreach (CharacterPosition position in positions)
		{
			if(position.GetDistance() > 5f)
			{
				return true;
			}
		}
		return false;
	}

	IEnumerator
	TeleportCharacters()
	{
		//yield return StartCoroutine (FindObjectOfType<CameraManager> ().Fade (1f, 2.4f));
		yield return FindObjectOfType<CameraManager> ().FadeOut (2.4f);
		yield return new WaitForSeconds (1f);
		foreach (CharacterPosition position in positions)
		{
			position.TeleportToLocation ();
		}
		//yield return StartCoroutine (FindObjectOfType<CameraManager> ().Fade (0f, 2.4f));
		yield return FindObjectOfType<CameraManager> ().FadeIn (2.4f);

	}

	IEnumerator
	MoveCharacters()
	{
		foreach (CharacterPosition position in positions)
		{
			//position.GetAvatar ().GetNavAgentController().MoveToPoint(position.destination.position);
		}

		while (CharactersAtPoints () == false)
		{
			yield return null;
		}
	}

	bool
	CharactersAtPoints()
	{
		foreach (CharacterPosition position in positions)
		{
			if (position.GetDistance () > .5f)
			{
				return false;
			}
		}
		return true;
	}
}



[System.Serializable]
public class CharacterPosition : System.Object
{
	public CharacterName2 character;
	public Transform destination;


	public AvatarFacade
    GetAvatar()
	{
		return FieldFind.GetPartyAvatar(character);
	}


	public float
	GetDistance()
	{
		return Vector3.Distance(destination.position, GetAvatar().transform.position);
	}

	public void
	TeleportToLocation()
	{
		GetAvatar ().transform.position = destination.position;
		GetAvatar ().transform.rotation = destination.rotation;
	}
}
}