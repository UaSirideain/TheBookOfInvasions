﻿using UnityEngine;

public class DoorOfThree : MonoBehaviour
{
    [SerializeField] private string doorName;
    [SerializeField] private Transform emblemPosition;

	[SerializeField] GameObject dangerStage;
	[SerializeField] GameObject safeStage;

    private bool isSafe = false;
    private GameObject emblem;

    public void
    SetAsDangerous()
    {
		dangerStage.SetActive (true);
		safeStage.SetActive (false);
        isSafe = false;
    }

    public void
    SetAsSafe()
    {
		dangerStage.SetActive (false);
		safeStage.SetActive (true);
        isSafe = true;
    }

    public void
    AssignAsEmblem(GameObject newEmblem)
    {
        emblem = Instantiate(newEmblem, emblemPosition, false);
    }

    public void
    Clear()
    {
        if (emblem != null)
        {
            Destroy(emblem);
        }
        SetAsDangerous();
    }

    public bool
    IsSafe()
    {
        return isSafe;
    }

    public GameObject
    GetEmblem()
    {
        return emblem;
    }

    public string
    GetName()
    {
        return doorName;
    }
}