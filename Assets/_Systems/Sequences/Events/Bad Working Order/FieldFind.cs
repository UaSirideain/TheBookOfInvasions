﻿using System.Collections.Generic;
using UnityEngine;

public enum CharacterName2 { Aon, AngusOg, None }

public static class FieldFind
{
    public static AvatarFacade
    GetPartyAvatar(CharacterName2 namedAvatar)
    {
        List<AvatarFacade> party = GameObject.FindObjectOfType<AvatarController>().GetAvatarsInParty();

        if (namedAvatar == CharacterName2.Aon)
        {
            foreach (AvatarFacade avatar in party)
            {
                if (avatar.name.Contains("aon"))
                {
                    return avatar;
                }
            }
        }
        else if (namedAvatar == CharacterName2.AngusOg)
        {
            foreach (AvatarFacade avatar in party)
            {
                if (avatar.name.Contains("angus"))
                {
                    return avatar;
                }
            }
        }

        Debug.Log("ERROR: No character found. Throwing bum value.");
        return null;
    }
}
