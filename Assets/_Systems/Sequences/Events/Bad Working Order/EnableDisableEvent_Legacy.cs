﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class EnableDisableEvent_Legacy : FieldEvent
{
	[SerializeField] List<GameObject> enable;
	[SerializeField] List<GameObject> disable;

	public override IEnumerator
	LocalRun()
	{
		foreach (GameObject go in enable)
		{
			go.SetActive (true);
		}

		foreach (GameObject go in disable)
		{
			go.SetActive (false);
		}

		yield return null;
	}
}
}