﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class AmbulateEvent : FieldEvent
{
    [SerializeField] SceneCharacterObject avatar;
    
	[HideInInspector][SerializeField] Transform destination;
	[HideInInspector][SerializeField] bool run = false;

	bool targetHasReachedDestination = false;

	public override IEnumerator
	LocalRun()
	{
		AvatarFacade targetAvatar = GetAppropriateAvatar();

		if (run)
		{
			targetAvatar.GetMovementSpeed ().SetRunning ();
		}
		else
		{
			targetAvatar.GetMovementSpeed ().SetWalking ();
		}

		targetAvatar.EnableSequenceMode ();
		targetAvatar.AmbulateToPoint (GetDestination(), RegisterTargetHasReachedDestination);

		while (targetHasReachedDestination == false) {yield return null;}
		targetAvatar.EndSequenceMode ();
	}
		
	void
	RegisterTargetHasReachedDestination()
	{
		targetHasReachedDestination = true;
	}
    
	Vector3
	GetDestination()
	{
		return destination.position;
	}

	AvatarFacade
	GetAppropriateAvatar()
	{
		if (avatar.GetAvatar () != null)
		{
			return avatar.GetAvatar ();
		}
		else
		{
			return sequenceInfo.triggeringAvatar;
		}
	}
}
}