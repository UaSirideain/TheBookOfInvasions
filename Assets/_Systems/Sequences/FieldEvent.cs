﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public delegate void EventDelegate();

public abstract class FieldEvent : MonoBehaviour
{
	protected SequenceInfo sequenceInfo;
	bool eventFinished = false;

	public void
	Initialise(SequenceInfo newInfo)
	{
		sequenceInfo = newInfo;
		eventFinished = false;
	}

	public IEnumerator
	Run()
	{
		yield return LocalRun();

		eventFinished = true;
	}

	public abstract IEnumerator LocalRun();

	public IEnumerator
	SubscribeToEventFinished(EventDelegate subscribedFunction)
	{
		while(eventFinished == false)
		{
			yield return null;
		}
			
		subscribedFunction ();
	}

	////////////////////////////////////////////////////////////////////////////

	//I intend to replace this functionality for the only thing that uses it.

	protected FieldStage stage;

	public void
	InitStage(FieldStage sdf)
	{
		stage = sdf;
	}
}
}