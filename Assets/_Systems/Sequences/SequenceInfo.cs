﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SequenceInfoType{TriggeringAvatar, StartBattle, SongPlayed}

public class SequenceInfo
{
	public readonly SequenceInfoType infoType;
	public readonly AvatarFacade triggeringAvatar;


	public SequenceInfo(AvatarFacade avatar)
	{
		infoType = SequenceInfoType.TriggeringAvatar;
		triggeringAvatar = avatar;
	}


	//battle

	public readonly bool playerAdvantage;
	public readonly bool enemyAdvantage;

	public SequenceInfo(bool newPlayerAdvantage, bool newEnemyAdvantage)
	{
		playerAdvantage = newPlayerAdvantage;
		enemyAdvantage = newEnemyAdvantage;
	}


	// music

	public readonly List<NoteType> notesToPlay;
	public readonly TuneMode modeToPlay;

	public SequenceInfo(List<NoteType> notes, TuneMode mode, AvatarFacade avatar)
	{
		infoType = SequenceInfoType.SongPlayed;
		notesToPlay = notes;
		modeToPlay = mode;
		triggeringAvatar = avatar;
	}
}