﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryInteractionTrigger : MonoBehaviour
{
	[SerializeField] bool depletesItemOnUse = false;

	[SerializeField] AbilityData targetItem;

	[SerializeField] public bool displayInteractionIcon;

	SequenceTrigger sequenceTrigger { get { return GetComponent<SequenceTrigger> (); } }

	List<LineOfInteraction> visitors = new List<LineOfInteraction>(); 

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<LineOfInteraction> () && sequenceTrigger.Enabled ())
		{
			visitors.Add (other.GetComponent<LineOfInteraction> ());
			other.GetComponent<LineOfInteraction> ().RegisterInteraction (this);
		}
	}

	void
	OnTriggerExit(Collider other)
	{
		if (other.GetComponent<LineOfInteraction> () && sequenceTrigger.Enabled ())
		{
			visitors.Remove (other.GetComponent<LineOfInteraction> ());
			other.GetComponent<LineOfInteraction> ().UnregisterInteraction (this);
		}
	}

	void
	OnDisable()
	{
		foreach (LineOfInteraction other in visitors)
		{
			other.UnregisterInteraction (this);
		}

		visitors.Clear ();
	}

	public void
	Trigger(AvatarFacade avatar, InventoryEntry entry)
	{
		if (entry.GetAbility ().Equals (targetItem))
		{
			SequenceInfo sequenceInfo = new SequenceInfo (avatar);
			sequenceTrigger.CheckAndRun (sequenceInfo);
			avatar.UnregisterInventoryInteractionTrigger (this);
			if(depletesItemOnUse)
			{
				avatar.RemoveItem (entry);
			}
		}
		else
		{
			print ("Using this item seemed to have no effect.");
		}
	}

	public bool
	DisplaysInteractionIcon()
	{
		return displayInteractionIcon;
	}
}