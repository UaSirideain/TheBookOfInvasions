﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class FieldStage : MonoBehaviour 
{
	SequenceInfo sequenceInfo;
	List<GameObject> eventObjects = new List<GameObject> ();
	int waitingForEvents = 0;

	void
	Awake()
	{
		PopulateEventObjects ();
	}

	void
	PopulateEventObjects()
	{
		eventObjects.Clear ();
		for (int i = 0; i < transform.childCount; i++)
		{
			eventObjects.Add (transform.GetChild(i).gameObject);
		}
	}

	public IEnumerator
	Run(SequenceInfo newSequenceInfo)
	{
		sequenceInfo = newSequenceInfo;

		yield return StartCoroutine(RunEvents ());
	}

	IEnumerator
	RunEvents()
	{
		foreach (GameObject eventObject in eventObjects)
		{
			if (eventObject.activeSelf)
			{
				RunEventObject (eventObject);

				while (waitingForEvents > 0)
				{
					yield return null;
				}
			}
		}
	}

	void
	RunEventObject(GameObject eventObject)
	{
		foreach (FieldEvent fieldEvent in eventObject.GetComponents<FieldEvent>())
		{
			waitingForEvents++;
			fieldEvent.Initialise (sequenceInfo);
			fieldEvent.InitStage (this);
			StartCoroutine(fieldEvent.SubscribeToEventFinished (DecrementEventWaiting));
			StartCoroutine (fieldEvent.Run ());
		}
	}
		
	void
	DecrementEventWaiting()
	{
		waitingForEvents--;
	}
}
}