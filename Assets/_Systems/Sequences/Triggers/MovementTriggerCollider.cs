﻿using System.Collections.Generic;
using UnityEngine;

public class MovementTriggerCollider : MonoBehaviour
{
	[SerializeField] MovementTrigger movementTrigger;
    List<AvatarFacade> avatarsInCollider = new List<AvatarFacade>();

    void 
    OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AvatarFacade>())
        {
            AvatarFacade avatar = other.GetComponent<AvatarFacade>();
            avatarsInCollider.Add(avatar);

			movementTrigger.RegisterMovement(avatar, FieldActionMovementType.Entered);
        }
    }

    void
    OnTriggerExit(Collider other)
    {
        if (other.GetComponent<AvatarFacade>())
        {
            AvatarFacade avatar = other.GetComponent<AvatarFacade>();
            avatarsInCollider.Remove(avatar);

			movementTrigger.RegisterMovement(avatar, FieldActionMovementType.Exited);
        }
    }
}