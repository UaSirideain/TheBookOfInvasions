﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum FieldActionMovementType { Entered, Exited }

[RequireComponent(typeof(SequenceTrigger))]
public class MovementTrigger : MonoBehaviour
{
    SequenceTrigger trigger { get { return GetComponent<SequenceTrigger>(); } }
    List<IMoveRequirement> moveRequirements = new List<IMoveRequirement>();

    public void
    RegisterMovement(AvatarFacade avatar, FieldActionMovementType moveType)
    {
        if (CheckRequirements(avatar, moveType))
        {
			SequenceInfo sequenceInfo = new SequenceInfo (avatar);
            trigger.CheckAndRun(sequenceInfo);
        }
    }

    bool
	CheckRequirements(AvatarFacade avatar, FieldActionMovementType moveType)
    {
        foreach (IMoveRequirement moveRequirement in moveRequirements)
        {
			if (!moveRequirement.RequirementIsMet(avatar, moveType))
            {
                return false;
            }
        }
        return true;
    }

    void
    Awake()
    {
        moveRequirements = GetComponents<IMoveRequirement>().ToList();
    }
}