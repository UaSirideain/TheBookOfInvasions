﻿using UnityEngine;
using System.Collections.Generic;

public class InteractionTrigger : MonoBehaviour
{
	List<LineOfInteraction> visitors = new List<LineOfInteraction>(); 

	void
	OnTriggerEnter(Collider other)
	{
        if (other.GetComponent<LineOfInteraction> () && sequenceTrigger.Enabled())
		{
			visitors.Add (other.GetComponent<LineOfInteraction> ());
            other.GetComponent<LineOfInteraction>().RegisterInteraction(this);
		}
	}

	void
	OnTriggerExit(Collider other)
	{
        if (other.GetComponent<LineOfInteraction> ())
		{
			visitors.Remove (other.GetComponent<LineOfInteraction> ());
            other.GetComponent<LineOfInteraction>().UnregisterInteraction(this);
		}
	}

	void
	OnDisable()
	{
		foreach (LineOfInteraction other in visitors)
		{
			other.UnregisterInteraction (this);
		}

		visitors.Clear ();
	}

	/// New Class beyond this point

	SequenceTrigger sequenceTrigger { get { return GetComponent<SequenceTrigger> (); } }

	[SerializeField] private string interactionName;

    public void
    SetTriggerName(string newName)
    {
        interactionName = newName;
    }

    public string
    GetTriggerName()
    {
        return interactionName;
    }

	public void
	Trigger(AvatarFacade avatar)
	{
		SequenceInfo sequenceInfo = new SequenceInfo (avatar);
		sequenceTrigger.CheckAndRun (sequenceInfo);
		avatar.UnregisterNewInteractionTrigger (this);
	}
}