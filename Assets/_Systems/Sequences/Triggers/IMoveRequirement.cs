﻿public interface IMoveRequirement
{
    bool RequirementIsMet(AvatarFacade avatar, FieldActionMovementType moveType);
}