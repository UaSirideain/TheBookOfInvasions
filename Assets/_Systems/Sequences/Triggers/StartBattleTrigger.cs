﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBattleTrigger : MonoBehaviour
{
	public void
	EnemyAttacked()
	{
		//print ("Enemy has advantage");
		SequenceInfo newInfo = new SequenceInfo (false, true);
		GetComponent<SequenceTrigger> ().CheckAndRun (newInfo);
	}

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<WeaponCollider> ())
		{
			//print ("Player has advantage");
			SequenceInfo newInfo = new SequenceInfo (true, false);
			GetComponent<SequenceTrigger> ().CheckAndRun (newInfo);
		}
		else if (other.GetComponent<AvatarFacade> () && other.GetComponent<AvatarFacade>().IsActiveCharacter())
		{
			//print ("No one has advantage");
			SequenceInfo newInfo = new SequenceInfo (false, false);
			GetComponent<SequenceTrigger> ().CheckAndRun (newInfo);
		}
	}
}