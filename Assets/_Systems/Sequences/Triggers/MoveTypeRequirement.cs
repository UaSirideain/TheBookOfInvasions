﻿using UnityEngine;

public class MoveTypeRequirement : MonoBehaviour, IMoveRequirement
{
	[SerializeField] bool playerOnly;
    [SerializeField] FieldActionMovementType triggerOn = FieldActionMovementType.Entered;

    public bool 
    RequirementIsMet(AvatarFacade avatar, FieldActionMovementType moveType)
    {
		return ((playerOnly && avatar.IsActiveCharacter ()) || !playerOnly) && moveType.Equals (triggerOn);
//		if (!moveType.Equals(triggerOn) && )
//        {
//            return false;
//        }
//        return true;
    }
}
