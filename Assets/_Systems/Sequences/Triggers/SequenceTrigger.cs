﻿using Field.Sequences;
using UnityEngine;

[RequireComponent(typeof(TriggerCounter))]
public class SequenceTrigger : MonoBehaviour
{
    TriggerCounter triggerCounter { get { return GetComponent<TriggerCounter>(); } }
    [SerializeField] private FieldStage stageToTrigger;

	bool active = true;

    public void
    CheckAndRun(SequenceInfo info)
    {
		if (active)
		{
			StartCoroutine (stageToTrigger.Run (info));
			active = triggerCounter.ValidateTriggerCounter();
		}
    }

	public void
	Enable()
	{
		active = true;
	}

	public void
	Disable()
	{
		active = false;
	}

	public bool
	Enabled()
	{
		return active;
	}
}