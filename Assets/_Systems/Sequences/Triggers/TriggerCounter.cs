﻿using UnityEngine;

public class TriggerCounter : MonoBehaviour
{
    [SerializeField] private bool isInfinite = false;
    [SerializeField] private int remainingTriggers = 1;

    public bool
    ValidateTriggerCounter()
    {
        if (!isInfinite)
        {
            remainingTriggers--;
            if (remainingTriggers <= 0)
            {
				return false;
            }
        }
		return true;
    }
}
