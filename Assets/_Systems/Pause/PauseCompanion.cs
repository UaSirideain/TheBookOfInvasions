﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseCompanion : MonoBehaviour
{
	void
	OnEnable()
	{
		SubscribeToPauseController ();
	}

	void
	SubscribeToPauseController()
	{
		FindObjectOfType<PauseController> ().RegisterPausable (GetComponents<IPausable>());
	}
}