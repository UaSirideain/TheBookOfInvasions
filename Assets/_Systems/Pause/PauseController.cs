﻿using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
	List<IPausable> pausables = new List<IPausable>();

	bool paused = false;

	public bool
	IsPaused()
	{
		return paused;
	}

	public void
	Pause()
	{
		paused = true;
		ClearDestroyedPausables ();

		foreach (IPausable pausable in pausables)
		{
			pausable.Pause ();
		}
	}

	public void
	Unpause()
	{
		paused = false;
		ClearDestroyedPausables ();

		foreach (IPausable pausable in pausables)
		{
			pausable.Unpause ();
		}
	}

	public void
	RegisterPausable(IPausable[] newPausables)
	{
		foreach (IPausable pausable in newPausables)
		{
			if (!pausables.Contains (pausable))
			{
				pausables.Add (pausable);
			}
		}
	}

	void
	ClearDestroyedPausables()
	{
		while (pausables.Contains (null))
		{
			pausables.Remove (null);
		}
	}
}