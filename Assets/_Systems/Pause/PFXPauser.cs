﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PauseCompanion))]
public class PFXPauser : MonoBehaviour, IPausable
{
	public void
	Pause()
	{
		foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
		{
			pfx.Pause ();
		}
	}

	public void
	Unpause()
	{
		foreach (ParticleSystem pfx in GetComponentsInChildren<ParticleSystem>())
		{
			pfx.Play ();
		}
	}
}