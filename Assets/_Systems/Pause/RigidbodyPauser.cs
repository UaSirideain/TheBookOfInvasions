﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PauseCompanion))]
public class RigidbodyPauser : MonoBehaviour, IPausable
{
	Rigidbody target { get { return GetComponent<Rigidbody> (); } }

	Vector3 bufferVelocity;
	bool bufferIsKinematic;
	bool bufferIsSleeping;

	public void
	Pause()
	{
		bufferVelocity = target.velocity;
		bufferIsSleeping = target.IsSleeping ();
		target.Sleep();
	}

	public void
	Unpause()
	{
		if (!bufferIsSleeping)
		{
			target.WakeUp ();
		}
		target.velocity = bufferVelocity;
	}
}