﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BattleInventoryUICategory : MonoBehaviour
{
	[SerializeField] private Transform columnContent;

	[SerializeField] BattleInventoryUIEntryButton entryPrefab;

	List<InventoryEntry> activeEntries = new List<InventoryEntry>();
	Dictionary<InventoryEntry, BattleInventoryUIEntryButton> uiEntries = new Dictionary<InventoryEntry, BattleInventoryUIEntryButton> ();

	public InventoryCategory category;

	public void
	MergeItems(List<InventoryEntry> pendingEntries)
	{
		bool entriesAdded;
		bool entriesDeleted;

		AddItems (pendingEntries, out entriesAdded);
		DeleteItems (pendingEntries, out entriesDeleted);

		if (entriesAdded || entriesDeleted)
		{
			Render (GetUIEntries());
		}

		UpdateItems ();
	}

	List<BattleInventoryUIEntryButton>
	GetUIEntries()
	{
		List<BattleInventoryUIEntryButton> entries = new List<BattleInventoryUIEntryButton> ();

		foreach (InventoryEntry entry in activeEntries)
		{
			entries.Add (uiEntries [entry]);
		}

		return entries;
	}

	void
	AddItems(List<InventoryEntry> pendingEntries, out bool entriesAdded)
	{
		entriesAdded = false;

		foreach (InventoryEntry pendingEntry in pendingEntries)
		{
			if (pendingEntry.IsInList (activeEntries) == false)
			{
				activeEntries.Add (pendingEntry);
				uiEntries.Add (pendingEntry, CreateUIEntry (pendingEntry));
				entriesAdded = true;
			}
		}
	}

	void
	DeleteItems(List<InventoryEntry> pendingEntries, out bool entriesDeleted)
	{
		entriesDeleted = false;

		if (activeEntries.Count > 0)
		{
			for (int i = Mathf.Clamp (activeEntries.Count - 1, 0, 1000); i >= 0; i--)
			{
				if (activeEntries [i].IsInList (pendingEntries) == false)
				{
					uiEntries.Remove (activeEntries [i]);
					activeEntries.RemoveAt (i);
					entriesDeleted = true;
				}
			}
		}
	}

	void
	UpdateItems()
	{
		foreach (KeyValuePair<InventoryEntry, BattleInventoryUIEntryButton> pair in uiEntries)
		{
			pair.Value.UpdateInfo ();
		}
	}

	BattleInventoryUIEntryButton
	CreateUIEntry(InventoryEntry plainEntry)
	{
		BattleInventoryUIEntryButton uiEntry = Instantiate (entryPrefab);
		uiEntry.Initialise (plainEntry);
		return uiEntry;
	}

	void
	Render(List<BattleInventoryUIEntryButton> entries2)
	{
		for (int i = 0; i < entries2.Count; i++)
		{
			RectTransform rt = entries2 [i].GetComponent<RectTransform> ();
			rt.SetAsChildOf (GetComponent<RectTransform> ());
			rt.anchoredPosition = new Vector3 (0, rt.sizeDelta.y * i, 0);
		}
	}
}
}