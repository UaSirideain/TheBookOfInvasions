﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Battle{

public class BattleInventoryUIEntryButton : MonoBehaviour
{
	Button button{get{ return GetComponent<Button>(); }}
	BattleInventoryUITooltip tooltip { get { return FindObjectOfType<BattleInventoryUITooltip> (); } }

	[SerializeField] Text UIName;
	[SerializeField] Image UIIcon;
	[SerializeField] string UIDescription;
	[SerializeField] Text UIAmount;

	InventoryEntry inventoryEntry;

	public void
	Initialise(InventoryEntry newInventoryEntry)
	{
		UIName.text = newInventoryEntry.GetAbility().GetUIName();
		UIIcon.sprite = newInventoryEntry.GetAbility().GetUIIcon();
		UIDescription = newInventoryEntry.GetAbility().GetUIDescription();
		inventoryEntry = newInventoryEntry;
		EnableText();
	}

	public void
	RenderTooltip()
	{
		tooltip.Render(UIDescription);
	}

	public void
	UnrenderTooltip()
	{
		tooltip.Render ("");
	}

	public void
	UpdateInfo()
	{
		EnableText ();
	}

	void
	SelectItem()
	{
		FindObjectOfType<BattleInventoryUI> ().SelectItem (inventoryEntry);
	}

	void
	EnableText()
	{
		if (inventoryEntry.GetAbility ().DepletesOnUse () == false)
		{
			UIAmount.gameObject.SetActive(false);
		}
		else
		{
			int amountHeld = inventoryEntry.GetAmount ();
			if (amountHeld > 1)
			{
				UIAmount.gameObject.SetActive (true);
				UIAmount.text = amountHeld.ToString ();
			}
			else
			{
				UIAmount.gameObject.SetActive (false);
			}
		}
	}

	void
	Awake()
	{
		EventTrigger trigger = gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry pointerEnter = new EventTrigger.Entry ();
		pointerEnter.eventID = EventTriggerType.PointerEnter;
		pointerEnter.callback.AddListener ((data) => {RenderTooltip();});

		EventTrigger.Entry pointerExit = new EventTrigger.Entry ();
		pointerExit.eventID = EventTriggerType.PointerExit;
		pointerExit.callback.AddListener ((data) => {UnrenderTooltip();});

		EventTrigger.Entry pointerDown = new EventTrigger.Entry ();
		pointerDown.eventID = EventTriggerType.PointerDown;
		pointerDown.callback.AddListener ((data) => {SelectItem();});

		trigger.triggers.Add (pointerEnter);
		trigger.triggers.Add (pointerExit);
		trigger.triggers.Add (pointerDown);
	}
}
}