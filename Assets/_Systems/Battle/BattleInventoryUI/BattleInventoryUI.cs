﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public delegate void ItemSkillVoid(ActionComposite itemAction, string itemName);

public class BattleInventoryUI : MonoBehaviour
{
	BattleInventoryUITooltip tooltip { get { return FindObjectOfType<BattleInventoryUITooltip> (); } }
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

	[SerializeField] BattleInventoryUICategory columnPrefab;

	List<BattleInventoryUICategory> columns = new List<BattleInventoryUICategory>();

	InventorySorter sorter = new InventorySorter();

	[SerializeField] RectTransform columnPanel;

	ItemSkillVoid function;

	public void
	SelectItem(InventoryEntry selectedItem)
	{
		if(selectedItem.GetAmount() > 0)
		{
			function(selectedItem.GetAbility().GetBattleEffect(), selectedItem.GetName());
			FindObjectOfType<Inventory> ().RemoveInventoryEntry (selectedItem);
			ResetUI ();
		}
	}

	public void
	ResetUI()
	{
		function = null;
		StartCoroutine(canvas.Fade (1f, 0f, .3f, false));
	}

	public void
	BuildColumns(ItemSkillVoid func)
	{
		BattleLogic.manager.UsingItem ();

		function += func;

		Dictionary<InventoryCategory, List<InventoryEntry>> categorisedEntries = sorter.SortEntriesIntoCategories (FindObjectOfType<Inventory>().GetBattleInventory());

		if (categorisedEntries.Count > 0)
		{
			CreateSpecificCategory (categorisedEntries, InventoryCategory.Tool);
			CreateSpecificCategory (categorisedEntries, InventoryCategory.Relic);
			CreateSpecificCategory (categorisedEntries, InventoryCategory.Salve);
			CreateSpecificCategory (categorisedEntries, InventoryCategory.Misc);
			CreateSpecificCategory (categorisedEntries, InventoryCategory.Flora);

			RenderColumns ();

			tooltip.Render ("");
		}
		else
		{
			tooltip.Render ("There are no items.");
		}

		StartCoroutine(canvas.Fade (0f, 1f, .3f, true));
	}

	void
	CreateSpecificCategory(Dictionary<InventoryCategory, List<InventoryEntry>> categorisedEntries, InventoryCategory targetType)
	{
		if (categorisedEntries.ContainsKey (targetType))
		{
			GetAppropriateCategory (targetType).MergeItems (categorisedEntries [targetType]);
		}
		else
		{
			DestroyAppropriateCategory (targetType);
		}
	}

	BattleInventoryUICategory
	GetAppropriateCategory(InventoryCategory targetType)
	{
		foreach (BattleInventoryUICategory oldCategory in columns)
		{
			if (oldCategory.category == targetType)
			{
				return oldCategory;
			}
		}

		BattleInventoryUICategory newCategory = Instantiate (columnPrefab);
		newCategory.category = targetType;
		columns.Add (newCategory);
		return newCategory;
	}

	void
	RenderColumns()
	{
		float nextXPosition = 0f;

		for (int i = 0; i < columns.Count; i++)
		{
			RectTransform rt = columns [i].GetComponent<RectTransform> ();
			rt.SetAsChildOf (columnPanel);
			rt.anchoredPosition = new Vector2 (nextXPosition, 0);
			nextXPosition += (rt.sizeDelta.x + 20);
		}
	}

	void
	DestroyAppropriateCategory(InventoryCategory targetType)
	{
		int i = 0;
		while (columns.Count > i)
		{
			if (columns [0].category == targetType)
			{
				Destroy (columns [0].gameObject);
				columns.RemoveAt (0);
			}
			else
			{
				i++;
			}
		}
	}
}
}