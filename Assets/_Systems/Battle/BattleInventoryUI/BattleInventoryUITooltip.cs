﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class BattleInventoryUITooltip : MonoBehaviour
{
	[SerializeField] Text tooltipField;

	public void
	Render(string message = null)
	{
		tooltipField.text = message;
	}
}
}