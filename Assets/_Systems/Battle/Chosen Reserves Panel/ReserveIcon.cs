﻿using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class ReserveIcon : MonoBehaviour
{
	Reserve reserve;

	public void
	SetReserve(Reserve reserve)
	{
		this.reserve = reserve;
	}

	public Reserve
	GetReserve()
	{
		return reserve;
	}

	public void
	PointerEnter()
	{
		reserve.GetComponent<PersistencyUI> ().OnMouseOver ();
	}

	public void
	PointerExit()
	{
		reserve.GetComponent<PersistencyUI> ().OnMouseExit ();
	}
}
}