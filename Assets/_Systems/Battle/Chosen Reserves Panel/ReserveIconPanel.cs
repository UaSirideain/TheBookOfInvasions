﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Battle{

public class ReserveIconPanel : MonoBehaviour
{
	[SerializeField] ReserveIcon imprintPrefab;

	List<ReserveIcon> attachedReserves = new List<ReserveIcon>();

	public void
	Attach(Reserve reserve)
	{
		ReserveIcon imprint = Instantiate(imprintPrefab);

		imprint.SetReserve(reserve);
		imprint.transform.SetParent (transform);

		attachedReserves.Add (imprint);

		Render ();
	}

	void
	Start()
	{
		BattleLogic.manager.backendNotifications.EInitialiseNewTurnConditions += OnTurnEnd;
	}

	void
	OnTurnEnd ()
	{
		while (attachedReserves.Count > 0)
		{
			GameObject icon = attachedReserves [0].gameObject;
			icon.SetActive (false);
			Destroy (icon);
			attachedReserves.RemoveAt (0);
		}
	}

	public void
	Detach(Reserve reserve)
	{
		ReserveIcon imprint = FindImprintFor (reserve);

		DestroyImprint (imprint);
	}

	ReserveIcon
	FindImprintFor(Reserve reserve)
	{
		foreach (ReserveIcon imprint in attachedReserves)
		{
			if (imprint.GetReserve() == reserve)
			{
				return imprint;
			}
		}
		return null;
	}

	void
	DestroyImprint(ReserveIcon imprint)
	{
		if (imprint != null)
		{
			attachedReserves.Remove (imprint);
			Destroy (imprint.gameObject);
		}
	}

	void
	Render()
	{
		for (int i = 0; i < attachedReserves.Count; i++)
		{
			RectTransform rect = attachedReserves [i].GetComponent<RectTransform> ();
			rect.anchoredPosition = new Vector2 (i * rect.sizeDelta.x, 0);
		}
	}
}
}