﻿using UnityEngine;
using System.Collections;

public class BattleParticleFX : MonoBehaviour
{
	[SerializeField] float destroyAfterSeconds;

	public void
	Fire(Vector3 startPosition)
	{
		transform.position = startPosition;

		StartCoroutine (DestroySelf ());
	}

	IEnumerator
	DestroySelf()
	{
		yield return new WaitForSeconds (destroyAfterSeconds);

		Destroy (gameObject);
	}
}