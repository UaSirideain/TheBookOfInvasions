﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BE_PreventAction : Effect
{
	protected override void
	Run(Target target)
	{
		target.GetComponent<Action> ().SetPrevented ();
	}
}
}