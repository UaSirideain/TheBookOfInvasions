﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BEAttack : Effect
{
    void
    OnValidate()
    {
	    if (values.Count != 1)
	    {
			values = new List<BattleEffectValue>(){new BattleEffectValue("Damage", ModificationType.LowerHealth)};
	    }
    }

	protected override void
	Run(Target target)
	{
		target.GetComponent<AvatarStats> ().LowerHealth (values [0].GetModifiedValue (target));
	}
}
}