﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BE_Sap : Effect
{
	void
	OnValidate()
	{
		if (values.Count != 1)
		{
			values = new List<BattleEffectValue>(){new BattleEffectValue("Manna Damage", ModificationType.LowerMana)};
		}
	}

	protected override void
	Run(Target target)
	{
		print (2);
		target.GetComponent<AvatarStats> ().LowerMana (values [0].GetModifiedValue (target));
	}
}
}