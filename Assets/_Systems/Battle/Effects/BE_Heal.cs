﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BE_Heal : Effect
{
	void
	OnValidate()
	{
		if (values.Count != 1)
		{
			values = new List<BattleEffectValue>(){new BattleEffectValue("Heal", ModificationType.RaiseHealth)};
		}
	}

	protected override void
	Run(Target target)
	{
		target.GetComponent<AvatarStats> ().RaiseHealth (values [0].GetModifiedValue(target));
	}
}
}