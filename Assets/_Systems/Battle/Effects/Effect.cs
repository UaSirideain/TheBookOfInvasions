﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Battle{

public abstract class Effect : Target
{	
	enum EffectStatus {Healthy, Prevented}
	EffectStatus status;

	List<Change> changesMade = new List<Change>();

	[SerializeField][HideInInspector] protected List<BattleEffectValue> values = new List<BattleEffectValue>();

	public void
	GeneralRun(Target target)
	{
		Run (target);
		changesMade.AddRange(target.GetChanges ());

		if (GetComponent<ChangeList> ().actuality == ChangeActuality.Actual)
		{
			target.CommitChanges ();
		}
		else
		{
			target.ResetChanges ();
		}
	}

	protected virtual void
	Run(Target target){}

	public List<Change>
	RetrieveChanges()
	{
		return changesMade;
	}

	public string
	GetValueString(int i)
	{
		return values [i].GetValueString ();
	}

	public void
	LoadRecordedData(ActionDataPersistency recordedData)
	{
		foreach (BattleEffectValue value in values)
		{
			value.LoadRecordedData (recordedData);
		}
	}

	//

	public List<BattleEffectValue>
	GetValues()
	{
		return values;
	}

	public void
	SetValues(List<BattleEffectValue> newValues)
	{
		values = newValues;
	}
		
	//deprecated
	public virtual void
	DeprecatedRun(ChangeActuality actuality){}
}
}