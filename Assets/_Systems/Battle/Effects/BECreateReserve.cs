﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class BECreateReserve : Effect
{
	[SerializeField] Reserve reserve;

	BattleSkill linkedSkill;

	protected override void
	Run(Target target)
	{
		if (GetComponent<ChangeList> ().actuality == ChangeActuality.Actual)
		{
			Reserve newReserve = Instantiate (reserve);
			Persistency newPersistency = newReserve.GetComponent<Persistency> ();

			ActionDataDefault actionData = GetComponent<Action> ().actionData;
			newPersistency.Initialise (actionData);

			newPersistency.SetEnchanted (target);
			newPersistency.SetOwner (actionData.caster);

			actionData.caster.GetComponent<ConditionsTray> ().LoadReserve (newReserve);
		}
	}
}
}