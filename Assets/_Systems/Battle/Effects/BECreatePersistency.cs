﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public class BECreatePersistency : Effect
{
	[SerializeField] Persistency persistency;

	BattleSkill linkedSkill;

	protected override void
	Run(Target target)
	{
		if (GetComponent<ChangeList> ().actuality == ChangeActuality.Actual)
		{
			Persistency newPersistency = Instantiate (persistency);
			ActionDataDefault actionData = GetComponent<Action> ().actionData;
			newPersistency.Initialise (actionData);
			newPersistency.SetEnchanted (target);
			newPersistency.SetOwner (actionData.caster);
			target.GetComponent<ConditionsTray> ().LoadPersistency (newPersistency);
		}
	}
}
}