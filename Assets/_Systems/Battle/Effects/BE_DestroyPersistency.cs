﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BE_DestroyPersistency : Effect
{
	protected override void
	Run (Target target)
	{
		target.GetComponent<Persistency> ().TargetDestroy ();
	}
}
}