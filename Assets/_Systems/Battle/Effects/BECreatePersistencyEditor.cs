﻿//using UnityEngine;
//using UnityEditor;
//using System.Collections;
//using System.Collections.Generic;
//using Battle;
//
//[CustomEditor(typeof(BECreatePersistency), true)]
//public class BECreatePersistencyEditor : Editor
//{
//	private SerializedObject script;
//
//	private SerializedProperty _uiName;
//	private SerializedProperty _uiDescription;
//
//	public void
//	OnEnable()
//	{
//		script = new SerializedObject (target);
//
//		_uiName = script.FindProperty ("uiName");
//		_uiDescription = script.FindProperty ("uiDescription");
//	
//	}
//
//	public override void
//	OnInspectorGUI()
//	{
//		script.Update ();
//		{
//			RenderInfo ();
//
//			DrawPropertiesExcluding (script, new string[]{ "m_Script" });
//		}
//		script.ApplyModifiedProperties ();
//	}
//
//
//	void
//	RenderInfo()
//	{
//		EditorGUILayout.BeginHorizontal ();
//		{
//			EditorGUILayout.LabelField ("Icon Name", GUILayout.Width (120));
//			_uiName.stringValue = EditorGUILayout.TextField (_uiName.stringValue);
//
//			EditorGUILayout.EndHorizontal ();
//			EditorGUILayout.BeginHorizontal ();
//
//			EditorGUILayout.LabelField ("& Description", GUILayout.Width (120));
//			_uiDescription.stringValue = EditorGUILayout.TextField (_uiDescription.stringValue, GUILayout.Height (34));
//		}
//		EditorGUILayout.EndHorizontal ();
//	}
//}