﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitchcock : MonoBehaviour
{
	[SerializeField] Transform target;

	Collider col { get { return target.GetComponent<Collider> (); } }

	float distanceStart = 2f;
	float distanceEnd = 1.2f;

	float orbitAngleStart = 25f;
	float orbitAngleEnd = -25f;

	float duration = 1.7f;

	public void
	CharacterSelected(Battle.Avatar av)
	{
		if (target == av.transform) return;

		target = av.transform;
		StartCoroutine(PanBackOnCharacterRight ());
	}

	public void
	SelectingTargets()
	{
		Vector3 position = Vector3.zero;

		foreach (Battle.Avatar avatar in Battle.BattleLogic.manager.actors.GetAvatarsBySpeed())
		{
			position += avatar.GetModel().position;
		}

		position /= Battle.BattleLogic.manager.actors.GetAvatarsBySpeed().Count;

		transform.position = position + new Vector3(2f, 3f, 3f);
		LookAt(position);
	}

	public void
	Victory()
	{
		target = Battle.BattleLogic.manager.actors.GetAvatarsBySpeed()[0].GetModel().transform;
		StartCoroutine(RisingOrbit());
	}

	public void
	Defeat()
	{

	}

	float lerpTicket = 0f;

	IEnumerator
	RisingOrbit()
	{
		lerpTicket++;
		float myLerp = lerpTicket;

		float distance = distanceStart;
		float orbitalAngle = orbitAngleStart;
		float height = GetCentrePoint().y;

		Vector3 angle = Quaternion.AngleAxis (orbitAngleStart, target.up) * target.forward;
		Vector3 unadjustedPosition = target.position + (angle * distanceStart);

		Vector3 position = new Vector3 (unadjustedPosition.x, height, unadjustedPosition.z);
		transform.position = unadjustedPosition;

		float t = 0f;

		while (t < 1f && myLerp == lerpTicket)
		{
			distance = Mathfx.Sinerp (distanceStart, distanceEnd, t);
			orbitalAngle = Mathfx.Sinerp(orbitAngleStart, orbitAngleEnd, t);
			height = Mathfx.Sinerp (GetCentrePoint ().y, GetTopPoint ().y, t);

			angle = Quaternion.AngleAxis (orbitalAngle, target.up) * target.forward;
			unadjustedPosition = target.position + angle * distance;

			position = new Vector3 (unadjustedPosition.x, height, unadjustedPosition.z);
			transform.position = position;

			LookAt(Mathfx.Sinerp(GetCentrePoint(), GetTopPoint(), t));

			t += Time.deltaTime / duration;
			yield return null;
		}
	}

	public IEnumerator
	PanBackOnCharacterRight()
	{
		lerpTicket++;
		float myLerp = lerpTicket;

		Vector3 startPosition = GetShoulderPoint() + (target.forward * -.5f) + (target.right * .9f);
		Vector3 endPosition = GetShoulderPoint() + (target.forward * .2f) + (target.right * .9f);
		//need maths to calculate height of target and how far back it should start

		transform.position = startPosition;
		transform.rotation = Quaternion.LookRotation (-target.right);

		float t = 0f;

		while (t < 1f && myLerp == lerpTicket)
		{
			transform.position = Mathfx.Sinerp (startPosition, endPosition, t);

			t += Time.deltaTime / .8f;
			yield return null;
		}
	}

	void
	LookAt(Vector3 point)
	{
		transform.LookAt (point);
	}

	Vector3
	GetCentrePoint()
	{
		SkinnedMeshRenderer rend = target.GetComponentInChildren<SkinnedMeshRenderer>();
		return new Vector3(rend.bounds.center.x, rend.bounds.center.y, rend.bounds.center.z);
	}

	Vector3
	GetTopPoint()
	{
		SkinnedMeshRenderer rend = target.GetComponentInChildren<SkinnedMeshRenderer> ();
		return new Vector3(rend.bounds.center.x, rend.bounds.center.y + (rend.bounds.extents.y), rend.bounds.center.z);
	}

	Vector3
	GetShoulderPoint()
	{
		SkinnedMeshRenderer rend = target.GetComponentInChildren<SkinnedMeshRenderer> ();
		return new Vector3(rend.bounds.center.x, rend.bounds.center.y + (rend.bounds.extents.y *.35f), rend.bounds.center.z);
	}
}