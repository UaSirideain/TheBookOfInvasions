﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cameras;

public class VirtualHitchcock : MonoBehaviour
{
	[SerializeField] ModelExtents model;

	Transform target { get { return model.transform; } }
	Camera cammo { get { return FindObjectOfType<Camera> (); } }

	float distanceStart = 2f;
	float distanceEnd = 1.2f;

	float orbitAngleStart = 25f;
	float orbitAngleEnd = -25f;

	float duration = 1.7f;

	void
	Update()
	{
		if (Input.GetKeyDown ("q"))
		{
			StartCoroutine(RisingOrbit ());
		}
		if (Input.GetKeyDown ("w"))
		{
			StartCoroutine(PanBackOnCharacterRight ());
		}
	}

	float lerpTicket = 0f;

	IEnumerator
	RisingOrbit()
	{
		lerpTicket++;
		float myLerp = lerpTicket;

		float distance = distanceStart;
		float orbitalAngle = orbitAngleStart;
		float height = GetCentrePoint(model).y;

		Vector3 angle = Quaternion.AngleAxis (orbitAngleStart, target.up) * target.forward;
		Vector3 unadjustedPosition = target.position + (angle * distanceStart);

		Vector3 position = new Vector3 (unadjustedPosition.x, height, unadjustedPosition.z);
		transform.position = unadjustedPosition;

		float t = 0f;

		while (t < 1.0f && myLerp == lerpTicket)
		{
			distance = Mathfx.Sinerp (distanceStart, distanceEnd, t);
			orbitalAngle = Mathfx.Sinerp(orbitAngleStart, orbitAngleEnd, t);
			height = Mathfx.Sinerp (GetCentrePoint (model).y, GetTopPoint (model).y, t);

			angle = Quaternion.AngleAxis (orbitalAngle, target.up) * target.forward;
			unadjustedPosition = target.position + angle * distance;

			position = new Vector3 (unadjustedPosition.x, height, unadjustedPosition.z);
			transform.position = position;

			LookAt(Mathfx.Sinerp(GetCentrePoint(model), GetTopPoint(model), t));

			t += Time.deltaTime / duration;
			yield return null;
		}
	}

	public IEnumerator
	PanBackOnCharacterRight()
	{
		lerpTicket++;
		float myLerp = lerpTicket;

		float heightDesired = Vector3.Distance (model.FindUpperExtreme (), model.FindPointAlongHeight (.6f));
		float distance = GetCameraDistance (heightDesired);

		Vector3 startPosition = GetShoulderPoint(model) + (target.forward * .47f) + (target.right * distance);
		Vector3 endPosition = GetShoulderPoint(model) + (target.forward * .27f) + (target.right * distance);

		transform.position = startPosition;
		transform.rotation = Quaternion.LookRotation (-target.right);

		float t = 0f;

		while (t < 1f && myLerp == lerpTicket)
		{
			transform.position = Mathfx.Sinerp (startPosition, endPosition, t);

			t += Time.deltaTime / .8f;
			yield return null;
		}
	}

	void
	LookAt(Vector3 point)
	{
		transform.LookAt (point);
	}

	Vector3
	GetCentrePoint(ModelExtents model)
	{
		return model.FindPointAlongHeight(.5f);
	}

	Vector3
	GetTopPoint(ModelExtents model)
	{
		return model.FindUpperExtreme ();
	}

	float
	GetCameraDistance(float desiredHeight)
	{
//		float verticalFOV = cammo.fieldOfView;
//		print (Mathf.Sin (verticalFOV) * desiredHeight);
//		print (cammo.fieldOfView);
//		return Mathf.Sin (36f) * desiredHeight;

		float baseAngle = (180f - cammo.fieldOfView) * .5f;

		return Mathf.Tan (baseAngle) * desiredHeight;
	}

//	public float shoulderHeight = 0f;
//	public float centrePoint = 0f;
//	public float topPoint = 0f;
//
//	Vector3 centreVector = Vector3.zero;
//	Vector3 shoulderVector = Vector3.zero;
//	Vector3 topVector = Vector3.zero;

	Vector3
	GetShoulderPoint(ModelExtents model)
	{
		return model.FindPointAlongHeight (.7f);
	}

//	void
//	OnDrawGizmos()
//	{
//		Gizmos.DrawSphere (topVector, .1f);
//		Gizmos.DrawSphere (shoulderVector, .1f);
//		Gizmos.DrawSphere (centreVector, .1f);
//	}
}