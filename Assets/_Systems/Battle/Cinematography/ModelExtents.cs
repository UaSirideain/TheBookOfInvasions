﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelExtents : MonoBehaviour
{
	List<SkinnedMeshRenderer> meshes = new List<SkinnedMeshRenderer>();

	Vector3 upperExtreme = Vector3.zero;
	Vector3 lowerExtreme = Vector3.zero;

	public Vector3
	FindLowerExtreme()
	{
		SetExtremes ();
		return lowerExtreme;
	}

	public Vector3
	FindUpperExtreme()
	{
		SetExtremes ();
		return upperExtreme;
	}

	public Vector3
	FindPointAlongHeight(float percentage)
	{
		Vector3 heightVector = upperExtreme - lowerExtreme;
		return lowerExtreme + (heightVector * percentage);
	}

	Bounds bound = new Bounds ();


	void
	Awake()
	{
		meshes.Clear ();

		bool first = true;

		foreach (SkinnedMeshRenderer mesh in GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			if (first)
			{
				first = false;
				bound = mesh.bounds;
			}
			bound.Encapsulate (mesh.bounds);
			//meshes.Add (mesh);
		}
		SetExtremes ();
	}

	void
	SetExtremes()
	{
		//bool upperIsValueless = false;
		//bool lowerIsValueless = false;

		upperExtreme = bound.center + new Vector3(0f, bound.extents.y, 0f);
		lowerExtreme = bound.center - new Vector3(0f, bound.extents.y, 0f);

//		foreach (SkinnedMeshRenderer mesh in meshes)
//		{
//			if (mesh.bounds.center.y + mesh.bounds.extents.y > upperExtreme.y || upperIsValueless == false)
//			{
//				upperIsValueless = true;
//				upperExtreme = mesh.bounds.center + new Vector3(0f, mesh.bounds.extents.y, 0f);
//			}
//			if (mesh.bounds.center.y - mesh.bounds.extents.y < lowerExtreme.y || lowerIsValueless == false)
//			{
//				lowerIsValueless = true;
//				lowerExtreme = mesh.bounds.center - new Vector3(0f, mesh.bounds.extents.y, 0f);
//			}
//		}
	}
		
	void
	OnDrawGizmos()
	{
		Gizmos.DrawSphere (upperExtreme, 0.1f);
		Gizmos.DrawSphere (lowerExtreme, 0.1f);
	}
}