﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace Battle{

public class NewAvatarMovement : MonoBehaviour
{
	NavMeshAgent navAgent { get { return GetComponentInChildren<NavMeshAgent> (); } }
	Animator animator { get { return GetComponentInChildren<Animator> (); } }
	Transform model { get { return GetComponentInChildren<Animator> ().transform; } }

	const float RANGE_DEADZONE = 1.5f;

	public IEnumerator
	MoveTo(Vector3 destination)
	{
		yield return StartCoroutine (RotateTowards (destination));
		navAgent.SetDestination (destination);


		SetMoving (NeedsToWalkToTarget(destination));

		while (NeedsToWalkToTarget(destination))
		{
			yield return null;
		}

		SetMoving (false);
	}

	public IEnumerator
	LookAt(Vector3 target)
	{
		yield return StartCoroutine(RotateTowards(target));
	}

	bool
	NeedsToWalkToTarget(Vector3 destination)
	{
		return Vector3.Distance(model.position, destination) > RANGE_DEADZONE;
	}

	int queueNumber = 0;
	float rotationDurationFor360Degrees = 3f;

	IEnumerator
	RotateTowards(Vector3 destination)
	{
		Quaternion targetRotation = Quaternion.LookRotation(destination - model.position);
		float durationMultiplier = Quaternion.Angle (targetRotation, model.rotation) / 360f;
		float adjustedRotationDuration = rotationDurationFor360Degrees * durationMultiplier;

		queueNumber++;
		int myQueueNumber = queueNumber;
		float lerpTime = 0;
		while (lerpTime < 1 && queueNumber == myQueueNumber)
		{
			lerpTime += Time.deltaTime / adjustedRotationDuration;
			model.rotation = Quaternion.Slerp (model.rotation, targetRotation, lerpTime);
			yield return null;
		}
	}

	void
	SetMoving(bool movingStatus)
	{
		animator.SetInteger ("Speed", movingStatus ? 1 : 0);
		animator.SetBool ("IsMoving", movingStatus);

		if (movingStatus)
		{
			animator.SetTrigger ("Move");
		}

		navAgent.isStopped = !movingStatus;

		SetIdleBools ();
	}
		
	public delegate void AnimationEvent();

	public IEnumerator
	ActionAnimation(AnimationType3 animation, AnimationEvent pointOfContact, AnimationEvent endOfAnimation)
	{
		pointOfContactTriggered = false;

		InitialiseAnimationEventsListener ();//????

		animator.SetTrigger ("Take Action");
		animator.SetTrigger(animationToTrigger [animation]);

		int i = 0;

		while (pointOfContactTriggered == false && i++ < 3000)
		{
			yield return null;
		}

		pointOfContact ();

		int currentAnimation = animator.GetCurrentAnimatorStateInfo (0).fullPathHash;

		while(currentAnimation == animator.GetCurrentAnimatorStateInfo(0).fullPathHash)
		{
			yield return null;
		}

		endOfAnimation ();
	}

	Dictionary<AnimationType3, string> animationToTrigger = new Dictionary<AnimationType3, string>()
	{
		{AnimationType3.MeleeAttack, "Melee Attack"},
		{AnimationType3.RangedAttack, "Ranged Attack"},
		{AnimationType3.CastEnchantment, "Cast Enchantment"},
		{AnimationType3.UseItem, "Use Item"}
	};

	bool pointOfContactTriggered;

	public void
	PointOfContact()
	{
		pointOfContactTriggered = true;
	}

	void
	InitialiseAnimationEventsListener()
	{
		GetComponentInChildren<BattleAvatarAnimationEvents> ().Initialise (this);
	}

	public void
	TakeHit()
	{
		GetComponentInChildren<Animator> ().SetTrigger ("Take Action");
		GetComponentInChildren<Animator> ().SetTrigger ("Take Hit");
	}

	public void
	SetIdleBools()
	{
		if (GetComponent<AvatarStats> ().IsWounded ())
		{
			animator.SetBool ("IsWounded", true);
			animator.SetBool ("IsDead", false);
		}
		else if (GetComponent<AvatarStats> ().IsDead ())
		{
			animator.SetBool ("IsWounded", false);
			animator.SetBool ("IsDead", true);
		}
		else
		{
			animator.SetBool ("IsWounded", false);
			animator.SetBool ("IsDead", false);
		}
	}
}
}