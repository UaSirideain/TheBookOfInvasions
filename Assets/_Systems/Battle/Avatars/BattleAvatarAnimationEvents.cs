﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BattleAvatarAnimationEvents : MonoBehaviour
{
	NewAvatarMovement movementController;

	public void
	Initialise(NewAvatarMovement newMovementController)
	{
		movementController = newMovementController;
	}

	public void
	FootstepSFX ()
	{
	}
	
	public void
	PointOfContact ()
	{
		movementController.PointOfContact ();
	}
}
}