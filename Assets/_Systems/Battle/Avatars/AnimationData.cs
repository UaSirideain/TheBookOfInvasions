﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AnimationData : MonoBehaviour
{
	public AnimationClip clip;
	public GameObject effects;
	public int contactFrameStamp;
	public int lastFrame;
	public float range;

	public float
	ContactTimeStamp()
	{
		float timeStamp = contactFrameStamp / 30.0f;
		return timeStamp;
	}

	public float
	EndTimeStamp()
	{
		float timeStamp = lastFrame / 30.0f;
		return timeStamp;
	}

	void
	OnValidate()
	{
		range = Mathf.Clamp (range, 0.1f, 100f);
	}
}