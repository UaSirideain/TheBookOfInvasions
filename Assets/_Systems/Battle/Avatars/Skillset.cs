﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public class Skillset : MonoBehaviour 
{
	[SerializeField][Range(3, 6)] int maxSkillsPerRow;

	List<RectTransform> skills = new List<RectTransform> ();
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
	bool rendered;

	void
	Start()
	{
		BattleLogic.manager.backendNotifications.EUndoRenderSkillsets += UndoRender;
	}
		
	void
	UndoRender()
	{
		if (rendered)
		{
			rendered = false;
			StartCoroutine (canvas.Fade (1f, 0f, .13f, false));
		}
	}

	public void
	RenderSkillset()
	{
		rendered = true;
		RenderSkillsetInfo ();
		StartCoroutine(canvas.Fade (0f, 1f, .13f, true));
	}
		
	public void
	BuildSkills()
	{
		RenderSkillsetPosition ();
		RenderSkillsetInfo ();
	}

	public void
	AddSkill(RectTransform skill)
	{
		skill.SetAsChildOf (transform);
		skills.Add (skill);
	}

	public List<BattleSkill>
	GetSkills()
	{
		List<BattleSkill> battleSkills = new List<BattleSkill> ();

		foreach (RectTransform t in skills)
		{
			battleSkills.Add (t.GetComponent<BattleSkill> ());
		}

		return battleSkills;
	}
		
	void
	RenderSkillsetPosition()
	{
		int topRowSkills = Mathf.Clamp(((skills.Count) - maxSkillsPerRow), 0, maxSkillsPerRow);

		for(int s = 0; s < skills.Count; s++)
		{
			if(s < topRowSkills)
			{
				skills [s].anchoredPosition = new Vector3 (0 + (skills[s].sizeDelta.x * s), skills[s].sizeDelta.y, 0);
			}
			else
			{
				skills [s].anchoredPosition = new Vector3 (0 + (skills[s].sizeDelta.x * (s - topRowSkills)), 0, 0);
			}
		}
	}

	void
	RenderSkillsetInfo()
	{
		foreach (BattleSkill skill in GetSkills())
		{
			skill.UpdateInfo ();
		}
	}
}
}