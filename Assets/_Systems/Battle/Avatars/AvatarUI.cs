﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class AvatarUI : MonoBehaviour
{
    [SerializeField] RectTransform portrait;
    [SerializeField] GameObject healthPanel;
    [SerializeField] Image woundedUI;
    [SerializeField] Text healthUI;
    [SerializeField] Text manaUI;

	Color deathUI = new Color(.4f, .4f, .4f, .4f);
    Color lifeUI = Color.white;

	[SerializeField] RectTransform targetedMarkerPrefab;
	List<RectTransform> targetedMarkers = new List<RectTransform>();

	public void
	SetTargeted(int displayNumber, int actualProfileIndex)
	{
		RectTransform newMarker = Instantiate (targetedMarkerPrefab);
		newMarker.GetComponent<TargetMarker> ().profile = actualProfileIndex;
		newMarker.GetComponent<TargetMarker> ().target = GetComponent<Target> ();
		newMarker.GetComponentInChildren<Text> ().text = (displayNumber).ToString();
		targetedMarkers.Add (newMarker);

		newMarker.SetAsChildOf(portrait);

		ArrangeTargetMarkers ();
	}

	void
	ArrangeTargetMarkers()
	{
		for(int i = 0; i < targetedMarkers.Count; i++)
		{
			targetedMarkers[i].anchoredPosition = new Vector2 (targetedMarkers[i].sizeDelta.x * -i * targetedMarkers[i].localScale.x, 0);
		}
	}

	public void
	UnsetTargeted(int profileIndex)
	{
		RectTransform toDestroy = null;

		foreach (RectTransform targetMarker in targetedMarkers)
		{
			if (targetMarker.GetComponent<TargetMarker> ().profile == profileIndex)
			{
				toDestroy = targetMarker;
			}
		}

		if (toDestroy != null)
		{
			targetedMarkers.Remove (toDestroy);
			Destroy (toDestroy.gameObject);
		}

		ArrangeTargetMarkers ();
	}

	public void
	ClearTargetMarkers()
	{
		while (targetedMarkers.Count > 0)
		{
			Destroy(targetedMarkers[0].gameObject);
			targetedMarkers.RemoveAt (0);
		}
	}

	public void
	UpdateStats(string health, string mana, bool isWounded, bool isDead)
	{
		healthUI.text = health;
		manaUI.text = mana;
		SetWounded (isWounded);
		SetDead (isDead);
	}

    public void
    Initialise(Character character)
    {
	    SetPortrait(character.battlePortrait);
    }

    public void
    SetWounded(bool wounded)
    {
	    woundedUI.gameObject.SetActive (wounded);
    }

    public void
    SetDead(bool dead)
	{
		healthPanel.SetActive (!dead);
        portrait.GetComponent<Image>().color = dead ? deathUI : lifeUI;
		GetComponent<ConditionsTrayAvatarUI> ().Refresh ();
    }

    void
    SetPortrait(Sprite portraitGraphic)
    {
        portrait.GetComponent<Image>().sprite = portraitGraphic;
    }

    public RectTransform
    GetPortrait()
    {
        return portrait;
    }
}
}