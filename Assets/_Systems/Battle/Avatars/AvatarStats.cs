﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class AvatarStats : MonoBehaviour
{
	AvatarUI ui { get { return GetComponent<AvatarUI>(); } }
	Target target { get { return GetComponent<Target>(); } }
	Avatar avatar { get { return GetComponent<Avatar>(); } }
	NewAvatarMovement avatarMovement {get { return GetComponent<NewAvatarMovement>(); } }

	public bool IsBattleFit => !IsDead();
	public int Mana => mana;
	public int Health => health;
	public int MaxHealth => maxHealth;

	public bool	IsWounded() => health <= woundedThreshold && health > 0;
	public bool	IsDead() => health <= 0;
	public Speed Speed => new Speed(speedLower, speedUpper);

	List<Change> changes = new List<Change>();
	int health, provisionalHealth;
	int maxHealth;

	int mana, provisionalMana;

	int speedLower;
	int speedUpper;

	int woundedThreshold;

	public void
	InitialiseStats(Character character)
	{
		gameObject.name = character.name;
		health = character.maxHealth;
		maxHealth = character.maxHealth;
		woundedThreshold = character.woundedThreshold;
		provisionalHealth = health;
		UpdateUI();
	}

	public void
	RaiseHealth(int healthIncrease)
	{
		provisionalHealth = Mathf.Clamp(health + healthIncrease, 0, maxHealth);
	}

	public void
	LowerHealth(int healthDecrease)
	{
		provisionalHealth = Mathf.Clamp (health - healthDecrease, 0, maxHealth);
	}

	public void
	SetHealth(int newHealthValue)
	{
		provisionalHealth = Mathf.Clamp (newHealthValue, 0, maxHealth);
	}

	public void
	SetWounded()
	{
		provisionalHealth = woundedThreshold;
	}

	public void
	HalfRevive()
	{
		if(IsDead())
		{
			provisionalHealth = woundedThreshold;
		}
	}

	public void
	FullRevive()
	{
		if (IsDead ())
		{
			provisionalHealth = maxHealth;
		}
	}

	public void
	Kill()
	{
		provisionalHealth = 0;
	}

	public void
	RaiseMana(int manaIncrease)
	{
		provisionalMana = Mathf.Clamp(mana + manaIncrease, 0, 99);
	}

	public void
	LowerMana(int manaDecrease)
	{
		print (3);
		provisionalMana = Mathf.Clamp (mana - manaDecrease, 0, 99);
	}

	public void
	SetMana(int newManaValue)
	{
		provisionalMana = Mathf.Clamp (newManaValue, 0, 99);
	}

	//FUNCTIONAL
	public List<Change>
	GetChanges()
	{
		changes.Clear();

		//health
		if (health > provisionalHealth)
		{
			changes.Add(new Change(ChangeType.LoweredHealth, health - provisionalHealth, target));
		}
		else if (health < provisionalHealth)
		{
			changes.Add(new Change(ChangeType.RaisedHealth, provisionalHealth - health, target));
		}

		//wounded
		if (health > woundedThreshold && provisionalHealth <= woundedThreshold)
		{
			changes.Add(new Change(ChangeType.Wounded, provisionalHealth, target));
		}

		//dead
		if (health > 0 && provisionalHealth <= 0)
		{
			changes.Add(new Change(ChangeType.Killed, 0, target));
		}

		//revived
		if (health <= 0 && provisionalHealth > 0)
		{
			changes.Add(new Change(ChangeType.Revived, provisionalHealth, target));
		}

		//mana
		if (mana > provisionalMana)
		{
			changes.Add(new Change(ChangeType.LoweredMana, mana - provisionalMana, target));
		}
		else if (mana < provisionalMana)
		{
			changes.Add(new Change(ChangeType.RaisedMana, provisionalMana - mana, target));
		}

		return changes;
	}
		
	public void
	CommitChanges()
	{
		MakeStatPopup (GetChanges());

		CommitProvisionalChanges();
		UpdateUI();

		avatarMovement.SetIdleBools ();
		avatar.CheckStances (mana);
	}
	
	public void
	ResetProvisionalStats()
	{
		provisionalHealth = health;
		provisionalMana = mana;
	}

	[SerializeField] NewStatPopup popupPrefab;

	const float POPUP_OFFSET = 34f;
	int preExistingPopups = 0;

	void
	MakeStatPopup(List<Change> changes)
	{
		foreach (Change change in changes)
		{
			if (change.type.In(ChangeType.RaisedHealth, ChangeType.LoweredHealth, ChangeType.RaisedMana, ChangeType.LoweredMana))
			{
				NewStatPopup popup = Instantiate (popupPrefab);
				popup.Initialise (change);
				popup.transform.SetAsChildOf (GameObject.Find ("Stat Popup Pooler").transform);
				StartCoroutine(popup.RiseAndFade (GetComponentInChildren<Animator> ().transform.position, Mathf.Clamp(POPUP_OFFSET * preExistingPopups, 0f, 500f), DecreasePopupOffset));
				preExistingPopups += 1;
			}
		}
	}
		
	void
	DecreasePopupOffset()
	{
		preExistingPopups -= 1;
	}

	void
	CommitProvisionalChanges()
	{
		health = provisionalHealth;
		mana = provisionalMana;
	}

	void 
	UpdateUI()
	{
		ui.UpdateStats(health.ToString(), mana.ToString(), IsWounded(), IsDead());
	}
}
}