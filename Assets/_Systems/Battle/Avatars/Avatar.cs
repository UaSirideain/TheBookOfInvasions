﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;
using Battle.Grid;

namespace Battle {

	[RequireComponent(typeof(AvatarMovement))]
	public class Avatar : Target
	{
		AvatarUI ui { get { return GetComponent<AvatarUI>(); } }
		AvatarStats stats { get { return GetComponent<AvatarStats>(); } }
		ConditionsTray conditions { get { return GetComponent<ConditionsTray>(); } }
		AvatarMovement movement { get { return GetComponent<AvatarMovement>(); } }

		public bool mustSelect;
		[SerializeField] public Transform characterModel;
		[SerializeField] Skillset skillset;

		InputRouter input { get { return FindObjectOfType<InputRouter>(); } }

		[SerializeField] GameObject skillCardPrefab;

		bool hidden = false;

		public override void
		MarkAsTargeted(int displayNumber, int profileIndex)
		{
			ui.SetTargeted(displayNumber, profileIndex);
		}

		public override void
		UnmarkAsTargeted(int profileIndex)
		{
			ui.UnsetTargeted(profileIndex);
		}

		void
		OnValidate()
		{
			foreach (Transform child in transform)
			{
				if (child.GetComponent<Animator>() != null)
				{
					characterModel = child;
				}
			}
		}

		public override void
		ClearTargetMarkers()
		{
			ui.ClearTargetMarkers();
		}

		public override void
		ArmAsValidTarget()
		{
			if (hidden == false)
			{
				armedAsValid = true;
			}
		}

		public override List<Change>
		GetChanges()
		{
			return GetComponent<AvatarStats>().GetChanges();
		}

		public override void
		CommitChanges()
		{
			GetComponent<AvatarStats>().CommitChanges();
		}

		public override void
		ResetChanges()
		{
			GetComponent<AvatarStats>().ResetProvisionalStats();
		}

		void
		OnEnable()
		{
			BattleLogic.manager.backendNotifications.EInitialiseNewTurnCharacters += EInitialiseNewTurn;
		}

		void
		OnDestroy()
		{
			BattleLogic.manager.backendNotifications.EInitialiseNewTurnCharacters -= EInitialiseNewTurn;
		}

		void
		EInitialiseNewTurn()
		{
			InitialiseNewTurn();
		}

		public List<BattleSkill>
		GetSkills()
		{
			return skillset.GetSkills();
		}

		public void
		PayForSkills(int cost)
		{
			if (cost > 0)
			{
				stats.LowerMana(cost);
				stats.CommitChanges();
			}
		}

		public int
		SkillsInReserve()
		{
			return conditions.SkillsInReserve();
		}

		public void
		SetMustSelectable(ref bool trigger)
		{
			if (stats.IsBattleFit)
			{
				if (mustSelect)
				{
					canTrigger = trigger = true;
				}
			}
		}

		public void
		SetSelectable()
		{
			if (stats.IsBattleFit)
			{
				canTrigger = true;
			}
		}

		private bool canTrigger;

		public void
		SelectAsCharacter()
		{
			if (canTrigger)
			{
				if (mustSelect)
				{
					mustSelect = false;
				}
				input.CharacterSelected(this);
			}
		}

		public void
		DisplaySkillset()
		{
			skillset.RenderSkillset();
		}

		public void
		Initialise(Character character, GridCell battleStation)
		{
			stats.InitialiseStats(character);
			ui.Initialise(character);

			CreateCharacterModel(battleStation, character);
			InitialiseAI(character);

			BuildSkills(character);
			BuildStances(character);

			turnPortrait = character.GetTurnPortrait();
		}

		Sprite turnPortrait;

		public Sprite
		GetTurnPortrait()
		{
			return turnPortrait;
		}

		public void
		ReturnToHome()
		{
			movement.SnapToHome();
		}

		void
		CreateCharacterModel(GridCell battleStation, Character character)
		{
			GameObject newBattleModel = Instantiate(character.mesh);
			characterModel = newBattleModel.transform;
			characterModel.SetParent(transform);

			movement.RecordHomePosition(battleStation);
			movement.SnapToHome();
		}

		void
		OccupyGridCell(GridCell cell)
		{
			characterModel.position = cell.transform.position;
		}

		void
		InitialiseAI(Character character)
		{
			if (character.aIData != null)
			{
				AIBattleAvatar newAI = gameObject.AddComponent<AIBattleAvatar>();
				AIData newAIData = Object.Instantiate(character.aIData);
				newAI.behaviours = newAIData.behaviours;

				foreach (AIBehaviour behaviour in newAI.behaviours)
				{
					behaviour.battleAvatar = this;
				}
			}
		}

		public AvatarUI
		GetUI()
		{
			return ui;
		}

		public AvatarStats
		GetStats()
		{
			return stats;
		}

		public ConditionsTray
		GetConditions()
		{
			return conditions;
		}

		void
		InitialiseNewTurn()
		{
			if (BattleLogic.manager.turnOrderHistory.GetActiveHost() == transform.parent.GetComponent<Host>() && this.IsAlive())
			{
				stats.RaiseMana(1);
				stats.CommitChanges();
			}
			BattleLogic.manager.backendNotifications.processingInitialiseNewTurnCharacters--;
		}

		void
		BuildSkills(Character character)
		{
			foreach (BattleSkill battleSkill in character.GetEquippedBattleSkills())
			{
				BattleSkill skill = Instantiate(battleSkill);
				skill.Initialise(this);
				skillset.AddSkill(skill.GetComponent<RectTransform>());
			}

			if (GetComponent<AIBattleAvatar>())
			{
				GetComponent<AIBattleAvatar>().BuildSkills();
			}

			skillset.BuildSkills();
		}

		void
		BuildStances(Character character)
		{
			foreach (Stance stance in character.GetEquippedStances())
			{
				Stance newStance = Instantiate(stance);
				newStance.InitialiseDescription();

				Persistency persistency = newStance.GetComponent<Persistency>();

				persistency.Initialise(new ActionDataDefault("Stance", this, null));

				conditions.LoadStance(newStance);
			}
		}

		public void
		CheckStances(int mana)
		{
			int costSoFar = 0;

			foreach (Stance stance in conditions.GetAllStances())
			{
				stance.CheckActivation(mana, ref costSoFar);
			}
		}

		public Transform
		GetModel()
		{
			return GetComponentInChildren<Animator>().transform;
		}

		public void
		SelfDestruct()
		{
			gameObject.SetActive(false);
			ui.GetPortrait().transform.SetParent(transform);
			DestroyImmediate(ui.GetPortrait().gameObject);
			DestroyImmediate(gameObject);
		}

		public bool
		IsHidden()
		{
			return hidden;
		}

		public void
		Hide()
		{
			hidden = true;
		}

		public void
		UnHide()
		{
			hidden = false;
		}
	}
}