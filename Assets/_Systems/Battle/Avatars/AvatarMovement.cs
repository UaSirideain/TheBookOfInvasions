﻿using UnityEngine;
using System.Collections;
using Battle.Grid;
using UnityEngine.AI;

namespace Battle{

public class AvatarMovement : MonoBehaviour
{
	Transform character {get{return GetComponentInChildren<Animator> ().transform;}}
	public Animator animator{get{return character.GetComponent<Animator>();}}
	NavMeshAgent navAgent {get{return character.GetComponent<NavMeshAgent>();}}
	AudioSource audioSource {get{return character.GetComponent<AudioSource>();}}

	//string noneAnimation = "attacked";
	//string attackAnimation = "attacked";

	Transform lastTarget;

	[SerializeField][Range(0.05f, 2f)] float rotateTowardsSeconds = 20.7f;


	void
	Start()
	{
		BattleLogic.manager.backendNotifications.EReturnCharactersToPlaces += EMoveHome;
	}


	void
	OnDestroy()
	{
		BattleLogic.manager.backendNotifications.EReturnCharactersToPlaces -= EMoveHome;
	}


	public IEnumerator
	PreAnimation(Transform target, AnimationData preAnimation)
	{
		target.GetComponent<AvatarMovement> ().ComeFaceMe (character);
		StartCoroutine(character.RotateTowards(target.GetComponent<AvatarMovement>().character.position, 2.0f));

		//UpdateAnimatorController(currentAnimation);
		//animator.SetTrigger("PreAnimation");

		yield return null;
	}


	public IEnumerator
	EffectAnimation(Target target, AnimationData effectAnimation)
	{
		//look at each other
		target.GetComponent<AvatarMovement> ().ComeFaceMe (character);
		StartCoroutine(character.RotateTowards(target.transform.position, 2.0f));


		//moving
		yield return StartCoroutine(MoveTo (target.GetComponent<Avatar>(), effectAnimation.range));


		//do effect animation
		LoadAnimationClip("generic_attack", effectAnimation.clip);

		animator.SetTrigger("attack");


		//tell target when it's being hit
		yield return new WaitForSeconds (effectAnimation.ContactTimeStamp ());

		PublishContactMade ();

		//it's good to remember who you fought last
		lastTarget = target.GetComponent<AvatarMovement>().character;


		//wait until animation is finished before we leave this coroutine
		yield return new WaitForSeconds (effectAnimation.EndTimeStamp () - effectAnimation.ContactTimeStamp ());
	}
		

	[SerializeField] AudioClip getHitSFX;

	public void
	GetHit()
	{
		animator.SetTrigger("get_hit");
		if (getHitSFX != null)
		{
			audioSource.clip = getHitSFX;
			audioSource.Play ();
		}
	}


	IEnumerator
	MoveTo(Avatar target, float range)
	{
		StartCoroutine (character.RotateTowards (target.characterModel.position, 2.0f));

		navAgent.SetDestination (target.characterModel.position);
		SetMoving (true);

		while (Vector3.Distance (character.position, target.characterModel.position) > range)
		{
			yield return null;
		}

		SetMoving (false);
	}


	void
	SetMoving(bool movingStatus)
	{
		animator.SetInteger ("speed", movingStatus ? 1 : 0);
		animator.SetBool ("moving", movingStatus);

		navAgent.isStopped = !movingStatus;
	}


	void
	EMoveHome()
	{
		StartCoroutine (MoveHome ());
	}


	IEnumerator
	MoveHome()
	{
		if (Vector3.Distance (character.position, occupiedCell.transform.position) > 1)
		{
			StartCoroutine(RotateTowards (occupiedCell.transform.position));

			SetMoving (true);
			navAgent.SetDestination (occupiedCell.transform.position);

			while(character.position.x != occupiedCell.transform.position.x || character.position.z != occupiedCell.transform.position.z)
			{
				//print("X: " + character.position.x + " does not equal " + home.x + " and Z: " + character.position.z + " does not equal " + home.z);
				yield return null;
			}

			SetMoving (false);

			StartCoroutine(RotateTowards (lastTarget));
		}

		BattleLogic.manager.backendNotifications.processingReturnCharactersToPlaces--;
	}
		

	public void
	LoadAnimationClip(string oldClip, AnimationClip newClip)
	{
		AnimatorOverrideController overrideController = new AnimatorOverrideController ();

		AnimatorStateInfo[] layerInfo = new AnimatorStateInfo[animator.layerCount];
		for (int j = 0; j < animator.layerCount; j++)
		{
			layerInfo [j] = animator.GetCurrentAnimatorStateInfo (j);
		}
			
		overrideController.runtimeAnimatorController = animator.runtimeAnimatorController;
		overrideController[oldClip] = newClip;
		animator.runtimeAnimatorController = overrideController;

		animator.Update (0.0f);

		for (int k = 0; k < animator.layerCount; k++)
		{
			animator.Play (layerInfo [k].fullPathHash, k, layerInfo [k].normalizedTime);
		}
	}

	GridCell occupiedCell;

	public void
	RecordHomePosition(GridCell cell)
	{
		occupiedCell = cell;
		occupiedCell.SetOccupied (gameObject);
	}

	public void
	SnapToHome()
	{
		navAgent.enabled = false;

		character.position = occupiedCell.transform.position;

		navAgent.enabled = true;
	}


	//EXTENSION METHOD

	int queueNumber;

	IEnumerator
	RotateTowards(Transform target)
	{
		Quaternion targetRotation = Quaternion.LookRotation(target.position - character.position);
		queueNumber++;
		int myQueueNumber = queueNumber;
		float lerpTime = 0;
		while (lerpTime < 1 && queueNumber == myQueueNumber)
		{
			lerpTime += Time.deltaTime / rotateTowardsSeconds;
			character.rotation = Quaternion.Slerp (character.rotation, targetRotation, lerpTime);
			yield return null;
		}
	}


	IEnumerator
	RotateTowards(Vector3 target)
	{
		Quaternion targetRotation = Quaternion.LookRotation(target - character.position);
		queueNumber++;
		int myQueueNumber = queueNumber;
		float lerpTime = 0;
		while (lerpTime < 1 && queueNumber == myQueueNumber)
		{
			lerpTime += Time.deltaTime / rotateTowardsSeconds;
			character.rotation = Quaternion.Slerp (character.rotation, targetRotation, lerpTime);
			yield return null;
		}
	}
		

	public void
	ComeFaceMe(Transform aggressor)
	{
		StartCoroutine(RotateTowards (aggressor));
	}


	void
	PublishContactMade()
	{
		if(EContactMade != null)
		{
			EContactMade ();
		}
	}

	public delegate void ContactMade();
	public event ContactMade EContactMade;
}
}