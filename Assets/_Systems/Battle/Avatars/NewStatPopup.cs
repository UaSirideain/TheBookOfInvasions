﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Cameras;

namespace Battle{

//public enum PopupType{ManaLowered, ManaRaised, HealthLowered, HealthRaised}

public delegate void OnPopupDestruction();

public class NewStatPopup : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

	[SerializeField] Text figureText;
	[SerializeField] Text mpText;

	public void
	Initialise(Change change)
	{
		figureText.text = change.value.ToString();

		if (change.type == ChangeType.RaisedHealth)
		{
			figureText.color = Color.green;
			mpText.gameObject.SetActive (false);
		}
		else if (change.type == ChangeType.LoweredHealth)
		{
			figureText.color = Color.white;
			mpText.gameObject.SetActive (false);
		}
		else if (change.type == ChangeType.RaisedMana)
		{
			figureText.color = Color.green;
			mpText.color = Color.green;
		}
		else if (change.type == ChangeType.LoweredMana)
		{
			figureText.color = Color.white;
			mpText.color = Color.white;
		}
	}
		
	public IEnumerator
	RiseAndFade(Vector3 vec, float offset, OnPopupDestruction onDestroy)
	{
		float t = 0f;
		float m = 0f;

		Vector3 rawStart = FindObjectOfType<BattleCamera> ().GetComponent<Camera> ().WorldToScreenPoint (vec);
		rawStart = new Vector3 (rawStart.x.ToCanvasSpace(), rawStart.y.ToCanvasSpace(), rawStart.z.ToCanvasSpace());
		Vector2 start = new Vector2 (rawStart.x + offset, rawStart.y + 80f);
		rt.anchoredPosition = start;
		Vector2 end = new Vector2(rt.anchoredPosition.x, rt.anchoredPosition.y + 30f);

		while (t < 1f)
		{
			rt.anchoredPosition = Mathfx.Sinerp (start, end, t);
			canvas.alpha = Mathfx.Coserp (0f, 1f, m);

			t += Time.deltaTime / .6f;
			m = Mathf.Clamp01 (t * 2.3f);
			yield return null;
		}
			
		t = 0f;

		while (t < 1f)
		{
			canvas.alpha = Mathfx.Coserp (1.5f, 0f, t);

			t += Time.deltaTime / 0.5f;
			yield return null;
		}

		Destroy (gameObject);
		onDestroy();
	}

	float
	Adjust(float already)
	{
		already /= Screen.width;
		already *= 1920f;
		return already;
	}
}
}