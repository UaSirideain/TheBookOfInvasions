﻿namespace Battle{

[System.Serializable]
public struct Speed
{
    public int lower;
    public int upper;

	public Speed (int newLower, int newUpper)
	{
		lower = newLower;
		upper = newUpper;
	}

    public int
    GetAverage()
    {
        return (lower + upper) / 2;
    }
}
}