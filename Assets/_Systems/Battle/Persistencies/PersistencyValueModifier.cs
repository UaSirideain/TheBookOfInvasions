﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class PersistencyValueModifier : MonoBehaviour
{
	[SerializeField] Modifier mod = new Modifier();

	void
	Awake()
	{
		mod.SetPersistency (GetComponent<Persistency> ());
	}

	public virtual List<Modifier>
	GetModifiers()
	{
		return new List<Modifier> (){ mod };
	}

	public void
	SetModifierAdditionValue(int newValue)
	{
		mod.addition = newValue;
	}
}
}