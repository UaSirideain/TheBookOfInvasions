﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Battle;

[CreateAssetMenu(fileName = "New Reserve UI Data", menuName = "SkillSelection/ReserveUI", order = 1)]
public class ReserveIconGraphics : ScriptableObject
{
	[SerializeField] Sprite unselected;
	[SerializeField] Sprite selected;
	[SerializeField] Sprite disabled;

	public Sprite
	GetUnselectedSprite()
	{
		return unselected;
	}

	public Sprite
	GetSelectedSprite()
	{
		return selected;
	}

	public Sprite
	GetDisabledSprite()
	{
		return disabled;
	}
}