﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Battle{

public class Reserve : MonoBehaviour
{
	Persistency persistency{ get { return GetComponent<Persistency> (); } }
	PersistencyUI ui { get { return GetComponent<PersistencyUI> (); } }

	[SerializeField] ReserveIconGraphics uiData;

	bool armed = false;

	public bool
	IsArmed()
	{
		return armed;
	}

	public void
	Disarm()
	{
		armed = false;
		ui.SetSprite (uiData.GetUnselectedSprite ());
	}

	void
	Awake()
	{
		ui.SetSprite (uiData.GetUnselectedSprite ());
	
		SetUpEvents ();
	}
		
	void
	MarkAsSelected()
	{
		ui.SetSprite(uiData.GetSelectedSprite ());
		armed = true;

		FindObjectOfType<ReserveIconPanel> ().Attach (this);
	}

	void
	MarkAsUnselected()
	{
		ui.SetSprite(uiData.GetUnselectedSprite ());
		armed = false;

		FindObjectOfType<ReserveIconPanel> ().Detach (this);
	}

	void
	SetUpEvents()
	{
		EventTrigger trigger = gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry mouseDownEntry = new EventTrigger.Entry();
		mouseDownEntry.eventID = EventTriggerType.PointerDown;
		mouseDownEntry.callback.AddListener ((data) => {PointerDown();});
		trigger.triggers.Add (mouseDownEntry);
	}
		
	void
	PointerDown()
	{
		Select ();
	}

	void
	Select()
	{
		if (BattleLogic.manager.battleState.GetState () == State.SelectingReserves)
		{
			if (armed)
			{
				MarkAsUnselected ();
			}
			else
			{
				MarkAsSelected ();
			}
		}
	}
}
}