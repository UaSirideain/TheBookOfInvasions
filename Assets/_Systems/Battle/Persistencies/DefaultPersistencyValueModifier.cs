﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class DefaultPersistencyValueModifier : PersistencyValueModifier
{
	public override List<Modifier>
	GetModifiers()
	{
		return new List<Modifier> (){};
	}
}
}