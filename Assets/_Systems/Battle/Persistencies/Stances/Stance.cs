﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Battle{

public class Stance : MonoBehaviour
{
	Persistency persistency{ get { return GetComponent<Persistency> (); } }
	Image icon { get { return GetComponent<Image> (); } }

	const string activatesAt1 = "\n\nActivates at ";
	const string activatesAt2 = " mana.";

	bool armed = false;

	[SerializeField] int manaCost;

	public bool
	IsArmed()
	{
		return armed;
	}

	public void
	InitialiseDescription()
	{
		GetComponent<PersistencyUI> ().AddToDescription (activatesAt1 + manaCost.ToString () + activatesAt2);
	}
		
	public bool
	CheckActivation(int avatarMana, ref int costSoFar)
	{
		if (avatarMana >= costSoFar + manaCost)
		{
			costSoFar += manaCost;
			Arm ();
		}
		return armed;
	}

	void
	Disarm()
	{
		armed = false;
		Render ();
	}

	void
	Arm()
	{
		armed = true;
		Render ();
	}

	void
	Awake()
	{
		Render ();
	}

	void
	Render()
	{
		if (armed)
		{
			icon.color = new Color (1, 1, 1, 1);
		}
		else
		{
			icon.color = new Color (1, 1, 1, .25f);
		}
	}
}
}