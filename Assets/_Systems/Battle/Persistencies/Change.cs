﻿using System.Collections.Generic;

namespace Battle
{
	public enum ChangeType
	{
		LoweredHealth = 0, RaisedHealth = 1, LoweredSpeed = 2, RaisedSpeed = 3,
		LoweredMana = 4, RaisedMana = 5, RaisedManaCost = 6, LoweredManaCost = 7,
		DisabledSkill = 8, EnabledSkill = 9, TookTurn = 10, SkippedTurn = 11,
		Killed = 12, Revived = 13, Wounded = 14, Restored = 15,
		SelectedSkill = 16, AddedTurn = 17, DestroyedTurn = 18,
		DidAnything = 19
	}

	public enum ChangeActuality { Actual = 0, Theoretical = 1 }

	[System.Serializable]
	public class Change : System.Object
	{
		public ChangeActuality actuality;
		public Target affectedTarget;

		public ChangeType type;
		public int value;

		public Change(ChangeType newType, int newValue, Target newAffectedTarget)
		{
			type = newType;
			value = newValue;
			affectedTarget = newAffectedTarget;
		}
	}

	public class ChangeResult
	{
		public ChangeType type;
		public int value;

		public ChangeResult(ChangeType newType, int newValue)
		{
			type = newType;
			value = newValue;
		}
	}
}