﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Battle{

public class PersistencyUI : MonoBehaviour
{
	Image image { get { return GetComponent<Image> (); } }

	Tooltip tooltip;
	OriginPoint tooltipOrigin = OriginPoint.Sides;

	[SerializeField] string uiName;
	[SerializeField] string uiDescription;
	[SerializeField] Sprite uiIcon;

	public void
	SetSprite(Sprite newSprite)
	{
		image.sprite = newSprite;
	}

	void
	Awake()
	{
		tooltip = FindObjectOfType<Tooltip> ();

		EventTrigger trigger = gameObject.AddComponent<EventTrigger> ();

		EventTrigger.Entry mouseEnterEntry = new EventTrigger.Entry();
		mouseEnterEntry.eventID = EventTriggerType.PointerEnter;
		mouseEnterEntry.callback.AddListener ((data) => {OnMouseOver();});
		trigger.triggers.Add (mouseEnterEntry);

		EventTrigger.Entry mouseExitEntry = new EventTrigger.Entry();
		mouseExitEntry.eventID = EventTriggerType.PointerExit;
		mouseExitEntry.callback.AddListener ((data) => {OnMouseExit();});
		trigger.triggers.Add (mouseExitEntry);
	}
		
	public void
	OnMouseOver()
	{
		tooltip.WriteText(uiName, uiDescription);
		tooltip.Show (GetComponent<RectTransform>(), tooltipOrigin);
	}

	public void
	OnMouseExit()
	{
		tooltip.Hide ();
	}

	public void
	SetUIText(string nameString, string descriptionString)
	{
		uiName = nameString;
		uiDescription = descriptionString;
	}

	public string
	GetName()
	{
		return uiName;
	}

	public void
	AddToDescription(string addendum)
	{
		uiDescription += addendum;
	}
}
}