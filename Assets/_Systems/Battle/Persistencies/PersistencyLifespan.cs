﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class PersistencyLifespan : MonoBehaviour
{
	[SerializeField] int lifespan;
	enum DurationType{Turns, TurnCycles, Triggers, EntireBattle}
	[SerializeField] DurationType durationType;

	//int lifespanLeft;
	int turnCreated;
	int turnCycleCreated;

	int triggersSoFar = 0;

	public virtual bool
	StillValid()
	{
		return lifespan > LifespanSoFar();
	}

	public void
	OnTrigger()
	{
		triggersSoFar++;
	}
		
	void
	Awake()
	{
		//lifespanLeft = lifespan;
		turnCreated = GetCurrentTurn();
		turnCycleCreated = GetCurrentTurnCycle ();
	}

	int
	GetCurrentTurn()
	{
		return BattleLogic.manager.turnOrderHistory.GetCurrentTurn ();
	}

	int
	GetCurrentTurnCycle()
	{
		return BattleLogic.manager.turnOrderHistory.GetCurrentTurnCycle ();
	}

	int
	LifespanSoFar()
	{
		if (durationType == DurationType.Triggers)
		{
			return triggersSoFar;
		}
		else if (durationType == DurationType.TurnCycles)
		{
			return GetCurrentTurnCycle() - turnCycleCreated;
		}
		else//if (durationType == DurationType.Turns)
		{
			return GetCurrentTurn() - turnCreated;
		}
	}
}
}