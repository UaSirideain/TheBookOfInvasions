﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class ActionWrapper
{
	public readonly Action action;

	public ActionWrapper(Action newAction)
	{
		action = newAction;
	}

	public bool
	CanBeFoundIn(List<ActionWrapper> others)
	{
		foreach (ActionWrapper wrapper in others)
		{
			if (wrapper.action == action)
			{
				return true;
			}
		}
		return false;
	}
}
}