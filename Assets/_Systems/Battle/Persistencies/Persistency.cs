﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public enum CasterType{Enchanted=0, CasterOfPersistency=1, None=2}
[RequireComponent(typeof(PersistencyUI))]
public class Persistency : Target
{
	PersistencyUI ui { get { return GetComponent<PersistencyUI> (); } }

	[SerializeField] Disposition disposition;

	[SerializeField] bool activeWhenDead;
	[SerializeField] CasterType casterType;
	[SerializeField] bool disableOriginalSkill;

	Condition condition;
	PersistencyLifespan lifespan;
	PersistencyValueModifier valueModification;

	ConditionsTray tray;
	Avatar source;
	Target enchanted;

	ActionDataDefault actionData;

	[SerializeField] ActionComposite action;

	public List<Modifier>
	GetModifiers()
	{
		if (valueModification)
		{
			return valueModification.GetModifiers ();
		}
		else
		{
			return new List<Modifier> ();
		}
	}

	public Avatar
	GetCasterOfGeneratedAction()
	{
		if (casterType == CasterType.CasterOfPersistency)
		{
			return actionData.caster;
		}
		else if (casterType == CasterType.Enchanted)
		{
			return enchanted as Avatar;
		}
		else
		{
			return null;
		}
	}

	public bool
	ActiveWhenDead()
	{
		return activeWhenDead;
	}

	public bool
	StillAlive()
	{
		return lifespan.StillValid ();
	}

	public void
	Initialise(ActionDataDefault newActionData)
	{
		actionData = newActionData;

		if (disableOriginalSkill)
		{
			actionData.linkedSkill.RegisterPersistency (this);
		}
	}

	public void
	Disable()
	{
		actionData.linkedSkill.DeregisterPersistency ();
	}

	public void
	SetEnchanted(Target newEnchanted)
	{
		enchanted = newEnchanted;
		condition.SetEnchanted (enchanted);
	}

	public void
	SetOwner(Avatar newSource)
	{
		source = newSource;
		condition.SetSource (source);
	}
//
//	public void
//	ReenableLinkedSkill()
//	{
//		if (actionData.linkedSkill != null)
//		{
//			actionData.linkedSkill.DeregisterActivePersistency (this);
//		}
//	}
//
//	public void
//	DisableLinkedSkill()
//	{
//		if (actionData.linkedSkill != null)
//		{
//			actionData.linkedSkill.RegisterActivePersistency (this);
//		}
//	}

	public void
	Evaluate(ActionWrapper newInfo, out ActionComposite generatedActions)
	{
		generatedActions = null;

		if (lifespan.StillValid ())//not sure this is necessary
		{
			bool triggered = false;
			condition.WriteAndEvaluate (newInfo, ref triggered);

			if(triggered)
			{
				generatedActions = GetGeneratedAction ();
				lifespan.OnTrigger ();
				CheckLifespan ();
			}
		}
	}

	public Disposition
	GetDisposition()
	{
		return disposition;
	}

	public void
	RegisterTray(ConditionsTray newTray)
	{
		tray = newTray;
	}

	void
	Awake()
	{
		if (GetComponent<Condition> ())
		{
			condition = GetComponent<Condition> ();
		}
		else
		{
			condition = gameObject.AddComponent<DefaultCondition> ();
		}

		if (GetComponent<PersistencyValueModifier> ())
		{
			valueModification = GetComponent<PersistencyValueModifier> ();
		}
		else
		{
			valueModification = gameObject.AddComponent<PersistencyValueModifier> ();
		}

		if (GetComponent<PersistencyLifespan> ())
		{
			lifespan = GetComponent<PersistencyLifespan> ();
			BattleLogic.manager.backendNotifications.EInitialiseNewTurnConditions += OnTurnEnd;
		}
		else
		{
			lifespan = gameObject.AddComponent<DefaultPersistencyLifespan> ();
		}
	}
		
	public ActionComposite
	GetGeneratedAction()
	{
		if (action != null)
		{
			ActionComposite actionInstance = Instantiate (action);
			actionInstance.Initialise(new ActionDataDefault(GetComponent<PersistencyUI>().GetName(), GetComponent<Persistency>().GetCasterOfGeneratedAction(), null));
			actionInstance.InitialiseWithPersistency (condition.GetRecordedData());
			return actionInstance;
		}
		else
		{
			return null;
		}
	}

	void
	CheckLifespan()
	{
		if (lifespan.StillValid () == false)
		{
			Destroy ();
		}
	}

	void
	OnTurnEnd()
	{
		CheckLifespan ();
		condition.OnTurnEnd ();

		if (GetComponent<Reserve> ())
		{
			GetComponent<Reserve> ().Disarm ();
		}
	}

	public Target
	GetEnchanted()
	{
		return enchanted;
	}

	public Avatar
	GetPersistencyCaster()
	{
		return actionData.caster;
	}

	void
	Destroy()
	{
		tray.RemovePersistency (this);
		BattleLogic.manager.backendNotifications.EInitialiseNewTurnConditions -= OnTurnEnd;
	}

	//TARGET STUFF

	bool provisionallyDestroyed = false;

	public void
	TargetDestroy()
	{
		provisionallyDestroyed = true;
	}

	public override void
	CommitChanges()
	{
		if (provisionallyDestroyed)
		{
			Destroy ();
		}
	}

	public override void
	ResetChanges()
	{
		provisionallyDestroyed = false;
	}
}
}