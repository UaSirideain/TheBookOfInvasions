﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle{

[System.Serializable]
public class StateCondition
{
	enum TargetToCheck{Source, Target}
	enum StateToCheck{IsWounded, IsDead}
	[SerializeField] TargetToCheck targetToCheck;
	[SerializeField] StateToCheck stateToCheck;

	public bool
	CheckTarget(Target target)
	{
		if (targetToCheck != TargetToCheck.Target)
		{
			return true;
		}
		else if (stateToCheck == StateToCheck.IsWounded)
		{
			return target.IsWounded ();
		}
		return false;
	}

	public bool
	CheckSource(Avatar avatar)
	{
		if (targetToCheck != TargetToCheck.Source)
		{
			return true;
		}
		else if (stateToCheck == StateToCheck.IsWounded)
		{
			return avatar.IsWounded ();
		}
		return false;
	}
}
}