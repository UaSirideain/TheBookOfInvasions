﻿using UnityEngine;
using System.Collections;

namespace Battle{

public class MenuInput : MonoBehaviour
{
	[SerializeField] string confirmButton;

	void
	Update()
	{
		if (Input.GetButton (confirmButton))
		{
			StartCoroutine(GetComponent<PostBattleScreen> ().FadeOut ());
		}
	}
}
}