﻿using UnityEngine;
using System.Collections;

namespace Battle
{

	public class PostBattleScreen : MonoBehaviour
	{
		Color transparent = new Color(0, 0, 0, 0);
		Color opaque = new Color(1, 1, 1, 1);

		[SerializeField] float fadeInDuration;
		[SerializeField] float fadeOutDuration;
		[SerializeField] GameObject victoryBackground;
		[SerializeField] GameObject defeatBackground;


		public IEnumerator
		Victory()
		{
			yield return StartCoroutine(FadeIn(victoryBackground));
		}

		public IEnumerator
		Defeat()
		{
			yield return StartCoroutine(FadeIn(defeatBackground));
		}

		IEnumerator
		FadeIn(GameObject screen)
		{
			screen.SetActive(true);
			yield return StartCoroutine(Lerp(screen.GetComponent<UnityEngine.UI.Image>(), transparent, opaque, fadeInDuration));
		}


		public IEnumerator
		FadeOut()
		{
			foreach (Transform child in transform)
			{
				StartCoroutine(Lerp(child.GetComponent<UnityEngine.UI.Image>(), opaque, transparent, fadeOutDuration));
			}

			victoryBackground.gameObject.SetActive(false);
			defeatBackground.gameObject.SetActive(false);

			yield return null;
		}


		IEnumerator
		Lerp(UnityEngine.UI.Image screen, Color start, Color end, float duration)
		{
			float lerpTime = 0.0f;
			while (lerpTime < 1)
			{
				lerpTime += Time.deltaTime / Mathf.Clamp(duration, 0.1f, 4.0f);
				screen.color = Color.Lerp(start, end, lerpTime);
				yield return null;
			}
		}
	}
}