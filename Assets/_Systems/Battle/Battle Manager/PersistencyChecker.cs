﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class PersistencyChecker : MonoBehaviour
{
	public void
	CheckActionWithPersistencies(ActionWrapper newInfo, out ActionComposite generatedActions)
	{
		generatedActions = null;
		List<Persistency> persistencyOrder = GenerateOrderedListOfPersistencies ();

		foreach (Persistency persistency in persistencyOrder)
		{
			persistency.Evaluate (newInfo, out generatedActions);

			if (generatedActions != null)
			{
				break;
			}
		}

		//FindObjectOfType<Events> ().RegisterAction (newInfo, out generatedActions);
	}

	public void
	CheckIntervalsOnPersistencies(Interval interval, ref ActionComposite generatedActions)
	{
		List<Persistency> persistencyOrder = GenerateOrderedListOfPersistencies ();

		foreach (Persistency persistency in persistencyOrder)
		{
			//persistency.RegisterInterval (interval, ref generatedActions);
		}
	}

	List<Persistency>
	GenerateOrderedListOfPersistencies()
	{
		Host activeHost = FindObjectOfType<TurnOrderHistory> ().GetActiveHost ();
		Host nonActiveHost = FindObjectOfType<TurnOrderHistory> ().GetNonActiveHost ();

		List<Persistency> persistencyOrder = new List<Persistency> ();

		persistencyOrder.AddRange (activeHost.GetConditions ().GetPositiveConditions());
		persistencyOrder.AddRange (nonActiveHost.GetConditions ().GetNegativeConditions ());

		persistencyOrder.AddRange (GetPositivePersistenciesFor (activeHost.GetAvatars ()));
		persistencyOrder.AddRange (GetNegativePersistenciesFor (nonActiveHost.GetAvatars ()));
		persistencyOrder.AddRange (GetNegativePersistenciesFor (activeHost.GetAvatars ()));
		persistencyOrder.AddRange (GetPositivePersistenciesFor (nonActiveHost.GetAvatars ()));

		persistencyOrder.AddRange (FindObjectOfType<BattlePersistencies> ().GetPersistencies ());

		return persistencyOrder;
	}

	List<Persistency>
	GetPositivePersistenciesFor(List<Avatar> avatars)
	{
		List<Persistency> persistencies = new List<Persistency>();

		foreach (Avatar avatar in avatars)
		{
			persistencies.AddRange (avatar.GetConditions ().GetPositiveConditions ());
		}
		return persistencies;
	}

	List<Persistency>
	GetNegativePersistenciesFor(List<Avatar> avatars)
	{
		List<Persistency> persistencies = new List<Persistency>();

		foreach (Avatar avatar in avatars)
		{
			persistencies.AddRange (avatar.GetConditions ().GetNegativeConditions ());
		}
		return persistencies;
	}

	public void
	EndOfTurn(bool endOfTurnCycle, ref ActionComposite generatedActions)
	{
		List<Persistency> persistencies = GenerateOrderedListOfPersistencies ();

		if (endOfTurnCycle)
		{
			foreach (Persistency persistency in persistencies)
			{
				//persistency.OnCycleEnd (ref generatedActions);
			}
		}

		foreach (Persistency persistency in persistencies)
		{
			//persistency.OnTurnEnd (ref generatedActions);
		}
	}
}
}