﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BattlePersistencies : MonoBehaviour
{
	List<Persistency> persistencies = new List<Persistency>();

	public List<Persistency>
	GetPersistencies()
	{
		return persistencies;
	}

	public void
	LoadPersistency(Persistency newPersistency)
	{
		persistencies.Add (Instantiate(newPersistency));
	}

	public void
	LoadPersistencies(List<Persistency> newPersistencies)
	{
		foreach (Persistency persistency in newPersistencies)
		{
			LoadPersistency(persistency);
		}
	}
}
}