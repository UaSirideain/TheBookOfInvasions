﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class TargetAssigner : MonoBehaviour
{
	[SerializeField] TargetAssignerUI ui;
	[SerializeField] Actors actors;

	TargetCollector targetCollector;
	List<Action> actions = new List<Action>();

	int activeTargetProfile = 0;

	public void
	InductActionForTargeting(TargetCollector action)
	{
		targetCollector = action;
	}

	public bool IsTargeting => targetCollector != null;

	enum CancellationType{Backtrack, CurrentProfileOnly}
	CancellationType cancellationType = CancellationType.Backtrack;

	public bool
	HasNoTargets()
	{
		if (targetCollector)
		{
			return targetCollector.GetIndexOfNextTargetList() == 0 && targetCollector.GetTargetProfile(targetCollector.GetIndexOfNextTargetList()).targets.Count == 0;
		}
		return false;
	}

	public void
	Reset()
	{
		Destroy (targetCollector.gameObject);
		targetCollector = null;
		StartCoroutine(ui.UnrenderInstructions ());
	}

	public void
	CancelLastTarget()
	{
		if (targetCollector)
		{
			int currentTargetProfile = activeTargetProfile;

			if (cancellationType == CancellationType.Backtrack)
			{
				while (currentTargetProfile >= 0 && !TargetProfileNeedsUserInput(currentTargetProfile))
				{
					currentTargetProfile--;
				}

				if (currentTargetProfile >= 0 && TargetProfileNeedsUserInput(currentTargetProfile))
				{
					targetCollector.CancelLastTarget (currentTargetProfile);
					activeTargetProfile = currentTargetProfile;
					SetUpInstructions ();
				}
			}
			else//if(cancellationType == CancellationType.CurrentProfileOnly)
			{
				if (currentTargetProfile >= 0 && TargetProfileNeedsUserInput(currentTargetProfile))
				{
					targetCollector.CancelLastTarget (currentTargetProfile);
				}
			}
		}
	}

	bool
	TargetProfileNeedsUserInput(int currentTargetProfile)
	{
		return targetCollector.GetTargetProfile(currentTargetProfile).targets.Count > 0 && targetCollector.GetTargetProfile(currentTargetProfile).autoTarget == AutoTarget.Inspector;
	}

	public void
	CancelTarget(Target target, int profileIndex)
	{
		if (targetCollector.GetTargetProfile (profileIndex).targets.Contains (target))
		{
			targetCollector.CancelTarget (target, profileIndex);
		}
	}

	public void
	PassTargetToCollector(Target target)
	{
		if (targetCollector != null)
		{
			targetCollector.AddTarget (target, activeTargetProfile);
		}
	}

    public void
    PrepareValidTargetsForNextAction(out bool finishedTargetingForThisAction)
	{
		FilterOutActionsWithTargets ();

		if (targetCollector != null)
		{
			activeTargetProfile = GetAppropriateTargetProfileIndex ();
			actors.ArmTargetsAsValidFor (targetCollector.GetTargetProfile(activeTargetProfile));
			SetUpInstructions ();

			finishedTargetingForThisAction = false;
		}
		else
		{
			StartCoroutine(ui.UnrenderInstructions ());
			finishedTargetingForThisAction = true;
        }
    }

	int
	GetAppropriateTargetProfileIndex()
	{
		if (targetCollector.GetTargetProfile (activeTargetProfile).targets.Count == targetCollector.GetTargetProfile (activeTargetProfile).maxTargets)
		{
			return targetCollector.GetIndexOfNextTargetList ();
		}
		return activeTargetProfile;
	}

	void
	SetUpInstructions()
	{
		ui.InitialiseInstructionsPane (targetCollector, activeTargetProfile, SetCurrentProfileIndexAs);
		ui.RenderInstructionsFor (activeTargetProfile);
	}

	void
	SetCurrentProfileIndexAs(int i)
	{
		cancellationType = CancellationType.CurrentProfileOnly;
		activeTargetProfile = i;
		ui.RenderInstructionsFor (activeTargetProfile);
	}

	public List<Action>
	GetFinishedActions()
	{
		List<Action> finishedActions = new List<Action> ();

		foreach (Action action in actions)
		{
			finishedActions.Add (action);
		}

		actions.Clear ();

		return finishedActions;
	}
		
	void
	FilterOutActionsWithTargets()
	{
		if (targetCollector != null)
		{
			if (targetCollector.NeedsTargets () == false)
			{
				actions.AddRange(targetCollector.GetComponent<Choreographer>().BuildActions());
				targetCollector = null;
			}
		}
	}
}
}