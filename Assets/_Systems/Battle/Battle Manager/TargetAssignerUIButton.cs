﻿using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class TargetAssignerUIButton : MonoBehaviour
{
	public void
	Initialise(int display)
	{
		GetComponentInChildren<Text> ().text = display.ToString ();
	}

	public void
	SetActiveButton ()
	{
		GetComponentInChildren<Text> ().fontStyle = FontStyle.Bold;
	}

	public void
	SetInactiveButton()
	{
		GetComponentInChildren<Text> ().fontStyle = FontStyle.Normal;
	}
}
}