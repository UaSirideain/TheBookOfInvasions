﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

namespace Battle{

public delegate void ButtonDelegate(int buttonIndex);

public class TargetAssignerUI : MonoBehaviour
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
	[SerializeField] Text instructions;
	[SerializeField] Image background;
	[SerializeField] TargetAssignerUIButton instructionsIndexButtonPrefab;
	[SerializeField] Text skillNamePanelText;
	[SerializeField] RectTransform skillNamePanel;

	List<TargetAssignerUIButton> profileIndexButtons = new List<TargetAssignerUIButton> ();

	public IEnumerator
	UnrenderInstructions()
	{
		if (Rendered ())
		{
			yield return StartCoroutine (Fade (1f, 0f));
			canvas.Disable ();

			instructions.text = "";

			instructions.enabled = false;
			background.enabled = false;

			DeleteTargetMarkers ();
			DeleteButtons ();
		}
	}

	public void
	RenderInstructionsFor(int activeProfileIndex)
	{
		int newProfileIndex = ConvertActiveProfileIndexToUIIndex (activeProfileIndex);

		instructions.text = targeter.GetInstructionsForProfile (activeProfileIndex);

		foreach (TargetAssignerUIButton button in profileIndexButtons.Excluding(newProfileIndex))
		{
			button.SetInactiveButton ();
		}

		background.color = Colour.opaque;
		profileIndexButtons [newProfileIndex].SetActiveButton ();
	}

	public void
	SetAsMarked(Target target, int profileIndex)
	{
		int displayIndex = ConvertActiveProfileIndexToUIIndex (profileIndex) + 1;
		target.MarkAsTargeted (displayIndex, profileIndex);
	}

	public void
	SetAsUnmarked(Target target, int profileIndex)
	{
		//profileIndex = ConvertActiveProfileIndexToUIIndex (profileIndex);
		target.UnmarkAsTargeted (profileIndex);
	}
		
	public void
	InitialiseInstructionsPane(TargetCollector targeter, int activeProfileIndex, ButtonDelegate buttonDel)
	{
		instructions.text = targeter.GetInstructionsForProfile (activeProfileIndex);
		background.enabled = true;
		instructions.enabled = true;

		if (profileIndexButtons.Count == 0)
		{
			BuildButtons (targeter, buttonDel);
			canvas.EnableTransparent ();
			StartCoroutine (Fade (0f, 1f));
		}
	}

	const float duration = .15f;

	IEnumerator
	Fade(float start, float end)
	{
		float t = 0f;
		while(t < 1.2f)
		{
			canvas.alpha = Mathf.Lerp (start, end, t);
			
			t += Time.deltaTime / duration;
			yield return null;
		}
		canvas.alpha = 1f;
	}

	TargetCollector targeter;

	void
	DeleteTargetMarkers()
	{
		FindObjectOfType<Actors> ().ClearTargetMarkers ();
	}

	void
	Update()
	{
		if (Input.GetMouseButtonDown (1))
		{
			skillNamePanelText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		}
	}

	void
	BuildButtons(TargetCollector newTargeter, ButtonDelegate buttonDel)
	{
		targeter = newTargeter;

		skillNamePanelText.text = targeter.GetComponent<ActionComposite>().GetActionData().uiName;
		skillNamePanelText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		float skillNamePanelWidth = skillNamePanelText.GetComponent<RectTransform>().sizeDelta.x + 30f;
		skillNamePanel.sizeDelta = new Vector2 (skillNamePanelWidth, skillNamePanel.sizeDelta.y);


		int displayIndex = 0;

		for (int i = 0; i < targeter.GetTargetProfileCount (); i++)
		{
			if (targeter.GetTargetProfile (i).autoTarget == AutoTarget.Inspector)
			{
				TargetAssignerUIButton button = Instantiate (instructionsIndexButtonPrefab);
				RectTransform t = button.GetComponent<RectTransform> ();

				button.Initialise (displayIndex + 1);
				int buttonIndex = i;

				button.GetComponent<Button>().onClick.AddListener(delegate {buttonDel(buttonIndex);});

				t.SetAsChildOf(transform);
				t.anchoredPosition = new Vector2 (skillNamePanelWidth + displayIndex * t.sizeDelta.x, 0);

				if (targeter.GetTargetProfileCount () == 1)
				{
					button.gameObject.AddComponent<CanvasGroup> ();
					button.GetComponent<CanvasGroup> ().alpha = 0f;
				}

				profileIndexButtons.Add (button);

				displayIndex++;
			}
		}
	}

	void
	DeleteButtons()
	{
		while (profileIndexButtons.Count > 0)
		{
			Destroy (profileIndexButtons [0].gameObject);
			profileIndexButtons.RemoveAt (0);
		}
		profileIndexButtons.Clear ();
	}

	int
	ConvertActiveProfileIndexToUIIndex(int original)
	{
		int newValue = 0;
		for (int i = 0; i < original; i++)
		{
			if (targeter.GetTargetProfile (i).autoTarget == AutoTarget.Inspector)
			{
				newValue++;
			}
		}
		return newValue;
	}

	bool
	Rendered()
	{
		return canvas.alpha > 0f;
	}
}
}