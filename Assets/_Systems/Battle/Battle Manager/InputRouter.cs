﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class InputRouter : MonoBehaviour, IInputtable
{
    private BattleState battleState { get { return GetComponent<BattleState>(); } }
    private BattleLogic battleLogic { get { return GetComponent<BattleLogic>(); } }
        
	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Up, InputButton.RightMouse, new List<InputState> (), RightUp);
	}

	void
	OnEnable()
	{
		GetComponent<InputListener> ().SetListening ();
	}

	void
	RightUp()
	{
		BattleLogic.manager.Cancel ();
	}

    public void
    CharacterSelected(Avatar avatar)
    {
		if (battleState.InSelectingCharacterPhase(avatar) || battleState.InSelectingSkillsPhase())
        {
            BattleLogic.manager.SetSelectedCharacter(avatar);
        }
    }

    public void
    SkillSelected(ActionComposite action)
    {
        if (battleState.InSelectingSkillsPhase())
        {
            BattleLogic.manager.MainSkillIs(action);
        }
    }

    public void
    TargetSelected(Target target)
    {
        if (battleState.InSelectingTargetsPhase())
        {
            BattleLogic.manager.TargetIs(target);
        }
    }
}
}