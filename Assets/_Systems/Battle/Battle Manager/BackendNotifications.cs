﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

//TODO: this class uses delegates and callbacks. It seems the callbacks absolutely aren't needed. Before we change the structure, it should be verified that the system is waiting where it needs to wait.
public class BackendNotifications : MonoBehaviour
{
	public IEnumerator
	InitialiseNewTurnConditions()
	{
		if (EInitialiseNewTurnConditions != null)
		{
			processingInitialiseNewTurnConditions = EInitialiseNewTurnConditions.GetInvocationList ().Length;
			EInitialiseNewTurnConditions ();
			//while (processingInitialiseNewTurnConditions > 0){yield return null;}
		}

		yield return null;
	}

	public delegate void dInitialiseNewTurnConditions();
	public event dInitialiseNewTurnConditions EInitialiseNewTurnConditions;
	[HideInInspector] public int processingInitialiseNewTurnConditions;

	/////////////////////////////////////////////////////////////////////////////////////////

	public IEnumerator
	InitialiseNewTurnCharacters()
	{
		if (EInitialiseNewTurnCharacters != null)
		{
			processingInitialiseNewTurnCharacters = EInitialiseNewTurnCharacters.GetInvocationList ().Length;
			EInitialiseNewTurnCharacters ();
			//while (processingInitialiseNewTurnCharacters > 0){yield return null;}
		}

		yield return null;
	}

	public delegate void dInitialiseNewTurnCharacters();
	public event dInitialiseNewTurnCharacters EInitialiseNewTurnCharacters;
	[HideInInspector] public int processingInitialiseNewTurnCharacters;

	/////////////////////////////////////////////////////////////////////////////////////////


	public void
	AISelectCharacter()
	{
		if (EAISelectCharacter != null)
		{
			EAISelectCharacter ();
		}
	}

	public delegate void dAISelectCharacter();
	public event dAISelectCharacter EAISelectCharacter;


	/////////////////////////////////////////////////////////////////////////////////////////


	public void
	AISelectSkill()
	{
		if (EAISelectSkill != null)
		{
			EAISelectSkill ();
		}
	}

	public delegate void dAISelectSkill();
	public event dAISelectSkill EAISelectSkill;


	/////////////////////////////////////////////////////////////////////////////////////////


	public void
	AISelectTarget(Action action)
	{
		if (EAISelectTarget != null)
		{
			EAISelectTarget (action);
		}
	}

	public delegate void dAISelectTarget(Action action);
	public event dAISelectTarget EAISelectTarget;
	/////////////////////////////////////////////////////////////////////////////////////////


	public void
	UndoRenderSkillsets()
	{
		if (EUndoRenderSkillsets != null)
		{
			EUndoRenderSkillsets ();
		}
	}

	public delegate void dUndoRenderSkillsets();
	public event dUndoRenderSkillsets EUndoRenderSkillsets;


	/////////////////////////////////////////////////////////////////////////////////////////


	public IEnumerator
	ReturnCharactersToPlaces()
	{
		if(EReturnCharactersToPlaces != null)
		{
			processingReturnCharactersToPlaces = EReturnCharactersToPlaces.GetInvocationList ().Length;
			EReturnCharactersToPlaces ();
			//while (processingReturnCharactersToPlaces > 0){yield return null;}
		}
		yield return null;
	}

	public delegate void dReturnCharactersToPlaces();
	public event dReturnCharactersToPlaces EReturnCharactersToPlaces;
	[HideInInspector] public int processingReturnCharactersToPlaces;


	////////////////////////////////////////////////////////////////////////////////////////////


	public IEnumerator
	EndOfTurnCleanUp()
	{
		if (EEndOfTurnCleanUp != null)
		{
			processingEndOfTurnCleanUp = EEndOfTurnCleanUp.GetInvocationList ().Length;
			EEndOfTurnCleanUp ();
			while (processingEndOfTurnCleanUp > 0){yield return null;}
		}
		yield return null;
	}

	public delegate void dEndOfTurnCleanUp();
	public event dEndOfTurnCleanUp EEndOfTurnCleanUp;
	[HideInInspector] public int processingEndOfTurnCleanUp;
}
}