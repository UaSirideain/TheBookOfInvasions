﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Battle{

public class ActionsList : MonoBehaviour
{
	CurrentActionUI ui { get { return FindObjectOfType<CurrentActionUI> (); } }

	List<Action> actionsList = new List<Action>();
	Action finishedAction;

	public void
	AddActionsToQueue(List<Action> newActions)
	{
		List<Action> beforeActions = new List<Action> ();
		Action currentAction = null;
		List<Action> afterActions = new List<Action> ();
		List<Action> newActionsList = new List<Action> ();

		foreach (Action action in newActions)
		{
			if (action.SortOrder == ActionSortOrder.Before)
			{
				beforeActions.Add (action);
			}
			else//if (action.actionPosition == ActionPosition.After)
			{
				afterActions.Add (action);
			}
		}

		if(actionsList.Count > 0)
		{
			currentAction = actionsList [0];
			actionsList.RemoveAt(0);
		}

		newActionsList.AddRange(beforeActions);
		if (currentAction != null)
		{
			newActionsList.Add (currentAction);
		}
		newActionsList.AddRange(afterActions);
		newActionsList.AddRange(actionsList);

		actionsList.Clear ();

		actionsList.AddRange (newActionsList);

		newActionsList.Clear ();
	}

	public IEnumerator
	Run(bool theoretical)
	{
		if (theoretical)
		{
			HypotheticalRun ();
		}
		else
		{
			yield return StartCoroutine(ActualRun ());
		}
	}
		
	public bool
	ActionsListEmpty()
	{
		return actionsList.Count == 0 && finishedAction == null;
	}

	public Action
	GetNextAction()
	{
		return actionsList [0];
	}

	public Action
	RetrieveFinishedActions()
	{
		Action action = finishedAction;
		finishedAction = null;
		return action;
	}

	public void
	EndBattle()
	{
		finishedAction = null;
	}

	void
	HypotheticalRun()
	{
		actionsList [0].TheoreticalRun ();
	}

	IEnumerator
	ActualRun()
	{
		yield return StartCoroutine(actionsList [0].ActualRun ());
		finishedAction = actionsList[0];
		actionsList.RemoveAt (0);
	}

	void
	OnGUI()
	{
		for (int i = 0; i < actionsList.Count; i++)
		{
			GUI.Label (new Rect (0, i * 55, 1000, 50), actionsList [i].name);
		}
	}
}
}