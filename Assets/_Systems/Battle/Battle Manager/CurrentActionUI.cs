﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentActionUI : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }
	Image bg { get { return GetComponent<Image> (); } }

	[SerializeField] Image actionIcon;
	[SerializeField] Text actionName;
	[SerializeField] SizeAdjuster container;

	const float leftRightMargins = 68f;

	int lerpTicket = 0;

	const float fadeDuration = 3f;

	public IEnumerator
	FadeIn()
	{
		yield return StartCoroutine (Fade (Colour.transparent, Colour.opaque));
	}

	public IEnumerator
	FadeOut()
	{
		yield return StartCoroutine (Fade (Colour.opaque, Colour.transparent));
	}
		
	IEnumerator
	Fade(Color start, Color end)
	{
		lerpTicket++;
		int myLerpTicket = lerpTicket;
		float t = 0f;

		Color targetColour;

		while (myLerpTicket == lerpTicket && t < 1.2f)
		{
			targetColour = Color.Lerp (start, end, t);

			actionIcon.color = targetColour;
			actionName.color = targetColour;
			bg.color = targetColour;

			t += Time.deltaTime * fadeDuration;

			yield return null;
		}
	}

	public void
	RenderAction(Sprite sprite, string text)
	{
		actionIcon.sprite = sprite;
		actionName.text = text;
		Fit ();
	}

	void
	Fit()
	{
		actionName.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		container.Fit ();
		rt.sizeDelta = new Vector2 (container.GetComponent<RectTransform> ().sizeDelta.x + leftRightMargins, rt.sizeDelta.y);
	}
}