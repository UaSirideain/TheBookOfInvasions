﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    //rename these to Phase?
    public enum State { InitialisingBattle, InitialisingNewTurn, SelectingCharacter, SelectingSkill, SelectingReserves, ResolvingEffects, CleaningUp, None, BattleOver }
    public enum SubState { Processing, General, SelectingTargets, CheckingVictoryConditions, SelectingItems }

    public class BattleState : MonoBehaviour
    {
        /* START: Everything in here can be put into Battle Logic */
       
        ActiveStates stateMachine { get { return GetComponent<ActiveStates>(); } }

        public void
        InitialiseBattle()
        {
            stateMachine.WriteState(InputState.Battle);
        }

        public void
        EndBattle()
        {
            stateMachine.RemoveState(InputState.Battle);
        }

        /* END: Everything in here can be put into Battle Logic */



        [SerializeField] State currentState = State.None;
        [SerializeField] SubState currentSubState = SubState.General;

        public void
        SetState(State newState)
        {
            currentState = newState;
        }

        public void
        SetSubState(SubState newSubState)
        {
            currentSubState = newSubState;
        }

        public bool
        InSelectingCharacterPhase(Avatar avatar)
        {
            if (currentState == State.SelectingCharacter && currentSubState == SubState.General)
            {
                return true;
            }
            return false;
        }

        public bool
        InSelectingTargetsPhase()
        {
            if (currentSubState == SubState.SelectingTargets)// && turnOrderHistory.IsSkillSelected())
            {
                return true;
            }
            return false;
        }

        public bool
        InSelectingSkillsPhase()
        {
            if (currentState == State.SelectingSkill && (currentSubState == SubState.General || currentSubState == SubState.SelectingItems))
            {
                return true;
            }
            return false;
        }

        public State
        GetState()
        {
            return currentState;
        }

        public SubState
        GetSubState()
        {
            return currentSubState;
        }
    }
}