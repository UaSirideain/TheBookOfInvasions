﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class ValueModifierService : MonoBehaviour
{
	List<Persistency>
	GenerateOrderedListOfPersistencies()
	{
		List<Persistency> persistencyOrder = new List<Persistency> ();

		persistencyOrder.AddRange(GetAllPersistenciesFor (FindObjectOfType<Actors> ().GetAvatarsBySpeed ()));
		persistencyOrder.AddRange (GetAllPersistenciesFor(FindObjectOfType<Actors>().GetHosts()));

		return persistencyOrder;
	}

	List<Persistency>
	GetAllPersistenciesFor(List<Avatar> avatars)
	{
		List<Persistency> persistencies = new List<Persistency>();

		foreach (Avatar avatar in avatars)
		{
			persistencies.AddRange (avatar.GetConditions ().GetAllConditions ());
		}
		return persistencies;
	}

	List<Persistency>
	GetAllPersistenciesFor(List<Host> hosts)
	{
		List<Persistency> persistencies = new List<Persistency>();

		foreach (Host host in hosts)
		{
			persistencies.AddRange (host.GetConditions ().GetAllConditions ());
		}

		return persistencies;
	}

	List<Modifier>
	GetModifications()
	{
		List<Modifier> mods = new List<Modifier>();
		foreach (Persistency pers in GenerateOrderedListOfPersistencies())
		{
			if (pers.StillAlive ())
			{
				mods.AddRange (pers.GetModifiers ());
			}
		}

		return mods;
	}

	public int
	ModifyValue(int rawValue, ModificationType type, Avatar owner, Target target = null)
	{
		List<int> additives = new List<int> ();
		List<int> multipliers = new List<int> ();

		foreach (Modifier mod in GetModifications())
		{
			if(mod.VerifyType(type) && mod.VerifySource(owner) && mod.VerifyTarget(target))
			{
				additives.Add (mod.addition);
				multipliers.Add (mod.multiplication);
			}
		}

		foreach (int additive in additives)
		{
			rawValue += additive;
		}

		foreach (int multiplier in multipliers)
		{
			rawValue *= multiplier;
		}

		return Maths.ClampPositive(rawValue);
	}
}
}