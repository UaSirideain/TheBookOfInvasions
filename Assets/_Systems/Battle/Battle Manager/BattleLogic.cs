﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cameras;

namespace Battle
{
	public class BattleLogic : MonoBehaviour
	{
		public static BattleLogic manager;

		void
		Awake()
		{
			if (manager == null)
			{
				//DontDestroyOnLoad (gameObject);
				manager = this;
			}
			else if (manager != this)
			{
				Destroy(gameObject);
			}
		}

		[SerializeField] public CameraManager cameraManager;

		public BackendNotifications backendNotifications { get { return GetComponent<BackendNotifications>(); } }
		CurrentActionUI actionUI { get { return FindObjectOfType<CurrentActionUI>(); } }

		[SerializeField] public TurnOrder turnOrder;
		[SerializeField] public TurnOrderHistory turnOrderHistory;
		[SerializeField] public ActionsList actionsList;
		[SerializeField] public ReserveTimer reserveTimer;
		[SerializeField] public PostBattleScreen victoryScreen;
		[SerializeField] Text breadcrumbs;
		[SerializeField] public Actors actors;
		[SerializeField] public TargetAssigner targetSorter;
		[SerializeField] public BattleState battleState { get { return GetComponent<BattleState>(); } }
		[SerializeField] Grid.Grid grid;
		CanvasGroup canvas { get { return GetComponent<CanvasGroup>(); } }
		PersistencyChecker persistencyChecker { get { return GetComponent<PersistencyChecker>(); } }
		[SerializeField] public ValueModifierService modService;
		[SerializeField] public BattlePersistencies battlePersistencies;
		[SerializeField] public Hitchcock cameraDirector;

		WaitForSeconds waitForPortraitTrays = new WaitForSeconds(.7f);
		WaitForSeconds endOfTurnWait = new WaitForSeconds(1.7f);

		////////////////////////////////////////BATTLE FLOW////////////////////////////////////////////	

		public IEnumerator
		InitialiseBattle(Host ally, Host enemy, List<Persistency> preloadedConditions)
		{
			canvas.alpha = 1f;
			canvas.blocksRaycasts = true;

			battleState.SetState(State.InitialisingBattle);

			grid.Initialise();

			yield return StartCoroutine(actors.BuildHosts(ally, enemy, grid));
			actors.InitialiseBattle();

			yield return waitForPortraitTrays;

			turnOrderHistory.InitialiseBattle();
			battleState.InitialiseBattle();

			battlePersistencies.LoadPersistencies(preloadedConditions);

			ResolveActions1();
			NextState();
		}

		Avatar avatar;

		public void
		SetSelectedCharacter(Avatar selectedAvatar)
		{
			if (avatar != null)
			{
				SelectingCharacterPhase();
			}

			if (turnOrderHistory.GetActiveHost() == selectedAvatar.GetHost())
			{
				avatar = selectedAvatar;

				battleState.SetState(State.SelectingSkill);
				battleState.SetSubState(SubState.General);

				backendNotifications.AISelectSkill();
				selectedAvatar.DisplaySkillset();

				breadcrumbs.text = selectedAvatar.name;

				cameraDirector.CharacterSelected(selectedAvatar);
			}
		}

		public void
		UsingItem()
		{
			battleState.SetSubState(SubState.SelectingItems);
			backendNotifications.UndoRenderSkillsets();
		}

		public void
		CancelItems()
		{
			battleState.SetSubState(SubState.General);
			avatar.DisplaySkillset();
			FindObjectOfType<BattleInventoryUI>().ResetUI();
		}

		public void
		MainSkillIs(ActionComposite action)
		{
			battleState.SetSubState(SubState.General);
			backendNotifications.UndoRenderSkillsets();
			breadcrumbs.text += " > Skill";

			targetSorter.InductActionForTargeting(action.GetComponent<TargetCollector>());
			TargetMode();
		}

		public void
		TargetIs(Target target)
		{
			targetSorter.PassTargetToCollector(target);

			TargetMode();
		}

		IEnumerator
		InitialiseNewTurnPhase()
		{
			battleState.SetState(State.InitialisingNewTurn);
			battleState.SetSubState(SubState.Processing);

			yield return StartCoroutine(backendNotifications.InitialiseNewTurnConditions());

			turnOrderHistory.InitialiseNewTurn();

			yield return StartCoroutine(backendNotifications.InitialiseNewTurnCharacters());

			ActionComposite generatedActions = null;

			persistencyChecker.CheckIntervalsOnPersistencies(Interval.StartOfTurn, ref generatedActions);

			GeneratedActions(generatedActions);
			NextState();
		}

		void
		SelectingCharacterPhase()
		{
			avatar = null;

			battleState.SetState(State.SelectingCharacter);
			battleState.SetSubState(SubState.General);

			backendNotifications.UndoRenderSkillsets();
			breadcrumbs.text = "";

			ActionComposite cachedMainAction = turnOrderHistory.RetrieveMainCachedAction();

			if (cachedMainAction == null)
			{
				turnOrderHistory.GetActiveHost().InitialiseSelectCharacters();
				backendNotifications.AISelectCharacter();
			}
			else
			{
				battleState.SetState(State.SelectingSkill);
				MainSkillIs(cachedMainAction);
			}
		}

		void
		TargetMode()
		{
			cameraDirector.SelectingTargets();

			bool finishedTargetingForThisAction;

			targetSorter.PrepareValidTargetsForNextAction(out finishedTargetingForThisAction);
			if (finishedTargetingForThisAction == true)
			{
				actionsList.AddActionsToQueue(targetSorter.GetFinishedActions());
				AllTargetsSelected();
			}
			else
			{
				battleState.SetSubState(SubState.SelectingTargets);
				//backendNotifications.AISelectTarget (actionWithoutTargets);
			}
		}

		void
		AllTargetsSelected()
		{
			if (battleState.GetState() == State.SelectingSkill)
			{
				NextState();
			}
			else
			{
				ResolveActions1();
				NextState();
			}
		}

		[SerializeField] public ActionComposite passTurnPrefab;

		void
		PassTurn()
		{
			//ActionComposite newAction = Instantiate(passTurnPrefab);
			//ActionDataDefault actionData = new ActionDataDefault("Pass Turn", caster, null);
			//newAction.Initialise(actionData);

			//MainSkillIs(newAction);
			////Invoke("CheckVictoryConditions", 1);
		}

		int
		SkillsInReserve()
		{
			int amount = 0;
			foreach (Host host in actors.GetHosts())
			{
				foreach (Avatar avatar in host.GetAvatars())
				{
					if (avatar.GetStats().IsBattleFit)
					{
						amount += avatar.SkillsInReserve();
					}
				}
			}
			return amount;
		}

		void
		SelectReservesPhase()
		{
			battleState.SetState(State.SelectingReserves);

			if (SkillsInReserve() > 0)
			{
				StartCoroutine(reserveTimer.StartTimer());
			}
			else
			{
				NextState();
			}
		}

		public void
		EndReserveSelection()
		{
			//fade out thingy
			NextState();
		}

		void
		ResolveEffectsPhase()
		{
			battleState.SetState(State.ResolvingEffects);
			ResolveActions1();
			NextState();
		}

		//Persistencies triggering other persistencies can cause infinite loops. This prevents that.
		//It probably wont be in the final game, but it helps us diagnose issues during development.
		int methodLimit = 0;

		bool actionsAreResolved = false;

		void
		ResolveActions1()
		{
			actionsAreResolved = false;
			StartCoroutine(ResolveActions());
		}

		IEnumerator
		ResolveActions(bool theoretical = true)
		{
			//while (canMove == false)
			//{
			//	yield return null;
			//}
			//canMove = false;

			battleState.SetSubState(SubState.General);
			if (actionsList.ActionsListEmpty() == false)
			{
				yield return StartCoroutine(actionsList.Run(theoretical));
			}

			if (battleState.GetState() != State.BattleOver)
			{
				if (targetSorter.IsTargeting == false)
				{
					if (actionsList.ActionsListEmpty() == true)
					{
						actionsAreResolved = true;
					}
					else if (methodLimit < 25)
					{
						CheckTriggerPersistencies(theoretical);
					}
					else
					{
						print("Broke method limit: probable cause: infinite loop.");
					}
				}
			}
		}

		void
		CheckTriggerPersistencies(bool theoretical)
		{
			methodLimit++;

			ActionComposite generatedActions;
			Action action;

			if (theoretical)
			{
				action = actionsList.GetNextAction();
			}
			else
			{
				action = actionsList.RetrieveFinishedActions();
			}

			ActionWrapper newInfo = BuildPersistencyInfo(action);

			persistencyChecker.CheckActionWithPersistencies(newInfo, out generatedActions);

			if (generatedActions != null)
			{
				GeneratedActions(generatedActions);
			}
			else
			{
				StartCoroutine(ResolveActions(!theoretical));
			}
		}

		ActionWrapper
		BuildPersistencyInfo(Action action)
		{
			return new ActionWrapper(action);
		}

		IEnumerator
		CleanUpPhase()
		{
			battleState.SetState(State.CleaningUp);
			yield return StartCoroutine(backendNotifications.EndOfTurnCleanUp());

			//yield return StartCoroutine(ResolveActions());

			breadcrumbs.text = "";

			ActionComposite generatedActions = null;

			bool endOfCycle = turnOrderHistory.IsLastTurnInCycle();
			persistencyChecker.EndOfTurn(endOfCycle, ref generatedActions);
			persistencyChecker.CheckIntervalsOnPersistencies(Interval.EndOfTurn, ref generatedActions);

			ReturnAvatarsToBattleStations();

			GeneratedActions(generatedActions);
			NextState();

			yield return endOfTurnWait;
		}

		void
		ReturnAvatarsToBattleStations()
		{
			foreach (Avatar ava in FindObjectsOfType<Avatar>())
			{
				ava.ReturnToHome();
			}
		}

		void
		GeneratedActions(ActionComposite actionComposite)
		{
			if (actionComposite != null)
			{
				targetSorter.InductActionForTargeting(actionComposite.GetComponent<TargetCollector>());
				TargetMode();
			}
			else
			{
				StartCoroutine(ResolveActions());
			}
		}

		public IEnumerator
		SetVictory()
		{
			battleState.SetState(State.BattleOver);
			yield return StartCoroutine(victoryScreen.Victory());
			EndBattle();
		}

		public IEnumerator
		SetDefeat()
		{
			battleState.SetState(State.BattleOver);
			yield return StartCoroutine(victoryScreen.Defeat());
			EndBattle();
		}

		void
		EndBattle()
		{
			actionsList.EndBattle();
			turnOrderHistory.ClearTurns();
			actors.EndBattle();
			battleState.EndBattle();
			cameraManager.DeactivateBattleCamera();

			breadcrumbs.text = "";

			canvas.blocksRaycasts = false;
			canvas.alpha = 0f;

			FindObjectOfType<PauseController>().Unpause();
		}

		///////////////////////CANCEL STATES///////////////////////

		public void
		Cancel()
		{
			if (battleState.GetState() == State.SelectingSkill)
			{
				if (battleState.GetSubState() == SubState.SelectingItems)
				{
					CancelItems();
				}
				else if (battleState.GetSubState() != SubState.SelectingTargets)
				{
					avatar = null;
					SelectingCharacterPhase();
				}
				else if (targetSorter.HasNoTargets())
				{
					SetSelectedCharacter(avatar);
					targetSorter.Reset();
				}
				else
				{
					targetSorter.CancelLastTarget();
				}
			}
		}

		////////////////////////////////////////UTILITIES////////////////////////////////////////////

		void
		NextState()
		{
			StartCoroutine(EnterNextPhase());
		}

		IEnumerator
		EnterNextPhase()
		{
			while (actionsAreResolved == false) yield return null;
	
			methodLimit = 0;

			switch (battleState.GetState())
			{
				case State.InitialisingBattle:
					StartCoroutine(InitialiseNewTurnPhase());
					break;

				case State.InitialisingNewTurn:
					SelectingCharacterPhase();
					break;

				case State.SelectingSkill:
					SelectReservesPhase();
					break;

				case State.SelectingReserves:
					ResolveEffectsPhase();
					break;

				case State.ResolvingEffects:
					StartCoroutine(CleanUpPhase());
					break;

				case State.CleaningUp:
					StartCoroutine(InitialiseNewTurnPhase());
					break;
			}
		}

		bool canMove = false;

		void
		Move()
		{
			canMove = true;
		}

		void
		Update()
		{
			if (Input.GetKeyDown(KeyCode.Space)) Move();
		}
	}
}