﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public enum AnimationType3{MeleeAttack=0, RangedAttack=1, CastEnchantment=2, UseItem=3}

public class BasicActionAnimation : Choreograph
{
	bool pointOfContactTriggered = false;

	[SerializeField][HideInInspector] Target target;

	[SerializeField] AnimationType3 animationType;

	public override IEnumerator
	ActualRun()
	{
		pointOfContactTriggered = false;

		StartCoroutine (action.actionData.caster.GetComponent<NewAvatarMovement> ().ActionAnimation (animationType, PointOfContact, EndOfAnimation));

		while (pointOfContactTriggered == false)
		{
			yield return null;
		}
		StartCoroutine(FindObjectOfType<Screenshake> ().Shake (.35f));
	}

	void
	PointOfContact()
	{
		pointOfContactTriggered = true;
	}

	void
	EndOfAnimation()
	{
		allThingsFired = true;
	}
}
}