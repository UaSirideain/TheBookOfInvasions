﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class WalkToAvatar : Choreograph
{
	[SerializeField] TargetList targetList;

	[SerializeField][HideInInspector] Target target;

	public override IEnumerator
	ActualRun()
	{
		target = targetList.GetTarget ();
		yield return StartCoroutine(action.actionData.caster.GetComponent<NewAvatarMovement> ().MoveTo (target.GetComponentInChildren<Animator> ().transform.position));
	}
}
}