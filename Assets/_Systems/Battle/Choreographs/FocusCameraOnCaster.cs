﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cameras;

namespace Battle{

public class FocusCameraOnCaster : Choreograph
{
	public override IEnumerator
	ActualRun()
	{
		FindObjectOfType<BattleCamera> ().LookAt (action.actionData.caster.GetComponentInChildren<Animator>().transform);
		yield return null;
	}
}
}