﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class DisplaySkillNameUI : Choreograph
{
	CurrentActionUI actionUI { get { return FindObjectOfType<CurrentActionUI> (); } }

	WaitForSeconds waitTime = new WaitForSeconds(1);

	public override IEnumerator
	ActualRun()
	{
		StartCoroutine (DisplayUI ());
		yield return null;
	}

	IEnumerator
	DisplayUI()
	{
		actionUI.RenderAction (action.actionData.uiIcon, action.actionData.uiName);
		StartCoroutine(actionUI.FadeIn ());

		yield return waitTime;

		StartCoroutine(actionUI.FadeOut ());
	}
}
}