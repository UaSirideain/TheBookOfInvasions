﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class Choreographer : MonoBehaviour
{
	List<ActionStage> provisionalActions = new List<ActionStage>();

	public List<Action>
	BuildActions()
	{
		Initialise ();

		List<Action> actions = new List<Action> ();

		foreach(ActionStage provisionalAction in provisionalActions)
		{
			actions.Add (provisionalAction.CreateAction ());
		}

		return actions;
	}

	void
	Initialise()
	{
		provisionalActions.Clear ();
		provisionalActions.AddRange(GetComponents<ActionStage> ());

		TargetList[] targetLists = GetComponents<TargetList>();
		Array.ForEach(targetLists, targetList => targetList.RecordTargets());
	}
}
}