﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

enum TargetProfileIndexType{SpecificTarget, AllTargetsInProfile}

public class Choreograph : MonoBehaviour
{
	protected TargetCollector targetCollector { get { return GetComponent<TargetCollector> (); } }
	protected Action action { get { return GetComponent<Action> (); } }

	public virtual List<Change>
	RetrieveChanges()
	{
		return new List<Change> ();
	}

	public virtual void
	TheoreticalRun(){}

	public virtual IEnumerator
	ActualRun()
	{
		yield return null;
	}
		
	protected bool allThingsFired = false;

	public bool
	AllThingsFired()
	{
		return allThingsFired;
	}
}
}