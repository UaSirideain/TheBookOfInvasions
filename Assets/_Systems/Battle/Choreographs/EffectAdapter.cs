﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class EffectAdapter : Choreograph
{
	enum AnimationType2{GetHit=0}

	[SerializeField] TargetList target;
	[SerializeField] Effect effect;
	[SerializeField] AnimationType2 targetAnimation;

	public override void
	TheoreticalRun()
	{
		effect.GeneralRun (target.GetTarget());
	}

	public override IEnumerator
	ActualRun()
	{
		effect.GeneralRun (target.GetTarget());
		GetAnimationForTarget ();
		yield return null;
	}

	public override List<Change>
	RetrieveChanges()
	{
		return effect.RetrieveChanges ();
	}
		
	void
	GetAnimationForTarget()
	{
		if (targetAnimation == AnimationType2.GetHit && target.GetTarget().GetComponent<NewAvatarMovement>())
		{
			target.GetTarget().GetComponent<NewAvatarMovement> ().TakeHit ();
		}
	}
}
}