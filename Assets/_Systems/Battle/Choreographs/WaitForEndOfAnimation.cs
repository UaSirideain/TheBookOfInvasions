﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class WaitForEndOfAnimation : Choreograph 
{
	[SerializeField] Choreograph animationChoreograph;

	public override IEnumerator
	ActualRun()
	{
		while (animationChoreograph.AllThingsFired () == false)
		{
			yield return null;
		}
	}
}
}