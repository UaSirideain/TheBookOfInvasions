﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Battle{

public class ActionStage : MonoBehaviour
{
	//List<Choreograph> choreography = new List<Choreograph> ();
	//List<Effect> effects = new List<Effect> ();
	//List<TargetList> targetLists = new List<TargetList> ();

	[SerializeField] Disposition disposition;
	[SerializeField] RangeType rangeType;
	[SerializeField] ActionType skillType;
	[SerializeField] bool isCounterAttack;

	public Action
	CreateAction()
	{
		int firstMonoIndex;
		int secondMonoIndex;

		CollectMonoIndicesForNewAction (out firstMonoIndex, out secondMonoIndex);

		GameObject action_go = Instantiate(gameObject);

		List<MonoBehaviour> monosToDestroy = new List<MonoBehaviour> ();

		for(int i = 0; i < action_go.GetComponents<MonoBehaviour>().Length; i++)
		{
			if (i.NotBetween(firstMonoIndex, secondMonoIndex))
			{
				monosToDestroy.Add (action_go.GetComponents<MonoBehaviour> () [i]);
			}
		}

		while (monosToDestroy.Count > 0)
		{
			monosToDestroy [0].enabled = false;
			Destroy (monosToDestroy [0]);
			monosToDestroy.RemoveAt (0);
		}
			
		Action action = action_go.AddComponent<Action> ();
		action_go.AddComponent<ChangeList> ();

		ActionDataDefault data = GetComponent<ActionComposite> ().GetActionData();
		data.Initialise (isCounterAttack, skillType, rangeType, disposition);
		action.SetUp (GetComponent<ActionComposite> ().GetActionData ());

		return action;
	}

	void
	CollectMonoIndicesForNewAction(out int firstMonoIndex, out int secondMonoIndex)
	{
		bool collecting = false;
		//choreography.Clear ();
		//effects.Clear ();
		//targetLists.Clear ();

		firstMonoIndex = 0;
		secondMonoIndex = 0;

		MonoBehaviour[] monos = GetComponents<MonoBehaviour> ();
		for (int i = 0; i < monos.Length; i++)
		{
			if (monos[i] == this)
			{
				collecting = true;
				firstMonoIndex = i;
			}
			else if (collecting == true && (monos[i].Is<Choreograph>() || monos[i].Is<Effect>() || monos[i].Is<TargetList>()))//GetType ().IsSubclassOf (typeof(Choreograph)))
			{
				//Choreograph newChor = (Choreograph)monos [i] as Choreograph;
				//choreography.Add (newChor);
				secondMonoIndex = i;
			}
			/*
			else if (collecting == true && monos[i].Is<Effect>())//GetType ().IsSubclassOf (typeof(Effect)))
			{
				//Effect newEffect = (Effect)monos [i] as Effect;
				//effects.Add (newEffect);
				secondMonoIndex = i;
			}
			else if (collecting == true && monos[i].Is<TargetList>())//GetType () == (typeof(TargetList)))
			{
				//TargetList targetList = (TargetList)monos [i] as TargetList;
				//targetLists.Add (targetList);
				secondMonoIndex = i;
			}*/
			else if (collecting == true && monos[i].Is<ActionStage>())//GetType () == this.GetType () && collecting)
			{
				collecting = false;
				break;
			}
		}
	}

		//print (firstMonoIndex);
		//print (secondMonoIndex);
	}
}