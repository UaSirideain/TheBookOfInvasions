﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

[System.Serializable]
public class ConditionTargets : System.Object
{
	enum Test{ AllyOfEnchanted, Enchanted, EnemyOfEnchanted };
	[SerializeField] Test target;

	public List<Avatar>
	GetTargets(Target enchanted)
	{
		List<Avatar> targets = new List<Avatar> ();

		if (target == Test.Enchanted)
		{
			targets.Add(enchanted.GetComponent<Avatar>());
		}
		else if (target == Test.AllyOfEnchanted)
		{
			foreach (Avatar avatar in GameObject.FindObjectOfType<Actors>().GetAvatarsBySpeed())
			{
				if (avatar.GetHost () == enchanted.GetHost())
				{
					targets.Add (avatar);
				}
			}
		}
		else//if(target == Test.EnemyOfEnchanted)
		{
			foreach (Avatar avatar in GameObject.FindObjectOfType<Actors>().GetAvatarsBySpeed())
			{
				if (avatar.GetHost () != enchanted.GetHost())
				{
					targets.Add (avatar);
				}
			}
		}

		return targets;
	}
}

[System.Serializable]
public class ConditionDisposition : System.Object
{
	enum Test{Friendly, Hostile, Neutral}
	[SerializeField] Test disposition;

	public void
	Check(Action action)
	{
		//if(action.GetChangeList().Getchan
	}
}
}