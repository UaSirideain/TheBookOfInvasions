﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public abstract class Condition : MonoBehaviour
{
	protected Target enchanted;
	protected Avatar source;
	protected ActionDataPersistency recordedData = new ActionDataPersistency();

	List<ActionWrapper> previouslyTriggeredActions = new List<ActionWrapper>();

	protected int recordedValue;

	public void
	WriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		bool previouslyTriggered = actionWrapper.CanBeFoundIn (previouslyTriggeredActions);

		if (!previouslyTriggered)
		{
			LocalWriteAndEvaluate (actionWrapper, ref triggered);
			if (triggered)
			{
				previouslyTriggeredActions.Add (actionWrapper);
				recordedData.recordedAction = actionWrapper.action;
			}
		}
	}

	protected virtual void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered){}

	public ActionDataPersistency
	GetRecordedData()
	{
		recordedData.recordedValue = recordedValue;
		recordedData.enchanted = enchanted;
		recordedData.persistency = GetComponent<Persistency> ();
		return recordedData;
	}

	public void
	SetEnchanted (Target newEnchanted)
	{
		enchanted = newEnchanted;
	}

	public void
	SetSource (Avatar newSource)
	{
		source = newSource;
	}

	public virtual void
	OnTurnEnd(){}

	//DEPRECATED

	protected virtual void
	Write (ActionWrapper newInfo){}

	public virtual bool
	Check (){return false;}
}
}