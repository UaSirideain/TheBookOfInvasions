﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_PartyDead : Condition
{
	enum HostType{AllyHostDead, EnemyHostDead, AnyHostDead};
	[SerializeField] HostType triggerWhen;

	protected override void
	Write(ActionWrapper newInfo)
	{
		
	}
		
    public override bool
    Check()
    {
		Actors actors = FindObjectOfType<Actors> ();

		if (actors.GetAllyHost ().CheckDefeatStatus () && (triggerWhen == HostType.AllyHostDead || triggerWhen == HostType.AnyHostDead))
		{
			return true;
		}
		else if (actors.GetEnemyHost ().CheckDefeatStatus () && (triggerWhen == HostType.EnemyHostDead || triggerWhen == HostType.AnyHostDead))
		{
			return true;
		}
		else
		{
			return false;
		}
    }
}
}