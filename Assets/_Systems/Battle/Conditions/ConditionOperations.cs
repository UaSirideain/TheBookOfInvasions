﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public static class ConditionOperations
{
	//HOST CONDITIONS

	public static bool
	HasOnlyBattleFitAvatars(this Host host)
	{
		foreach (Avatar avatar in host.GetAvatars())
		{
			if (avatar.GetComponent<AvatarStats> ().IsBattleFit == false)
			{
				return false;
			}
		}
		return true;
	}

	//AVATAR CONDITIONS
	//STATE CONDITIONS

	public static bool
	IsLastAliveInHost(this Target avatar)
	{
		foreach(Avatar candidate in avatar.GetHost().GetAvatars().Excluding(avatar.GetComponent<Avatar>()))//would ideally hook into a quicker function, sorting by speed is not necessary
		{
			if (candidate.GetComponent<AvatarStats> ().IsBattleFit )
			{
				return false;
			}
		}
		return true;
	}

	public static bool
	IsBattleFit(this Target belligerent)
	{
		return belligerent.GetComponent<AvatarStats> ().IsBattleFit ;
	}

	public static bool
	IsWounded(this Target belligerent)
	{
		return belligerent.GetComponent<AvatarStats> ().IsWounded ();
	}

	public static bool
	IsDead(this Target belligerent)
	{
		return belligerent.GetComponent<AvatarStats> ().IsDead ();
	}

	public static bool
	IsAlive(this Target belligerent)
	{
		return !belligerent.GetComponent<AvatarStats> ().IsDead ();
	}

	public static bool
	HasMaxHealth(this Target belligerent)
	{
		int currentHealth = belligerent.GetComponent<AvatarStats> ().Health;
		int maxHealth = belligerent.GetComponent<AvatarStats> ().MaxHealth;

		return currentHealth == maxHealth;
	}

	//AVATAR CONDITIONS
	//CHANGE CONDITIONS

	public static bool
	WasKilled(this Target belligerent, ActionWrapper info)
	{
		foreach (Change change in info.action.ChangeList.GetChanges())
		{
			if (change.affectedTarget == belligerent && change.type == ChangeType.Killed)
			{
				return true;
			}
		}
		return false;
	}

	public static bool
	WasWounded(this Target belligerent, ActionWrapper info)
	{
		foreach (Change change in info.action.ChangeList.GetChanges())
		{
			if (change.affectedTarget == belligerent && change.type == ChangeType.Wounded)
			{
				return true;
			}
		}
		return false;
	}

	public static bool
	WasAfflictedWithLowerHealth(this Target belligerent, ActionWrapper info)
	{
		int amount = 0;
		return (belligerent.WasAfflictedWithLowerHealth (info, out amount));
//		{
//			return true;
//		}
//		return false;
	}

	public static bool
	WasAfflictedWithLowerHealth(this Target belligerent, ActionWrapper info, out int amount)
	{
		amount = 0;
		foreach (Change change in info.action.ChangeList.GetChanges())
		{
			if (change.affectedTarget == belligerent && change.type == ChangeType.LoweredHealth)
			{
				amount = change.value;
				return true;
			}
		}
		return false;
	}

	public static bool
	WasTargeted(this Target belligerent, ActionWrapper info)
	{
		foreach (Change change in info.action.ChangeList.GetChanges())
		{
			if (change.affectedTarget == belligerent)
			{
				return true;
			}
		}
		return false;
	}

	//ACTION CONDITIONS

	public static bool
	IsActual(this Action action)
	{
		return action.ChangeList.actuality == ChangeActuality.Actual;
	}

	public static bool
	LoweredHealth(this Action action)
	{
		foreach (Change change in action.GetChanges())
		{
			if (change.type == ChangeType.LoweredHealth)
			{
				return true;
			}
		}
		return false;
	}

	public static bool
	IsTheoretical(this Action action)
	{
		return action.ChangeList.actuality == ChangeActuality.Theoretical;
	}

	public static bool
	IsCounterAttack(this Action action)
	{
		return action.IsCounterAttack;
	}

	public static bool
	IsNotCounterAttack(this Action action)
	{
		return action.IsCounterAttack () == false;
	}

	public static bool
	IsFeat(this Action action)
	{
		return action.SkillType == ActionType.Feat;
	}

	public static bool
	IsEnchantment(this Action action)
	{
		return action.SkillType == ActionType.Enchantment;
	}

	public static bool
	IsItem(this Action action)
	{
		return action.SkillType == ActionType.Item;
	}

	public static bool
	IsMelee(this Action action)
	{
		return action.RangeType == RangeType.Melee;
	}

	public static bool
	IsRanged(this Action action)
	{
		return action.RangeType == RangeType.Ranged;
	}

	public static bool
	IsInstantiative(this Action action)
	{
		return action.RangeType == RangeType.Instantiative;
	}

	public static bool
	IsHostile(this Action action)
	{
		return action.Disposition == Disposition.Hostile;
	}

	public static bool
	IsFriendly(this Action action)
	{
		return action.Disposition == Disposition.Friendly;
	}
}
}