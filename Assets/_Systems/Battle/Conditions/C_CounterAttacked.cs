﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_CounterAttacked : Condition
{
	protected override void
	Write(ActionWrapper newInfo)
	{
		if (newInfo.action.IsCounterAttack ())
		{
			foreach (Change change in newInfo.action.ChangeList.GetChanges())
			{
				if (change.affectedTarget == enchanted)
				{
					//print (gameObject.name + " was triggered by " + newInfo.action);
					recordedData.recordedAction = newInfo.action;
					attackSuccessful = true;
				}
			}
		}
	}
		
	bool attackSuccessful = false;

	public override bool
	Check()
	{
		bool triggered = attackSuccessful;

		attackSuccessful = false;

		return triggered;
	}
}
}