﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public abstract class ConditionUnit : MonoBehaviour
{
	protected ActionWrapper info;

	protected int valueToWrite = 0;

	public abstract void
	Write (ActionWrapper newInfo);

	public virtual bool
	Check()
	{
		return info != null;
	}
		
	public virtual void
	ResetInformation()
	{
		info = null;
	}

	public int
	GetRecordedValue()
	{
		return valueToWrite;
	}
}

public class ConditionUnitInfo
{
	public Avatar actionCaster;
	public Change change;
}
}