﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class CU_CasterIs : ConditionUnit
{
	enum CasterType {Enchanted, PersistencyCaster, Ally, Enemy}
	[SerializeField] CasterType casterType;

	public override void
	Write(ActionWrapper newInfo)
	{
		if (GetCasterProfile(newInfo.action.actionData.caster))
		{
			info = newInfo;
		}
	}

	bool
	GetCasterProfile(Avatar caster)
	{
		Avatar enchanted = null;

		if (GetComponent<Persistency> ().GetEnchanted ().GetComponent<Avatar> ())
		{
			enchanted = GetComponent<Persistency> ().GetEnchanted ().GetComponent<Avatar> ();
		}

		if (casterType == CasterType.Enchanted && enchanted != null)
		{
			return caster == enchanted;
		}
		else if (casterType == CasterType.PersistencyCaster)
		{
			return caster == GetComponent<Persistency>().GetPersistencyCaster();
		}
		else if (casterType == CasterType.Ally && enchanted != null)
		{
			return enchanted.GetHost ().GetAvatars ().Contains (caster);
		}
		else//if(casterType == CasterType.Enemy)
		{
			return enchanted.GetEnemyHost ().GetAvatars ().Contains (caster);
		}
	}
}
}