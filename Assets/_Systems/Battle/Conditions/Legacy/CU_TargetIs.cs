﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class CU_TargetIs : ConditionUnit
{
	enum CasterType {Enchanted, PersistencyCaster, Ally, Enemy}
	[SerializeField] CasterType casterType;

	public override void
	Write(ActionWrapper newInfo)
	{
		foreach (Change change in newInfo.action.GetComponent<ChangeList>().GetChanges())
		{
			if (GetTargetProfile (change.affectedTarget))
			{
				info = newInfo;
			}
		}
	}

	bool
	GetTargetProfile(Target target)
	{
		Target enchanted = GetComponent<Persistency> ().GetEnchanted ();

		if (casterType == CasterType.Enchanted)
		{
			return target == enchanted;
		}
		else if (casterType == CasterType.PersistencyCaster)
		{
			return target == GetComponent<Persistency> ().GetPersistencyCaster ();
		}
		else if (casterType == CasterType.Ally && enchanted.GetComponent<Avatar> ())
		{
			return enchanted.GetHost ().GetAvatars ().Contains (target as Avatar);
		}
		else if (/*casterType == CasterType.Enemy &&*/enchanted.GetComponent<Avatar> ())
		{
			return enchanted.GetEnemyHost ().GetAvatars ().Contains (target as Avatar);
		}
		else
		{
			return false;
		}
	}
}
}