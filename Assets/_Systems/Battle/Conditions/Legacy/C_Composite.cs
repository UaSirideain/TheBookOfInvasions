﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_Composite : Condition
{
	//we might have to have these set on a per ConditionUnit basis
	[SerializeField] bool resetInfoOnTurnEnd;
	[SerializeField] bool resetInfoAfterCheck;
	[SerializeField] bool resetInfoAfterTrigger;

	protected override void
	Write(ActionWrapper newInfo)
	{
		foreach (Change change in newInfo.action.ChangeList.GetChanges())
		{
			ConditionUnitInfo cInfo = new ConditionUnitInfo ();
			cInfo.actionCaster = newInfo.action.actionData.caster;
			cInfo.change = change;

			foreach (ConditionUnit condition in GetComponents<ConditionUnit>())
			{
				condition.Write (newInfo);
			}
		}
	}
		
	public override bool
	Check()
	{
		bool triggered = true;

		foreach (ConditionUnit condition in GetComponents<ConditionUnit>())
		{
			if (condition.Check () == false)
			{
				triggered = false;
			}

			if (resetInfoAfterCheck)
			{
				condition.ResetInformation ();
			}
			if (resetInfoAfterTrigger == true && triggered == true)
			{
				condition.ResetInformation ();
			}
		}

		return triggered;
	}
		
	void
	Awake()
	{
		BattleLogic.manager.backendNotifications.EEndOfTurnCleanUp += EndOfTurn;
	}

	void
	EndOfTurn()
	{
		if (resetInfoOnTurnEnd)
		{
			foreach (ConditionUnit condition in GetComponents<ConditionUnit>())
			{
				condition.ResetInformation ();
			}
		}
		BattleLogic.manager.backendNotifications.processingEndOfTurnCleanUp--;
	}

	public Target
	GetEnchanted()
	{
		return enchanted;
	}
}
}