﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class CU_ChangeTypeIs : ConditionUnit
{
	[SerializeField] ChangeType desiredType;

	public override void
	Write(ActionWrapper newInfo)
	{
		if (newInfo.action.ChangeList.CheckForType (desiredType))
		{
			info = newInfo;
			valueToWrite = newInfo.action.ChangeList.GetValueForType (desiredType);
		}
	}
}
}