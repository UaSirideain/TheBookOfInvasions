﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class CU_Actual : ConditionUnit
{
	public override void
	Write(ActionWrapper newInfo)
	{
		if (newInfo.action.ChangeList.actuality == ChangeActuality.Actual)
		{
			info = newInfo;
		}
	}
}
}