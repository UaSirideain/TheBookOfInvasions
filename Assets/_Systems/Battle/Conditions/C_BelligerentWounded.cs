﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_BelligerentWounded : Condition
{
	[SerializeField] ConditionTargets targets;

	protected override void
	Write(ActionWrapper newInfo)
	{
		
	}

    public override bool
    Check()
    {
		foreach (Avatar avatar in targets.GetTargets(enchanted))
		{
			if (avatar.GetComponent<AvatarStats>().IsWounded())
			{
				return true;
			}
		}
		return false;
    }
}
}