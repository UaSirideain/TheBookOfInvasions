﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_BelligerentTargeted : Condition
{
	[SerializeField] ConditionTargets targets;
	[SerializeField] ConditionDisposition disposition;

	protected override void
	Write(ActionWrapper newInfo)
	{
		action = newInfo.action;
	}

	Action action;

    public override bool
    Check()
    {
		foreach (Avatar avatar in targets.GetTargets(enchanted))
		{
			foreach (Change change in action.ChangeList.GetChanges())
			{
				if (change.affectedTarget == enchanted)
				{
					return true;
				}
			}
		}
		return false;
    }
}
}