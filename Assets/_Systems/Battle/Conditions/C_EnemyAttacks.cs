﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_EnemyAttacks : Condition
{
	protected override void
	Write(ActionWrapper newInfo)
	{
		foreach (Change change in newInfo.action.ChangeList.GetChanges())
		{
			if (change.type == ChangeType.LoweredHealth && change.actuality == ChangeActuality.Actual)//&& enchanted.GetHost() == newInfo.action.GetCaster().GetHost())
			{
				attackSuccessful = true;
			}
		}
	}

	bool attackSuccessful = false;

	public override bool
	Check()
	{
		bool triggered = attackSuccessful;

		attackSuccessful = false;

		return triggered;
	}
}
}