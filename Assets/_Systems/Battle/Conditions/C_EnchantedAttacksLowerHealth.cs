﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_EnchantedAttacksLowerHealth : Condition
{
	bool triggered = false;

	protected override void
	Write(ActionWrapper newInfo)
	{
		if (newInfo.action.ChangeList.CheckForType (ChangeType.LoweredHealth))
		{
			triggered = true;
		}
	}

	public override bool
	Check()
	{
		bool returnValue = triggered;
		triggered = false;
		return returnValue;
	}
}
}