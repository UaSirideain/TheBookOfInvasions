﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_Aon_Careful : Condition
{
	protected override void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		triggered =  
			(
				actionWrapper.action.IsTheoretical() &&
				actionWrapper.action.IsFeat() &&
				enchanted.WasAfflictedWithLowerHealth(actionWrapper) &&
				Percentage.Get(50)
			);
	}
}
}