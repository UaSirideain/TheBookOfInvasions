﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_BelligerentAttacked : Condition
{
	protected override void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		triggered =  
			(
				enchanted.WasAfflictedWithLowerHealth (actionWrapper, out recordedValue) && 
				actionWrapper.action.IsActual () && 
				actionWrapper.action.IsNotCounterAttack ()
			);
	}
}
}