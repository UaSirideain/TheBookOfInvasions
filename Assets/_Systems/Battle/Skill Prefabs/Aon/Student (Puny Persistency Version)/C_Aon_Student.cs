﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_Aon_Student : Condition
{
	protected override void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		if (actionWrapper.action.IsActual () &&
			actionWrapper.action.LoweredHealth () &&
			actionWrapper.action.actionData.caster != (Avatar)enchanted)
		{
			GetComponent<PersistencyValueModifier> ().SetModifierAdditionValue (1);
		}
	}

	public override void
	OnTurnEnd()
	{
		if (BattleLogic.manager.turnOrderHistory.IsLastTurnInCycle ())
		{
			GetComponent<PersistencyValueModifier> ().SetModifierAdditionValue (0);
		}
	}
}
}