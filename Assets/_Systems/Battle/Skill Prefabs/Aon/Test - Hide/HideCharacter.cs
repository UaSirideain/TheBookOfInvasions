﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class HideCharacter : Choreograph
{
	[SerializeField] TargetList targetList;

	[SerializeField][HideInInspector] Target target;

	public override IEnumerator
	ActualRun()
	{
		target = targetList.GetTarget ();

		foreach (SkinnedMeshRenderer mesh in target.GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			mesh.enabled = false;
		}
		yield return null;
	}
}
}