﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_Aon_Docile : Condition
{
	protected override void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		triggered =
			(
				actionWrapper.action.IsCounterAttack() &&
				actionWrapper.action.IsTheoretical() &&
				actionWrapper.action.actionData.caster == enchanted
			);
	}
}
}