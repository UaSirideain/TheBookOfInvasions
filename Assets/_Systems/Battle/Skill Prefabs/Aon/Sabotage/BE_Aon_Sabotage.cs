﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BE_Aon_Sabotage : Effect
{
	protected override void
	Run(Target target)
	{
		TurnObject turn = BattleLogic.manager.turnOrderHistory.GetNextTurnFor (target.GetComponent<Avatar>());

		if (turn != null)
		{
			turn.CacheMainAction (BattleLogic.manager.passTurnPrefab);
		}
		else
		{
			print ("Target has no preassigned turns");
		}
	}
}
}