﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_TestReserve : Condition
{
	bool trigged = false;

	protected override void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		if (trigged == false)
		{
			trigged = true;
			triggered = (actionWrapper.action.actionData.caster == enchanted);
		}
	}
}
}