﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class RemoveCharacter : Choreograph
{
	[SerializeField][HideInInspector] Target target;

	[SerializeField] TargetList targetList;

	public override IEnumerator
	ActualRun()
	{
		target = targetList.GetTarget ();

		if (GetComponent<ChangeList> ().actuality == ChangeActuality.Actual)
		{
			target.GetHost ().RemoveCharacter (target.GetComponent<Avatar> ());
		}
		yield return null;
	}
}
}