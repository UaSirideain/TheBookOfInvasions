﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class C_Aon_Shnaky : Condition
{
	protected override void
	LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
	{
		triggered =
			(
				actionWrapper.action.IsCounterAttack() &&
				enchanted.WasTargeted(actionWrapper) &&
				actionWrapper.action.IsTheoretical()
			);
	}
}
}