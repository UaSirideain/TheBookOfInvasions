﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BE_PhoenixDown : Effect
{
	protected override void
	Run(Target target)
	{
		if (target.IsDead ())
		{
			target.GetComponent<AvatarStats> ().HalfRevive ();
		}
	}
}
}