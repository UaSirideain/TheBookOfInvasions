﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class BattleSkillUseItem : MonoBehaviour
{
	public void
	BuildItemList(ItemSkillVoid func)
	{
		FindObjectOfType<BattleInventoryUI> ().BuildColumns (func);
	}
}
}