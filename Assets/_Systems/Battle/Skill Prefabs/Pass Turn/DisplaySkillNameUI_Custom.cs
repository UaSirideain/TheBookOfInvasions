﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class DisplaySkillNameUI_Custom : Choreograph
{
	CurrentActionUI actionUI { get { return FindObjectOfType<CurrentActionUI> (); } }

	WaitForSeconds waitTime = new WaitForSeconds(1);

	[SerializeField] string message;

	public override IEnumerator
	ActualRun()
	{
		StartCoroutine (DisplayUI ());
		yield return null;
	}

	IEnumerator
	DisplayUI()
	{
		actionUI.RenderAction (null, message);
		StartCoroutine(actionUI.FadeIn ());

		yield return waitTime;

		StartCoroutine(actionUI.FadeOut ());
		allThingsFired = true;
	}
}
}