﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class FaceAvatar : Choreograph
{
	[SerializeField] TargetList targetList;

	[SerializeField][HideInInspector] Target target;

	public override IEnumerator
	ActualRun()
	{
		target = targetList.GetTarget ();
		yield return StartCoroutine(action.actionData.caster.GetComponent<NewAvatarMovement> ().LookAt (target.GetComponentInChildren<Animator> ().transform.position));
	}
}
}