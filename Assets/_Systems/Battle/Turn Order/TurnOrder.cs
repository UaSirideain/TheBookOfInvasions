﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
	public class TurnOrder : MonoBehaviour
	{
		Actors actors { get { return FindObjectOfType<Actors>(); } }

		const bool AVATARS_CAN_LOSE_TURNS = false;

		public List<TurnObject>
		GenerateTurnCycle()
		{
			return GenerateCycle();
		}

		List<TurnObject>
		GenerateCycle()
		{
			List<TurnObject> newTurns = new List<TurnObject>();
			List<Avatar> firstTurns = new List<Avatar>();
			List<Avatar> extraTurns = new List<Avatar>();

			foreach (Avatar avatar in actors.GetAvatarsBySpeed())
			{
				int turnsWon = TurnsWonFor(avatar);

				if (AVATARS_CAN_LOSE_TURNS == false || turnsWon > 0)
				{
					firstTurns.Add(avatar);
				}
				if (turnsWon > 1)
				{
					extraTurns.Add(avatar);
				}
			}

			firstTurns.AddRange(extraTurns);

			bool cycleStart = true;

			foreach (Avatar avatar in firstTurns)
			{
				newTurns.Add(GenerateTurn(cycleStart, avatar.GetHost(), avatar));
				cycleStart = false;
			}

			return newTurns;
		}

		int
		TurnsWonFor(Avatar avatar)
		{
			int diceRoll = Random.Range(0, 101);

			if (diceRoll < avatar.GetStats().Speed.lower)
			{
				return 0;
			}
			else if (diceRoll < avatar.GetStats().Speed.upper)
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}

		TurnObject
		GenerateTurn(bool cycleStart, Host host, Avatar turnWonBy)
		{
			TurnObject newTurn;

			if (FindObjectOfType<Actors>().GetAllyHost() == host)
			{
				newTurn = new TurnObject(cycleStart, host, null);
				newTurn.SetWinner(turnWonBy);
			}
			else
			{
				newTurn = new TurnObject(cycleStart, host, turnWonBy);
				newTurn.SetWinner(turnWonBy);
			}

			return newTurn;
		}
	}
}