﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class TurnOrderUI : MonoBehaviour
{
	[SerializeField] Image turnMarker;

	[SerializeField] GameObject turnIconPrefab;

	List<TurnIcon> turnIcons = new List<TurnIcon> ();

	Color startColour = Color.clear;
	Color endColour = Color.white;
	float moveDuration = 0.7f;
	float fadeDuration = 0.7f;

	public IEnumerator
	RenderTurnUI()
	{
        float lerpTime = 0;
		float startPosition = -GetComponent<RectTransform>().sizeDelta.y;
		float endPosition = 0;
		while (lerpTime < 1)
		{
			lerpTime += Time.deltaTime / fadeDuration;
			GetComponent<RectTransform> ().anchoredPosition = new Vector3(-1, Mathf.Lerp (startPosition, endPosition, lerpTime), 0);
			yield return null;
		}

		StartCoroutine(RenderTurnMarker ());
	}

	IEnumerator
	RenderTurnMarker()
	{
		turnMarker.color = startColour;
		turnMarker.gameObject.SetActive (true);

		float lerpTime = 0;
		while (lerpTime < 1)
		{
			lerpTime += Time.deltaTime / fadeDuration;
			turnMarker.color = Color.Lerp (startColour, endColour, lerpTime);
			yield return null;
		}
	}
		
	public IEnumerator
	RenderNextTurn(int currentTurn)
	{
		float lerpTime = 0f;
		float startPosition = GetComponent<RectTransform>().anchoredPosition.x;
		float endPosition = GetComponent<RectTransform>().anchoredPosition.x - 77;

		while (lerpTime < 1.2f)
		{
			lerpTime += Time.deltaTime / moveDuration;
			GetComponent<RectTransform> ().anchoredPosition = new Vector3(Mathf.Lerp (startPosition, endPosition, lerpTime), 0, 0);
			yield return null;
		}

		turnIcons [currentTurn].SetAsActiveTurn ();
	}
		
	public void
	AddNewTurn(TurnObject newTurn)
	{
		turnIcons.Add (CreateTurnIcon (newTurn));

		if (turnIcons.Count == 1)
		{
			turnIcons [0].SetAsActiveTurn ();
		}
	}
		
	TurnIcon
	CreateTurnIcon(TurnObject newTurn)
	{
		GameObject newTurnIcon = Instantiate (turnIconPrefab);
		newTurnIcon.transform.SetParent (transform);
		newTurnIcon.name = "Turn Icon " + (newTurn.index + 1).ToString ();
		newTurnIcon.GetComponent<TurnIcon> ().RenderToUI ((newTurn.index + 1), newTurn.host, newTurn.cycleStart, newTurn.preChosenAvatar);

		return newTurnIcon.GetComponent <TurnIcon>();
	}

	public void
	UndoRender()
	{
		foreach (Transform turnIcon in transform)
		{
			Destroy (turnIcon.gameObject);
		}

		turnMarker.gameObject.SetActive (false);
		turnIcons.Clear ();
	}

	public void
	WipeTurns()
	{
		while (turnIcons.Count > 0)
		{
			Destroy (turnIcons [0].gameObject);
			turnIcons.RemoveAt (0);
		}

		turnIcons.Clear ();
	}
}
}