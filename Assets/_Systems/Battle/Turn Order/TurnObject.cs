﻿using System.Collections.Generic;

namespace Battle
{
[System.Serializable]
public class TurnObject : System.Object
{
	public Host host;
	public Avatar preChosenAvatar;

	public int cycle;
	public bool cycleStart;
	public int index;
	public Avatar turnWonBy;

	ActionComposite newCachedMainAction = null;
	List<ActionComposite> newCachedActions = new List<ActionComposite>();

	public TurnObject(bool isCycleStart, Host newHost, Avatar newPreChosenAvatar)
	{
		host = newHost;
		cycleStart = isCycleStart;
		preChosenAvatar = newPreChosenAvatar;
	}

	public void
	SetWinner(Avatar winner)
	{
		turnWonBy = winner;
	}

	public void
	CacheMainAction(ActionComposite newAction)
	{
		newCachedMainAction = newAction;
	}

	public void
	CacheActions(List<ActionComposite> newActions)
	{
		newCachedActions.AddRange (newActions);
	}

	public ActionComposite
	GetCachedMainAction()
	{
		return newCachedMainAction;
	}

	public List<ActionComposite>
	GetCachedActions()
	{
		return newCachedActions;
	}
}
}