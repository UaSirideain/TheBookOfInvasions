﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
	public enum Interval { StartOfTurn, EndOfTurn, StartOfCycle, EndOfCycle, None }

	public class TurnOrderHistory : MonoBehaviour
	{
		[SerializeField] List<TurnObject> turns = new List<TurnObject>();
		int currentTurn = 0;
		bool firstTurn = true;

		const int AMOUNT_TURNS_ACROSS = 21;

		int infiniteLoopBreaker = 0;

		public List<TurnObject> Turns => turns;

		TurnOrderUI ui { get { return GetComponent<TurnOrderUI>(); } }
		TurnOrder ord { get { return GetComponent<TurnOrder>(); } }

		public int 
		GetCurrentTurn() => currentTurn;

		public bool 
		IsCurrentTurnLastInCycle() => turns[currentTurn].cycle < turns[currentTurn + 1].cycle;

		public bool 
		IsCurrentTurnFirstInCycle() => turns[currentTurn].cycleStart;

		public Host 
		GetActiveHost() => turns[currentTurn].host;

		public bool 
		HasEnoughTurns() => turns.Count > currentTurn + AMOUNT_TURNS_ACROSS;

		public bool	
		IsLastTurnInCycle() => turns[GetCurrentTurn() + 1].cycleStart;

		public ActionComposite 
		RetrieveMainCachedAction() => turns[currentTurn].GetCachedMainAction();

		public List<ActionComposite> 
		RetrieveCachedActions() => turns[currentTurn].GetCachedActions();

		public void
		CacheMainAction(ActionComposite action, int turnToAffect)
		{
			turns[turnToAffect].CacheMainAction(action);
		}

		public void
		CacheAdditionalActions(List<ActionComposite> actions, int turnToAffect)
		{
			turns[turnToAffect].CacheActions(actions);
		}

		public int
		GetLastTurn()
		{
			if (turns.Count == 0)
			{
				return -1;
			}
			return turns[turns.Count - 1].index;
		}

		public void
		RemoveAllTurnsFor(Avatar avatar)
		{
			int i = currentTurn + 1;
			while (i < turns.Count)
			{
				if (turns[i].turnWonBy == avatar)
				{
					turns.RemoveAt(i);
				}
				else
				{
					i++;
				}
			}

			RedoTurns();

			//		List<TurnObject> newT = new List<TurnObject> ();
			//		newT.AddRange (turns);
			//		turns.Clear ();
			//		AddTurnCycle (newT);

			ui.RenderTurnUI();
		}

		public int
		GetCurrentTurnCycle()
		{
			if (turns.Count == 0)
			{
				return 0;
			}
			else
			{
				return turns[currentTurn].cycle;
			}
		}

		public void
		ClearTurns()
		{
			turns.Clear();
			currentTurn = 0;
			ui.UndoRender();
		}

		public Host
		GetNonActiveHost()
		{
			foreach (TurnObject turn in turns)
			{
				if (turn.host != GetActiveHost())
				{
					return turn.host;
				}
			}
			return null;
		}

		public TurnObject
		GetNextTurnFor(Avatar avatar)
		{
			for (int i = currentTurn; i < turns.Count; i++)
			{
				if (turns[i].preChosenAvatar == avatar)
				{
					return turns[i];
				}
			}
			return null;
		}

		public void
		InitialiseBattle()
		{
			firstTurn = true;
			PopCycles();
			StartCoroutine(ui.RenderTurnUI());
		}

		public void
		InitialiseNewTurn()
		{
			PopCycles();

			NextTurn();
		}

		void
		PopCycles()
		{
			while (!HasEnoughTurns() && infiniteLoopBreaker < 9) //should be able to get rid of this
			{
				AddTurnCycle(ord.GenerateTurnCycle());
				infiniteLoopBreaker++;
			}
		}

		void
		RedoTurns()
		{
			int lastCycle = turns[currentTurn].cycle;

			ui.WipeTurns();

			for (int turnIndex = turns[currentTurn].index; turnIndex < turns.Count; turnIndex++)
			{
				turns[turnIndex].index = turnIndex;
				turns[turnIndex].cycleStart = (turns[turnIndex].cycle > lastCycle);

				ui.AddNewTurn(turns[turnIndex]);

				lastCycle = (int)turns[turnIndex].cycle;
			}
		}

		void
		NextTurn()
		{
			if (firstTurn == true)
			{
				firstTurn = false;
			}
			else
			{
				currentTurn++;
				StartCoroutine(ui.RenderNextTurn(currentTurn));
			}
		}

		void
		AddTurnCycle(List<TurnObject> newTurns)
		{
			int turnIndex = 0;
			int cycleIndex = 0;

			if (turns.Count > 0)
			{
				turnIndex = turns.GetLast().index + 1;
				cycleIndex = turns.GetLast().cycle + 1;
			}

			bool cycleStart = true;

			foreach (TurnObject turn in newTurns)
			{
				turn.With(x => 
				{
					x.index = turnIndex;
					x.cycle = cycleIndex;
					x.cycleStart = cycleStart;
				});
				ui.AddNewTurn(turn);
				turnIndex++;
				cycleStart = false;
			}

			turns.AddRange(newTurns);
		}

		//	public int
		//	GetLastTurnCycle()
		//	{
		//		if (turns.Count == 0)
		//		{
		//			return -1;
		//		}
		//		return turns [turns.Count - 1].cycle;
		//	}
	}
}