﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Battle{

public class TurnIcon : MonoBehaviour
{
	[SerializeField] Text textField;
	[SerializeField] GameObject cycleMarker;
	[SerializeField] Image backgroundField;
	[SerializeField] Image characterPortrait;

	[SerializeField] Color activeTurnColour;

	public void
	SetAsActiveTurn ()
	{
		textField.color = activeTurnColour;
	}

	public void
	RenderToUI(int turn, Host host, bool newCycle, Avatar preChosenAvatar)
	{
		GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
		GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0 + (GetComponent<RectTransform> ().sizeDelta.x * (turn - 1)), 0, 0);
		textField.text = turn.ToString();
		backgroundField.sprite = host.GetTurnIconSprite();//TODO: this should be gotten from TurnObject in some way instead of BattleHost
		cycleMarker.SetActive (newCycle);

		if (preChosenAvatar != null)
		{
			characterPortrait.sprite = preChosenAvatar.GetTurnPortrait ();
			characterPortrait.enabled = (characterPortrait.sprite != null);
		}
	}

	public void
	RenderTurnCycleIcon()
	{
		cycleMarker.SetActive (true);
	}
}
}