﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class ReserveTimerUI : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

	[SerializeField] Text timer;
	[SerializeField] GameObject pauseText;
	[SerializeField] GameObject playText;
	[SerializeField] List<MaskableGraphic> fadeOutGraphics;

	const float duration = .3f;
	Vector2 startPosition = new Vector2(189.5f, 0f);
	Vector2 endPosition = new Vector2 (189.5f, -31f);

	public IEnumerator
	SlideDown()
	{
		startPosition = GetComponent<RectTransform> ().anchoredPosition;
		endPosition = new Vector2 (startPosition.x, startPosition.y - 31f);
		canvas.Enable ();

		float lerpTime = 0f;

		while (lerpTime < 1.2f)
		{
			rt.anchoredPosition = Vector2.Lerp (startPosition, endPosition, lerpTime);
			lerpTime += Time.deltaTime / duration;
			yield return null;
		}
	}
		
	public void
	FadeOut()
	{
		foreach (MaskableGraphic graphic in fadeOutGraphics)
		{
			StartCoroutine (FadeToTransparent (graphic));
		}
	}

	public IEnumerator
	FadeToTransparent(MaskableGraphic graphic)
	{
		float lerpTime = 0f;

		while (lerpTime < 1.2f)
		{
			canvas.alpha = Mathf.Lerp(1f, 0f, lerpTime);
			lerpTime += Time.deltaTime / duration;
			yield return null;
		}

		canvas.Disable ();
	}

	public void
	RenderSecondsLeft(float secondsLeft)
	{
		timer.text = Mathf.CeilToInt (secondsLeft).ToString();
	}

	public void
	RenderPromptText(bool paused)
	{
		playText.SetActive (paused);
		pauseText.SetActive (!paused);
	}
}
}