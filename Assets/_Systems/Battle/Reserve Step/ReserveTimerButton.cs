﻿using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class ReserveTimerButton : MonoBehaviour
{
	Image image { get { return GetComponent<Image> (); } }

	Color opaque { get { return new Color (1, 1, 1, 1); } }
	Color transparent { get { return new Color (1, 1, 1, 0); } }

	public void
	HoverOn()
	{
		image.color = opaque;
	}

	public void
	HoverOff()
	{
		image.color = transparent;
	}
}
}