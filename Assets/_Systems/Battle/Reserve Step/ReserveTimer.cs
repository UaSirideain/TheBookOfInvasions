﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class ReserveTimer : MonoBehaviour
{
	[SerializeField] float secondsLeft = 3f;
	[SerializeField] ReserveTimerUI ui;
	CurrentActionUI actionUI { get { return FindObjectOfType<CurrentActionUI> (); } }
		
	bool paused = false;

	public IEnumerator
	StartTimer()
	{
		actionUI.RenderAction (null, "Main Skill");
		yield return StartCoroutine (actionUI.FadeIn ());
		PlaceTimer ();
		paused = false;
		ui.RenderPromptText (paused);

		secondsLeft = 2.5f;
		ui.RenderSecondsLeft (secondsLeft);
		yield return ui.SlideDown ();
		yield return CountDown ();
		ui.FadeOut ();
	}

	public void
	PausePlayTimer()
	{
		paused = !paused;
		secondsLeft = (float)Mathf.CeilToInt (secondsLeft);
		ui.RenderPromptText (paused);
	}

	void
	PlaceTimer()
	{
		RectTransform rect = actionUI.GetComponent<RectTransform> ();
		Vector2 lowerRight = new Vector2 (rect.anchoredPosition.x + (rect.sizeDelta.x * .5f), rect.anchoredPosition.y);
		GetComponent<RectTransform> ().anchoredPosition = lowerRight;
	}
		

	IEnumerator
	CountDown()
	{
		while(secondsLeft > 0)
		{
			if (paused == false)
			{
				secondsLeft -= Time.deltaTime;
				ui.RenderSecondsLeft (secondsLeft);
			}
			yield return null;
		}

		StartCoroutine(EndTimer ());
	}

	IEnumerator
	EndTimer()
	{
		yield return StartCoroutine (actionUI.FadeOut ());
		BattleLogic.manager.EndReserveSelection ();
	}
}
}