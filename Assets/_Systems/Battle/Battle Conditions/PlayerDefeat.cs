﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{

	public class PlayerDefeat : Choreograph
	{
		public override IEnumerator
		ActualRun()
		{
			StartCoroutine(BattleLogic.manager.SetDefeat());
			yield return null;
		}

		public override List<Change>
		RetrieveChanges()
		{
			//make a change with victory in it
			return new List<Change>();
		}
	}
}