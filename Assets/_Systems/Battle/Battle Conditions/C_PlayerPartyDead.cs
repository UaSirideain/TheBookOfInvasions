﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
	public class C_PlayerPartyDead : Condition
	{
		bool alreadyFired = false;

		protected override void
		LocalWriteAndEvaluate(ActionWrapper actionWrapper, ref bool triggered)
		{
			if (alreadyFired == false)
			{
				triggered = true;

				foreach (Avatar avatar in FindObjectOfType<Actors>().GetAllyHost().GetAvatars())
				{
					if (avatar.IsBattleFit())
					{
						triggered = false;
					}
				}

				if (triggered)
				{
					alreadyFired = true;
				}
			}
		}
	}
}