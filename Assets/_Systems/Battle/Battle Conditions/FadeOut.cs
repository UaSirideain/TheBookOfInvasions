﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cameras;

namespace Battle{

public class FadeOut : Choreograph
{
	public override IEnumerator
	ActualRun()
	{
		StartCoroutine (Fade ());
		yield return null;
	}

	IEnumerator
	Fade()
	{
		yield return FindObjectOfType<BattleCamera> ().FadeOut ();
		allThingsFired = true;
	}
}
}