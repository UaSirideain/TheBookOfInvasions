﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
	public class PlayerVictoryPoses : Choreograph
	{
		public override IEnumerator
		ActualRun()
		{
			BattleLogic.manager.cameraDirector.Victory();
			yield return new WaitForSeconds(1f);
		}
	}
}