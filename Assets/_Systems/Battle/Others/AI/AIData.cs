﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

[CreateAssetMenu(fileName = "new AIData", menuName = "Battle System/AIData", order = 1)]
public class AIData : ScriptableObject
{
	[SerializeField] public List<AIBehaviour> behaviours;

	void
	OnValidate()
	{
		foreach (AIBehaviour behaviour in behaviours)
		{
			behaviour.Initialise ();
		}
	}
}
}