﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

[System.Serializable]
public class AISkillRoutine : System.Object
{
	public Avatar caster;
	public BattleSkill skill;
	public AIBehaviour behaviour;
	public bool passTurn = false;
}
}