﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public class AIBattleAvatar : MonoBehaviour
{
	[SerializeField] public int turnsTaken;
	[SerializeField] public List<AIBehaviour> behaviours;

	[SerializeField] private GameObject skillCardPrefab;
	[SerializeField] BattleSkill skillPrefab;


	public void
	BuildSkills()
	{
		foreach(AIBehaviour behaviour in behaviours)
		{
			BattleSkill newSkill = Instantiate (behaviour.skill);
			newSkill.transform.SetParent (transform);
			behaviour.skill = newSkill;
		}
	}


	public AISkillRoutine
	GetPotentialSkill()
	{
		AISkillRoutine newRoutine = new AISkillRoutine ();
		AIBehaviour triggeredBehaviour = new AIBehaviour ();

		foreach (AIBehaviour behaviour in behaviours)
		{
			if (behaviour.CheckCondition ())
			{
				triggeredBehaviour = behaviour;
				break;
			}
		}

		if (triggeredBehaviour == null)
		{
			newRoutine.passTurn = true;
			return newRoutine;
		}

		newRoutine.caster = GetComponent<Avatar> ();
		newRoutine.skill = triggeredBehaviour.skill;
		newRoutine.behaviour = triggeredBehaviour;

		//[flag] We should be making the imprints in advance since that will dictate how suitable the skill actually is
			
		return newRoutine;
	}
}
}