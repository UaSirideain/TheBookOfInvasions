﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public enum OldTargetAlignment{Any, Enemy, Ally}
public enum AICondition{Default, IfWounded, IfManaBelow}
public enum AIResponse{Nothing, Attack, Heal, PassTurn}
public enum AITarget{None, Random, LowestAttack, LowestHealth, HighestAttack, HighestHealth, HealthBelow, HealthAbove, Wounded, ManaBelow, ManaAbove}
public enum TargetPreference{Random, LowestHealth, HighestHealth, LowestMana, HighestMana}

[System.Serializable]
public class AIBehaviour : System.Object
{
	public string name;

	public AICondition condition;
	public int conditionValue;
	public Avatar battleAvatar;
	public BattleSkill skill;
	public List<AITargetProfile> aiTargetProfiles;


	public void
	Initialise()
	{
//		int thingAmount = 0;
//		foreach (BattleAction action in skill.GetComponents<BattleAction>())
//		{
//			foreach (TargetProfile tp in action.targetProfiles)
//			{
//				if (tp.autoTarget == AutoTarget.ChosenByAI)
//				{
//					thingAmount++;
//				}
//			}
//		}
//		if (aiTargetProfiles.Count != thingAmount)
//		{
//			aiTargetProfiles.Clear ();
//			for (int i = 0; i < thingAmount; i++)
//			{
//				aiTargetProfiles.Add (new AITargetProfile ());
//			}
//		}
	}


	public bool
	CheckCondition()
	{
		if (battleAvatar.GetStats().Mana > 0)
		{
			if (condition == AICondition.IfManaBelow
				&& battleAvatar.GetStats().Mana < conditionValue)
			{
				return true;
			}
			/*if (condition == AICondition.IfWounded
				&& battleAvatar.GetStats().SetWounded ())
			{
				return true;
			}*/
			if (condition == AICondition.Default)
			{
				return true;
			}
		}
		return false;
	}


	public Target
	CalculateTarget(int targetProfileIndex)
	{
		List<Target> potentialTargets = GameObject.FindObjectsOfType<Target> ().ToList();

		potentialTargets = RemoveByTargetType (potentialTargets, targetProfileIndex);
		potentialTargets = RemoveByAlignment (potentialTargets, targetProfileIndex);
		potentialTargets = SortByPreference (potentialTargets, targetProfileIndex);

		return potentialTargets [0];
	}

	public List<Target>
	RemoveByTargetType(List<Target> targets, int l)
	{
		List<Target> sortedTargets = new List<Target> ();

		if (aiTargetProfiles [l].targetType == OldTargetType.Belligerent)
		{
			foreach (Target target in targets)
			{
				if (target.GetComponent<Avatar> ())
				{
					sortedTargets.Add (target);
				}
			}
		}

		return sortedTargets;
	}


	public List<Target>
	RemoveByAlignment(List<Target> targets, int l)
	{
		List<Target> sortedTargets = new List<Target> ();
//
//		if (aiTargetProfiles [l].targetAlignment == OldTargetAlignment.Any)
//		{
//			return targets;
//		}
//		else if (aiTargetProfiles [l].targetAlignment == OldTargetAlignment.Ally)
//		{
//			foreach (Target target in targets)
//			{
//				if (target.GetHost () == skill.GetCaster().GetHost())
//				{
//					sortedTargets.Add (target);
//				}
//			}
//		}
//		else if (aiTargetProfiles [l].targetAlignment == OldTargetAlignment.Enemy)
//		{
//			foreach (Target target in targets)
//			{
//				if (target.GetHost () != skill.GetCaster().GetHost())
//				{
//					sortedTargets.Add (target);
//				}
//			}
//		}
		return sortedTargets;
	}


	public List<Target>
	SortByPreference(List<Target> targets, int l)
	{
		List<Target> sortedTargets = new List<Target> ();

		switch (aiTargetProfiles [l].targetPreference)
		{
		case TargetPreference.Random:
			{
				return targets;
			}

		case TargetPreference.LowestHealth:
			{
				for (int i = 0; i < 100; i++)
				{
					foreach (Target target in targets)
					{
						if (target.GetComponent<Avatar> ().GetStats().Health == i)
						{
							sortedTargets.Add (target);
						}
					}
				}
				break;
			}

		case TargetPreference.HighestHealth:
			{
				for (int i = 100; i > -1; i--)
				{
					foreach (Target target in targets)
					{
						if (target.GetComponent<Avatar> ().GetStats().Health == i)
						{
							sortedTargets.Add (target);
						}
					}
				}
				break;
			}

		case TargetPreference.LowestMana:
			{
				for (int i = 0; i < 100; i++)
				{
					foreach (Target target in targets)
					{
						if (target.GetComponent<Avatar> ().GetStats().Mana == i)
						{
							sortedTargets.Add (target);
						}
					}
				}
				break;
			}

		case TargetPreference.HighestMana:
			{
				for (int i = 100; i > -1; i--)
				{
					foreach (Target target in targets)
					{
						if (target.GetComponent<Avatar> ().GetStats().Mana == i)
						{
							sortedTargets.Add (target);
						}
					}
				}
				break;
			}
		}
		return sortedTargets;
	}
}

public enum OldTargetType{Belligerent, Host, LocalPersistency, GlobalPersistency, Stance, Reserve}

[System.Serializable]
public class AITargetProfile : System.Object
{
	public OldTargetType targetType;
	public OldTargetAlignment targetAlignment;
	public TargetPreference targetPreference;
}
}