﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

[RequireComponent(typeof(Host))]
public class AIHost : MonoBehaviour
{
	void
	Start()
	{
		BattleLogic.manager.backendNotifications.EAISelectCharacter 	+= SelectRoutine;
		BattleLogic.manager.backendNotifications.EAISelectSkill 		+= SelectSkill;
	}

	void
	OnDestroy()
	{
		BattleLogic.manager.backendNotifications.EAISelectCharacter 	-= SelectRoutine;
		BattleLogic.manager.backendNotifications.EAISelectSkill 		-= SelectSkill;
	}

	const bool ALWAYS_SKIP_GO = false;

	void
	SelectRoutine()
	{
//		if (BattleLogic.manager.turnOrder.GetActiveHost () == GetComponent<BattleHost> ())
//		{
//			List<AISkillRoutine> routines = SolicitCharactersPreferredRoutines ();
//
//			if (ALWAYS_SKIP_GO)
//			{
//				routines = new List<AISkillRoutine>();
//			}
//		
//			routines.Add (PassTurn ());
//			currentRoutine = routines [0];
//			SelectCharacter ();
//		}
	}


	AISkillRoutine
	PassTurn()
	{
		//[[FLAG]]// Can't use new on monobehaviour
		AISkillRoutine passTurn = new AISkillRoutine();

		passTurn.caster = null;
		passTurn.skill = null;
		passTurn.passTurn = true;

		return passTurn;
	}

	void
	SelectCharacter()
	{
		//BattleLogic.manager.CharacterSelected (currentRoutine.caster);
	}


	void
	SelectSkill()
	{
//		if (BattleLogic.manager.turnOrder.GetActiveHost () == GetComponent<BattleHost> ())
//		{
//			currentRoutine.skill.AISelectAsSkill(currentRoutine.behaviour);
//		}
	}


	List<AISkillRoutine>
	SolicitCharactersPreferredRoutines()
	{
		List<AISkillRoutine> potentialSkills = new List<AISkillRoutine> ();

		foreach (Avatar character in GetComponent<Host>().GetAvatars())
		{
			potentialSkills.Add(character.GetComponent<AIBattleAvatar> ().GetPotentialSkill ());
		}

		potentialSkills = DestroyPassTurns (potentialSkills);
		potentialSkills = DestroyRepeats (potentialSkills);
		potentialSkills = SortByTurnsTaken (potentialSkills);

		return potentialSkills;
	}


	List<AISkillRoutine>
	DestroyPassTurns(List<AISkillRoutine> skillRoutines)
	{
		for (int i = skillRoutines.Count - 1; i > -1; i--)
		{
			if (skillRoutines[i].passTurn)
			{
				skillRoutines.RemoveAt(i);
			}
		}
		return skillRoutines;
	}


	List<AISkillRoutine>
	DestroyRepeats(List<AISkillRoutine> skillRoutines)
	{
		List<AISkillRoutine> sortedSkillRoutines = new List<AISkillRoutine>();
		//sort descending by mana of caster
		for (int i = 100; i > -1; i--)
		{
			foreach (AISkillRoutine routine in skillRoutines)
			{
				if (routine.caster.GetStats().Mana == i)
				{
					sortedSkillRoutines.Add (routine);
				}
			}
		}

		//if a different caster is doing the same skill, destroy their candidate
		foreach (AISkillRoutine routine in sortedSkillRoutines)
		{
			for (int v = 0; v < sortedSkillRoutines.Count; v++)
			{
				{
					if (routine.caster != sortedSkillRoutines [v].caster
					    && routine.skill == sortedSkillRoutines [v].skill)
					{
						sortedSkillRoutines [v].passTurn = false;
					}
				}
			}
		}


		for (int l = sortedSkillRoutines.Count - 1; l > -1; l--)
		{
			if (sortedSkillRoutines [l].passTurn == true)
			{
				sortedSkillRoutines.RemoveAt (l);
			}
		}
		return sortedSkillRoutines;
	}


	List<AISkillRoutine>
	SortByTurnsTaken(List<AISkillRoutine> skillRoutines)
	{
		List<AISkillRoutine> sortedSkillRoutines = new List<AISkillRoutine> ();

		for (int i = 0; i < 100; i++)
		{
			foreach(AISkillRoutine routine in skillRoutines)
			{
				if(routine.caster.GetComponent<AIBattleAvatar>().turnsTaken == i)
				{
					sortedSkillRoutines.Add(routine);
				}
			}
		}
		return sortedSkillRoutines;
	}
}
}