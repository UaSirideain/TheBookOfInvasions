﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Battle{

public enum SetByType{Inspector=0, Persistency=1}
public enum NewReplacementType{RecordedValue=0, CasterHealth=1, CasterMana=2, AllyHostAvatarCount=3, EnemyHostAvatarCount=4}
public enum ModificationType
{
	RaiseHealth=0, LowerHealth=1, SetHealth=2,
	Kill=3,
	RaiseMana=4, LowerMana=5, SetMana=6,
	ManaCost=7,
	EnableSkill=8, DisableSkill=9
}

[System.Serializable]
public class BattleEffectValue : System.Object
{
	[SerializeField][HideInInspector] Avatar source;

    [SerializeField] [HideInInspector] SetByType setBy;
	[SerializeField] [HideInInspector] NewReplacementType newReplacementValue;

	//[SerializeField][HideInInspector] string inspectorName;
	[SerializeField][HideInInspector] ModificationType type;
    [SerializeField][HideInInspector] int value;

	public BattleEffectValue(string newInspectorName, ModificationType newType, int newValue = 0)
	{
		//inspectorName = newInspectorName;
		type = newType;
		value = newValue;
	}

	public void
	LoadRecordedData(ActionDataPersistency recordedData)
	{
		if (setBy == SetByType.Persistency)
		{
			if (newReplacementValue == NewReplacementType.RecordedValue)
			{
				value = recordedData.recordedValue;
			}
			else if (newReplacementValue == NewReplacementType.CasterHealth)
			{
				value = source.GetComponent<AvatarStats> ().Health;
			}
			else if (newReplacementValue == NewReplacementType.CasterMana)
			{
				value = source.GetComponent<AvatarStats> ().Mana;
			}
			else if (newReplacementValue == NewReplacementType.AllyHostAvatarCount)
			{
				value = source.GetHost ().GetAvatars ().Count;
			}
			else if (newReplacementValue == NewReplacementType.EnemyHostAvatarCount)
			{
				value = source.GetEnemyHost ().GetAvatars ().Count;
			}
		}
	}
		
    public string
    GetValueString()
    {
		return GetModifiedValue().ToString();
    }

	public int
	GetModifiedValue(Target target)
	{
		return BattleLogic.manager.modService.ModifyValue (value, type, source, target);
	}
		
	int
	GetModifiedValue()
	{
		return BattleLogic.manager.modService.ModifyValue (value, type, source);
	}

	public void
	WriteSource(Avatar newSource)
	{
		source = newSource;
	}
}
}