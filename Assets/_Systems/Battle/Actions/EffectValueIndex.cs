﻿using UnityEngine;
using System.Collections.Generic;

namespace Battle{

[System.Serializable]
public class EffectValueIndex
{
	[SerializeField][HideInInspector] public bool present;
	[SerializeField][HideInInspector] public string valueReference;
	[SerializeField][HideInInspector] public Effect effect;
	[SerializeField][HideInInspector] public int valueIndex;
}
}