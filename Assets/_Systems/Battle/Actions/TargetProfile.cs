﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public enum AutoTarget				{Inspector=0 , Caster=1, CasterHost=2, CasterAllies=3, EnemyHost=4, EnemyAllies=5, OtherTargetProfile=6, AIRoutine=7, Persistency=8}
public enum PersistencyTargets		{Enchanted=0, TriggerActionCaster=1, TriggerAction=2, Persistency=3}
public enum TargetType				{ Belligerent=0, Host=1, Persistency=2, Reserve=3, Stance=4 }

[System.Serializable]
public class TargetProfile
{
	[SerializeField] public List<TPCondition> conditions; //This is to be depreciated
	public TargetProfileConditions conditionList;	//Needs a custom editor
	public int copyTargetsFrom;
	public AutoTarget autoTarget;
	public PersistencyTargets persistencyAutoTarget;
	public string instructions;

	public List<Target> targets = new List<Target>();
	public List<Target> targetsWhoAvoided = new List<Target> ();

	//target restrictions
	public int maxTargets;
	public TargetType targetType = TargetType.Belligerent;
	public Disposition targetAlignment = Disposition.Neutral;
	public string displayInstructions;

	Avatar caster;

	public void
	Awake()
	{
		conditionList = new TargetProfileConditions (conditions);
	}

	public void
	LoadActionData(ActionDataDefault actionData)
	{
		caster = actionData.caster;
		switch (autoTarget)
		{
			case AutoTarget.Caster:
				maxTargets = 1;
				targets.Add(caster);
				break;

			case AutoTarget.CasterHost:
				maxTargets = 1;
				targets.Add(caster.GetHost());
				break;

			case AutoTarget.EnemyHost:
				maxTargets = 1;
				targets.Add(caster.GetEnemyHost());
				break;

			case AutoTarget.OtherTargetProfile:
				//maxTargets = How do we get this?
				break;

			case AutoTarget.CasterAllies:
				maxTargets = caster.GetHost().GetAvatars().Count;
				foreach (Avatar avatar in caster.GetHost().GetAvatars())
				{
					targets.Add((Target)avatar);
				}
				break;

			case AutoTarget.EnemyAllies:
				maxTargets = caster.GetHost().GetAvatars().Count;
				foreach (Avatar avatar in caster.GetEnemyHost().GetAvatars())
				{
					targets.Add((Target)avatar);
				}
				break;
		}
	}

	public void
	LoadPersistencyData(ActionDataPersistency newPersistencyData)
	{
		if (autoTarget == AutoTarget.Persistency)
		{
			switch (persistencyAutoTarget)
			{
				case PersistencyTargets.Enchanted:
					maxTargets = 1;
					targets.Add(newPersistencyData.enchanted);
					break;

				case PersistencyTargets.TriggerActionCaster:
					maxTargets = 1;
					targets.Add(newPersistencyData.recordedAction.actionData.caster);
					break;

				case PersistencyTargets.Persistency:
					maxTargets = 1;
					targets.Add(newPersistencyData.persistency);
					break;

				case PersistencyTargets.TriggerAction:
					maxTargets = 1;
					targets.Add(newPersistencyData.recordedAction);
					break;
			}
		}
	}

	public bool
	AllTargetsSelected()
	{
		return targets.Count + targetsWhoAvoided.Count >= maxTargets;
	}

	public bool
	NotAllTargetsSelected()
	{
		return !AllTargetsSelected();
	}

	public bool
	IsTargetValid(Target target)
	{
		if (target != null)
		{
			return 
			(
				((targets.Count < maxTargets))

					&&

				((target.HasComponent<Avatar> () && targetType == TargetType.Belligerent)
					||
				(target.HasComponent<Host>() && targetType == TargetType.Host)
					||
				(target.HasComponent<Persistency> () && targetType == TargetType.Persistency))

					&&

				((targetAlignment == Disposition.Neutral)
					||
				(targetAlignment == Disposition.Hostile && target.GetHost() != caster.GetHost())
					||
				(targetAlignment == Disposition.Friendly && target.GetHost() == caster.GetHost()))

					&&

				(conditionList.Validate(target))
			);
		}
		return false;
	}
}
}