﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
	public enum TPCondition { IsAlive = 0, IsDead = 1, IsWounded = 2 }

	[System.Serializable]
	public class TargetProfileConditions
	{
		[SerializeField] List<TPCondition> conditions;

		public int
		GetCount()
		{
			return conditions.Count;
		}

		public TargetProfileConditions(List<TPCondition> newConditions)
		{
			conditions = new List<TPCondition> ();
			conditions.AddRange (newConditions);
		}

		public bool
		Validate(Target target)
		{
			bool goodToGo = true;

			foreach (TPCondition condition in conditions)
			{
				if (condition == TPCondition.IsAlive && !target.IsAlive())
				{
					goodToGo = false;
				}
				else if (condition == TPCondition.IsDead && !target.IsDead())
				{
					goodToGo = false;
				}
				else if (condition == TPCondition.IsWounded && !target.IsWounded())
				{
					goodToGo = false;
				}
			}
			return goodToGo;
		}
	}
}