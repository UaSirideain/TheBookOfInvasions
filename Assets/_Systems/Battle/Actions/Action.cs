﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Cameras;
using System;

namespace Battle{

public enum ActionStatus{Healthy=0, Prevented=1, Blocked=2, Dodged=3}
public enum ActionSortOrder {Before=0, After=1}

public enum RangeType{Melee=1, Ranged=2, Instantiative=3}
public enum ActionType{Feat=1, Enchantment=2, Music=3, Item=4}
public enum Disposition{Hostile=1, Friendly=2, Neutral=3}

public class Action : Target
{
	Disposition dispositionType;
	RangeType rangeType;
	ActionType skillType;

	public ActionDataDefault actionData;
	ActionStatus status = ActionStatus.Healthy;

	ChangeList changeList { get { return GetComponent<ChangeList> (); } }
	BattleCamera cam { get { return FindObjectOfType<BattleCamera> (); } }
	CurrentActionUI ui { get { return FindObjectOfType<CurrentActionUI> (); } }

	//ActionSortOrder sortOrder;
	bool isCounterAttack;
	bool renderToCurrentActionUI;

	public ChangeList ChangeList => changeList;
	public ActionSortOrder SortOrder => ActionSortOrder.Before;
	public ActionType SkillType => skillType;
	public Disposition Disposition => dispositionType;
	public RangeType RangeType => rangeType;
	public bool IsCounterAttack => isCounterAttack;

	public override List<Change> GetChanges() => ChangeList.GetChanges();
	public void	SetPrevented() => status = ActionStatus.Prevented;

	List<Choreograph> choreography = new List<Choreograph>();

	public void
	SetUp(ActionDataDefault newActionData)
	{
		CopyActionData(newActionData);
		RecordChoreographies();
	}

	void
	CopyActionData(ActionDataDefault newActionData)
	{
		actionData = newActionData;
		isCounterAttack = actionData.isCounterAttack;
		dispositionType = actionData.disposition;
		rangeType = actionData.rangeType;
		skillType = actionData.skillType;
	}

	void 
	RecordChoreographies()
	{
		choreography.Clear();
		Choreograph[] choreographs = GetComponents<Choreograph>();

		foreach (Choreograph choreograph in choreographs)
		{
			if (choreograph.enabled)
			{
				choreography.Add(choreograph);
			}
		}
	}


	public void
	TheoreticalRun()
	{
		if (VerifyStatus())
		{
			changeList.Initialise (ChangeActuality.Theoretical);

			foreach (Choreograph choreograph in choreography)
			{
				choreograph.TheoreticalRun ();
				GetComponent<ChangeList> ().Induct (choreograph.RetrieveChanges ());
			}
		}
	}

	public IEnumerator
	ActualRun()
	{
		if (VerifyStatus())
		{
			changeList.Initialise (ChangeActuality.Actual);

			foreach (Choreograph choreograph in choreography)
			{
				yield return StartCoroutine(choreograph.ActualRun ());
				GetComponent<ChangeList> ().Induct (choreograph.RetrieveChanges ());
			}
		}
	}

	bool
	VerifyStatus()
	{
		bool fizzle = false;
		TargetList[] targetLists = GetComponents<TargetList>();

		if (status != ActionStatus.Healthy)
		{
			fizzle = true;
		}

		foreach (TargetList list in targetLists)
		{
			if (!list.IsValid ())
			{
				fizzle = true;
			}
		}

		return fizzle == false;
	}
}
}