﻿using UnityEngine;
using System.Collections.Generic;

namespace Battle
{
	public class BattleSkill : MonoBehaviour
	{
		BattleSkillUI ui { get { return GetComponent<BattleSkillUI>(); } }
		InputRouter input { get { return FindObjectOfType<InputRouter>(); } }

		[SerializeField] [HideInInspector] string uiName;
		[SerializeField] [HideInInspector] int manaCost;
		[SerializeField] [HideInInspector] string uiDescription;

		[SerializeField] [HideInInspector] List<EffectValueIndex> effectValueIndices = new List<EffectValueIndex>();

		[SerializeField] ActionComposite action;

		Avatar caster;
		Persistency activePersistency;

		public BattleSkillInfo
		GetInfo()
		{
			BattleSkillInfo info = new BattleSkillInfo(uiName, manaCost.ToString(), uiDescription);
			return ui.GetInfo(info);
		}

		public void
		Initialise(Avatar newCaster)
		{
			caster = newCaster;
			UpdateInfo();
		}

		public void
		MouseDown()
		{
			if (GetComponent<BattleSkillUseItem>())
			{
				GetComponent<BattleSkillUseItem>().BuildItemList(SelectItemAsSkill);
			}
			else
			{
				SelectAsSkill();
			}
		}

		public void
		UpdateInfo()
		{
			ui.RenderTitleAndMana(uiName, GetManaCostAdjusted().ToString());
			ui.RenderDescription(GetDescription());
			ui.RenderDisabledStatus(IsDisabled());
		}

		public bool
		IsDisabled()
		{
			return activePersistency != null;
		}

		public void
		RegisterPersistency(Persistency newPersistency)
		{
			activePersistency = newPersistency;
		}

		public void
		DeregisterPersistency()
		{
			activePersistency = null;
		}

		void
		SelectAsSkill()
		{
			if (IsDisabled() == false)
			{
				input.SkillSelected(GenerateActionsForThisSkill());
			}
		}

		void
		SelectItemAsSkill(ActionComposite itemAction, string itemName)
		{
			ActionComposite newAction = Instantiate(itemAction);

			WriteSourceToEffects(newAction);

			ActionDataDefault actionData = new ActionDataDefault(itemName, caster, this);
			newAction.Initialise(actionData);

			input.SkillSelected(newAction);
		}

		ActionComposite
		GenerateActionsForThisSkill()
		{
			ActionComposite newAction = Instantiate(action);

			WriteSourceToEffects(newAction);

			ActionDataDefault actionData = new ActionDataDefault(uiName, caster, this);
			newAction.Initialise(actionData);

			return newAction;
		}

		int
		GetManaCostAdjusted()
		{
			return BattleLogic.manager.modService.ModifyValue(manaCost, ModificationType.ManaCost, caster);
		}

		string
		GetDescription()
		{
			WriteSourceToEffects(action);

			string adjustedUIDescription = uiDescription;

			for (int i = 0; i < effectValueIndices.Count; i++)
			{
				string variableString = "<value" + i.ToString().Replace("0", "") + ">";

				if (adjustedUIDescription.Contains(variableString))
				{
					Effect effect = effectValueIndices[i].effect;
					int index = effectValueIndices[i].valueIndex;
					string replacementString = effect.GetValueString(index);

					adjustedUIDescription = adjustedUIDescription.Replace(variableString, replacementString);
				}
			}

			adjustedUIDescription = adjustedUIDescription.Replace("<caster>", caster.name);

			return adjustedUIDescription;
		}

		void
		WriteSourceToEffects(ActionComposite targetAction)
		{
			foreach (Effect effect in targetAction.GetComponents<Effect>())
			{
				effect.GetValues().WriteSource(caster);
			}
		}

		const int MAX_VALUE_REFERENCES = 4;

		void
		OnValidate()
		{
			effectValueIndices.EnsureSize(MAX_VALUE_REFERENCES);

			for (int i = 0; i < MAX_VALUE_REFERENCES; i++)
			{
				if (uiDescription.Contains("<value" + i.ToString().Replace("0", "") + ">"))
				{
					effectValueIndices[i].valueReference = "<value" + i.ToString().Replace("0", "") + ">";
					effectValueIndices[i].present = true;
				}
				else
				{
					effectValueIndices[i].present = false;
				}
			}
		}
	}
}