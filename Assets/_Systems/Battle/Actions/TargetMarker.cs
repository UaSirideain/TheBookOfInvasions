﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class TargetMarker : MonoBehaviour
{
	public int profile;
	public Target target;

	public void
	PointerDown()
	{
		FindObjectOfType<TargetAssigner> ().CancelTarget (target, profile);
	}
}
}