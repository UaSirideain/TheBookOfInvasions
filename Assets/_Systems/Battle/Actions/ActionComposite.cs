﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class ActionComposite : MonoBehaviour
{
	ActionDataDefault actionData;

	public void
	Initialise(ActionDataDefault newActionData)
	{
		actionData = newActionData;
		GetComponent<TargetCollector> ().LoadActionData (actionData);
	}

	public void
	InitialiseWithPersistency(ActionDataPersistency recordedData)
	{
		GetComponent<TargetCollector> ().LoadRecordedData (recordedData);
		foreach (Effect effect in GetComponents<Effect>())
		{
			effect.LoadRecordedData (recordedData);
		}
	}

	public ActionDataDefault
	GetActionData()
	{
		return actionData;
	}
}

public class ActionDataDefault
{
	public readonly string uiName;
	public readonly Avatar caster;
	public readonly BattleSkill linkedSkill;

	public ActionDataDefault(string newUIName, Avatar newCaster, BattleSkill newLinkedSkill)
	{
		uiName = newUIName;
		caster = newCaster;
		linkedSkill = newLinkedSkill;

		if (newLinkedSkill != null)//happens when created by stances
		{
			uiIcon = newLinkedSkill.GetComponent<BattleSkillUI> ().GetSmallIcon ();
		}
	}

	public bool isCounterAttack;
	public Disposition disposition;
	public ActionType skillType;
	public RangeType rangeType;
	public readonly Sprite uiIcon;

	public void
	Initialise(bool newIsCounterAttack, ActionType newSkillType, RangeType newRangeType, Disposition newDisposition)
	{
		disposition = newDisposition;
		skillType = newSkillType;
		rangeType = newRangeType;
		isCounterAttack = newIsCounterAttack;
	}
}

public class ActionDataPersistency
{
	public ActionDataPersistency()
	{

	}

	public int recordedValue;
	public Action recordedAction;
	public Target enchanted;
	public Persistency persistency;
}
}