﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Battle;

namespace Battle
{

	public enum SkillCardIcon { Feat = 1, Enchantment = 2, Music = 3, Item = 4, Reserve = 5, Stance = 5 }
	public enum SkillCardColour { Red = 1, Blue = 2, Green = 3 }

	public class BattleSkillUI : MonoBehaviour
	{
		Image cardBackground { get { return GetComponent<Image>(); } }

		[SerializeField] CardData uiGraphics;

		[SerializeField] SkillCardIcon cardIcon;
		[SerializeField] SkillCardColour cardColour;

		Image uiIcon;
		Text uiTitle;
		Text uiDescription;

		public BattleSkillInfo
		GetInfo(BattleSkillInfo input)
		{
			return new BattleSkillInfo(input.title, input.cost, input.description, cardIcon, cardColour);
		}

		public void
		RenderTitleAndMana(string uiName, string manaCost)
		{
			uiTitle.text = uiName + "  |  " + manaCost.ToString();
		}

		public void
		RenderDescription(string uiDescriptionString)
		{
			uiDescription.text = uiDescriptionString;
		}

		public void
		RenderDisabledStatus(bool disabled)
		{
			if (disabled)
			{
				cardBackground.overrideSprite = uiGraphics.GetDisabledBackgroundSprite();
				uiIcon.overrideSprite = uiGraphics.GetDisabledIconSprite(cardIcon);
			}
			else
			{
				cardBackground.overrideSprite = null;
				uiIcon.overrideSprite = null;
			}
		}

		public Sprite
		GetSmallIcon()
		{
			return uiGraphics.GetSmallIcon(cardIcon);
		}

		void
		Awake()
		{
			BuildCardFace();
			InitialiseEventTriggers();
		}

		void
		BuildCardFace()
		{
			uiIcon = Instantiate(uiGraphics.GetUIIconPrefab());
			uiTitle = Instantiate(uiGraphics.GetUITitlePrefab());
			uiDescription = Instantiate(uiGraphics.GetUIDescriptionPrefab());

			uiIcon.transform.SetParent(transform, false);
			uiTitle.transform.SetParent(transform, false);
			uiDescription.transform.SetParent(transform, false);

			uiIcon.sprite = uiGraphics.GetIconSprite(cardIcon);
			cardBackground.sprite = uiGraphics.GetBackgroundSprite(cardColour);
		}

		void
		InitialiseEventTriggers()
		{
			EventTrigger trigger = gameObject.AddComponent<EventTrigger>();

			EventTrigger.Entry mouseEnterEntry = new EventTrigger.Entry();
			mouseEnterEntry.eventID = EventTriggerType.PointerEnter;
			mouseEnterEntry.callback.AddListener((data) => { MouseEnter(); });
			trigger.triggers.Add(mouseEnterEntry);

			EventTrigger.Entry mouseExitEntry = new EventTrigger.Entry();
			mouseExitEntry.eventID = EventTriggerType.PointerExit;
			mouseExitEntry.callback.AddListener((data) => { MouseExit(); });
			trigger.triggers.Add(mouseExitEntry);

			EventTrigger.Entry mouseDownEntry = new EventTrigger.Entry();
			mouseDownEntry.eventID = EventTriggerType.PointerDown;
			mouseDownEntry.callback.AddListener((data) => { GetComponent<BattleSkill>().MouseDown(); });
			trigger.triggers.Add(mouseDownEntry);
		}

		void
		MouseEnter()
		{
			GetComponent<Image>().sprite = uiGraphics.GetBackgroundHoverSprite(cardColour);
		}

		void
		MouseExit()
		{
			GetComponent<Image>().sprite = uiGraphics.GetBackgroundSprite(cardColour);
		}
	}
}