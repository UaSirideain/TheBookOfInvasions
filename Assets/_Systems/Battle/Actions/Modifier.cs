﻿using System.Collections.Generic;
using UnityEngine;

namespace Battle{

[System.Serializable]
public class Modifier
{
	[SerializeField] List<StateCondition> conditions = new List<StateCondition>();

	public ModificationType type = ModificationType.DisableSkill;
	public int addition = 0;
	public int multiplication = 1;


	enum SourceMustBe{Anyone=0, EnchantedBelligerent=1, Ally=2, Enemy=3}
	enum TargetMustBe{Anyone=0, EnchantedBelligerent=1, Ally=2, Enemy=3}

	[SerializeField] SourceMustBe sourceMustBe;
	[SerializeField] TargetMustBe targetMustBe;

	Persistency persistency;

	public void
	SetPersistency(Persistency newPersistency)
	{
		persistency = newPersistency;
	}

	public bool
	VerifyType(ModificationType otherType)
	{
		return type == otherType;
	}

	public bool
	VerifySource(Avatar source)
	{
		if (VerifyConditions (source) == false)
		{
			return false;
		}

		if (sourceMustBe == SourceMustBe.Anyone)
		{
			return true;
		}
		else if (sourceMustBe == SourceMustBe.EnchantedBelligerent)
		{
			Target enchanted = persistency.GetEnchanted ();
			return (Target)source == enchanted;
		}
		else if (sourceMustBe == SourceMustBe.Ally)
		{
			Host allyHost = persistency.GetHost ();
			return source.GetHost () == allyHost;
		}
		else if (sourceMustBe == SourceMustBe.Enemy)
		{
			Host enemyHost = persistency.GetEnemyHost ();
			return source.GetHost () == enemyHost;
		}

		return false;
	}

	public bool
	VerifyTarget(Target target)
	{
		if (VerifyConditions (target) == false)
		{
			return false;
		}

		if (targetMustBe == TargetMustBe.Anyone)
		{
			return true;
		}
		else if (target == null)
		{
			return false;
		}
		else if (targetMustBe == TargetMustBe.EnchantedBelligerent)
		{
			Target enchanted = persistency.GetEnchanted ();
			return (Target)target == enchanted;
		}
		else if (targetMustBe == TargetMustBe.Ally)
		{
			Host allyHost = persistency.GetHost ();
			return target.GetHost () == allyHost;
		}
		else if (targetMustBe == TargetMustBe.Enemy)
		{
			Host enemyHost = persistency.GetEnemyHost ();
			return target.GetHost () == enemyHost;
		}
			
		return false;
	}

	bool
	VerifyConditions(Target target)
	{
		foreach (StateCondition condition in conditions)
		{
			if (condition.CheckTarget (target) == false)
			{
				return false;
			}
		}
		return true;
	}

	bool
	VerifyConditions(Avatar source)
	{
		foreach (StateCondition condition in conditions)
		{
			if (condition.CheckSource (source) == false)
			{
				return false;
			}
		}
		return true;
	}
}
}