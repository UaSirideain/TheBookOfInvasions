﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class TargetCollector : MonoBehaviour
{
	[SerializeField] List<TargetProfile> targetProfiles = new List<TargetProfile>();
	[SerializeField] TargetMarker targetMarkerPrefab;

	TargetAssignerUI ui { get { return FindObjectOfType<TargetAssignerUI> (); } }

	void
	Awake()
	{
		foreach (TargetProfile tp in targetProfiles)
		{
			tp.Awake ();
		}
	}

	public bool
	NeedsTargets()
	{
		foreach (TargetProfile profile in targetProfiles)
		{
			if (profile.NotAllTargetsSelected())
			{
				return true;
			}
		}
		return false;
	}

	public void
	AddTarget(Target target, int profileIndex)
	{
		if (targetProfiles [profileIndex].IsTargetValid (target))
		{
			targetProfiles [profileIndex].targets.Add (target);
			ui.SetAsMarked (target, profileIndex);//target.MarkAsTargeted (profileIndex + 1);
		}
	}

	public void
	CancelLastTarget(int profileIndex)
	{
		Target canceledTarget = targetProfiles [profileIndex].targets.GetLast ();

		ui.SetAsUnmarked (canceledTarget, profileIndex);
		targetProfiles [profileIndex].targets.RemoveLast ();
	}

	public void
	CancelTarget(Target target, int profileIndex)
	{
		ui.SetAsUnmarked (target, profileIndex);
		targetProfiles [profileIndex].targets.Remove(target);
	}

	public TargetProfile
	GetTargetProfile(int profileIndex) => targetProfiles[profileIndex];

	public int
	GetTargetProfileCount() => targetProfiles.Count;

	public void
	LoadActionData(ActionDataDefault actionData)
	{
		foreach (TargetProfile profile in targetProfiles)
		{
			profile.LoadActionData (actionData);
		}
	}

	public void
	LoadRecordedData(ActionDataPersistency data)
	{
		foreach (TargetProfile profile in targetProfiles)
		{
			profile.LoadPersistencyData (data);
		}
	}

	public string
	GetInstructionsForProfile(int profileIndex) => targetProfiles[profileIndex].displayInstructions;

		public int
	GetIndexOfNextTargetList()
	{
//		if (targetProfiles [currentProfile].targets.Count < targetProfiles [currentProfile].maxTargets && targetProfiles[currentProfile].targets.Count > 0)
//		{
//			return currentProfile;
//		}

		for (int i = 0; i < targetProfiles.Count; i++)
		{
			if (targetProfiles [i].targets.Count + targetProfiles [i].targetsWhoAvoided.Count < targetProfiles [i].maxTargets)
			{
				return i;
			}
		}
		return 0;
	}
}
}