﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{

	public enum TargetCapturing {SingleTargetAtIndex=1, AllTargetsFromProfile=0}

public class TargetList : MonoBehaviour
{
	TargetCollector targeter { get { return GetComponent<TargetCollector> (); } }

	[SerializeField][HideInInspector] TargetProfileConditions conditions;

	[SerializeField] int profileIndex;
		[SerializeField] TargetCapturing capture;
	[SerializeField] int targetIndex;

	[SerializeField][HideInInspector] List<Target> targets = new List<Target>();

	public void
	RecordTargets()
	{
		TargetProfile targetProfileAtProfileIndex = targeter.GetTargetProfile(profileIndex);
		conditions = targetProfileAtProfileIndex.conditionList;

		if (capture == TargetCapturing.SingleTargetAtIndex)
		{
			targets.Add (targetProfileAtProfileIndex.targets [targetIndex]);
		}
		else
		{
			targets.AddRange (targetProfileAtProfileIndex.targets);
		}
	}

	public Target
	GetTarget()
	{
		SanitiseTargets ();
		return targets [0];
	}

	public List<Target>
	GetTargets()
	{
		SanitiseTargets ();
		return targets;
	}

	public bool
	IsValid()
	{
		for(int i = 0; i < targets.Count; i++)
		{
			if (conditions.Validate (targets [i]) == false)
			{
				targets [i] = null;
			}
		}

		return GetTargets ().Count > 0;
	}

	void
	SanitiseTargets()
	{
		targets.Sanitise ();
	}
}
}