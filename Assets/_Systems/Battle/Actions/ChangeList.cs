﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle{

public class ChangeList : MonoBehaviour
{
	[SerializeField] List<Change> changes = new List<Change>();
	[HideInInspector] public ChangeActuality actuality = ChangeActuality.Theoretical;

	public List<Change>	GetChanges() => changes;

		public bool
	CheckForType(ChangeType type)
	{
		foreach (Change change in changes)
		{
			if (change.type == type)
			{
				return true;
			}
		}

		return false;
	}

	public int
	GetValueForType(ChangeType type)
	{
		foreach (Change change in changes)
		{
			if (change.type == type)
			{
				return change.value;
			}
		}

		return 0;
	}

	public bool
	CheckForType(ChangeType type, out int value)
	{
		foreach (Change change in changes)
		{
			if (change.type == type)
			{
				value = change.value;
				return true;
			}
		}

		value = 0;
		return false;
	}

	public void
	Induct(List<Change> newChanges)
	{
		foreach(Change newChange in newChanges)
		{
			newChange.actuality = actuality;
			changes.Add (newChange);
		}
	}

	public void
	Initialise(ChangeActuality newActuality)
	{
		changes.Clear ();
		actuality = newActuality;
	}
}
}