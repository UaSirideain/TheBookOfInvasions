﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Battle{

[RequireComponent(typeof(ConditionsTrayHostUI))]
public class ConditionsTrayHost : ConditionsTray
{
	ConditionsTrayHostUI ui{ get { return GetComponent<ConditionsTrayHostUI> (); } }

	public override void//TODO: should this return a battle change?
	LoadPersistency(Persistency persistency)
	{
		if (persistency.GetDisposition () == Disposition.Friendly)
		{
			positivePersistencies.Add (persistency);
			persistency.RegisterTray (this);
		}
		else
		{
			negativePersistencies.Add (persistency);
			persistency.RegisterTray (this);//this line wasn't initially present. Not sure if that was a mistake or intentional. Keep an eye on it in case anything weird happens.
		}

//		persistency.DisableLinkedSkill ();
		persistency.SetEnchanted (GetComponent<Target> ());
			
		ui.RenderPersistencies (GetAllPersistencies());
	}

	public override void
	RemovePersistency(Persistency persistency)
	{
		if (positivePersistencies.Contains (persistency))
		{
			positivePersistencies.Remove (persistency);
		}
		else if (negativePersistencies.Contains (persistency))
		{
			negativePersistencies.Contains(persistency);
		}

		persistency.Disable ();
//		persistency.ReenableLinkedSkill ();
		Destroy (persistency.gameObject);

		ui.RenderPersistencies(GetAllPersistencies());
	}
		
	protected override List<Persistency>
	GetAllPersistencies()
	{
		List<Persistency> persistencies = new List<Persistency> ();
		persistencies.AddRange (positivePersistencies);
		persistencies.AddRange (negativePersistencies);
		return persistencies;
	}

	public override List<Persistency>
	GetPositiveConditions()
	{
		return positivePersistencies;
	}

	public override List<Persistency>
	GetNegativeConditions()
	{
		return negativePersistencies;
	}
}
}