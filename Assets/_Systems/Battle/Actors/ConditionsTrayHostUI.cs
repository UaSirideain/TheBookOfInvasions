﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class ConditionsTrayHostUI : MonoBehaviour
{
	[SerializeField] RectTransform persistencyTray;

	int lerpTicket = 0;
	const float yPositionHidden = 70f;
	const float yPositionVisible = 0f;

	public void
	RenderPersistencies(List<Persistency> icons)
	{
		foreach (Persistency icon in icons)
		{
			RenderPersistencyIcon (icon.GetComponent<RectTransform>(), icons.IndexOf (icon));
		}

		if (icons.Count > 0)
		{
			StartCoroutine(Show ());
		}
		else
		{
			StartCoroutine(Hide ());
		}
	}

	void
	RenderPersistencyIcon(RectTransform icon, int i)
	{
		icon.SetAsChildOf (persistencyTray);
		icon.pivot = new Vector2 (.5f, .5f);
		icon.anchorMin = new Vector2 (.5f, .5f);
		icon.anchorMax = new Vector2 (.5f, .5f);
		icon.anchoredPosition = new Vector3 (icon.sizeDelta.x * i, 10f, 0f);
	}

	IEnumerator
	Show()
	{
		persistencyTray.GetComponent<Image> ().enabled = true;
		yield return StartCoroutine(RenderTray (yPositionVisible));
	}

	IEnumerator
	Hide()
	{
		yield return StartCoroutine(RenderTray (yPositionHidden));
		persistencyTray.GetComponent<Image> ().enabled = false;
	}

	IEnumerator
	RenderTray(float yOffset)
	{
		lerpTicket++;
		int myLerpTicket = lerpTicket;
		float t = 0f;

		Vector2 trayPos = persistencyTray.anchoredPosition;

		while (myLerpTicket == lerpTicket && t < 1)
		{
			persistencyTray.anchoredPosition = Vector2.Lerp (trayPos, new Vector2(trayPos.x, yOffset), t);
			t += Time.deltaTime;
			yield return null;
		}
	}
}
}