﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public class Target : MonoBehaviour
{
    private InputRouter input { get { return FindObjectOfType<InputRouter>(); } }

	protected bool armedAsValid = false;

	public virtual void
	ArmAsValidTarget()
	{
		armedAsValid = true;
	}

	public void
	DisarmAsValidTarget()
	{
		armedAsValid = false;
	}

	public virtual void
	MarkAsTargeted(int displayNumber, int profileIndex){}

	public virtual void
	UnmarkAsTargeted(int profileIndex){}

	public virtual void
	ClearTargetMarkers(){}

    public void
	SelectAsTarget()
	{
		if (armedAsValid)
		{
			input.TargetSelected (this);
		}
	}

	public bool
	CheckMatch(TargetProfile profile)
	{
		//we need to figure out where details like "This action is a feat, and this action is ranged" go.
		//Maybe when someone targets someone that's immune, it just fizzles.
		return true;
	}

	public virtual List<Change>
	GetChanges()
	{
		return new List<Change> ();
	}

	public virtual void
	CommitChanges(){}

	public virtual void
	ResetChanges(){}

	public Host
	GetHost()
	{
		Transform futureHost = transform;
		Host host = futureHost.GetComponent<Host>();
		
		while (host == null)
		{
			futureHost = futureHost.parent;
			host = futureHost.GetComponent<Host>();
		}

		if (host) return host;
		else return null;
	}

	public Host
	GetEnemyHost()
	{
		Host allyHost = GetHost ();
		List<Host> hosts = BattleLogic.manager.actors.GetHosts ();
		hosts.Remove (allyHost);
		return hosts [0];
	}

	public bool
	IsEnemy()
	{
		return (GetHost () == BattleLogic.manager.turnOrderHistory.GetNonActiveHost ());
	}
}
}