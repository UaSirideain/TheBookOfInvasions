﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Battle{

public abstract class ConditionsTray : MonoBehaviour
{
	protected List<Persistency> positivePersistencies = new List<Persistency>();
	protected List<Persistency> negativePersistencies = new List<Persistency>();

	public virtual void//TODO: should this return a battle change?
	LoadPersistency(Persistency persistency){}

	public virtual void
	RemovePersistency(Persistency persistency){}

	public virtual void
	LoadReserve(Reserve reserve){}

	public virtual void
	LoadStance(Stance stance){}





	public virtual List<Persistency>
	GetAllConditions()
	{
		return new List<Persistency> ();
	}

	public virtual List<Persistency>
	GetPositiveConditions()
	{
		return new List<Persistency> ();
	}

	public virtual List<Persistency>
	GetNegativeConditions()
	{
		return new List<Persistency> ();
	}

	protected virtual List<Persistency>
	GetAllPersistencies()
	{
		return new List<Persistency> ();
	}

	protected virtual List<Persistency>
	GetArmedReservePersistencies()
	{
		return new List<Persistency> ();
	}

	public virtual List<Stance>
	GetAllStances()
	{
		return new List<Stance>();
	}

	protected virtual List<Persistency>
	GetArmedStancePersistencies()
	{
		return new List<Persistency> ();
	}

	public virtual int
	SkillsInReserve()
	{
		return 0;
	}
}
}