﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Battle.Grid;

namespace Battle{

public class Actors : MonoBehaviour
{
	[SerializeField] List<Transform> twoHostBattleStations;
	GridCell[,] allyStarts;
	GridCell[,] enemyStarts;

	Host allyHost;
	Host enemyHost;

	public void
	InitialiseBattle ()
	{
		foreach (Host host in GetHosts())
		{
			host.InitialiseBattle ();
		}
	}

	public List<Avatar>
	GetAvatarsBySpeed()
	{
		List<Avatar> unsortedAvatars = new List<Avatar>();

		foreach(Host host in GetHosts())
        {
            foreach (Avatar avatar in host.GetAvatars())
			{
				if (avatar.GetStats().IsBattleFit )
				{
					unsortedAvatars.Add (avatar);
				}
			}
		}
		return SortAvatarsBySpeed (unsortedAvatars);
	}
		
	public IEnumerator
	BuildHosts(Host allyHost, Host enemyHost, Grid.Grid grid)
	{
		this.allyHost = allyHost;
		this.enemyHost = enemyHost;

		allyHost.transform.SetParent (transform);
        enemyHost.transform.SetParent (transform);

		allyStarts = grid.GetAllyFormationBounds (enemyHost);
		enemyStarts = grid.GetEnemyFormationBounds (allyHost);

		allyHost.CreateAvatars (allyStarts);
		enemyHost.CreateAvatars (enemyStarts);

		yield return null;
	}

	public void
	EndBattle()
	{
		allyHost.EndBattle ();
		enemyHost.EndBattle ();
	}

	public List<Host>
	GetHosts()
	{
		List<Host> hosts = new List<Host>();

		hosts.Add (allyHost);
		hosts.Add (enemyHost);

        return hosts;
    }

	public Host
	GetAllyHost()
	{
		return allyHost;
	}

	public Host
	GetEnemyHost()
	{
		return enemyHost;
	}

	List<Avatar>
	SortAvatarsBySpeed(List<Avatar> unsortedCharacters)
	{
		List<Avatar> sortedCharacters = new List<Avatar>();

        for (var speed = 102; speed >= 0; speed--)
		{
			foreach (Avatar character in unsortedCharacters)
			{
				if (character.GetStats().Speed.GetAverage() == speed)
				{
					sortedCharacters.Add (character);
				}
			}
		}
		return sortedCharacters;
    }

    public List<Target>
    GetTargets()
    {
        List<Target> targets = new List<Target>();
        foreach (Host host in GetHosts())
        {
            foreach (Avatar avatar in host.GetAvatars())
            {
				foreach (Persistency persistency in avatar.GetConditions().GetAllConditions())
                {
                    targets.Add(persistency);
                }
                targets.Add(avatar);
            }
            targets.Add(host);
        }
        return targets;
    }

	public void
	ArmTargetsAsValidFor(TargetProfile profile)
	{
		foreach (Target target in GetTargets())
		{
			if (profile.IsTargetValid (target) && target.CheckMatch(profile))
			{
				target.ArmAsValidTarget ();
			}
		}
	}

	public void
	DisarmTargetsAsValid()
	{
		foreach (Target target in GetTargets())
		{
			target.DisarmAsValidTarget ();
		}
	}

	public void
	ClearTargetMarkers()
	{
		foreach (Target target in GetTargets())
		{
			target.ClearTargetMarkers ();
		}
	}
}
}