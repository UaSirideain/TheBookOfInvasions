﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public class PortraitsTray : MonoBehaviour 
{
	[SerializeField] int midSpacer;
	[SerializeField] int endSpacerY;
	[SerializeField] int endSpacerX;
	[SerializeField] float lerpRate;
	[SerializeField] bool enemyHost;
	[SerializeField] bool enterScreenFromSide;
	RectTransform rTransform {get{ return GetComponent<RectTransform> (); }}

	Vector3 startPosition;

	List<RectTransform> portraits = new List<RectTransform>();

	void
	OnValidate()
	{
		if (enemyHost)
		{
			ReverseAnchors ();
		}
	}

	public void
	AddPortraits(List<RectTransform> newPortraits)
	{
		portraits.AddRange(newPortraits);
	}

	void
	FilterOutEmptyPortraits()
	{
		int i = 0;

		while (i < portraits.Count)
		{
			if (portraits [i] == null)
			{
				portraits.RemoveAt (i);
			}
			else
			{
				i++;
			}
		}
	}

	bool rendered = false;

	public void
	RenderPortraits ()
	{
		FilterOutEmptyPortraits ();
		float trayHeightOffset;
		SetPortraitsInTray(portraits, out trayHeightOffset);

		Vector3 endPosition;
		SetTrayInPlace(portraits, trayHeightOffset, out endPosition);

		if (rendered)
		{
			Vector3 startPosition = rTransform.anchoredPosition;
			StartCoroutine (Lerp (startPosition, endPosition));
		}
		else
		{
			StartCoroutine (Lerp (endPosition));
		}

		rendered = true;
	}
		
	public void
	EndBattle()
	{
		DeletePortraits ();
		HidePortraitsTray ();
	}
		
	void
	DeletePortraits()
	{
		foreach (Transform portrait in transform)
		{
			Destroy (portrait.gameObject);
		}
	}

	void
	ReverseAnchors()
	{
		endSpacerX = 37;
		rTransform.anchorMin = new Vector2 (1, 0);
		rTransform.anchorMax = new Vector2(1, 0);
	}

	void
	HidePortraitsTray()
	{
		rTransform.anchoredPosition = new Vector2 (rTransform.anchoredPosition.x, -2000);
	}

	void
	SetPortraitsInTray(List<RectTransform> portraits, out float trayHeightOffset)
	{
		trayHeightOffset = 0;

		for(var p = 0; p < portraits.Count; p++)
		{
			portraits [p].SetParent (transform);
			portraits [p].anchoredPosition = new Vector3 (endSpacerX, (-endSpacerY) - (p * midSpacer) - (trayHeightOffset), 0);

			trayHeightOffset += portraits [p].sizeDelta.y;
		}
	}
		
	void
	SetTrayInPlace(List<RectTransform> portraits, float trayHeightOffset, out Vector3 endPosition)
	{
		if (enterScreenFromSide && enemyHost)
		{
			startPosition = new Vector3 (+rTransform.sizeDelta.x, (-1080) + (midSpacer) + (portraits.Count * midSpacer) + (trayHeightOffset), 0);
		}
		else if (enterScreenFromSide && !enemyHost)
		{
			startPosition = new Vector3 (-rTransform.sizeDelta.x, (-1080) + (midSpacer) + (portraits.Count * midSpacer) + (trayHeightOffset), 0);
		}
		else//if (enterFromBottom)
		{
			startPosition = new Vector3 (0, -rTransform.sizeDelta.y, 0);
		}

		endPosition = new Vector3 (rTransform.anchoredPosition.x, (-1080) + (midSpacer) + (portraits.Count * midSpacer) + (trayHeightOffset));
	}

	IEnumerator
	Lerp(Vector3 endPosition)
	{
		float lerpTime = 0.0f;

		while (lerpTime < 1f)
		{
			lerpTime += Time.deltaTime * lerpRate;
			rTransform.anchoredPosition = Vector2.Lerp (startPosition, endPosition, lerpTime);

			yield return null;
		}
	}

	IEnumerator
	Lerp(Vector3 startPosition, Vector3 endPosition)
	{
		float lerpTime = 0.0f;

		while (lerpTime < 1f)
		{
			lerpTime += Time.deltaTime * lerpRate;
			rTransform.anchoredPosition = Mathfx.Hermite (startPosition, endPosition, lerpTime);

			yield return null;
		}
	}
}
}