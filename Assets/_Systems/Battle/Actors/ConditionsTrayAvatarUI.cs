﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Battle{

public class ConditionsTrayAvatarUI : MonoBehaviour
{
	[SerializeField] RectTransform persistencyTray;
	[SerializeField] RectTransform reserveTray;
	[SerializeField] RectTransform stanceTray;

	const int reservePadding = 7;

	public void
	RenderPersistencies(List<Persistency> icons)
	{
		foreach (Persistency icon in icons)
		{
			RenderPersistencyIcon (icon.GetComponent<RectTransform>(), icons.IndexOf (icon));
		}

		//persistencyTray.GetComponent<Image> ().enabled = (icons.Count > 0);
		RenderTrays ();
	}

	void
	RenderPersistencyIcon(RectTransform icon, int i)
	{
		icon.SetAsChildOf (persistencyTray);
		icon.anchoredPosition = new Vector3 (icon.sizeDelta.x * i, 0f, 0f);
	}
		
	public void
	RenderReserves(List<Reserve> icons)
	{
		foreach (Reserve icon in icons)
		{
			RenderReserveIcon (icon.GetComponent<RectTransform> (), icons.IndexOf(icon));
		}
	}

	void
	RenderReserveIcon(RectTransform icon, int i)
	{
		icon.SetAsChildOf (reserveTray);
		icon.anchoredPosition = new Vector3 (0, 0 + (-i * (icon.sizeDelta.y + reservePadding)), 0);
	}

	public void
	RenderStances(List<Stance> icons)
	{
		foreach (Stance icon in icons)
		{
			RenderStanceIcon (icon.GetComponent<RectTransform> (), icons.IndexOf (icon));
		}
		//stanceTray.GetComponent<Image> ().enabled = (icons.Count > 0);
	}

	void
	RenderStanceIcon(RectTransform icon, int i)
	{
		icon.SetAsChildOf (stanceTray);
		icon.anchoredPosition = new Vector3 (icon.sizeDelta.x * i, 0f, 0f);
	}

	void
	RenderTrays()
	{
		if (StancesPresent ())
		{
			persistencyTray.anchoredPosition = new Vector2 (4, HealthPanelOffset() + 28);
		}
		else
		{
			persistencyTray.anchoredPosition = new Vector2 (4, HealthPanelOffset());
		}
	}

	public void
	Refresh()
	{
		RenderTrays();
	}

	int
	HealthPanelOffset()
	{
		if (GetComponent<AvatarStats> ().IsDead ())
		{
			return 0;
		}
		else
		{
			return 44;
		}
	}
	bool
	StancesPresent()
	{
		return stanceTray.childCount > 0;
	}

	int
	PersistenciesRendered()
	{
		int persistenciesRendered = 0;

		foreach (RectTransform rt in persistencyTray.transform)
		{
			persistenciesRendered++;
		}
		return persistenciesRendered;
	}
}
}