﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Battle{

[RequireComponent(typeof(ConditionsTrayAvatarUI))]
public class ConditionsTrayAvatar : ConditionsTray
{
	ConditionsTrayAvatarUI ui{ get { return GetComponent<ConditionsTrayAvatarUI> (); } }

	List<Reserve> reserves = new List<Reserve>();
	List<Stance> stances = new List<Stance>();

	Avatar avatar{ get { return GetComponent<Avatar> (); } }

	public override void//TODO: should this return a battle change?
	LoadPersistency(Persistency persistency)
	{
		if (persistency.GetDisposition () == Disposition.Friendly)
		{
			positivePersistencies.Add (persistency);
			persistency.RegisterTray (this);
		}
		else
		{
			negativePersistencies.Add (persistency);
			persistency.RegisterTray (this);//this line wasn't initially present. Not sure if that was a mistake or intentional. Keep an eye on it in case anything weird happens.
		}
						
		ui.RenderPersistencies (GetAllPersistencies());
	}

	public override void
	LoadReserve(Reserve reserve)
	{
		reserves.Add (reserve);
		reserve.GetComponent<Persistency> ().RegisterTray (this);
		ui.RenderReserves (reserves);
	}

	public override void
	LoadStance(Stance stance)
	{
		stances.Add (stance);
		stance.GetComponent<Persistency> ().RegisterTray (this);
		stance.GetComponent<Persistency> ().SetEnchanted (GetComponent<Target> ());
		ui.RenderStances (stances);
	}

	public override void
	RemovePersistency(Persistency persistency)
	{
		if (persistency.GetComponent<Reserve> ())
		{
			reserves.Remove (persistency.GetComponent<Reserve>());
		}
		else if (positivePersistencies.Contains (persistency))
		{
			positivePersistencies.Remove (persistency);
		}
		else if (negativePersistencies.Contains (persistency))
		{
			negativePersistencies.Contains(persistency);
		}

		persistency.Disable ();
		Destroy (persistency.gameObject);

		ui.RenderPersistencies(GetAllPersistencies());
		ui.RenderReserves (reserves);
		ui.RenderStances (stances);
	}
		
	protected override List<Persistency>
	GetAllPersistencies()
	{
		List<Persistency> persistencies = new List<Persistency> ();
		//persistencies.AddRange (GetArmedStancePersistencies());
		persistencies.AddRange (positivePersistencies);
		//persistencies.AddRange (GetArmedReservePersistencies ());
		persistencies.AddRange (negativePersistencies);
		return persistencies;
	}

	public override List<Persistency>
	GetAllConditions()
	{
		List<Persistency> persistencies = new List<Persistency> ();
		persistencies.AddRange (GetArmedStancePersistencies());
		persistencies.AddRange (positivePersistencies);
		persistencies.AddRange (GetArmedReservePersistencies ());
		persistencies.AddRange (negativePersistencies);
		return persistencies;
	}

	public override List<Persistency>
	GetPositiveConditions()
	{
		List<Persistency> persistencies = new List<Persistency> ();
		persistencies.AddRange (GetArmedReservePersistencies());
		persistencies.AddRange (GetArmedStancePersistencies ());
		persistencies.AddRange (positivePersistencies);

		for(int i = persistencies.Count - 1; i > 0; i--)
		{
			if (persistencies[i].ActiveWhenDead () == false && avatar.GetStats ().IsBattleFit == false)
			{
				persistencies.RemoveAt (i);
			}
		}

		return persistencies;
	}

	public override List<Persistency>
	GetNegativeConditions()
	{
		List<Persistency> persistencies = new List<Persistency> ();

		foreach(Persistency persistency in negativePersistencies)
		{
			if (persistency.ActiveWhenDead () == true || avatar.GetStats ().IsBattleFit )
			{
				persistencies.Add (persistency);
			}
		}

		return persistencies;
	}

	protected override List<Persistency>
	GetArmedReservePersistencies()
	{
		List<Persistency> reservePersistencies = new List<Persistency> ();

		foreach (Reserve reserve in reserves)
		{
			if (reserve.IsArmed ())
			{
				reservePersistencies.Add (reserve.GetComponent<Persistency> ());
			}
		}

		return reservePersistencies;
	}

	protected override List<Persistency>
	GetArmedStancePersistencies()
	{
		List<Persistency> stancePersistencies = new List<Persistency> ();

		foreach (Stance stance in stances)
		{
			if (stance.IsArmed ())
			{
				stancePersistencies.Add (stance.GetComponent<Persistency> ());
			}
		}

		return stancePersistencies;
	}

	public override int
	SkillsInReserve()
	{
		return reserves.Count;
	}
}
}