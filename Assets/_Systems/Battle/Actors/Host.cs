﻿using System.Collections.Generic;
using UnityEngine;
using Battle.Grid;

namespace Battle
{
public enum IntelligenceType{Player=0, AI=1}

public class Host : Target 
{
	ConditionsTray conditions { get { return GetComponent<ConditionsTray> (); } }
	[SerializeField] List<Avatar> avatars = new List<Avatar>();
	[SerializeField] PortraitsTray portraitsTray;

	[SerializeField] Sprite turnIcon;

	[SerializeField] public Formation.Formation formation;
	public bool advantage = false;

	public void
	InitialiseBattle()
	{
		List<RectTransform> portraits = new List<RectTransform>();

		foreach (Avatar avatar in avatars)
		{
			portraits.Add(avatar.GetUI().GetPortrait());
		}

		portraitsTray.AddPortraits (portraits);
		portraitsTray.RenderPortraits ();
	}

	//TODO: this is surplus to requirements. This condition can be checked in ConditionOperations.cs
	public bool
	CheckDefeatStatus()
	{
		foreach (Avatar avatar in avatars)
		{
			if (avatar.GetStats().IsBattleFit == true)
			{
				return false;
			}
		}
		return true;
	}

	public void
	InitialiseSelectCharacters()
	{
		bool trigger = false;
		
		foreach (Avatar avatar in avatars)
		{
			avatar.SetMustSelectable(ref trigger);
		}

		if (!trigger)
		{
			foreach (Avatar avatar in avatars)
			{
				avatar.SetSelectable();
			}
		}
	}

	public ConditionsTray
	GetConditions()
	{
		return conditions;
	}

	public Sprite
	GetTurnIconSprite()
	{
		return turnIcon;
	}

	public List<Avatar>
	GetAvatars()
	{
		return avatars;
	}

	public void
	CreateAvatars(GridCell[,] battleStations)
	{
		for (int i = 0; i < formation.entries.Count; i++)
		{
			GameObject newAvatar = Instantiate (formation.entries [i].character.battleAvatarPrefab);
			newAvatar.transform.SetParent (transform);
			int valX = (int)formation.entries[i].position.x;
			int valY = (int)formation.entries[i].position.y;
			newAvatar.GetComponent<Avatar> ().Initialise (formation.entries [i].character, battleStations[valX, valY]);
			avatars.Add (newAvatar.GetComponent<Avatar> ());
		}
	}

	public void
	EndBattle()
	{			
		
		//if (disposition == Disposition.Hostile)
		//{
			Destroy (gameObject);
		//}
		//else//if player host
		//{
		//	DestroyAvatars ();
		//	portraitsTray.EndBattle ();
		//}
	}

	void
	DestroyAvatars()
	{
		foreach (Avatar avatar in avatars)
		{
			Destroy (avatar.gameObject);
		}
		avatars.Clear ();
	}

	public void
	RemoveCharacter(Avatar avatar)
	{
		avatars.Remove (avatar);
		BattleLogic.manager.turnOrderHistory.RemoveAllTurnsFor (avatar);
		avatar.SelfDestruct ();
		portraitsTray.RenderPortraits ();
	}
}
}