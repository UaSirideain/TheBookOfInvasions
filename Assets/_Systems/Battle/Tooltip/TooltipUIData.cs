﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Battle;

[CreateAssetMenu(fileName = "New Tooltip Graphics Pack", menuName = "Tooltip/Graphics Pack", order = 1)]
public class TooltipUIData : ScriptableObject
{
	[SerializeField] Sprite topLeft;
	[SerializeField] Sprite topRight;
	[SerializeField] Sprite bottomLeft;
	[SerializeField] Sprite bottomRight;

	[SerializeField] Sprite leftTop;
	[SerializeField] Sprite rightTop;
	[SerializeField] Sprite leftBottom;
	[SerializeField] Sprite rightBottom;

	public Sprite
	GetOriginPoint(Vector2 pivot, OriginPoint origin)
	{
		if (origin == OriginPoint.Ends)
		{
			return GetVerticalFor (pivot);
		}
		else//if(origin == OriginPoint.Sides)
		{
			return GetHorizontalFor (pivot);
		}
	}

	Sprite
	GetHorizontalFor(Vector2 pivot)
	{
		if (pivot.x == 1 && pivot.y == 1)
		{
			return topRight;
		}
		else if (pivot.x == 1 && pivot.y == 0)
		{
			return bottomRight;
		}
		else if (pivot.x == 0 && pivot.y == 1)
		{
			return topLeft;
		}
		else//if (pivot.x == 0 && pivot.y == 0)
		{
			return bottomLeft;
		}
	}

	Sprite
	GetVerticalFor(Vector2 pivot)
	{
		if (pivot.x == 1 && pivot.y == 1)
		{
			return rightTop;
		}
		else if (pivot.x == 1 && pivot.y == 0)
		{
			return rightBottom;
		}
		else if (pivot.x == 0 && pivot.y == 1)
		{
			return leftTop;
		}
		else//if (pivot.x == 0 && pivot.y == 0)
		{
			return leftBottom;
		}
	}
}