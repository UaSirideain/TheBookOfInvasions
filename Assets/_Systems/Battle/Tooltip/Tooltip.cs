﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Battle{

public enum OriginPoint{Sides, Ends}

public class Tooltip : MonoBehaviour
{
	[SerializeField] RectTransform rt;
	[SerializeField] Image background;
	[SerializeField] Text title;
	[SerializeField] Text description;

	[SerializeField] TooltipUIData tooltipGraphics;

	const float nameHeight = 55;
	const float lineHeight = 20;

	Vector3 hidden = new Vector3(0, 0, 0);
	Vector3 shown = new Vector3(1, 1, 1);

	public void
	WriteText(string title, string description)
	{
		this.title.text = title;
		this.description.text = description;
	}

	public void
	Show(RectTransform target, OriginPoint origin)
	{
		SetSize ();
		SetPivot (target, origin);
		rt.localScale = shown;
	}

	void
	SetSize()
	{
		rt.sizeDelta = new Vector2(600, nameHeight + (lineHeight * CalculateAmountOfLines(description.text)));
	}

	void
	SetPivot(RectTransform target, OriginPoint origin)
	{
		Vector3 targetCentre = new Vector3 (target.rect.center.x + target.position.x, target.rect.center.y + target.position.y, 0);

		float positionX;
		float positionY;

		float pivotX;
		float pivotY;

		if (target.position.x < Screen.width / 2)
		{
			pivotX = 0f;
			positionX = targetCentre.x + (target.sizeDelta.x / 2);
		}
		else
		{
			pivotX = 1f;
			positionX = targetCentre.x - (target.sizeDelta.x / 2);
		}

		if (target.position.y < Screen.height / 2)
		{
			pivotY = 0f;
			positionY = targetCentre.y - (target.sizeDelta.y / 2);
		}
		else
		{
			pivotY = 1f;
			positionY = targetCentre.y + (target.sizeDelta.y / 2);
		}

		Vector2 tooltipPivot = new Vector2 (pivotX, pivotY);
		rt.pivot = tooltipPivot;
		background.sprite = tooltipGraphics.GetOriginPoint (tooltipPivot, origin);

		Vector3 tooltipAnchor = new Vector3 (positionX, positionY, 0);
		rt.anchoredPosition = tooltipAnchor;
	}

	public void
	Hide()
	{
		rt.localScale = hidden;
	}

	int CalculateAmountOfLines(string passage)
	{
		Canvas.ForceUpdateCanvases ();
		return description.cachedTextGenerator.lineCount;
	}
}
}