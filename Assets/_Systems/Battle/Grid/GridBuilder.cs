﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Formation;

namespace Battle.Grid{

public class GridBuilder : MonoBehaviour
{
	[SerializeField] Transform gridOriginMarker;
	[SerializeField] Transform gridTerminationMarker;
	[SerializeField] GridCell gridCellPrefab;

	[SerializeField] Transform allyFormationMarker;
	[SerializeField] Transform enemyFormationMarker;

	int xS = 15;
	int zS = 15;

	const float cellSize = 1.4f;

	public void
	Build(GridInfo gridInfo)
	{
		SetGridSize (gridInfo);
		Generate(gridInfo);
		AssignMarkers (gridInfo);
	}

	void
	SetGridSize(GridInfo gridInfo)
	{
		xS = (int)(Mathf.Abs(gridOriginMarker.position.x - gridTerminationMarker.position.x) / cellSize + cellSize);
		zS = (int)(Mathf.Abs(gridOriginMarker.position.z - gridTerminationMarker.position.z) / cellSize + cellSize);

		gridInfo.grid = new GridCell[xS, zS];
	}

	void
	Generate(GridInfo gridInfo)
	{
		for (int i = 0, z = 0; z < zS; z++)
		{
			for (int x = 0; x < xS; x++, i++)
			{
				gridInfo.grid [x, z] = CreateCell(x, z);
			}
		}
	}

	GridCell
	CreateCell(int x, int z)
	{
		GridCell cell = Instantiate (gridCellPrefab);

		cell.SetPosition (x, z);

		cell.transform.SetParent (gridOriginMarker);
		cell.transform.localPosition = new Vector3 (x, 0, z) * cellSize;

		return cell;
	}

	public void
	AssignMarkers(GridInfo gridInfo)
	{
		gridInfo.allyFormationOrigin = GetCellFor (allyFormationMarker, gridInfo);
		gridInfo.enemyFormationOrigin = GetCellFor (enemyFormationMarker, gridInfo);
	}

	public GridCell
	GetCellFor(Transform marker, GridInfo gridInfo)
	{
		float distance = 100f;
		GridCell cell = null;

		for (int x = 0; x < gridInfo.grid.GetLength(0); x++)
		{
			for (int y = 0; y < gridInfo.grid.GetLength (1); y++)
			{
				float distanceBetweenCellAndMarker = Vector3.Distance (gridInfo.grid [x, y].transform.position, marker.position);

				if(distanceBetweenCellAndMarker < distance)
				{
					distance = distanceBetweenCellAndMarker;
					cell = gridInfo.grid [x, y];
				}
			}
		}

		MarkerNotFoundError (cell, marker);

		return cell;
	}

	void
	MarkerNotFoundError(GridCell cell, Transform marker)
	{
		if (cell == null)
		{
			print ("WARNING: Marker: " + marker.name + " not found within grid confines.");
		}
	}

	///GIZMOS

	void
	SnapToGrid(Transform marker)
	{
		Vector3 rawPosition = marker.localPosition;

		float x = Mathf.Round (rawPosition.x / cellSize) * cellSize;
		float y = Mathf.Round (rawPosition.y / cellSize) * cellSize;
		float z = Mathf.Round (rawPosition.z / cellSize) * cellSize;

		marker.localPosition = new Vector3 (x, y, z);
	}


	GizmoRect gridGizmo;
	GizmoRect allyFormationGizmo;
	GizmoRect enemyFormationGizmo;

	Vector3
	CalcVec(Transform marker)
	{
		float x = marker.position.x + cellSize * FindObjectOfType<ActiveFormation> ().GetFormation ().tilesAcross - cellSize;
		float z = allyFormationMarker.position.z + cellSize * FindObjectOfType<ActiveFormation>().GetFormation().tilesBelow - cellSize;

		return new Vector3 (x, marker.position.y, z);
	}

	void
	OnDrawGizmos()
	{
		if (gridGizmo == null)
		{
			gridGizmo = new GizmoRect ();
			allyFormationGizmo = new GizmoRect ();
			enemyFormationGizmo = new GizmoRect ();
		}

		SnapToGrid (allyFormationMarker);
		SnapToGrid (enemyFormationMarker);
		SnapToGrid (gridOriginMarker);
		SnapToGrid (gridTerminationMarker);

		gridGizmo.UpdateGizmoData (gridOriginMarker, gridTerminationMarker);
		allyFormationGizmo.UpdateGizmoData (allyFormationMarker, CalcVec(allyFormationMarker));
		enemyFormationGizmo.UpdateGizmoData (enemyFormationMarker, CalcVec(enemyFormationMarker));

		DrawDaGizms (gridGizmo, Color.red);
		DrawDaGizms (allyFormationGizmo, Color.green);
	}

	void
	DrawDaGizms(GizmoRect data, Color colour)
	{
		Gizmos.color = colour;

		Gizmos.DrawLine(data.bottomLeft, 	data.bottomRight);
		Gizmos.DrawLine(data.bottomRight, 	data.topRight);
		Gizmos.DrawLine(data.topRight, 		data.topLeft);
		Gizmos.DrawLine(data.topLeft, 		data.bottomLeft);
	}
}
}