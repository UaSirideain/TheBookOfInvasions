﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle.Grid{

public class GizmoRect : System.Object
{
	public float x1;
	public float z1;
	public float y1;

	public float x2;
	public float z2;

	public Vector3 bottomLeft;
	public Vector3 bottomRight;
	public Vector3 topRight;
	public Vector3 topLeft;

	public void
	UpdateGizmoData(Transform t1, Transform t2)
	{
		x1 = t1.position.x;
		z1 = t1.position.z;
		y1 = t1.position.y;

		x2 = t2.position.x;
		z2 = t2.position.z;

		bottomLeft = new Vector3 (x1, y1, z1);
		bottomRight = new Vector3 (x2, y1, z1);
		topRight = new Vector3 (x2, y1, z2);
		topLeft = new Vector3 (x1, y1, z2);
	}

	public void
	UpdateGizmoData(Transform t1, Vector3 t2)
	{
		x1 = t1.position.x;
		z1 = t1.position.z;
		y1 = t1.position.y;

		x2 = t2.x;
		z2 = t2.z;

		bottomLeft = new Vector3 (x1, y1, z1);
		bottomRight = new Vector3 (x2, y1, z1);
		topRight = new Vector3 (x2, y1, z2);
		topLeft = new Vector3 (x1, y1, z2);
	}
}
}