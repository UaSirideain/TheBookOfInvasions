﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle.Grid{

public class GridInfo : System.Object
{
	public GridCell[,] grid;
	public GridCell allyFormationOrigin;
	public GridCell enemyFormationOrigin;
}
}