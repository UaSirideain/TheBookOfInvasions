﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle.Grid{

public class GridCell : MonoBehaviour
{
	int x;
	int z;

	GameObject occupiedBy;

	Vector2 index;

	public Vector2
	GetIndex()
	{
		return new Vector2 (x, z);
	}

	public void
	SetPosition(int x, int z)
	{
		this.x = x;
		this.z = z;
	}

	public void
	SetOccupied(GameObject occupier)
	{
		occupiedBy = occupier;
	}

	public GameObject
	GetOccupier()
	{
		return occupiedBy;
	}
}
}