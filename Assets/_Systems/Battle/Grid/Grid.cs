﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Formation;

namespace Battle.Grid{

public enum MarkerType{PlayerFormation=0, EnemyFormation=1}

public class Grid : MonoBehaviour
{
	[SerializeField] GridInfo gridInfo = new GridInfo();

	public void
	Initialise()
	{
		GetComponent<GridBuilder> ().Build (gridInfo);
	}

	public GridCell[,]
	GetAllyFormationBounds(Host host)
	{
		return GetFormationBounds (host, gridInfo.allyFormationOrigin.GetIndex());
	}

	public GridCell[,]
	GetEnemyFormationBounds(Host host)
	{
		return GetFormationBounds (host, gridInfo.enemyFormationOrigin.GetIndex());
	}

	public GridCell[,]
	GetFormationBounds(Host host, Vector2 formation)
	{
		GridCell[,] cells = new GridCell[host.formation.tilesAcross, host.formation.tilesBelow];

		for(int newX = 0, x = (int)formation.x; x < formation.x + host.formation.tilesAcross; x++, newX++)
		{
			for (int newY = 0, y = (int)formation.y; y < formation.y + host.formation.tilesBelow; y++, newY++)
			{
				cells[newX, newY]  = gridInfo.grid [x, y];
			}
		}

		return cells;
	}

	public List<GridCell>
	GetSquaresWithinRangeOf(GridCell cell, int range)
	{
//		int cellX = (int)cell.GetIndex ().x;
//		int cellY = (int)cell.GetIndex ().y;

		List<GridCell> newCells = new List<GridCell> ();

		for (int x = 0; x < gridInfo.grid.GetLength (0); x++)
		{
			for (int y = 0; y < gridInfo.grid.GetLength (1); y++)
			{
				if (GetDistanceBetweenCells (gridInfo.grid[x, y], cell) <= range)
				{
					newCells.Add (gridInfo.grid [x, y]);
				}
			}
		}

		return newCells;
	}

	public int
	GetDistanceBetweenCells(GridCell cell1, GridCell cell2)
	{
		float rawDistance = (cell1.GetIndex ().x - cell2.GetIndex ().x) + (cell1.GetIndex ().y - cell2.GetIndex ().y);
		return Mathf.Abs((int)rawDistance);
	}
}
}