﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(TextMesh))]
public class WorldSpaceUI : MonoBehaviour
{
	[SerializeField]
	bool maintainPixelSize = true;

	[SerializeField]
	float distanceFromCamera;
	[SerializeField]
	float sizeAtDistance;

	[Header("OnValidate()")]
	[SerializeField]
	float percievedSize;

	[SerializeField]
	Vector3 initialScale;

	float axisScale;

	[SerializeField]
	Transform cam;



	void OnValidate()
	{
		percievedSize = distanceFromCamera / sizeAtDistance;
		//initialScale = transform.localScale;
	}


	void Awake()
	{
		foreach (GameObject nextCamera in GameObject.FindGameObjectsWithTag ("MainCamera"))
		{
			if (nextCamera.GetComponent<Camera>().enabled)
			{
				cam = nextCamera.transform;
			}
		}
	}
		

	//This could potentially be subscribed to an event under another name to save calls. Nah, there wont be many of them.
	void Update ()
	{
		transform.LookAt (transform.position + cam.rotation * Vector3.forward, cam.transform.rotation * Vector3.up);

		if (maintainPixelSize)
		{
			axisScale = cam.CalculateLocalDistanceTo (transform, "z") / percievedSize;

			transform.localScale = new Vector3 (axisScale, axisScale, axisScale);
		}
	}
}
