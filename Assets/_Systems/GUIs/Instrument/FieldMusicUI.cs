﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//TODO: Arrange functions to represent trigger order
//TODO: Namespace Field.Music
public class FieldMusicUI : MonoBehaviour
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } } //TODO: Do not use GetComponent<>()

	[SerializeField] Text strain;

	public IEnumerator
	ChangeStrain(MusicalStrain newMode)
	{
		strain.text = newMode.ToString ();

		StartCoroutine(FadeIn ());

		yield return null;
	}

	int timerTicket = 0;

	IEnumerator
	StartTimer(int myTimerTicket)
	{
		yield return new WaitForSeconds (2);

		if (myTimerTicket == timerTicket)
		{
			StartCoroutine(FadeOut ());
		}
	}

	public void
	TurnOffUI()
	{
		currentFadeTicket++;
		timerTicket++;
		canvas.Disable ();
	}

	int currentFadeTicket = 0;

	IEnumerator
	FadeIn ()
	{
		timerTicket++;
		int myTimerTicket = timerTicket;
		yield return Fade(0f, 1f, .2f);
		canvas.Enable ();
		yield return StartTimer (myTimerTicket);
	}

	IEnumerator
	FadeOut()
	{
		yield return Fade (1f, 0f, 1.1f);
		canvas.Disable ();
	}

	IEnumerator
	Fade(float start, float end, float duration)
	{
		currentFadeTicket++;
		int myFadeTicket = currentFadeTicket;
		float t = 0f;

		while (t < 1f && myFadeTicket == currentFadeTicket)
		{
			canvas.alpha = Mathf.Lerp (start, end, t);
			t += Time.deltaTime / duration;

			yield return null;
		}
	}
}