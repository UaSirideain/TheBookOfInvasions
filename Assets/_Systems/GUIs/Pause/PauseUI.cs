﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GUIManagement;

public class PauseUI : MonoBehaviour, IGUI, IInputtable
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
	GUIController controller;
	bool uiOpen = false;
	BackgroundDimmer backgroundDimmer { get { return GetComponentInChildren<BackgroundDimmer> (); } }

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (){ InputState.UI, InputState.FullscreenUI, InputState.SettingsUI }, ExitUI_Input);
	}

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	public void
	Activate()
	{
		backgroundDimmer.DimBackground ();
		canvas.Enable ();
		controller.RegisterActivatedUI (this);
		FindObjectOfType<PauseController> ().Pause ();
		uiOpen = true;
	}

	public void
	Deactivate()
	{
		canvas.Disable ();
		controller.RegisterDeactivatedGUI (this);
		FindObjectOfType<PauseController> ().Unpause ();
		uiOpen = false;
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.PausingUI;
	}

	void
	ExitUI_Input()
	{
		if (uiOpen)
		{
			Deactivate ();
		}
		else
		{
			Activate ();
		}
	}
}