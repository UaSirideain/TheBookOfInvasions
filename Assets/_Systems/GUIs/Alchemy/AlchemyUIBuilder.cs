﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlchemyUIBuilder : MonoBehaviour
{
	[SerializeField] AlchemyUIColumn salvePane;
	[SerializeField] AlchemyUIColumn floraPane;

	Inventory inventory {get { return FindObjectOfType<Inventory> (); }}

	public void
	Build()
	{
		salvePane.RefreshButtons (inventory.GetEntriesOfCategory (InventoryCategory.Salve));
		floraPane.RefreshButtons (inventory.GetEntriesOfCategory (InventoryCategory.Flora));
	}
}