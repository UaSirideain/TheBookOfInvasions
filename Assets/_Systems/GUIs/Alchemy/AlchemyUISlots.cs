﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlchemyUISlots : MonoBehaviour
{
	[SerializeField] AlchemyUISlot slot1;
	[SerializeField] AlchemyUISlot slot2;
	[SerializeField] AlchemyUISlot slot3;

	public void
	CreateSalve()
	{				
		List<AbilityData> mix = GetAbilitiesFromSlots ();

		foreach (RecipeData recipe in FindObjectOfType<AlchemyUI>().recipes)
		{			
			if (recipe.CheckMatch (mix))
			{
				AdjustInventoryFor (slot1);
				AdjustInventoryFor (slot2);
				AdjustInventoryFor (slot3);
				FindObjectOfType<Inventory> ().AddInventoryEntry (recipe.GetSalve ());

				FindObjectOfType<AlchemyUIBuilder> ().Build ();

				RenderSlots ();
				break;
			}
		}
	}

	List<AbilityData>
	GetAbilitiesFromSlots()
	{
		List<AbilityData> mix = new List<AbilityData> ();

		if (slot1.IsEmpty () == false)
		{
			mix.Add (slot1.GetAbilityData ());
		}
		if (slot2.IsEmpty () == false)
		{
			mix.Add (slot2.GetAbilityData ());
		}
		if (slot3.IsEmpty () == false)
		{
			mix.Add (slot3.GetAbilityData ());
		}

		return mix;
	}

	void
	AdjustInventoryFor(AlchemyUISlot slot)
	{
		if (!slot.IsEmpty ())
		{
			FindObjectOfType<Inventory> ().RemoveInventoryEntry (slot.RemoveEntry ());
		}
	}

	public void
	AddFloraToNextSlot (InventoryEntry entry)
	{
		if (slot1.IsEmpty ())
		{
			slot1.AddEntry(entry);
		}
		else if (slot2.IsEmpty ())
		{
			slot2.AddEntry(entry);
		}
		else if (slot3.IsEmpty ())
		{
			slot3.AddEntry(entry);
		}

		RenderSlots ();
	}

	public void
	RemoveFloraFromSlot(int index)
	{
		if (index == 1)
		{
			slot1.RemoveEntry ();
		}
		else if (index == 2)
		{
			slot2.RemoveEntry ();
		}
		else if (index == 3)
		{
			slot3.RemoveEntry ();
		}

		ReorganiseSlots ();
		RenderSlots ();
	}

	void
	ReorganiseSlots()
	{
		if (slot1.IsEmpty () && !slot2.IsEmpty ())
		{
			slot1.AddEntry (slot2.RemoveEntry ());
		}
		if (slot2.IsEmpty () && !slot3.IsEmpty ())
		{
			slot2.AddEntry (slot3.RemoveEntry ());
		}
	}

	void
	RenderSlots()
	{
		slot1.Render();
		slot2.Render();
		slot3.Render();
	}
}