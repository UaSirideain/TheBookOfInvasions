﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlchemyUISlot : MonoBehaviour
{
	InventoryEntry currentOccupier;
	[SerializeField] Image entryImage;

	public bool
	IsEmpty()
	{
		return currentOccupier == null;
	}

	public void
	AddEntry(InventoryEntry newOccupier)
	{
		currentOccupier = newOccupier;
		entryImage.sprite = currentOccupier.GetAbility ().GetUIIcon ();
	}

	public InventoryEntry
	RemoveEntry()
	{
		InventoryEntry oldOccupier = currentOccupier;
		currentOccupier = null;
		return oldOccupier;
	}

	public AbilityData
	GetAbilityData()
	{
		return currentOccupier.GetAbility ();
	}

	public void
	Render()
	{
		entryImage.enabled = !IsEmpty ();
	}
}