﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlchemyUIColumn : MonoBehaviour
{
	List<AlchemyUIColumnButton> activeButtons = new List<AlchemyUIColumnButton>();
	RectTransform t { get { return GetComponent<RectTransform> (); } }

	[SerializeField] AlchemyUIColumnButton columnButtonPrefab;

	public void
	RefreshButtons(List<InventoryEntry> entries)
	{
		MergeLists (entries);
		RenderButtons ();
	}

	void
	MergeLists(List<InventoryEntry> entries)
	{
		for (int i = 0; i < activeButtons.Count; i += 0)
		{
			if (!entries.Contains (activeButtons [i].entry))
			{
				DestroyImmediate (activeButtons [i].gameObject);
				activeButtons.RemoveAt (i);
			}
			else
			{
				entries.Remove (entries [i]);
				i++;
			}
		}

		foreach (InventoryEntry entry in entries)
		{
			AlchemyUIColumnButton newB = Instantiate (columnButtonPrefab);
			newB.Initialise (entry, transform.parent.GetComponent<AlchemyUI>());
			activeButtons.Add (newB);
		}
	}
		
	void
	RenderButtons()
	{
		for(int i = 0; i < activeButtons.Count; i++)
		{
			RectTransform rt = activeButtons[i].GetComponent<RectTransform> ();

			rt.SetAsChildOf(t);

			rt.anchoredPosition = new Vector2 (0, -i * rt.sizeDelta.y);
			rt.sizeDelta = new Vector2 (t.sizeDelta.x, 30f);
		}
	}
}