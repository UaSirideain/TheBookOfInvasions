﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Recipe", menuName = "Alchemy/New Recipe", order = 1)]
public class RecipeData : ScriptableObject
{
	[SerializeField] List<AbilityData> requiredFlora;
	[SerializeField] AbilityData salveProduced;

	public bool
	CheckMatch(List<AbilityData> presentFlora)
	{
		foreach (AbilityData flora in presentFlora)
		{
			if (requiredFlora.Contains (flora) == false)
			{
				return false;
			}
		}

		foreach (AbilityData flora in requiredFlora)
		{
			if (presentFlora.Contains (flora) == false)
			{
				return false;
			}
		}
		return true;
	}

	public List<AbilityData>
	GetRequirements()
	{
		return requiredFlora;
	}

	public AbilityData
	GetSalve()
	{
		return salveProduced;
	}
}