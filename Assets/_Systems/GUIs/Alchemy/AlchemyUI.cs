﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GUIManagement;

public class AlchemyUI : MonoBehaviour, IGUI, IInputtable
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
	GUIController controller;
	bool uiOpen = false;

	[SerializeField] AlchemyUISlots slots;

	[SerializeField] public List<RecipeData> recipes;

	public void
	Clicko(AlchemyUIColumnButton button)
	{
		slots.AddFloraToNextSlot (button.entry);
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> () {InputState.UI, InputState.FullscreenUI, InputState.SettingsUI, InputState.Battle}, Escape);
		input.SubscribeToStringInput (InputMode.Down, InputButton.OpenAlchemyUI, new List<InputState> (){InputState.FullscreenUI, InputState.PausingUI, InputState.Battle }, O);
	}

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	public void
	Activate()
	{
		if (uiOpen == false)
		{
			uiOpen = true;
			canvas.Enable ();
			controller.RegisterActivatedUI (this);
			GetComponent<AlchemyUIBuilder> ().Build ();
		}
	}

	public void
	Deactivate()
	{
		if (uiOpen == true)
		{
			uiOpen = false;
			canvas.Disable ();
			controller.RegisterDeactivatedGUI (this);
		}
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.UI;
	}

	void
	Escape()
	{
		Deactivate ();
	}

	void
	O()
	{
		if (uiOpen)
		{
			Deactivate ();
		}
		else
		{
			Activate ();
		}
	}
}