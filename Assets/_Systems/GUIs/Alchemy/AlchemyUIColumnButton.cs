﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlchemyUIColumnButton : MonoBehaviour
{
	[SerializeField] Image image;
	[SerializeField] Text text;

	[HideInInspector] public InventoryEntry entry;
	AlchemyUI alchemyUI;

	public void
	Initialise(InventoryEntry newEntry, AlchemyUI newAlchemyUI)
	{
		alchemyUI = newAlchemyUI;
		entry = newEntry;
		text.text = entry.GetName ();
	}

	public void
	OnClick()
	{
		alchemyUI.Clicko (this);
	}

	public void
	OnMouseExit()
	{
		image.color = new Color(1f, 1f, 1f, 0f);
		text.color = Color.white;
	}

	public void
	OnMouseEnter()
	{
		image.color = new Color(1f, 1f, 1f, 1f);
		text.color = Color.black;
	}
}