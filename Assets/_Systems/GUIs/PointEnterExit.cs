﻿using UnityEngine;
using UnityEngine.UI;

public class PointEnterExit : MonoBehaviour
{
	[SerializeField] Text text;

	Color defaultColour = Color.white;
	//[SerializeField] Color hoverColour;

	void
	Awake()
	{
		defaultColour = text.color;
	}

	public void
	OnPointerEnter()
	{
		text.color = new Color (1f, 1f, 1f, 1f);
	}

	public void
	OnPointerExit()
	{
		text.color = defaultColour;
	}
}