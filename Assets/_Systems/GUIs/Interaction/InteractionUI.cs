﻿using GUIManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionUI : MonoBehaviour, IInputtable, IGUI
{
	CanvasGroup canvas {get {return GetComponent<CanvasGroup>();}}

	[SerializeField] Button interactionOptionPrefab;
	[SerializeField] RectTransform panel;

	List<Button> buttons = new List<Button> ();

	const float margin = 33f;

	GUIController controller;

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToNumericInput (InputMode.Down, new List<InputState> (), NumericInput);
	}

	public void
	Activate(List<InteractionTrigger> triggers, InteractionDelegate del)
	{
		controller.RegisterActivatedUI (this);

		CreateButtons (triggers, del);
		RenderButtonsToScreen (buttons);
		canvas.Enable ();
	}

	public void
	Deactivate()
	{
		canvas.Disable ();
		DestroyButtons ();
		controller.RegisterDeactivatedGUI (this);
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.UI;
	}

	void
	NumericInput(string numeric)
	{
		int buttonIndex;
		if (numeric.NumericStringToArrayIndex (out buttonIndex))
		{
			if (buttonIndex < buttons.Count)
			{
				buttons [buttonIndex].onClick.Invoke ();
			}
		}
	}

	void
	DestroyButtons()
	{
		while (buttons.Count > 0)
		{
			Destroy(buttons [0]);
			buttons [0].gameObject.SetActive (false);
			buttons.Remove (buttons [0]);
		}

		buttons.Clear ();
	}

	void
	CreateButtons(List<InteractionTrigger> triggers, InteractionDelegate del)
	{
		for (int i = 0; i < triggers.Count; i++)
		{
			Button newButton = Instantiate (interactionOptionPrefab);
			InteractionTrigger trigger = triggers [i];
			newButton.onClick.AddListener (delegate { del(trigger); } );
			newButton.onClick.AddListener (delegate { Deactivate (); } );
			newButton.GetComponent<InteractionButton> ().InitialiseText (i, triggers [i].GetTriggerName());

			buttons.Add (newButton);
		}
	}

	void
	RenderButtonsToScreen(List<Button> buttons)
	{
		float runningXPosition = 0f;

		foreach(Button button in buttons)
		{
			RectTransform rt = button.GetComponent<RectTransform> ();
			rt.SetParent (panel);

			rt.localScale = new Vector2 (1, 1);
			rt.anchoredPosition = new Vector2 (runningXPosition, 0f);

			runningXPosition += rt.sizeDelta.x + margin;
		}

		float panelWidth = runningXPosition - margin;
		panel.sizeDelta = new Vector2 (panelWidth, panel.sizeDelta.y);
	}
}