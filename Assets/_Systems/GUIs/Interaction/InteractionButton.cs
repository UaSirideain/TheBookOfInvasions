﻿using UnityEngine;
using UnityEngine.UI;

public class InteractionButton : MonoBehaviour
{
	RectTransform rt {get{ return GetComponent<RectTransform>(); }}

	[SerializeField] Image icon;
	[SerializeField] Text numericText;
	[SerializeField] RectTransform rtInteractions;
	[SerializeField] Text interactionText;

	public void
	InitialiseText(int index, string textPlate)
	{
		numericText.text = index.ArrayIndexToNumericString();
		interactionText.text = textPlate;
		interactionText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
		rt.sizeDelta = new Vector2 (rtInteractions.sizeDelta.x + rtInteractions.anchoredPosition.x, rt.sizeDelta.y);
	}
//
//	public void
//	Render(int index, InteractionHandler interaction)
//	{
//		numericText.text = (index.ToNumericKey()).ToString();
//		interactionText.text = interaction.GetName().ToString();
//		interactionText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
//		rt.sizeDelta = new Vector2 (rtInteractions.sizeDelta.x + rtInteractions.anchoredPosition.x, rt.sizeDelta.y);
//		name = index.ToString();
//	}
//
//	public void
//	Render(int index, IInteractable interaction)
//	{
//		icon.sprite = interaction.GetUIIcon ();
//		numericText.text = (index.ToNumericKey()).ToString();
//		interactionText.text = interaction.GetUIName().ToString();
//		interactionText.GetComponent<ContentSizeFitter> ().SetLayoutHorizontal ();
//		rt.sizeDelta = new Vector2 (rtInteractions.sizeDelta.x + rtInteractions.anchoredPosition.x, rt.sizeDelta.y);
//		name = index.ToString();
//	}
}