﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class QuickItemsUIEntry : MonoBehaviour
{
    [SerializeField] Text amountText;
    private bool isAssigned = false;

    public bool
    ButtonIsAssigned()
    {
        return isAssigned;
    }

    public void
    AssignButton(bool assign)
    {
        isAssigned = assign;
    }

    public void
	Brighten(float duration)
	{
		currentFadeTicket++;
		StartCoroutine(ChangeColour (1f, duration));
	}

	public void
	Darken(float duration)
	{
		currentFadeTicket++;
		StartCoroutine(ChangeColour (.15f, duration));
	}

	public void
	Blackout()
	{
		GetComponent<Image> ().color = new Color (1, 1, 1, 0);
		GetComponentInChildren<Text> ().color = new Color (1, 1, 1, 0);
	}

	int currentFadeTicket = 0;

	public IEnumerator
	FadeOut()
	{
		int myFadeTicket = currentFadeTicket;

		yield return new WaitForSeconds (2);

		if (currentFadeTicket == myFadeTicket)
		{
			StartCoroutine (ChangeColour (0f, 1f));
		}
	}

	IEnumerator
	ChangeColour(float opacity, float duration)
	{
		int myFadeTicket = currentFadeTicket;

		Image image = GetComponent<Image> ();
		Text text = GetComponentInChildren<Text> ();

		float t = 0f;
		float r = image.color.r;
		float g = image.color.g;
		float b = image.color.b;
		float a = image.color.a;

		while (t < 1.2f && myFadeTicket == currentFadeTicket)
		{
			image.color = Color.Lerp (new Color(r, g, b, a), new Color(r, g, b, opacity), t);
			text.color = Color.Lerp (new Color(r, g, b, a), new Color(r, g, b, opacity), t);
			amountText.color = Color.Lerp (new Color(r, g, b, a), new Color(r, g, b, opacity), t);
			t += Time.deltaTime / duration;

			yield return null;
		}
		yield return null;
	}
}