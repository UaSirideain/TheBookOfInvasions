﻿using GUIManagement;
using System.Collections.Generic;
using UnityEngine;

public class QuickItemsUIController : MonoBehaviour, IGUI, ICountDown
{
    CanvasGroup canvas { get { return GetComponent<CanvasGroup>(); } }

    [SerializeField] Sprite emptySlot;
    List<QuickItemsUIEntry> hotbarButtons = new List<QuickItemsUIEntry>();
    QuickItemsUIRenderer buttonRenderer;
	[SerializeField] GameObject categoryPanel;

	GUIController controller;

    bool editingHotbar = false;

	//I think these are just random equipped abilities rather than specifically equipped item or skill
	private int readiedSkillIndex = 0;
	private int readiedItemIndex = 0;

	public void
	SetIndicesOfReadiedAbilities(List<InventoryEntry> assigned, List<InventoryEntry> readied)
	{
		readiedSkillIndex = assigned.IndexOf(readied[0]);
		readiedItemIndex = assigned.IndexOf (readied [1]);
	}

    public void
    EnableEditMode()
    {
        editingHotbar = true;
		canvas.Enable ();
        foreach (QuickItemsUIEntry button in hotbarButtons)
        {
            button.Darken(.2f);
        }
		categoryPanel.SetActive (true);
    }

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.HUD;
	}

    public void
    DisableEditMode()
    {
        editingHotbar = false;
		canvas.Disable ();
        foreach (QuickItemsUIEntry button in hotbarButtons)
        {
            button.Blackout();
        }
		categoryPanel.SetActive (false);
    }

	public void
	Deactivate()
	{
		canvas.Disable ();
	}

    public void
    FadeOutButtons()
    {
		if (editingHotbar == false)
		{
			foreach (QuickItemsUIEntry button in hotbarButtons)
			{
				StartCoroutine (button.FadeOut ());
			}
			controller.RegisterDeactivatedGUI (this);
		}
    }

	public void
	RenderHotbar()
	{
		if (!editingHotbar)
		{
			canvas.Enable ();
			buttonRenderer.RenderButtonsNormal(hotbarButtons, readiedSkillIndex, readiedItemIndex);
			controller.RegisterActivatedUI (this);
		}
	}

	public void
	TurnOffUI()
	{
		FadeOutButtons ();
	}

	public void
	RenderHotbarForEditor(int index)
	{
		QuickItemsUIEntry heldButton = hotbarButtons[index];

		if (editingHotbar)
		{
			buttonRenderer.DarkenPreviouslyHeldButton (hotbarButtons, heldButton);
		}
	}

    public void
	BrightenHeldButtonForEditor(int index)
    {
		if(editingHotbar)
        {
			QuickItemsUIEntry heldButton = hotbarButtons[index];
			canvas.Enable ();

            buttonRenderer.BrightenHeldButtonEditor(hotbarButtons, heldButton);
        }
    }

    public QuickItemsUIEntry
    GetButtonAtIndex(int index)
    {
        return hotbarButtons[index];
    }

    public void
	AssignTooltip(List<InventoryEntry> slots)
    {
        for (int index = 0; index < slots.Count; index++)
        {
            if (slots[index] == null)
            {
                hotbarButtons[index].GetComponent<IToolTip>().DestroyTooltip(emptySlot);
            }
            else
            {
                hotbarButtons[index].GetComponent<IToolTip>().BuildTooltip(slots[index]);
            }
        }
    }

    private void
    Awake()
    {
        hotbarButtons.AddRange(GetComponentsInChildren<QuickItemsUIEntry>());
        buttonRenderer = new QuickItemsUIRenderer();
    }
}