﻿using GUIManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(QuickItemsUIEditorBuilder))]
[RequireComponent(typeof(QuickItemsUIController))]
public class QuickItemsUI : MonoBehaviour, IInputtable, IGUI
{
    AvatarController avatarController { get { return FindObjectOfType<AvatarController>(); } }
    QuickItemsUIEditorBuilder uiBuilder { get { return GetComponent<QuickItemsUIEditorBuilder>(); } }
    QuickItemsUIController hotbar { get { return GetComponent<QuickItemsUIController>(); } }

    [SerializeField] Text tooltip;
    [SerializeField] GameObject panel;
    [SerializeField] Text helpText;

	private bool activated = false;

	GUIController controller;

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.UI;
	}

    public Text
    GetTooltip()
    {
        return tooltip;
    }

	public void
	Deactivate()
	{
		hotbar.DisableEditMode ();
		helpText.gameObject.SetActive (false);
		activated = false;
		controller.RegisterDeactivatedGUI (this);
		tooltip.text = "";
	}

    public void
    SubscribeTo(InputValidator newInput)
    {
		newInput.SubscribeToStringInput(InputMode.Down, InputButton.OpenQuickItemsUI, new List<InputState>(){InputState.FullscreenUI, InputState.PlayingMusic, InputState.PausingUI, InputState.Battle}, QuickItemsUI_Input);
		newInput.SubscribeToStringInput(InputMode.Down, InputButton.ExitUI, new List<InputState>(){InputState.FullscreenUI, InputState.PlayingMusic, InputState.PausingUI, InputState.Battle}, ExitUI_Input);
    }
		
	private void
	QuickItemsUI_Input()
	{
		EditHotbarToggle(avatarController.GetActiveCharacter(), avatarController.GetActiveCharacter().GetFieldAbilities().GetAvatarHotbar().GetSlots());
	}

	private void
	ExitUI_Input()
	{
		if (activated)
		{
			Deactivate ();
		}
	}

    private void
    EditHotbarToggle(AvatarFacade activeAvatar, List<InventoryEntry> slots)
    {
        if (!activated)
        {
            Initialise(activeAvatar, slots);
        }
        else
        {
            Deactivate();
        }
    }

    private void
	Initialise(AvatarFacade activeAvatar, List<InventoryEntry> slots)
    {
        uiBuilder.BuildUI(activeAvatar);
        hotbar.AssignTooltip(slots);
        Activate();
    }

    private void
    Activate()
    {
		controller.RegisterActivatedUI (this);

        helpText.gameObject.SetActive(true);
        helpText.color = new Color(1f, 1f, 1f, 0.15f);
        hotbar.EnableEditMode();
		activated = true;
    }
}