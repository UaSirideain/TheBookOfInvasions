﻿using UnityEngine;
using UnityEngine.UI;

public abstract class IToolTip : MonoBehaviour
{
    [SerializeField] protected Text tooltipDisplay;
    [SerializeField] protected string objectName;
    [SerializeField] protected string tooltipText;

    public abstract void DisplayTooltip(bool status);
	public virtual void BuildTooltip(InventoryEntry gameObject) { }
    public virtual void DestroyTooltip(Sprite emptySprite) { }
}
