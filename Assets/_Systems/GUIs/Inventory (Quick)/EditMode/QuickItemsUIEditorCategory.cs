﻿using Field.Abilities;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class QuickItemsUIEditorCategory : MonoBehaviour
{
    [SerializeField] private Transform columnContent;

	[SerializeField] QuickItemsUIEditorEntry entryPrefab;

	List<InventoryEntry> activeEntries = new List<InventoryEntry>();
	Dictionary<InventoryEntry, QuickItemsUIEditorEntry> uiEntries = new Dictionary<InventoryEntry, QuickItemsUIEditorEntry> ();

	public InventoryCategory category;

	public void
	MergeItems(List<InventoryEntry> pendingEntries)
	{
		bool entriesAdded;
		bool entriesDeleted;

		AddItems (pendingEntries, out entriesAdded);
		DeleteItems (pendingEntries, out entriesDeleted);

		if (entriesAdded || entriesDeleted)
		{
			Render (GetUIEntries());
		}
	}

	public void
	SendHotbar (QuickItemsUIInput hotbar, AvatarFacade avatar)
	{
		foreach (QuickItemsUIEditorEntry button in GetUIEntries())
		{
			button.SetUpOnClick (hotbar, avatar);
		}
	}

	List<QuickItemsUIEditorEntry>
	GetUIEntries()
	{
		List<QuickItemsUIEditorEntry> entries = new List<QuickItemsUIEditorEntry> ();

		foreach (InventoryEntry entry in activeEntries)
		{
			entries.Add (uiEntries [entry]);
		}

		return entries;
	}

	void
	AddItems(List<InventoryEntry> pendingEntries, out bool entriesAdded)
	{
		entriesAdded = false;

		foreach (InventoryEntry pendingEntry in pendingEntries)
		{
			if (pendingEntry.IsInList (activeEntries) == false && pendingEntry.GetAbility().UsableFromQuickInventory())
			{
				activeEntries.Add (pendingEntry);
				uiEntries.Add (pendingEntry, CreateUIEntry (pendingEntry));
				entriesAdded = true;
			}
		}
	}

	void
	DeleteItems(List<InventoryEntry> pendingEntries, out bool entriesDeleted)
	{
		entriesDeleted = false;

		if (activeEntries.Count > 0)
		{
			for (int i = Mathf.Clamp (activeEntries.Count - 1, 0, 1000); i >= 0; i--)
			{
				if (activeEntries [i].IsInList (pendingEntries) == false)
				{
					uiEntries.Remove (activeEntries [i]);
					activeEntries.RemoveAt (i);
					entriesDeleted = true;
				}
			}
		}
	}

	QuickItemsUIEditorEntry
	CreateUIEntry(InventoryEntry plainEntry)
	{
		QuickItemsUIEditorEntry uiEntry = Instantiate (entryPrefab);
		uiEntry.Initialise2 (plainEntry);
		return uiEntry;
	}

	void
	Render(List<QuickItemsUIEditorEntry> entries2)
	{
		for (int i = 0; i < entries2.Count; i++)
		{
			RectTransform rt = entries2 [i].GetComponent<RectTransform> ();
			rt.SetAsChildOf (GetComponent<RectTransform> ());
			rt.anchoredPosition = new Vector3 (0, rt.sizeDelta.y * i, 0);
		}
	}
}