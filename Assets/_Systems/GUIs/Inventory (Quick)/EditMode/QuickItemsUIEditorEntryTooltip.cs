﻿using UnityEngine;
using UnityEngine.UI;

public class QuickItemsUIEditorEntryTooltip : IToolTip
{
    private QuickItemsUIEntry button { get { return GetComponent<QuickItemsUIEntry>(); } }
    [SerializeField] private Text amountText;

    public override void 
    DisplayTooltip(bool status)
    {
        tooltipDisplay.text = objectName + " - " + tooltipText;
        tooltipDisplay.gameObject.SetActive(status);
    }

    public override void
	BuildTooltip(InventoryEntry slot)
    {
		if (slot.GetAbility())
        {
			AbilityData ability = slot.GetAbility();

			GetComponent<Image>().sprite = ability.GetUIIcon();
			objectName = ability.GetUIName();
			tooltipText = ability.GetUIDescription();

			if (ability.DepletesOnUse())
            {
                amountText.text = slot.GetAmount().ToString();
            }
            else
            {
                amountText.text = "";
            }

            button.AssignButton(true);
        }
    }

    public override void
    DestroyTooltip(Sprite emptySprite)
    {
        GetComponent<Image>().sprite = emptySprite;
        objectName = "";
        tooltipText = "";
        amountText.text = "";
        button.AssignButton(false);
    }
}
