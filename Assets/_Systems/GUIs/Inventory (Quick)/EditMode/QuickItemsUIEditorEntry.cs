﻿using UnityEngine;
using UnityEngine.UI;

namespace Field.Abilities{

public class QuickItemsUIEditorEntry : MonoBehaviour
{
	[SerializeField] Text UIName;
	[SerializeField] Image UIIcon;
	[SerializeField] string UIDescription;
	[SerializeField] Text UIAmount;
	InventoryEntry inventoryEntry;

	QuickItemsUI ui { get { return GameObject.FindObjectOfType<QuickItemsUI> (); } }

	Button button{get{ return GetComponent<Button>(); }}

    public void
    Initialise2(InventoryEntry newInventoryEntry)
    {
		UIName.text = newInventoryEntry.GetAbility().GetUIName();
		UIIcon.sprite = newInventoryEntry.GetAbility().GetUIIcon();
		UIDescription = newInventoryEntry.GetAbility().GetUIDescription();
        inventoryEntry = newInventoryEntry;
        EnableText();
    }

	public void
	SetUpOnClick(QuickItemsUIInput hotbar, AvatarFacade avatar)
	{
		GetComponent<Button> ().onClick.AddListener (delegate { hotbar.ClickButton(this, avatar); });
	}

    private void
    EnableText()
    {
		if (inventoryEntry.GetAbility ().GetUICategory () == InventoryCategory.Skill || inventoryEntry.GetAbility ().DepletesOnUse () == false)
		{
			UIAmount.gameObject.SetActive(false);
		}
		else//if (ability.GetAbility().GetUICategory() != InventoryCategory.Skill)
        {
			int amountHeld = inventoryEntry.GetAmount ();
			if (amountHeld > 1)
			{
				UIAmount.gameObject.SetActive (true);
				UIAmount.text = amountHeld.ToString ();
			}
			else
			{
				//disable the icon and prevent use
			}
        }
    }

    public void
    RenderTooltip(bool status)
    {
        if (status)
        {
            ui.GetTooltip().text = UIDescription;
        }
        else
        {
            ui.GetTooltip().text = "";
        }
    }

	public FieldAbility
	GetFieldAbility()
	{
		return inventoryEntry.GetAbility ().GetFieldAbility ();
	}

    public InventoryEntry
    GetAbilityObject()
    {
        return inventoryEntry;
    }
}
}