﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(QuickItemsUIInput))]
public class QuickItemsUIEditorBuilder : MonoBehaviour
{
    QuickItemsUIInput hotbar { get { return GetComponent<QuickItemsUIInput>(); } }

	[SerializeField] QuickItemsUIEditorCategory columnPrefab;

	List<QuickItemsUIEditorCategory> columns = new List<QuickItemsUIEditorCategory>();

	InventorySorter sorter = new InventorySorter();

	[SerializeField] RectTransform columnPanel;

    public void
    BuildUI(AvatarFacade activeAvatar)
    {
		BuildColumns(activeAvatar);
		SendHotbarToColumns (activeAvatar);
    }

	void
	SendHotbarToColumns(AvatarFacade avatar)
	{
		foreach (QuickItemsUIEditorCategory column in columns)
		{
			column.SendHotbar (hotbar, avatar);
		}
	}

	void
	BuildColumns(AvatarFacade avatar)
	{
		Dictionary<InventoryCategory, List<InventoryEntry>> categorisedEntries = sorter.SortEntriesIntoCategories (avatar.GetCharacter().GetAllFieldAbilities());

		CreateSpecificCategory (categorisedEntries, InventoryCategory.Skill);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Tool);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Relic);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Salve);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Misc);

		RenderColumns ();
	}

	void
	CreateSpecificCategory(Dictionary<InventoryCategory, List<InventoryEntry>> categorisedEntries, InventoryCategory targetType)
	{
		if (categorisedEntries.ContainsKey (targetType))
		{
			GetAppropriateCategory (targetType).MergeItems (categorisedEntries [targetType]);
		}
		else
		{
			DestroyAppropriateCategory (targetType);
		}
	}

	QuickItemsUIEditorCategory
	GetAppropriateCategory(InventoryCategory targetType)
	{
		foreach (QuickItemsUIEditorCategory oldCategory in columns)
		{
			if (oldCategory.category == targetType)
			{
				return oldCategory;
			}
		}

		QuickItemsUIEditorCategory newCategory = Instantiate (columnPrefab);
		newCategory.category = targetType;
		columns.Add (newCategory);
		return newCategory;
	}
		
	void
	RenderColumns()
	{
		float nextXPosition = 0f;

		for (int i = 0; i < columns.Count; i++)
		{
			RectTransform rt = columns [i].GetComponent<RectTransform> ();
			rt.SetAsChildOf (columnPanel);
			rt.anchoredPosition = new Vector2 (nextXPosition, 0);
			nextXPosition += (rt.sizeDelta.x + 20);
		}
	}

	void
	DestroyAppropriateCategory(InventoryCategory targetType)
	{
		for (int i = Mathf.Clamp (columns.Count - 1, 0, 1000); i >= 0; i--)
		{
			if (columns[i].category == targetType)
			{
				GameObject emptyCategory = columns [i].gameObject;
				columns.RemoveAt (i);
				Destroy (emptyCategory);
			}
		}
	}
}