﻿using System.Collections.Generic;

public class QuickItemsUIRenderer
{
    public void
    RenderButtonsNormal(List<QuickItemsUIEntry> hotbarButtons, int equippedEnchantmentIndex, int equippedItemIndex)
    {
        foreach (QuickItemsUIEntry button in hotbarButtons)
        {
            if (button.ButtonIsAssigned())
            {
                if (!hotbarButtons.IndexOf(button).Equals(equippedEnchantmentIndex) && !hotbarButtons.IndexOf(button).Equals(equippedItemIndex))
                {
                    button.Darken(.2f);
                }
                else if (hotbarButtons.IndexOf(button).Equals(equippedEnchantmentIndex) || hotbarButtons.IndexOf(button).Equals(equippedItemIndex))
                {
                    button.Brighten(.2f);
                }
            }
        }
    }
		
	public void
	DarkenPreviouslyHeldButton(List<QuickItemsUIEntry> hotbarButtons, QuickItemsUIEntry heldButton)
	{
		foreach (QuickItemsUIEntry button in hotbarButtons)
		{
			if (button.Equals(heldButton))
			{
				button.Darken(.2f);
			}
		}
	}

	public void
	BrightenHeldButtonEditor(List<QuickItemsUIEntry> hotbarButtons, QuickItemsUIEntry heldButton)
	{
		foreach (QuickItemsUIEntry button in hotbarButtons)
		{
			if (button.Equals (heldButton))
			{
				button.Brighten (.2f);
			}
			else
			{
				button.Darken (.2f);
			}
		}
	}
}