﻿using Field.Abilities;
using System.Collections.Generic;
using UnityEngine;

public class QuickItemsUIInput : MonoBehaviour, IInputtable
{
    QuickItemsUIController hotbar { get { return GetComponent<QuickItemsUIController>(); } }

    private int heldKey = -1;

    public void
	ClickButton(QuickItemsUIEditorEntry button, AvatarFacade avatar)
    {
        if (heldKey >= 0 && heldKey <= 11)
        {
            avatar.GetFieldAbilities().GetAvatarHotbar().AssignHotbarButton(button.GetAbilityObject(), heldKey);
            hotbar.GetButtonAtIndex(heldKey).GetComponent<IToolTip>().BuildTooltip(button.GetAbilityObject());
        }
    }

    public void 
    SubscribeTo(InputValidator newInput)
    {
		newInput.SubscribeToNumericInput(InputMode.Down, new List<InputState>(){InputState.UI, InputState.PausingUI, InputState.FullscreenUI}, NumericInputDown);
		newInput.SubscribeToNumericInput(InputMode.Down, new List<InputState>(){InputState.PausingUI, InputState.FullscreenUI}, NumericInputDownEditMode);
		newInput.SubscribeToNumericInput(InputMode.Up, new List<InputState>(){InputState.PausingUI, InputState.FullscreenUI}, NumericInputUpEditMode);
    }

	ShutdownTimer timer;

	void
	Awake()
	{
		timer = new ShutdownTimer (hotbar, 1f);
	}

	void
	NumericInputDown(string numeric)
	{
		int arrayIndex;
		if (numeric.NumericStringToArrayIndex (out arrayIndex))
		{
			if (arrayIndex >= 0 && arrayIndex <= 11)
			{
				hotbar.RenderHotbar();
				StartCoroutine(timer.RunCountdown ());
			}
		}
	}

    private void
    NumericInputDownEditMode(string numeric)
    {
		int arrayIndex;
		if (numeric.NumericStringToArrayIndex (out arrayIndex))
		{
			if (arrayIndex >= 0 && arrayIndex <= 11)
			{
				heldKey = arrayIndex;
				hotbar.BrightenHeldButtonForEditor(heldKey);
			}
		}
    }

	void
	NumericInputUpEditMode(string numeric)
	{
		int arrayIndex;

		if (numeric.NumericStringToArrayIndex (out arrayIndex))
        {
			if (arrayIndex >= 0 && arrayIndex <= 11)
            {
				hotbar.RenderHotbarForEditor(arrayIndex);
				if (arrayIndex == heldKey)
                {
                    heldKey = -1;
                }
            }
        }
	}
}