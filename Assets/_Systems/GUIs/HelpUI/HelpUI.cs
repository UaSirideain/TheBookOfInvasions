﻿using GUIManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpUI : MonoBehaviour, IInputtable, IGUI
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
    GUIController controller;

	bool visible = false;

    public void
    SetOverrideController(GUIController newController)
    {
        controller = newController;
    }

    public GUIType
    GetGUIType()
    {
        return GUIType.UI;
    }

    public void
    Deactivate()
    {
		visible = false;
		canvas.Disable ();
		controller.RegisterDeactivatedGUI(this);
    }

    public void
    SubscribeTo(InputValidator newInput)
    {
		newInput.SubscribeToStringInput(InputMode.Down, InputButton.HelpUI, new List<InputState>(){InputState.PausingUI, InputState.Battle}, HelpUI_Input);
		newInput.SubscribeToStringInput(InputMode.Down, InputButton.ExitUI, new List<InputState>(), Deactivate);
    }

    private void
    HelpUI_Input()
    {
        if (!visible)
        {
            ActivateHelpUI();
        }
        else
        {
            Deactivate();
        }
    }

    void
    ActivateHelpUI()
    {
		visible = true;
		GetComponentInChildren<BackgroundDimmer> ().DimBackground ();
		canvas.Enable ();
		controller.RegisterActivatedUI(this);
    }
}
