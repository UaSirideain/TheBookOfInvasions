﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SaveCompanion))]
public class ResolutionSettingsSaveable : MonoBehaviour, ISaveable
{
	SaveGameController saveData { get { return FindObjectOfType<SaveGameController> (); } }

	SaveDataHeading saveDataHeading = SaveDataHeading.Settings;

	public void
	Save()
	{
		Dictionary<string, string> brightnessData = new Dictionary<string, string> ();

		brightnessData.Add ("ResolutionX", Screen.currentResolution.width.ToString ());
		brightnessData.Add ("ResolutionY", Screen.currentResolution.height.ToString ());
		brightnessData.Add ("Fullscreen", Screen.fullScreen.ToString ());

		SaveDataBlock newBlock = new SaveDataBlock (saveDataHeading, brightnessData);

		saveData.RecordSaveData (newBlock);
	}

	public void
	Load(List<SaveDataBlock> saveBlocks)
	{
		foreach (SaveDataBlock block in saveBlocks)
		{
			if (block.GetHeading () == saveDataHeading)
			{
				Dictionary<string, string> data = block.GetData ();

				int resX = Screen.width;
				int resY = Screen.height;
				bool fullscreen = false;


				if (data.ContainsKey ("Fullscreen"))
				{
					bool.TryParse (data ["Fullscreen"], out fullscreen);
				}

				if (data.ContainsKey ("ResolutionX") && data.ContainsKey ("ResolutionY"))
				{
					int.TryParse (data ["ResolutionX"], out resX);
					int.TryParse (data ["ResolutionY"], out resY);
				}

				Screen.SetResolution (resX, resY, fullscreen);
			}
		}
	}
}