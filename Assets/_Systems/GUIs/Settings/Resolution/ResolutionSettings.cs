﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GUIManagement{

public class ResolutionSettings : SettingsUI
{
	List<Vector2> supportedResolutions = new List<Vector2>();
	[SerializeField] Text resolutionSelector;
	[SerializeField] Image fullscreenSelectorEnabledGraphic;
	[SerializeField] Image fullscreenSelectorDisabledGraphic;

	bool currentFullscreenSetting;
	int currentResolution = 0;

	void
	FindSupportedResolutions()
	{
		if (supportedResolutions.Count < 1)
		{
			foreach (Resolution res in Screen.resolutions)
			{
				if (res.height / 9f == res.width / 16f)
				{
					Vector2 newResOption = new Vector2 (res.width, res.height);
					supportedResolutions.Add (newResOption);
				}
			}
		}
	}

	void
	FindCurrentSettings()
	{
		currentFullscreenSetting = Screen.fullScreen;
		currentResolution = 0;

		for (int i = 0; i < supportedResolutions.Count; i++)
		{
			if (supportedResolutions [i].x == Screen.width && supportedResolutions [i].y == Screen.height)
			{
				currentResolution = i;
			}
		}
	}

	public void
	ApplyChanges()
	{
		Screen.SetResolution ((int)supportedResolutions[currentResolution].x, (int)supportedResolutions[currentResolution].y, currentFullscreenSetting);
		GetComponent<SaveCompanion>().Save ();
	}

	public void
	PreviousResolution()
	{
		AdjustResolutionIndex (-1);
		RenderSelectedSettings ();
	}

	public void
	NextResolution()
	{
		AdjustResolutionIndex (1);
		RenderSelectedSettings ();
	}

	void
	RenderSelectedSettings()
	{
		resolutionSelector.text = supportedResolutions [currentResolution].x + " x " + supportedResolutions [currentResolution].y;
		fullscreenSelectorEnabledGraphic.enabled = currentFullscreenSetting;
		fullscreenSelectorDisabledGraphic.enabled = !currentFullscreenSetting;
	}

	void
	AdjustResolutionIndex(int adjustment)
	{
		currentResolution += adjustment;

		if (currentResolution < 0)
		{
			currentResolution = supportedResolutions.Count - 1;
		}
		else if (currentResolution >= supportedResolutions.Count)
		{
			currentResolution = 0;
		}
	}

	public void
	ToggleFullscreen()
	{
		currentFullscreenSetting = !currentFullscreenSetting;
		RenderSelectedSettings ();
	}

	public override void
	StoreCurrentSettings(){}

	public override KeyWord
	GetMenuName()
	{
		return KeyWord.Resolution;
	}

	protected override void
	Initialise()
	{
		FindSupportedResolutions ();
		FindCurrentSettings ();
		RenderSelectedSettings ();
	}
}
}