﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GUIManagement{

public abstract class SettingsUI : MonoBehaviour, IInputtable, IGUI
{
	CanvasGroup canvas {get{return GetComponent<CanvasGroup>();}}

	bool pauseUIActive;
	bool uiActive = false;

	GUIController controller;

	public void
	Activate()
	{
		if (uiActive == false)
		{
			uiActive = true;
			canvas.Enable ();
			RecordStateOfPauseUI ();
			HidePauseUI ();
			controller.RegisterActivatedUI (this);
		}

		Initialise ();
	}

	public void
	Deactivate()
	{
		if (uiActive)
		{
			uiActive = false;
			canvas.Disable ();
			UnhidePauseUI ();
			controller.RegisterDeactivatedGUI (this);
		}
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (), Deactivate);
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.Settings;
	}

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	void
	RecordStateOfPauseUI()
	{
		pauseUIActive = FindObjectOfType<PauseUI> ().GetComponent<CanvasGroup> ().blocksRaycasts;
	}

	void
	HidePauseUI()
	{
		if (pauseUIActive)
		{
			FindObjectOfType<PauseUI> ().GetComponent<CanvasGroup> ().Disable ();
		}
	}

	void
	UnhidePauseUI()
	{
		if (pauseUIActive)
		{
			FindObjectOfType<PauseUI> ().GetComponent<CanvasGroup> ().Enable ();
		}
	}

	public abstract void
	StoreCurrentSettings ();

	public abstract KeyWord
	GetMenuName ();

	protected virtual void
	Initialise(){}
}
}