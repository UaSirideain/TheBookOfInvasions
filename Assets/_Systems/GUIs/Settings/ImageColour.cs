﻿using UnityEngine;
using UnityEngine.UI;

public class ImageColour : MonoBehaviour
{
	Color hoverColour = new Color(1f, 1f, 1f, 1f);
	Color regularColour = new Color(1f, 1f, 1f, .15f);

	void
	Awake()
	{
		if (GetComponent<Image> ())
		{
			GetComponent<Image> ().color = regularColour;
		}

		if (GetComponent<Text> ())
		{
			GetComponent<Text> ().color = regularColour;
		}
	}

	public void
	HoverColour()
	{
		if (GetComponent<Image> ())
		{
			GetComponent<Image> ().color = hoverColour;
		}

		if (GetComponent<Text> ())
		{
			GetComponent<Text> ().color = hoverColour;
		}
	}

	public void
	RegularColour()
	{
		if (GetComponent<Image> ())
		{
			GetComponent<Image> ().color = regularColour;
		}

		if (GetComponent<Text> ())
		{
			GetComponent<Text> ().color = regularColour;
		}
	}
}