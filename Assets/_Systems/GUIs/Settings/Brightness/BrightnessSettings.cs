﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GUIManagement{

public class BrightnessSettings : SettingsUI, ISaveable
{
	[SerializeField] Slider brightnessSlider;
	SaveGameController saveData { get { return FindObjectOfType<SaveGameController> (); } }

	float oldBrightnessValue;

	SaveDataHeading saveDataHeading = SaveDataHeading.Settings;

	public void
	Save()
	{
		Dictionary<string, string> brightnessData = new Dictionary<string, string> ();

		brightnessData.Add ("Brightness", brightnessSlider.value.ToString ());

		SaveDataBlock newBlock = new SaveDataBlock (saveDataHeading, brightnessData);

		saveData.RecordSaveData (newBlock);
	}

	void
	Load(Dictionary<string, string> savedData)
	{
		string stringValue;
		if (savedData.TryGetValue ("Brightness", out stringValue))
		{
			float intValue;
			if(float.TryParse(stringValue, out intValue))
			{
				brightnessSlider.value = intValue;
			}
		}
	}

	public void
	Load(List<SaveDataBlock> block)
	{
		foreach (SaveDataBlock blocko in block)
		{
			if (blocko.GetHeading () == saveDataHeading)
			{
				Load (blocko.GetData ());
			}
		}
	}

	public void
	DiscardChanges()
	{
		brightnessSlider.value = oldBrightnessValue;
	}

	public override void
	StoreCurrentSettings()
	{
		oldBrightnessValue = brightnessSlider.value;
	}

	public void
	SetBrightness(float newBrightness)
	{
		FindObjectOfType<CameraManager> ().SetBrightness (newBrightness);
	}

	public override KeyWord
	GetMenuName()
	{
		return KeyWord.Brightness;
	}
}
}