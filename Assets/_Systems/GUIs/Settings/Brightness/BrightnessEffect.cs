﻿using System;
using UnityEngine;
using Cameras;

[ExecuteInEditMode]
[AddComponentMenu("Image Effects/Color Adjustments/Brightness")]
public class BrightnessEffect : MonoBehaviour
{
	public Shader usedShader;
	Material m_Material;

	[SerializeField] float brightness = 1f;
	const float brightnessMin = 0.3f;
	const float brightnessMax = 2.3f;

	public void
	SetBrightness(float newValue)
	{
		//brightness = Mathf.Lerp(brightnessMin, brightnessMax, newValue);
		brightness = Mathf.Clamp(newValue, brightnessMin, brightnessMax);
	}

	void
	Start()
	{
		if (!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}

		if (!usedShader || !usedShader.isSupported)
		{
			enabled = false;
		}
	}


	Material
	material
	{
		get 
		{
			if (m_Material == null)
			{
				m_Material = new Material (usedShader);
				m_Material.hideFlags = HideFlags.HideAndDontSave;
			}
			return m_Material;
		}
	}


	void
	OnDisable()
	{
		if (m_Material)
		{
			DestroyImmediate (m_Material);
		}
	}

	void
	OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		material.SetFloat ("_Brightness", brightness);
		Graphics.Blit (source, destination, material);
	}
}