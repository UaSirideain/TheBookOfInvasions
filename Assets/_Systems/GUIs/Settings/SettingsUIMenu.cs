﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GUIManagement{

public class SettingsUIMenu : MonoBehaviour, IInputtable
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

	[SerializeField] RectTransform menuBounds;
	[FormerlySerializedAs("displaySettingsUIMenuEntry")][SerializeField] RectTransform settingsUIMenuEntry;

	bool menuOpen = false;
	bool built = false;

	public void
	OpenMenu()
	{
		if (menuOpen == false)
		{
			menuOpen = true;

			if (!built)
			{
				built = true;
				List<SettingsUI> uis = new List<SettingsUI> ()
				{
					FindObjectOfType<BrightnessSettings> (),
					FindObjectOfType<ResolutionSettings> (),
					FindObjectOfType<LanguageSettingsUI> ()
				};

				for (int i = 0; i < uis.Count; i++)
				{
					RectTransform newEntry = Instantiate (settingsUIMenuEntry);
					newEntry.SetAsChildOf (menuBounds);
					newEntry.anchoredPosition = new Vector2 (0, newEntry.sizeDelta.y * i);

					newEntry.GetComponentInChildren<TextCompanion> ().Assign(uis [i].GetMenuName ());

					SettingsUI targetUI = uis [i].GetComponent<SettingsUI> ();
					newEntry.GetComponent<Button> ().onClick.AddListener (delegate
					{
						targetUI.Activate ();
					});
					targetUI.StoreCurrentSettings ();
					newEntry.GetComponent<Button> ().onClick.AddListener (delegate
					{
						CloseMenu ();
					});
				}
			}
			canvas.Enable ();
		}
	}

	public void
	CloseMenu()
	{
		if (menuOpen)
		{
			menuOpen = false;
			canvas.Disable ();
		}
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (), CloseMenu);
	}
}
}