﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formation{

public class GridCell : MonoBehaviour
{
	[SerializeField] Vector2 index;
	RectTransform rt { get { return GetComponent<RectTransform> (); } }

	public void
	RenderPosition(int x, int y, int padding)
	{
		rt.anchoredPosition = new Vector2 (x * (rt.sizeDelta.x + padding), y * (rt.sizeDelta.y + padding));
		index = new Vector2 (x, y);
	}

	public Vector2
	GetPosition()
	{
		return index;
	}
}
}