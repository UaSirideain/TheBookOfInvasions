﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Formation{

public class MouseSelection : MonoBehaviour
{
	RectTransform rt {get{return GetComponent<RectTransform>();}}
	Image image { get { return GetComponent<Image> (); } }

	bool active = false;
	Character character;
		
	public void
	ActivateSelectionPortrait(FormationPortrait portrait)
	{
		active = true;
		image.sprite = portrait.GetComponent<Image> ().sprite;
		image.color = Color.white;
		character = portrait.GetCharacter ();
	}

	public void
	DeactivateSelectionPortrait()
	{
		active = false;
		image.sprite = null;
		image.color = new Color (0, 0, 0, 0);
		character = null;
	}

	public Character
	GetSelectedCharacter()
	{
		return character;
	}

	void
	FixedUpdate()
	{
		if (active)
		{
			rt.position = Input.mousePosition + new Vector3 (0, 0, 0);
		}
	}
}
}