﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Formation{

public class FormationIcon : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }
	SavedFormations savedFormations{ get { return FindObjectOfType<SavedFormations> (); } }

	[SerializeField] Button saveButton;
	[SerializeField] Button loadButton;

	[SerializeField] RectTransform positionMarkerPrefab;

	List<GameObject> positionMarkers = new List<GameObject> ();

	public void
	UpdateButton(Formation formation)
	{
		if (formation != null)
		{
			saveButton.onClick.AddListener (delegate {savedFormations.OverwriteFormation (formation);});
			loadButton.onClick.AddListener(delegate{savedFormations.LoadFormation (formation);});
		}
		else
		{
			saveButton.onClick.AddListener (delegate {savedFormations.WriteFormation ();});
		}
	}
		
	public void
	RenderPosition(int index, float padding, ref float panelHeight)
	{
		rt.anchoredPosition = new Vector2 (0, -index * (rt.sizeDelta.y + padding));
		panelHeight += rt.sizeDelta.y + (Mathf.Clamp01(index) * padding);
	}
		
	public void
	RenderAvatarIcons(Formation formation)
	{
		foreach (GameObject marker in positionMarkers)
		{
			Destroy (marker);
		}

		positionMarkers.Clear ();

		foreach(FormationEntry pos in formation.entries)
		{
			RectTransform posMarker = Instantiate (positionMarkerPrefab);
			posMarker.SetParent (transform);
			posMarker.localScale = new Vector3 (1, 1, 1);
			posMarker.anchoredPosition = new Vector2(14f, 14f) + pos.position * 26f;
			posMarker.GetComponent<Image> ().color = pos.character.GetThematicColour ();
			positionMarkers.Add (posMarker.gameObject);
		}
	}
}
}