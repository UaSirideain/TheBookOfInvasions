﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Formation{

public class FormationPortrait : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }

	Character character;

	public void
	Initialise(Character newCharacter, ActiveFormationUI activeFormation)
	{
		character = newCharacter;

		GetComponent<Image> ().sprite = character.GetFormationPortrait ();

		GetComponent<Button> ().onClick.AddListener (delegate{activeFormation.SelectPortrait(this);});
	}

	public void
	RenderPosition(int index, float padding, ref float panelWidth)
	{
		rt.anchoredPosition = new Vector2(index * (rt.sizeDelta.x + padding), 0);
		panelWidth += rt.sizeDelta.x + (Mathf.Clamp01(index) * padding);
	}

	public void
	AttachToCell(GridCell cell)
	{
		rt.SetParent (cell.transform);
		rt.transform.localScale = new Vector3 (1, 1, 1);

		rt.anchorMin = new Vector2 (.5f, .5f);
		rt.anchorMax = new Vector2 (.5f, .5f);
		rt.pivot = new Vector2 (.5f, .5f);

		rt.anchoredPosition = new Vector2 (0, 0);
	}

	public Character
	GetCharacter()
	{
		return character;
	}
}
}