﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Formation{

public class SavedFormations : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }

	[SerializeField] ActiveFormation system;

	[SerializeField] FormationIcon formationIconPrefab;
	[SerializeField] FormationIcon newFormationIconPrefab;
	[SerializeField] const float padding = 20;

	[SerializeField] List<Formation> savedFormations = new List<Formation>();
	List<FormationIcon> savedFormationIcons = new List<FormationIcon>();

	public void
	Initialise()
	{
		ClearIcons ();

		float panelHeight = 0;

		for(int i = 0; i < savedFormations.Count; i++)
		{
			CreateSavedFormationIcon (i, ref panelHeight);
		}

		CreateAddFormationIcon (ref panelHeight);

		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, panelHeight);
	}
		
	public void
	OverwriteFormation(Formation formation)
	{
		formation.CloneFrom (system.GetFormation ());

		savedFormationIcons[savedFormations.IndexOf (formation)].RenderAvatarIcons(formation);
	}

	public void
	WriteFormation()
	{
		Formation newFormation = new Formation ();
		newFormation.CloneFrom (system.GetFormation ());
		savedFormations.Add (newFormation);
		Initialise ();
	}

	public void
	LoadFormation(Formation currentFormation)
	{
		system.Load (currentFormation);
	}

	void
	CreateSavedFormationIcon(int i, ref float panelHeight)
	{
		FormationIcon formationIcon = Instantiate (formationIconPrefab);
		formationIcon.transform.SetParent (transform);
		formationIcon.transform.localScale = new Vector3 (1, 1, 1);

		formationIcon.UpdateButton (savedFormations[i]);
		formationIcon.RenderPosition (i, padding, ref panelHeight);
		formationIcon.RenderAvatarIcons (savedFormations[i]);

		savedFormationIcons.Add (formationIcon);
	}

	void
	CreateAddFormationIcon(ref float panelHeight)
	{
		FormationIcon saveNewIcon = Instantiate (newFormationIconPrefab);
		saveNewIcon.transform.SetParent (transform);
		saveNewIcon.transform.localScale = new Vector3 (1, 1, 1);

		saveNewIcon.UpdateButton (null);
		saveNewIcon.RenderPosition (savedFormations.Count, padding, ref panelHeight);
	}

	void
	ClearIcons()
	{
		foreach (Transform formationIcons in transform)
		{
			Destroy (formationIcons.gameObject);
		}
		savedFormationIcons.Clear ();
	}
}
}