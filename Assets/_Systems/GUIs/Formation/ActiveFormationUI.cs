﻿using Field;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GUIManagement;

namespace Formation{

public class ActiveFormationUI : MonoBehaviour, IGUI, IInputtable
{
    CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

    [SerializeField] ActiveFormation activeFormation;

    [SerializeField] Transform cellPanel;
    [SerializeField] Vector2 cellCount;

    [SerializeField] GridCell cellPrefab; 
    [SerializeField] int padding = 16;

    [SerializeField] RectTransform portraitPanel;
    [SerializeField] ActiveAvatars activeAvatars;
    [SerializeField] Transform characters;

    [SerializeField] FormationPortrait portraitPrefab;
    //[SerializeField] int portraitPadding = 20;

    [SerializeField] MouseSelection mouseSelection;

    [SerializeField] SavedFormations savedFormations;

	InputValidator input;

    List<GridCell> cells = new List<GridCell>();
    List<FormationPortrait> portraits = new List<FormationPortrait>();

	public void
	SetInputValidator(InputValidator newInput)
	{
		input = newInput;
	}

	[SerializeField] GUIController overrideController;

	public void
	SetOverrideController(GUIController newOverrideController)
	{
		overrideController = newOverrideController;
		transform.SetParent (overrideController.transform);
	}

	public void
	SubscribeTo(InputValidator newInput)
	{
		input = newInput;

		input.SubscribeToStringInput (InputMode.Down, InputButton.RightMouse, new List<InputState> (){ InputState.Battle}, Right);
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (){ InputState.Battle}, Escape);
		input.SubscribeToStringInput (InputMode.Down, InputButton.OpenFormationUI, new List<InputState> (){InputState.FullscreenUI, InputState.PausingUI, InputState.Battle }, F);
	}

	void
	Right()
	{
		mouseSelection.DeactivateSelectionPortrait ();
	}

	void
	Escape()
	{
		if (viewable)
		{
			Deactivate ();
		}
	}

	void
	F()
	{
		if (viewable)
		{
			Deactivate ();
		}
		else
		{
			Activate ();
		}
	}

    void
    Start()
    {
        Build();
    }

    private bool viewable = false;

    void
    Activate()
    {
	    Build ();
		
	    canvas.Enable ();
        viewable = true;
		overrideController.RegisterActivatedUI (this);
    }

	public GUIType
	GetGUIType()
	{
		return GUIType.UI;
	}

    public void
    Deactivate()
    {
	    canvas.Disable ();
        viewable = false;
		overrideController.RegisterDeactivatedGUI (this);
    }

    private bool built = false;

    void
    Build()
    {
	    if (built == false)
	    {
		    activeAvatars.Initialise (characters);
		    BuildCells ();
		    RenderOccupiedCells ();
		    savedFormations.Initialise ();
		    built = true;
	    }
    }
	
    void
    BuildCells()
    {
	    for(int y = 0; y < cellCount.y; y++)
	    {
		    for (int x = 0; x < cellCount.x; x++)
		    {
			    GridCell cell = Instantiate (cellPrefab);
			    cells.Add (cell);

			    cell.transform.SetParent (cellPanel);
				cell.transform.localScale = new Vector3 (1, 1, 1);
			    cell.RenderPosition (x, y, padding);
			    cell.GetComponent<Button> ().onClick.AddListener (delegate{SelectPosition(cell);});
		    }
	    }
    }

    public void
    RenderOccupiedCells()
    {
	    foreach (FormationPortrait portrait in portraits)
	    {
		    Destroy (portrait.gameObject);
	    }

	    portraits.Clear ();

	    foreach(FormationEntry entry in activeFormation.GetFormation().entries)
	    {
		    foreach (GridCell cell in cells)
		    {
			    if (cell.GetPosition () == entry.position)
			    {
				    CreatePortrait (entry.character, cell);
			    }
		    }
	    }
    }

    public void
    SelectPortrait(FormationPortrait portrait)
    {
	    if (mouseSelection.GetSelectedCharacter() == null)
	    {
		    activeFormation.RemovePositionOf (portrait.GetCharacter ());

		    DestroyPortraitOf (portrait.GetCharacter ());
	
		    mouseSelection.ActivateSelectionPortrait(portrait);
	    }
    }

    void
    SelectPosition(GridCell cell)
    {
	    Character character = mouseSelection.GetSelectedCharacter ();

	    if (character != null)
	    {
		    CreatePortrait (character, cell);

		    activeFormation.AddPosition (character, cell.GetPosition());

		    mouseSelection.DeactivateSelectionPortrait();
	    }
    }

    void
    DestroyPortraitOf(Character character)
    {
	    FormationPortrait targetPortrait = null;
	    foreach (FormationPortrait portrait in portraits)
	    {
		    if (portrait.GetCharacter () == character)
		    {
			    targetPortrait = portrait;
		    }
	    }

	    if (targetPortrait != null)
	    {
		    portraits.Remove (targetPortrait);
		    Destroy (targetPortrait.gameObject);
	    }
    }

    void
    CreatePortrait(Character character, GridCell cell)
    {
	    FormationPortrait portrait = Instantiate(portraitPrefab);
	    portraits.Add (portrait);
	    portrait.Initialise (character, this);
	    portrait.AttachToCell (cell);
    }

    public List<Character>
    GetCharacters()
    {
	    List<Character> characterList = new List<Character> ();

	    foreach (Transform child in characters)
	    {
		    characterList.Add (child.GetComponent<Character>());
	    }

	    return characterList;
    }
}
}