﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Commands{

public class CommandRaycast : MonoBehaviour, IInputtable
{
	CommandUI ui { get { return GetComponent<CommandUI> (); } }
	InputValidator input;

	public void
	SubscribeTo(InputValidator newInput)
	{
		input = newInput;

		input.SubscribeToStringInput (InputMode.Down, InputButton.RightMouse, new List<InputState> (){InputState.UI, InputState.FullscreenUI, InputState.PausingUI, InputState.Battle}, Right);
	}

	void
	Right()
	{
		Ray rClick = GetActiveCamera ().ScreenPointToRay (Input.mousePosition);

		RaycastHit rightClick;

		if(Physics.Raycast(rClick, out rightClick, Mathf.Infinity))
		{
			if (rightClick.collider.GetComponentInChildren<ICommandable> () != null)
			{
				ui.BuildMenu (rightClick.collider.GetComponentInChildren<ICommandable>().GetCommands(), Input.mousePosition);
				return;
			}
		}

		ui.Deactivate ();
	}

	Camera
	GetActiveCamera()
	{
		List<Camera> unsortedCameras = new List<Camera> ();

		foreach(Camera camera in FindObjectsOfType<Camera>())
		{
			if (camera.enabled)
			{
				unsortedCameras.Add (camera);
			}
		}
			
		List<Camera> sortedCameras = new List<Camera>();

		for (int i = 100; i > -100; i--)
		{
			foreach (Camera camera in unsortedCameras)
			{
				if (camera.depth == i)
				{
					sortedCameras.Add (camera);
				}
			}
		}
		return sortedCameras [0];
	}
}
}