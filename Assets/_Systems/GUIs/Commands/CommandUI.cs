﻿using System.Collections.Generic;
using UnityEngine;

namespace Field.Commands{

public class CommandUI : MonoBehaviour, IInputtable
{
    CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
    CommandRaycast raycaster { get { return GetComponent<CommandRaycast> (); } }

    [SerializeField] CommandButton commandPrefab;
    [SerializeField] CommandButton labelPrefab;
    [SerializeField] RectTransform panel;

    Dictionary<string, CommandButton> buttonMaps = new Dictionary<string, CommandButton>();

	InputValidator input;

	public void
	SubscribeTo(InputValidator newInput)
	{
		input = newInput;

		input.SubscribeToNumericInput (InputMode.Down, new List<InputState> (), NumericInput);
	}

	void
	NumericInput(string numeric)
	{
		if (isUIActive)
		{
			CommandButton button;
			buttonMaps.TryGetValue (numeric, out button);

			if (button != null)
			{
				button.OnPointerDown ();
			}
		}
	}
		
    public void
    BuildMenu(List<Command> commands, Vector3 originPoint)
    {
	    panel.anchoredPosition = originPoint;

	    buttonMaps.Clear ();
	    buttons.Clear ();
	    buttonIndex = 0;

	    for(int i = 0; i < commands.Count; i++)
	    {
		    BuildButton (commands[i]);
	    }

	    Render ();
    }

    List<RectTransform> buttons = new List<RectTransform>();

    int buttonIndex;

    void
    BuildButton(Command command)
    {
	    if (command.labelOnly == false)
	    {
		    CommandButton button = Instantiate (commandPrefab);

			button.Initialise (command, buttonIndex.ArrayIndexToNumericString());

			buttonMaps.Add (buttonIndex.ArrayIndexToNumericString(), button);

		    buttonIndex++;

		    buttons.Add (button.GetComponent<RectTransform> ());
	    }
	    else
	    {
		    CommandButton button = Instantiate (labelPrefab);
		    button.Initialise (command);

		    buttons.Add (button.GetComponent<RectTransform> ());
	    }
    }

    void
    Render()
    {
	    Activate ();

	    for (int i = 0; i < buttons.Count; i++)
	    {
		    buttons[i].SetParent (panel);
		    buttons [i].anchoredPosition = new Vector2 (0, -i * buttons[i].sizeDelta.y);
	    }
    }

    bool isUIActive = false;
    void
    Activate()
    {
        isUIActive = true;
	    canvas.Enable ();
    }

    public void
    Deactivate()
    {
        isUIActive = false;
	    canvas.Disable ();
    }
}
}