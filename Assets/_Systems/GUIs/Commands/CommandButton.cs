﻿using UnityEngine;
using UnityEngine.UI;

namespace Field.Commands{

public class CommandButton : MonoBehaviour
{
	[SerializeField] Text numeric;
	[SerializeField] Text message;
	Button button { get { return GetComponent<Button> (); } }
	CommandUI ui { get { return FindObjectOfType<CommandUI> (); } }

	Command command;

	public void
	Initialise(Command command, string keymap)
	{
		this.command = command;
		message.text = command.label;
		button.onClick.AddListener (delegate{OnPointerDown();});
		numeric.text = keymap;
	}

	public void
	Initialise(Command command)
	{
		this.command = command;
		message.text = command.label;
	}

	public void
	OnPointerDown()
	{
		command.myCommand ();
		ui.Deactivate ();
	}
}
}