﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Colour
{
	public static Color greyedOut = new Color(1f, 1f, 1f, .3f);
	public static Color opaque = new Color(1f, 1f, 1f, 1f);
	public static Color opaqueBlack = new Color(0f, 0f, 0f, 1f);
	public static Color transparentBlack = new Color(0f, 0f, 0f, 0f);
	public static Color transparent = new Color(1f, 1f, 1f, 0f);
}