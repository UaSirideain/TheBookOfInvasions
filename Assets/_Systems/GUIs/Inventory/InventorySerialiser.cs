﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySerialiser : MonoBehaviour, ISaveable
{
	Inventory inventory { get { return GetComponent<Inventory> (); } }
	SaveGameController saveData { get { return FindObjectOfType<SaveGameController> (); } }
	SaveDataHeading saveDataHeading = SaveDataHeading.Inventory;
	[SerializeField] ItemDatabase itemDatabase;

	public void
	Save()
	{
		List<InventoryEntry> items = inventory.GetEntries ();

		Dictionary<string, string> inventoryData = new Dictionary<string, string> ();

		for(int i = 0; i < items.Count; i++)
		{
			inventoryData.Add (("slot" + i + "item"), items[i].GetAbility ().GetInstanceID ().ToString ());
			inventoryData.Add (("slot" + i + "amount"), items [i].GetAmount ().ToString ());
		}

		SaveDataBlock newBlock = new SaveDataBlock (saveDataHeading, inventoryData);

		saveData.RecordSaveData (newBlock);
	}

	public void
	Load(List<SaveDataBlock> saveData)
	{
		foreach (SaveDataBlock blocko in saveData)
		{
			if (blocko.GetHeading () == saveDataHeading)
			{
				Load (blocko.GetData ());
			}
		}
	}

	void
	Load(Dictionary<string, string> savedData)
	{
		string itemValue;
		string amountValue;

		for(int i = 0; i < savedData.Count; i++)
		{
			if (savedData.TryGetValue ("slot" + i + "item", out itemValue) && savedData.TryGetValue("slot" + i + "amount", out amountValue))
			{
				AbilityData newItem;
				int newAmount;

				if (itemDatabase.GetItemFromID (itemValue, out newItem) && int.TryParse (amountValue, out newAmount))
				{
					inventory.AddInventoryEntry (newItem, newAmount);
				}
			}
		}
	}
}