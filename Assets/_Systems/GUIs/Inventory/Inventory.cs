﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
	[SerializeField] List<InventoryEntry> inventory = new List<InventoryEntry>();

	public List<InventoryEntry>
	GetEntries()
	{
		return inventory;
	}

	public List<InventoryEntry>
	GetBattleInventory()
	{
		List<InventoryEntry> battleInventory = new List<InventoryEntry> ();

		foreach (InventoryEntry item in inventory)
		{
			if (item.GetAbility ().GetBattleEffect ())
			{
				battleInventory.Add (item);
			}
		}

		return battleInventory;
	}

	public List<InventoryEntry>
	GetEntriesOfCategory(InventoryCategory category)
	{
		List<InventoryEntry> categorisedEntries = new List<InventoryEntry> ();

		foreach (InventoryEntry item in inventory)
		{
			if (item.GetEntryType () == category)
			{
				categorisedEntries.Add (item);
			}
		}

		return categorisedEntries;
	}
		
	public void
	AddInventoryEntry(AbilityData item, int amount = 1)
	{
		foreach (InventoryEntry entry in inventory)
		{
			if (entry.GetAbility () == item)
			{
				entry.IncreaseAmount(amount);
			}
		}

		InventoryEntry newItem = new InventoryEntry (item, amount);
		inventory.Add (newItem);
	}

	public void
	RemoveInventoryEntry(InventoryEntry removedEntry)
	{
		foreach (InventoryEntry entry in inventory)
		{
			if (entry.GetAbility () == removedEntry.GetAbility ())
			{
				entry.DecreaseAmount ();
			}
		}

		DeleteEmptyEntries ();
	}

	void
	DeleteEmptyEntries()
	{
		int lastIndex = Mathf.Clamp (inventory.Count - 1, 0, 1000);
		for (int i = lastIndex; i >= 0; i--)
		{
			if (inventory[i].GetAmount () < 1)
			{
				inventory.RemoveAt (i);
			}
		}
	}
}