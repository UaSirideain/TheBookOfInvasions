﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUICategory : MonoBehaviour
{
	RectTransform rt { get { return GetComponent<RectTransform> (); } }
	const int maxEntriesInRow = 15;

	[SerializeField] RectTransform rowPrefab;
	[SerializeField] InventoryUIEntry uiEntryPrefab;

	/*const*/ bool onlyShowUsableItems = true;

	public InventoryCategory categoryType; 

	List<RectTransform> rows = new List<RectTransform> ();
	List<InventoryEntry> activeEntries = new List<InventoryEntry>();
	Dictionary<InventoryEntry, InventoryUIEntry> uiEntries = new Dictionary<InventoryEntry, InventoryUIEntry> ();

	public void
	MergeItems(List<InventoryEntry> pendingEntries)
	{
		bool entriesAdded;
		bool entriesDeleted;

		AddItems (pendingEntries, out entriesAdded);
		DeleteItems (pendingEntries, out entriesDeleted);

		if (entriesAdded || entriesDeleted)
		{
			RenderCategory ();
		}

		foreach (InventoryUIEntry entry in GetUIList(activeEntries))
		{
			entry.Reset ();
		}
	}

	void
	AddItems(List<InventoryEntry> pendingEntries, out bool entriesAdded)
	{
		entriesAdded = false;

		foreach (InventoryEntry pendingEntry in pendingEntries)
		{
			if(pendingEntry.IsInList(activeEntries) == false && FilterOutUnableItems(pendingEntry))
			{
				activeEntries.Add (pendingEntry);
				uiEntries.Add (pendingEntry, CreateUIEntry(pendingEntry));
				entriesAdded = true;
			}
		}
	}

	bool
	FilterOutUnableItems(InventoryEntry entry)
	{
		if (onlyShowUsableItems)
		{
			return entry.GetAbility ().UsableFromInventory ();
		}
		else
		{
			return true;
		}
	}

	void
	DeleteItems(List<InventoryEntry> pendingEntries, out bool entriesDeleted)
	{
		entriesDeleted = false;

		if (activeEntries.Count > 0)
		{
			for (int i = Mathf.Clamp (activeEntries.Count - 1, 0, 1000); i >= 0; i--)
			{
				if (activeEntries [i].IsInList (pendingEntries) == false)
				{
					uiEntries [activeEntries [i]].onMouseClick -= EntrySelected;
					uiEntries.Remove (activeEntries [i]);
					activeEntries.RemoveAt (i);
					entriesDeleted = true;
				}
			}
		}
	}

	InventoryUIEntry
	CreateUIEntry(InventoryEntry plainEntry)
	{
		InventoryUIEntry uiEntry = Instantiate (uiEntryPrefab);
		uiEntry.Initialise (plainEntry);
		uiEntry.onMouseClick += EntrySelected;
		return uiEntry;
	}

	//It'd be lovely to get this function in the main ui script rather than the category script
	void
	EntrySelected(InventoryEntry entry)
	{
		if (FindObjectOfType<AvatarController> ().GetActiveCharacter ().UseFromInventory (entry))
		{
			transform.parent.parent.parent.GetComponent<InventoryUI> ().Deactivate ();
		}
	}

	void
	RenderCategory()
	{
		int rowsNeeded = CountRows (activeEntries.Count);
		List<RectTransform> newRows = new List<RectTransform> ();

		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 0);

		for (int i = 0; i < rowsNeeded; i++)
		{
			int startIndexForRow = i * maxEntriesInRow;
			int amountInRow = GetRowAmount (startIndexForRow, activeEntries.Count);
			List<InventoryEntry> entriesForNextRow = activeEntries.GetRange (startIndexForRow, amountInRow);
			List<InventoryUIEntry> uiEntriesForNextRow = GetUIList (entriesForNextRow);

			RectTransform row = PopulateNewRow (uiEntriesForNextRow);
			row.SetAsChildOf (rt);
			row.anchoredPosition = new Vector3 (0f, row.sizeDelta.y * -i, 0f);
			rt.sizeDelta += new Vector2 (0, row.sizeDelta.y);
			newRows.Add (row);
		}

		DeleteRows ();
		rows = newRows;
	}

	List<InventoryUIEntry>
	GetUIList(List<InventoryEntry> entries)
	{
		List<InventoryUIEntry> uiEntriesn = new List<InventoryUIEntry> ();

		foreach (InventoryEntry entry in entries)
		{
			uiEntriesn.Add (uiEntries [entry]);
		}

		return uiEntriesn;
	}
		
	void
	DeleteRows()
	{
		while (rows.Count > 0)
		{
			GameObject target = rows [0].gameObject;
			rows.RemoveAt (0);
			Destroy (target);
		}
		rows.Clear ();
	}

	RectTransform
	PopulateNewRow(List<InventoryUIEntry> entries)
	{
		RectTransform row = Instantiate (rowPrefab);

		if (entries.Count > 0)
		{
			float scaleFactor = (rt.sizeDelta.x / maxEntriesInRow) / entries [0].GetComponent<RectTransform> ().sizeDelta.x;

			float entryWidth = entries [0].GetComponent<RectTransform> ().sizeDelta.x;
			row.sizeDelta = new Vector2 (entries.Count * entryWidth, entryWidth) * scaleFactor;

			for (int i = 0; i < entries.Count; i++)
			{
				RectTransform rt2 = entries [i].GetComponent<RectTransform> ();
				rt2.SetAsChildOf (row);
				rt2.localScale = new Vector3 (scaleFactor, scaleFactor, scaleFactor);
				rt2.anchoredPosition = new Vector3 (rt2.sizeDelta.x * i, 0, 0f) * scaleFactor;
			}
		}
		return row;
	}

	int
	CountRows(int totalEntries)
	{
		int rowCount = 0;

		while (totalEntries >= maxEntriesInRow)
		{
			rowCount++;
			totalEntries -= maxEntriesInRow;
		}

		if (totalEntries > 0)
		{
			rowCount++;
		}

		return rowCount;
	}

	int
	GetRowAmount(int currentIndex, int finalIndex)
	{
		return Mathf.Clamp (finalIndex - currentIndex, 0, maxEntriesInRow);
	}
}