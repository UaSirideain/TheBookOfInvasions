﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item Database", menuName = "Inventory/ItemDatabase", order = 1)]
public class ItemDatabase : ScriptableObject
{
	[SerializeField] List<AbilityData> abilities;

	public bool
	GetItemFromID(string itemIdentity, out AbilityData foundAbility)
	{
		int itemID;

		foundAbility = null;

		if(int.TryParse(itemIdentity, out itemID))
		{
			foreach (AbilityData ability in abilities)
			{
				if (ability.GetInstanceID () == itemID)
				{
					foundAbility = ability;
					return true;
				}
			}
		}
		return false;
	}
}