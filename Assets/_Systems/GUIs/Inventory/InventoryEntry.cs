﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class InventoryEntry
{
	[SerializeField] AbilityData ability;
	[SerializeField] int amountHeld;

	public InventoryEntry(AbilityData newAbility, int newAmountHeld = 1)
	{
		ability = newAbility;
		amountHeld = newAmountHeld;
	}

	public InventoryCategory
	GetEntryType()
	{
		return ability.entryType;
	}

	public bool
	IsInList(List<InventoryEntry> anotherEntry)
	{
		foreach (InventoryEntry entry in anotherEntry)
		{
			if (entry.ability == ability)
			{
				return true;
			}
		}
		return false;
	}

	public string
	GetName()
	{
		return ability.GetSortingName ();
	}

	public AbilityData
	GetAbility()
	{
		return ability;
	}

	public int
	GetAmount()
	{
		return amountHeld;
	}

	public void
	IncreaseAmount(int amount = 1)
	{
		amountHeld += amount;
	}

	public void
	DecreaseAmount(int amount = 1)
	{
		amountHeld -= amount;
	}
}