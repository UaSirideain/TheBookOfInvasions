﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryUIBuilder : MonoBehaviour 
{
	[SerializeField] RectTransform inventoryPanel;
	[SerializeField] InventoryUICategory categoryPrefab;

	InventorySorter sorter = new InventorySorter();

	[SerializeField] List<InventoryUICategory> activeCategories = new List<InventoryUICategory> ();

	public void
	Build()
	{
		List<InventoryEntry> abilities = FindObjectOfType<AvatarController> ().GetActiveCharacter ().GetCharacter ().GetAllFieldAbilities ();

		Dictionary<InventoryCategory, List<InventoryEntry>> categorisedEntries = sorter.SortEntriesIntoCategories (abilities);

		CreateSpecificCategory (categorisedEntries, InventoryCategory.Skill);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Tool);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Relic);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Salve);
		CreateSpecificCategory (categorisedEntries, InventoryCategory.Misc);

		RenderCategories ();
	}

	void
	CreateSpecificCategory(Dictionary<InventoryCategory, List<InventoryEntry>> categorisedEntries, InventoryCategory targetType)
	{
		if (categorisedEntries.ContainsKey (targetType))
		{
			GetAppropriateCategory (targetType).MergeItems (categorisedEntries [targetType]);
		}
		else
		{
			DestroyAppropriateCategory (targetType);
		}
	}
		
	InventoryUICategory
	GetAppropriateCategory(InventoryCategory targetType)
	{
		foreach (InventoryUICategory oldCategory in activeCategories)
		{
			if (oldCategory.categoryType == targetType)
			{
				return oldCategory;
			}
		}

		InventoryUICategory newCategory = Instantiate (categoryPrefab);
		newCategory.categoryType = targetType;
		activeCategories.Add (newCategory);
		return newCategory;
	}

	void
	DestroyAppropriateCategory(InventoryCategory targetType)
	{
		for (int i = Mathf.Clamp (activeCategories.Count - 1, 0, 1000); i >= 0; i--)
		{
			if (activeCategories[i].categoryType == targetType)
			{
				GameObject emptyCategory = activeCategories [i].gameObject;
				activeCategories.RemoveAt (i);
				Destroy (emptyCategory);
			}
		}
	}

	void
	RenderCategories()
	{
		float nextCategoryYPosition = 0f;

		for (int i = 0; i < activeCategories.Count; i++)
		{
			RectTransform category_t = activeCategories[i].GetComponent<RectTransform> ();
			category_t.SetAsChildOf (inventoryPanel);
			category_t.anchoredPosition = new Vector2 (0, nextCategoryYPosition);
			nextCategoryYPosition -= (category_t.sizeDelta.y + 20);
		}
	}
}