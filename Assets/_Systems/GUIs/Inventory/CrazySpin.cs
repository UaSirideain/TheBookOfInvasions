﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazySpin : MonoBehaviour
{
	float x = 0f;
	float y = 0f;
	float z = 0f;

	[SerializeField] bool activated = false;

	void
	Start()
	{
		x = (float)(Random.Range (0, 100) / 50f);
		y = (float)(Random.Range (0, 100) / 50f);
		z = (float)(Random.Range (0, 100) / 50f);
	}

	void 
	Update () 
	{
		if (activated)
		{
			transform.Rotate (x, y, z);
		}
	}
}