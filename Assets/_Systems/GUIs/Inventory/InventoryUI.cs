﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GUIManagement;

public class InventoryUI : MonoBehaviour, IGUI, IInputtable
{
	GUIController controller;
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }
	[SerializeField] GameObject uiCamera;
	BackgroundDimmer backgroundDimmer { get { return GetComponentInChildren<BackgroundDimmer> (); } }

	[SerializeField] Text itemNameTooltip;
	[SerializeField] Text itemDescriptionTooltip;

	bool viewable = false;

	public void
	Deactivate()
	{
		uiCamera.SetActive (false);
		viewable = false;
		canvas.Disable ();
		controller.RegisterDeactivatedGUI (this);

		controller.UnhideUIs ();
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.FullscreenUI;
	}

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.OpenInventoryUI, new List<InputState> () {InputState.PausingUI, InputState.Battle}, OpenInventoryUI_Input);
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (){InputState.PausingUI, InputState.Battle}, ExitUI_Input);
	}

	void
	ExitUI_Input()
	{
		if (viewable)
		{
			Deactivate ();
		}
	}

	void
	OpenInventoryUI_Input()
	{
		if (!viewable)
		{
			Activate ();
		}
		else
		{
			Deactivate ();
		}
	}

	void
	Activate()
	{
		viewable = true;
		GetComponent<InventoryUIBuilder>().Build ();

		backgroundDimmer.DimBackground ();

		uiCamera.SetActive (true);
		viewable = true;
		controller.RegisterActivatedUI (this);
		canvas.Enable ();

		controller.TemporarilyHideUIs ();
	}

	public void
	RenderTooltip(InventoryEntry entry)
	{
		itemNameTooltip.text = entry.GetAbility ().GetUIName ();
		itemDescriptionTooltip.text = entry.GetAbility ().GetUIDescription ();
	}

	public void
	KillTooltip()
	{
		itemNameTooltip.text = "";
		itemDescriptionTooltip.text = "";
	}
}