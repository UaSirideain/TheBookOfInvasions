﻿using UnityEngine;
using Battle;
using Field.Abilities;

public enum InventoryCategory{Skill=0, Tool=1, Relic=2, Salve=3, Flora=4, Misc=5}

[CreateAssetMenu(fileName = "New Item-Ability", menuName = "Inventory/New Item-Ability", order = 1)]
public class AbilityData : ScriptableObject
{
	[SerializeField] public InventoryCategory entryType;
	[SerializeField] bool depletesOnUse;
	[SerializeField] bool mustBeReadied;
	[SerializeField] bool usableFromInventory;
	[SerializeField] bool usableFromQuickInventory;

	[SerializeField] GameObject displayModel;
	[SerializeField] Sprite displayIcon;
	//[SerializeField] string displayName;	//the name that renders to the player
	[SerializeField] string sortingName;	//the name that defines alphabetical sorting
	//[SerializeField] string description;

	[SerializeField] ActionComposite battleAction;
	[SerializeField] FieldAbility ability;

	[SerializeField] KeyWord displayName;
	[SerializeField] KeyWord description;

	public bool
	MustBeReadied()
	{
		return mustBeReadied;
	}

	public Sprite
	GetUIIcon()
	{
		return displayIcon;
	}

	public GameObject
	GetUIModel()
	{
		return displayModel;
	}

	public string
	GetUIName()
	{
		return Language.Localise (displayName);
		//return displayName;
	}

	public Battle.ActionComposite
	GetBattleEffect()
	{
		return battleAction;
	}

	public string
	GetSortingName()
	{
		if (sortingName == null || sortingName == "")
		{
			return Language.Localise(displayName);
		}
		else
		{
			return sortingName;
		}
	}

	public string
	GetUIDescription()
	{
		return Language.Localise (description);
		//return description;
	}

	public InventoryCategory
	GetUICategory()
	{
		return entryType;
	}

	public bool
	DepletesOnUse()
	{
		return depletesOnUse;
	}

	public FieldAbility
	GetFieldAbility()
	{
		return ability;
	}

	public bool
	UsableFromQuickInventory()
	{
		return usableFromQuickInventory;
	}

	public bool
	UsableFromInventory()
	{
		return usableFromInventory;
	}
}