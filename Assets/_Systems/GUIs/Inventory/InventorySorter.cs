﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySorter
{
	Dictionary<InventoryCategory, List<InventoryEntry>> sortedEntries = new Dictionary<InventoryCategory, List<InventoryEntry>> ();

	public Dictionary<InventoryCategory, List<InventoryEntry>>
	SortEntriesIntoCategories(List<InventoryEntry> unsortedEntries)
	{
		sortedEntries.Clear ();

		foreach (InventoryEntry entry in unsortedEntries)
		{
			SortEntryIntoCategory (entry);
		}

		SortCategoriesByEntryNames ();

		return sortedEntries;
	}

	void
	SortEntryIntoCategory(InventoryEntry entry)
	{
		InventoryCategory entryType = entry.GetEntryType ();
		if (sortedEntries.ContainsKey(entryType))
		{
			sortedEntries [entryType].Add (entry);
		}
		else
		{
			sortedEntries.Add (entryType, new List<InventoryEntry>(){entry});
		}
	}

	void
	SortCategoriesByEntryNames()
	{
		foreach (KeyValuePair<InventoryCategory, List<InventoryEntry>> keyValuePair in sortedEntries)
		{
			sortedEntries[keyValuePair.Key].Sort (CompareNames);
		}
	}

	int
	CompareNames(InventoryEntry entry1, InventoryEntry entry2)
	{
		return entry1.GetName ().CompareTo (entry2.GetName ());
	}
}