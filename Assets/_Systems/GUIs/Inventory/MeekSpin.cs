﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeekSpin : MonoBehaviour
{
	const float x = 0f;
	const float y = 2f;
	const float z = 0f;

	bool activated = false;

	public void
	StartSpinning()
	{
		activated = true;
	}

	public void
	StopSpinning()
	{
		activated = false;
	}

	void 
	Update () 
	{
		if (activated)
		{
			transform.Rotate (x, y, z);
		}
	}
}