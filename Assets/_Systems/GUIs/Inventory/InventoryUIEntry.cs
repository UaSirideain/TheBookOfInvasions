﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIEntry : MonoBehaviour
{
	[SerializeField] MeekSpin meekSpin;
	[SerializeField] Transform modelContainer;
	[SerializeField] Image iconGraphic;
	InventoryEntry myEntry;

	Quaternion qIdentity;

	public void
	Initialise(InventoryEntry entry)
	{
		qIdentity = modelContainer.localRotation;

		myEntry = entry;
		if (entry.GetAbility ().GetUIModel () != null)
		{
			Transform uiModel = Instantiate (entry.GetAbility ().GetUIModel ()).transform;
			uiModel.SetParent (modelContainer);
			uiModel.localScale = new Vector3 (1, 1, 1);
			uiModel.localPosition = Vector3.zero;
			uiModel.localRotation = Quaternion.identity;
			uiModel.SetAs (UnityLayer.UIModels);
		}
		else
		{
			iconGraphic.sprite = entry.GetAbility().GetUIIcon();
			iconGraphic.enabled = true;
		}
	}

	public void
	Reset()
	{
		modelContainer.transform.localRotation = qIdentity;
	}

	public void
	MouseEnter()
	{
		meekSpin.StartSpinning();
		FindObjectOfType<InventoryUI> ().RenderTooltip (myEntry);
	}

	public void
	MouseExit()
	{
		meekSpin.StopSpinning();
		FindObjectOfType<InventoryUI> ().KillTooltip ();
	}

	public delegate void OnMouseClick(InventoryEntry entry);
	public OnMouseClick onMouseClick;

	public void
	MouseClick()
	{
		if (onMouseClick != null)
		{
			onMouseClick (myEntry);
		}
	}
}