﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GUIManagement{

public class GUIController : MonoBehaviour, IStateChanger
{
	ActiveStates activeStates;

	List<IGUI> activeGUIs = new List<IGUI> ();

	GUIRankSorter guiSorter = new GUIRankSorter();

	public void
	SetStateMachine(ActiveStates newActiveStates)
	{
		activeStates = newActiveStates;
	}

	public void
	InductNewUI(IGUI gui)
	{
		gui.SetOverrideController (this);

		GetComponent<InputValidator> ().ValidateSubscriptions ();
	}

	void
	OnEnable()
	{
		GetComponent<InputListener> ().SetListening ();

		foreach (IGUI gui in GetComponentsInChildren<IGUI>())
		{
			gui.SetOverrideController (this);
		}
	}

	public void
	RegisterActivatedUI(IGUI newGUI)
	{
		List<IGUI> inferiorGUIs = guiSorter.GetInferiorGUIs (newGUI, activeGUIs);

		foreach (IGUI gui in inferiorGUIs)
		{
			gui.Deactivate ();
		}

		activeGUIs.Add (newGUI);

		UpdateStates ();
	}

	public void
	RegisterDeactivatedGUI(IGUI newGUI)
	{
		activeGUIs.Remove (newGUI);

		UpdateStates ();
	}

	void
	UpdateStates()
	{
		activeStates.Clear ();

		foreach (IGUI gui in activeGUIs)
		{
			if (gui.GetGUIType () == GUIType.PausingUI)
			{
				activeStates.WriteState (InputState.PausingUI);
			}
			else if (gui.GetGUIType () == GUIType.FullscreenUI)
			{
				activeStates.WriteState (InputState.FullscreenUI);
			}
			else if (gui.GetGUIType () == GUIType.UI)
			{
				activeStates.WriteState (InputState.UI);
			}
			else if (gui.GetGUIType () == GUIType.Settings)
			{
				activeStates.WriteState (InputState.SettingsUI);
			}
		}
	}

	public List<InputState>
	GetActiveStates()
	{
		return activeStates.GetActiveStates ();
	}

	public void
	TemporarilyHideUIs()
	{
		foreach (OverlayUI overlayUI in FindObjectsOfType<OverlayUI> ())
		{
			overlayUI.TemporarilyHide ();
		}
	}

	public void
	UnhideUIs()
	{
		foreach (OverlayUI overlayUI in FindObjectsOfType<OverlayUI> ())
		{
			overlayUI.ReturnToPreviousState ();
		}
	}
}
}