﻿using System.Collections.Generic;

namespace GUIManagement{

public class GUIRankSorter
{
	public List<IGUI>
	GetInferiorGUIs(IGUI newGUI, List<IGUI> activeGUIs)
	{
		List<IGUI> guisToDeactivate = new List<IGUI> ();

		GUIType newType = newGUI.GetGUIType ();

		foreach (IGUI gui in activeGUIs)
		{
			GUIType oldType = gui.GetGUIType ();

			if (newType == GUIType.UI)
			{
				if (oldType == GUIType.UI || oldType == GUIType.HUD)
				{
					guisToDeactivate.Add (gui);
				}
			}
			else if (newType == GUIType.FullscreenUI)
			{
				if (oldType == GUIType.HUD || oldType == GUIType.UI || oldType == GUIType.HUD || oldType == GUIType.FullscreenUI)
				{
					guisToDeactivate.Add (gui);
				}
			}
			else if (newType == GUIType.PausingUI)
			{
				if (oldType == GUIType.HUD)
				{
					guisToDeactivate.Add (gui);
				}
			}
		}

		return guisToDeactivate;
	}
}
}