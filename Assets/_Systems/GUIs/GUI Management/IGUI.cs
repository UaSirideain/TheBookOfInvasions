﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GUIManagement{

public enum GUIType{HUD, UI, FullscreenUI, PausingUI, Settings, Cohabiting}

public interface IGUI
{
	void
	Deactivate ();

	GUIType
	GetGUIType ();

	void
	SetOverrideController (GUIController overrideController);
}
}