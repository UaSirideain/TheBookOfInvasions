﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayUI : MonoBehaviour
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

	float originalState;
	float setState;

	public void
	TemporarilyHide()
	{
		originalState = canvas.alpha;
		canvas.alpha = 0f;
		setState = canvas.alpha;
	}

	public void
	ReturnToPreviousState()
	{
		if (canvas.alpha == setState)
		{
			canvas.alpha = originalState;
		}
	}
}