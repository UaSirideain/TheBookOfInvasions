﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GUIManagement{

public class LanguageSettingsUI : SettingsUI
{
	[SerializeField] RectTransform uiBounds;

	[SerializeField] Text irishText;
	[SerializeField] Text englishText;

	[SerializeField] RectTransform languageOptionButtonPrefab;
	[SerializeField] RectTransform languageOptionBounds;

	Dictionary<LanguageType, RectTransform> buttonD = new Dictionary<LanguageType, RectTransform>();

	bool built = false;

	protected override void
	Initialise()
	{
		if (!built)
		{
			built = true;
			for (int i = 0; i < Language.GetSupportedLanguages ().Count; i++)
			{
				LanguageType language = Language.GetSupportedLanguages () [i];

				RectTransform languageOption = Instantiate (languageOptionButtonPrefab);
				languageOption.SetAsChildOf (languageOptionBounds);
				languageOption.anchoredPosition = new Vector2 (0f, -i * languageOption.sizeDelta.y);
				languageOption.GetComponentInChildren<Text> ().text = language.ToString ();

				languageOption.GetComponentInChildren<Button> ().onClick.AddListener (delegate{SetLanguage (language);});

				buttonD.Add (language, languageOption);
			}

			languageOptionBounds.sizeDelta = new Vector2 (languageOptionBounds.sizeDelta.x, Language.GetSupportedLanguages ().Count * languageOptionButtonPrefab.sizeDelta.y);
			uiBounds.sizeDelta = new Vector2 (uiBounds.sizeDelta.x, uiBounds.sizeDelta.y + languageOptionBounds.sizeDelta.y);
		}
		RefreshButtons ();
	}

	public void
	SetLanguage(LanguageType language)
	{
		initialLanguage = Language.activeLanguage;

		Language.activeLanguage = language;
		RefreshButtons ();
		RefreshTexts ();
	}

	void
	RefreshButtons()
	{
		GreyButtons ();
		buttonD[Language.activeLanguage].GetComponentInChildren<Text>().color = Colour.opaque;
	}

	void
	GreyButtons()
	{
		foreach (KeyValuePair<LanguageType, RectTransform> pair in buttonD)
		{
			pair.Value.GetComponentInChildren<Text>().color = Colour.greyedOut;
		}
	}

	void
	RefreshTexts()
	{
		foreach (TextCompanion text in GameObject.FindObjectsOfType<TextCompanion>())
		{
			text.Refresh ();
		}
	}

	public override void
	StoreCurrentSettings(){}

	public override KeyWord
	GetMenuName()
	{
		return KeyWord.Language;
	}

	LanguageType initialLanguage;

	public void
	DiscardChanges()
	{
		SetLanguage (initialLanguage);
		Deactivate ();
	}

	public void
	SaveChanges()
	{
		Deactivate ();
	}
}
}