﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class LanguageSettingsSerialiser : MonoBehaviour, ISaveable
{
	SaveGameController saveData { get { return FindObjectOfType<SaveGameController> (); } }
	SaveDataHeading saveDataHeading = SaveDataHeading.LanguageSettings;

	public void
	Save()
	{
		Dictionary<string, string> languageData = new Dictionary<string, string> ();

		languageData.Add ("Language", Language.activeLanguage.ToString());
		//seanchló

		SaveDataBlock newBlock = new SaveDataBlock (saveDataHeading, languageData);

		saveData.RecordSaveData (newBlock);
	}

	void
	Load(Dictionary<string, string> savedData)
	{
		string stringValue;
		if (savedData.TryGetValue ("Language", out stringValue))
		{
			Language.activeLanguage = (LanguageType)Enum.Parse (typeof(LanguageType), stringValue);
		}
		//seanchló
	}

	public void
	Load(List<SaveDataBlock> block)
	{
		foreach (SaveDataBlock blocko in block)
		{
			if (blocko.GetHeading () == saveDataHeading)
			{
				Load (blocko.GetData ());
			}
		}
	}
}