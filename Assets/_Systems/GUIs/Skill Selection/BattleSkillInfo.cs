﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Battle;

public class BattleSkillInfo
{
	public readonly string title;
	public readonly string cost;
	public readonly string description;

	public readonly SkillCardIcon skillCardIcon;
	public readonly SkillCardColour skillCardColour;

	public BattleSkillInfo(string newTitle, string newCost, string newDescription, SkillCardIcon newSkillCardIcon = SkillCardIcon.Stance, SkillCardColour newSkillCardColour = SkillCardColour.Green)
	{
		title = newTitle;
		cost = newCost;
		description = newDescription;
		skillCardIcon = newSkillCardIcon;
		skillCardColour = newSkillCardColour;
	}
}