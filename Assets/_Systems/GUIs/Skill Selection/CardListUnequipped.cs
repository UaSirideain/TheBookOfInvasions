﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardListUnequipped : MonoBehaviour
{
	RectTransform t{ get { return GetComponent<RectTransform> (); } }

	[SerializeField] RectTransform viewport;
	[SerializeField] RectTransform limiter;

	public void
	SetSizeOfViewport()
	{
		if (viewport != null)
		{
			float newHeight = t.sizeDelta.y;

			if (newHeight > GetBottomOfEquippedPane() - 150)
			{
				newHeight = GetBottomOfEquippedPane() - 150;
			}

			viewport.sizeDelta = new Vector2(viewport.sizeDelta.x, newHeight);
		}
	}

	float
	GetBottomOfEquippedPane()
	{
		Vector3[] corners = new Vector3[4];
		limiter.GetWorldCorners (corners);

		float lowestY = 99999;
		foreach(Vector3 corner in corners)
		{
			if(corner.y < lowestY)
			{
				lowestY = corner.y;
			}
		}

		return lowestY;
	}
}