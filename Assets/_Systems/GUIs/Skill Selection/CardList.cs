﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Battle;

namespace Field.SkillSelection{

public class CardList : MonoBehaviour
{
	RectTransform t { get { return GetComponent<RectTransform> (); } }
	[SerializeField] Text costSorterButton;
	[SerializeField] Text typeSorterButton;
	[SerializeField] Image contractExpandButton;

	List<Card> cards = new List<Card> ();

	enum SortOrder{Type, Cost}
	SortOrder sortOrder;

	bool compact = true;

	public void
	ToggleSize()
	{
		compact = !compact;
		PositionCards ();
		UpdateGraphic ();
	}

	public void
	SortByCost()
	{
		sortOrder = SortOrder.Cost;
		PositionCards ();
	}

	public void
	SortByType()
	{
		sortOrder = SortOrder.Type;
		PositionCards ();
	}

	void
	CostSort()
	{
		List<Card> sortedCards = new List<Card> ();

		for(int i = 0; i < 20; i++)
		{
			foreach (Card card in cards)
			{
				if (card.GetCost () == i)
				{
					sortedCards.Add (card);
				}
			}
		}

		cards = sortedCards;
	}

	void
	TypeSort()
	{
		List<Card> sortedCards = new List<Card> ();

		for (int i = 0; i < 7; i++)
		{
			foreach (Card card in cards)
			{
				if ((int)card.GetSkillType () == i)
				{
					sortedCards.Add (card);
				}
			}
		}

		cards = sortedCards;
	}

	public void
	Induct(List<Card> newCards)
	{
		DestroyCards ();
		cards.Clear ();

		foreach (Card card in newCards)
		{
			if (cards.Contains (card) == false)
			{
				cards.Add (card);
				card.transform.SetParent(transform);
				card.transform.localScale = new Vector3 (1, 1, 1);
			}
		}

		foreach (Card card in cards)
		{
			if (newCards.Contains (card) == false)
			{
				newCards.Remove (card);
			}
		}

		PositionCards ();
	}

	public void
	PositionCards()
	{
		CheckSortOrder ();
		LayoutCards ();

		CallUnequippedMethods ();
		CallEquippedMethods ();
	}

	void
	LayoutCards()
	{
		int currentRow = 0;
		float paneHeight = 0;

		for (int i = 0; i < cards.Count; i++)
		{
			RectTransform card_t = cards[i].GetComponent<RectTransform> ();

			cards [i].SetCompact (compact);

			if (i.IsEven())
			{
				card_t.anchoredPosition = new Vector2 (0, -2 + currentRow * (card_t.sizeDelta.y + 2));
				paneHeight += card_t.sizeDelta.y + 2;
			}
			else
			{
				card_t.anchoredPosition = new Vector2 (card_t.sizeDelta.x + 2, -2 + currentRow * (card_t.sizeDelta.y + 2));
				currentRow--;
			}
		}

		t.sizeDelta = new Vector2 (t.sizeDelta.x, paneHeight);
	}

	void
	CheckSortOrder()
	{
		if (sortOrder == SortOrder.Type)
		{
			TypeSort ();
			costSorterButton.fontStyle = FontStyle.Normal;
			typeSorterButton.fontStyle = FontStyle.Bold;
		}
		else
		{
			CostSort ();
			costSorterButton.fontStyle = FontStyle.Bold;
			typeSorterButton.fontStyle = FontStyle.Normal;
		}
	}
		
	void
	DestroyCards()
	{
		foreach (Card card in cards)
		{
			Destroy (card.gameObject);
		}
	}
		
	void
	CallEquippedMethods()
	{
		if (GetComponent<CardListEquipped> ())
		{
			GetComponent<CardListEquipped> ().SetSizeOfUnequipped ();
		}
	}

	void
	CallUnequippedMethods()
	{
		if (GetComponent<CardListUnequipped> ())
		{
			GetComponent<CardListUnequipped> ().SetSizeOfViewport ();
		}
	}

	[SerializeField] Sprite expandSprite;
	[SerializeField] Sprite contractSprite;

	void
	Start()
	{
		UpdateGraphic ();
	}

	void
	UpdateGraphic()
	{
		if (compact)
		{
			contractExpandButton.sprite = expandSprite;
		}
		else
		{
			contractExpandButton.sprite = contractSprite;
		}
	}
}
}