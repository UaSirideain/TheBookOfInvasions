﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Battle;

[CreateAssetMenu(fileName = "New Card Data", menuName = "SkillSelection/CardData", order = 1)]
public class CardData : ScriptableObject
{
	[SerializeField] Sprite enchantment;
	[SerializeField] Sprite enchantmentDisabled;

	[Space(4)]

	[SerializeField] Sprite feat;
	[SerializeField] Sprite featDisabled;

	[Space(4)]

	[SerializeField] Sprite item;
	[SerializeField] Sprite itemDisabled;


	[Space(4)]

	[SerializeField] Sprite music;
	[SerializeField] Sprite musicDisabled;


	[Space(4)]

	[SerializeField] Sprite reserve;
	[SerializeField] Sprite reserveDisabled;


	[Space(4)]

	[SerializeField] Sprite stance;
	[SerializeField] Sprite stanceDisabled;


	[Space(16)]

	[SerializeField] Sprite neutral;
	[SerializeField] Sprite neutralHover;

	[Space(4)]

	[SerializeField] Sprite friendly;
	[SerializeField] Sprite friendlyHover;

	[Space(4)]

	[SerializeField] Sprite hostile;
	[SerializeField] Sprite hostileHover;

	[Space(4)]

	[SerializeField] Sprite disabled;

	[Space(4)]
	[SerializeField] Image uiIconPrefab;
	[SerializeField] Text uiTitlePrefab;
	[SerializeField] Text uiDescriptionPrefab;


	public Sprite
	GetIconSprite(SkillCardIcon type)
	{
		if (type == SkillCardIcon.Enchantment)
		{
			return enchantment;
		}
		if (type == SkillCardIcon.Feat)
		{
			return feat;
		}
		if (type == SkillCardIcon.Item)
		{
			return item;
		}
		else if (type == SkillCardIcon.Music)
		{
			return music;
		}
		else if (type == SkillCardIcon.Stance)
		{
			return music;
		}
		else if (type == SkillCardIcon.Reserve)
		{
			return reserve;
		}

		return null;
	}

	public Sprite
	GetDisabledIconSprite(SkillCardIcon type)
	{
		if (type == SkillCardIcon.Enchantment)
		{
			return enchantmentDisabled;
		}
		if (type == SkillCardIcon.Feat)
		{
			return featDisabled;
		}
		if (type == SkillCardIcon.Item)
		{
			return itemDisabled;
		}
		else if (type == SkillCardIcon.Music)
		{
			return musicDisabled;
		}
		return null;
	}

	public Sprite
	GetBackgroundSprite(SkillCardColour disposition)
	{
		if (disposition == SkillCardColour.Blue)
		{
			return friendly;
		}
		else if (disposition == SkillCardColour.Red)
		{
			return hostile;
		}
		else
		{
			return neutral;
		}
	}

	public Sprite
	GetBackgroundHoverSprite(SkillCardColour disposition)
	{
		if (disposition == SkillCardColour.Blue)
		{
			return friendlyHover;
		}
		else if (disposition == SkillCardColour.Red)
		{
			return hostileHover;
		}
		else
		{
			return neutral;
		}
	}

	public Sprite
	GetDisabledBackgroundSprite()
	{
		return disabled;
	}

	public Image
	GetUIIconPrefab()
	{
		return uiIconPrefab;
	}

	public Text
	GetUITitlePrefab()
	{
		return uiTitlePrefab;
	}

	public Text
	GetUIDescriptionPrefab()
	{
		return uiDescriptionPrefab;
	}

	public Sprite
	GetSmallIcon(SkillCardIcon cardIcon)
	{
		return GetIconSprite (cardIcon);
	}
}