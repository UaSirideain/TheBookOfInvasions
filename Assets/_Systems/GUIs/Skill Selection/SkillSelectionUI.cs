﻿using System.Collections.Generic;
using UnityEngine;
using GUIManagement;

namespace Field.SkillSelection{

public class SkillSelectionUI : MonoBehaviour, IGUI, IInputtable
{
    CanvasGroup canvas {get{return GetComponent<CanvasGroup>();}}

    [SerializeField] CharacterPane characterPanePrefab;

    [SerializeField] Transform characters;

    List<CharacterPane> panes = new List<CharacterPane>();

    bool opened = false;

	GUIController overrideController;

	public void
	SetOverrideController(GUIController newOverrideController)
	{
		overrideController = newOverrideController;
		transform.SetParent (overrideController.transform);
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.OpenSkillSelectionUI, new List<InputState> (){InputState.FullscreenUI, InputState.PausingUI, InputState.Battle}, OpenSkillSelectionUI_Input);
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (), ExitUI_Input);
	}

	void
	OpenSkillSelectionUI_Input()
	{
		CreateCharacterPanes ();
		ToggleCanvas ();
	}

	void
	ExitUI_Input()
	{
		if (opened)
		{
			Deactivate ();
		}
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.UI;
	}

    void
    ToggleCanvas()
    {
	    if (!opened)
	    {
			Activate ();
	    }
	    else
	    {
			Deactivate ();
	    }
    }

	void
	Activate()
	{
		canvas.Enable ();
		overrideController.RegisterActivatedUI (this);
		opened = true;
	}

	public void
	Deactivate()
	{
		canvas.Disable ();
		overrideController.RegisterDeactivatedGUI (this);
		opened = false;
	}

    void
    CreateCharacterPanes()
    {
	    if (panes.Count == 0)
	    {
		    Vector2 newPosition = new Vector2 (44, 0);

		    foreach (Transform character in characters)
		    {
			    CharacterPane pane = Instantiate (characterPanePrefab, transform);
			    panes.Add (pane);

			    pane.CreateSkills (character.GetComponent<Character> ());

			    pane.GetComponent<RectTransform> ().anchoredPosition = newPosition;

			    newPosition = new Vector2 (newPosition.x + 24 + pane.GetComponent<RectTransform> ().sizeDelta.x, 0);
		    }
	    }
    }
}
}