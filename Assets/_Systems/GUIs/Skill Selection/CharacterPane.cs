﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Battle;

namespace Field.SkillSelection{

public class CharacterPane : MonoBehaviour
{
	[SerializeField] Card cardPrefab;
	[SerializeField] Text characterName;
	[SerializeField] CardList equipped;
	[SerializeField] CardList unequipped;

	public void
	CreateSkills(Character character)
	{
		characterName.text = character.name;
	
		List<Card> equippedCards = CreateCards (character.GetEquippedBattleSkills (), character);
		equipped.Induct(equippedCards);

		List<Card> unequippedCards = CreateCards (character.GetUnequippedBattleSkills (), character);
		unequipped.Induct(unequippedCards);
	}

	List<Card>
	CreateCards(List<BattleSkill> skills, Character character)
	{
		List<Card> cards = new List<Card> ();

		foreach (BattleSkill skill in skills)
		{
			Card card = Instantiate(cardPrefab);

			card.Initialise (skill);
			card.GetComponent<Button>().onClick.AddListener(delegate{character.ToggleSkillEquipped(skill);});
			card.GetComponent<Button>().onClick.AddListener(delegate{CreateSkills(character);});

			cards.Add (card);
		}

		return cards;
	}
}
}