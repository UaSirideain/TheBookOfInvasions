﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Battle;

namespace Field.SkillSelection{

public class Card : MonoBehaviour
{
	[SerializeField] Image background;
	[SerializeField] Image icon;
	[SerializeField] Text title;
	[SerializeField] Text cost;
	[SerializeField] Text description;

	[SerializeField] CardData data;

	SkillCardIcon skillType;

	RectTransform t { get { return GetComponent<RectTransform> (); } }

	public void
	Initialise(BattleSkill skill)
	{
		BattleSkillInfo info = skill.GetInfo ();
		title.text = skill.GetInfo ().title;
		cost.text = skill.GetInfo ().cost;
		description.text = skill.GetInfo ().description;

		icon.sprite = data.GetIconSprite (info.skillCardIcon);
		background.sprite = data.GetBackgroundSprite (info.skillCardColour);
		skillType = info.skillCardIcon;
	}

	public void
	SetCompact(bool compact)
	{
		if (compact)
		{
			Contract ();
		}
		else
		{
			Expand ();
		}
	}

	void
	Contract()
	{
		description.enabled = false;
		t.sizeDelta = new Vector2 (t.sizeDelta.x, 30);
	}

	void
	Expand()
	{
		description.enabled = true;
		t.sizeDelta = new Vector2 (t.sizeDelta.x, 100);
	}

	public int
	GetCost()
	{
		return int.Parse(cost.text);
	}

	public SkillCardIcon
	GetSkillType()
	{
		return skillType;
	}
}
}