﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardListEquipped : MonoBehaviour
{
	[SerializeField] CardListUnequipped unequippedList;

	public void
	SetSizeOfUnequipped()
	{
		if (unequippedList != null)
		{
			unequippedList.SetSizeOfViewport ();
		}
	}
}