﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleScreenNewButton : MonoBehaviour
{
	[SerializeField] Text text;
	[SerializeField] LoadingUI loadingUI;

	public void
	OnPointerClick()
	{
		StartCoroutine(loadingUI.StartLoading (1));
	}

	public void
	OnPointerEnter()
	{
		text.color = new Color (1f, 1f, 1f);
	}

	public void
	OnPointerExit()
	{
		text.color = new Color (.184f, .219f, .145f);
	}
}