﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour
{
	[SerializeField] Image background;
	[SerializeField] Slider loadingBar;

	AsyncOperation sceneLoad;

	public IEnumerator
	StartLoading(int sceneToLoad)
	{
		StartCoroutine (Run (sceneToLoad));
		yield return null;
	}

	IEnumerator
	Run(int sceneToLoad)
	{
		yield return FadeToBlack ();
		LoadLevel (sceneToLoad);
		yield return LoadingBar ();
		yield return new WaitForSeconds (.3f);
		yield return FadeOut ();
	}

	void
	LoadLevel(int levelIndex)
	{
		sceneLoad = SceneManager.LoadSceneAsync (1);
		sceneLoad.allowSceneActivation = true;
	}

	void
	Awake()
	{
		DontDestroyOnLoad (this.gameObject);
	}
		
	IEnumerator
	LoadingBar()
	{
		while (sceneLoad.isDone == false)
		{
			loadingBar.value = sceneLoad.progress;

			yield return null;
		}
	}

	const float duration = .8f;

	IEnumerator
	FadeToBlack()
	{
		float t = 0f;

		while (t < 1.3f)
		{
			background.color = Color.Lerp (Colour.transparentBlack, Colour.opaqueBlack, t);
			t += Time.deltaTime / duration;
			yield return null;
		}
		loadingBar.gameObject.SetActive (true);
	}

	IEnumerator
	FadeOut()
	{
		loadingBar.gameObject.SetActive (false);

		float t = 0f;

		while (t < 1.3f)
		{
			background.color = Color.Lerp (Colour.opaqueBlack, Colour.transparentBlack, t);
			t += Time.deltaTime / duration;
			yield return null;
		}
	}
}