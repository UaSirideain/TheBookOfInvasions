﻿using UnityEngine;
using UnityEngine.UI;

public class TitleScreenLoadButton : MonoBehaviour
{
	[SerializeField] Text text;

	public void
	OnPointerClick()
	{

	}

	public void
	OnPointerEnter()
	{
		text.color = new Color (1f, 1f, 1f);
	}

	public void
	OnPointerExit()
	{
		text.color = new Color (.184f, .219f, .145f);
	}
}