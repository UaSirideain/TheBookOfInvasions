﻿using UnityEngine;

namespace Field.Flora{

public class FloraBuilder : MonoBehaviour
{
    public GameObject
    BuildFlora(FloraViewer viewer, FloraViewerData data, FloraUI ui)
    {
        GameObject floraInstance = InstantiateFlora(data, ui);
        InitialiseFlora(viewer, ui, floraInstance);
        return floraInstance;
    }

    private GameObject
    InstantiateFlora(FloraViewerData data, FloraUI ui)
    {
        GameObject floraInstance = Instantiate(data.gameObject, Vector3.zero, Quaternion.identity);
        floraInstance.transform.SetParent(ui.GetRenderPoint(), false);
        return floraInstance;
    }

    private void
    InitialiseFlora(FloraViewer viewer, FloraUI ui, GameObject floraInstance)
    {
        foreach (FloraPart floraPart in floraInstance.GetComponent<FloraViewerData>().GetFloraParts())
        {
            floraPart.gameObject.AddComponent<MeshCollider>();
            floraPart.Initialise(viewer, ui);
        }
    }
}
}