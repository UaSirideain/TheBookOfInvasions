﻿using UnityEngine;

namespace Field.Flora
{
    public class CursorManager : System.Object
    {
        public void
           SetCursor(bool activeState)
        {
            Cursor.visible = activeState;
            if (!activeState)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}