﻿using GUIManagement;
using Notifications;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Flora{

public class FloraViewer : MonoBehaviour, IInputtable, IGUI
{
    Inventory inventory { get { return FindObjectOfType<Inventory>(); } }
    NotificationUI notifications { get { return FindObjectOfType<NotificationUI>(); } }

    FloraUI ui { get { return GetComponent<FloraUI>(); } }
    FloraBuilder builder { get { return GetComponent<FloraBuilder>(); } }
    MouseRotate mouseRotate { get { return GetComponentInChildren<MouseRotate>(true); } }

	[SerializeField] BackgroundDimmer backgroundDimmer;

    bool active = false;

    public void
    SubscribeTo(InputValidator input)
    {
		input.SubscribeToStringInput (InputMode.Down, InputButton.ExitUI, new List<InputState> (), Escape);
    }

    void
    Escape()
    {
	    if (active)
	    {
		    Deactivate ();
	    }
    }

    GUIController controller;

    public void
    SetOverrideController(GUIController newController)
    {
	    controller = newController;
	    transform.SetParent (controller.transform);
    }

    public GUIType
    GetGUIType()
    {
	    return GUIType.FullscreenUI;
    }
            
    public void
    Initialise(FloraViewerData data)
    {
        mouseRotate.Initialise(builder.BuildFlora(this, data, ui));
		backgroundDimmer.DimBackground ();
        controller.RegisterActivatedUI(this);
        SetActive(true);
    }

    public void
    Deactivate()
    {
        SetActive(false);
        mouseRotate.Deactivate();
        controller.RegisterDeactivatedGUI(this);
    }

    public void
	AddToInventory(int amount, AbilityData item)
    {
		inventory.AddInventoryEntry(item, amount);
		notifications.DisplayNotification(item.GetUIName() + " has been added to the inventory");
        CheckForTarget();
    }

    private void
    CheckForTarget()
    {
        if (!mouseRotate.WeHaveATarget())
        {
            Deactivate();
        }
    }

    private void
    SetActive(bool active)
    {
        this.active = active;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(active);
        }
    }
}
}