﻿using UnityEngine;
using UnityEngine.UI;

public class FloraUI : MonoBehaviour
{
    [SerializeField] Text floraName;
    [SerializeField] Text floraDescription;

    [SerializeField] Transform background;
    [SerializeField] Transform renderPoint;

    bool stopped;
    
    public void
    SetText(string fName, string fDescription)
    {
        if (!stopped)
        {
            floraName.text = fName;
            floraDescription.text = fDescription;
        }
    }

    public void
    StopTooltip()
    {
        SetText("", "");
        stopped = true;
    }

    public void
    ResumeTooltip()
    {
        stopped = false;
    }

    public Transform
    GetRenderPoint()
    {
        return renderPoint;
    }
}
