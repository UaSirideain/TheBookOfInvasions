﻿using System.Collections.Generic;
using UnityEngine;

namespace Field.Flora{

public class MouseRotate : MonoBehaviour, IInputtable
{
    Camera cam { get { return GetComponent<Camera>(); } }
    FloraUI ui { get { return GetComponentInParent<FloraUI>(); } }
    CursorManager manager = new CursorManager();

    bool weShouldRotate = false;

    private Transform target;
    private Vector3 distance = new Vector3();

    private float speed = .01f;

    private float x = 0f;
    private float y = 0f;

    private Vector3 startPosition;
    private Quaternion startOrientation;

    public bool
    WeHaveATarget()
    {
        return target.GetComponent<FloraViewerData>().GetPickableFloraCount() > 0;
    }

    public void
    Initialise(GameObject floraInstance)
    {
        cam.enabled = true;
        Setup(floraInstance);

        x = target.eulerAngles.y;
        y = target.eulerAngles.x;

        distance = new Vector3 (0f, 0f, -Vector3.Distance(transform.position, target.position));
    }

    public void
    Deactivate()
    {
        HardReset();
    }

    public void
    SubscribeTo(InputValidator input)
    {
        input.SubscribeToMouseAxesInput(new List<InputState>(), MouseAxes);
		input.SubscribeToStringInput(InputMode.Down, InputButton.RightMouse, new List<InputState>(), RightMouseDown);
		input.SubscribeToStringInput(InputMode.Up, InputButton.RightMouse, new List<InputState>(), RightMouseUp);
        input.SubscribeToStringInput(InputMode.Down, InputButton.ResetFloraPosition, new List<InputState>(), ResetFloraPosition_Input);
    }

    private void
    MouseAxes(float mouseX, float mouseY)
    {
        if (target && weShouldRotate && cam.enabled)
        {
            x += mouseX * speed * Time.deltaTime;
            y -= mouseY * speed * Time.deltaTime;

            transform.rotation = UpdateRotation(x, y);
            transform.position = UpdatePosition(transform.rotation);
        }
    }

    private void
    RightMouseDown()
    {
        if (cam.enabled)
        {
            manager.SetCursor(false);
            weShouldRotate = true;
            ui.StopTooltip();
        }
    }

    private void
    RightMouseUp()
    {
        if (cam.enabled)
        {
            weShouldRotate = false;
            manager.SetCursor(true);
            ui.ResumeTooltip();
        }
    }

    private void
    ResetFloraPosition_Input()
    {
        if (cam.enabled)
        {
            SoftReset();
        }
    }

    private Quaternion
    UpdateRotation(float x, float y)
    {
        return Quaternion.Euler(y, x, 0);
    }

    private Vector3
    UpdatePosition(Quaternion rotation)
    {
        return (rotation * distance) + target.position;
    }

    private void
    Setup(GameObject floraInstance)
    {
        target = floraInstance.transform;
        startPosition = transform.position;
        startOrientation = transform.rotation;
    }

    private void
    SoftReset()
    {
        transform.rotation = startOrientation;
        transform.position = startPosition;
        x = 0f;
        y = 0f;
    }

    private void 
    HardReset()
    {
        SoftReset();
        if (target.GetComponent<FloraPart>())
        {
            target.GetComponent<FloraPart>().Deactivate();
        }
        cam.enabled = false;
    }
}
}