﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundDimmer : MonoBehaviour
{
	Image dimmerGraphic { get { return GetComponent<Image> (); } }

	float maxOpacity = 1f;
	float minOpacity = .64f;

	public void
	DimBackground()
	{
		float brightness = FindObjectOfType<CameraManager> ().GetBrightnessOfScreen ();
		float targetBrightness = (brightness + .7f) * (maxOpacity - minOpacity) + minOpacity;
		dimmerGraphic.color = new Color (.12f, .12f, .12f, targetBrightness);
	}
}