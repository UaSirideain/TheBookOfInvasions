﻿using UnityEngine;
using UnityEngine.UI;

namespace GUIManagement{

public class NoiseUI : MonoBehaviour, IGUI
{
	CanvasGroup canvas { get { return GetComponent<CanvasGroup> (); } }

	GUIController controller;

	public void
	Activate()
	{
		canvas.Enable ();
	}

	public void
	Deactivate()
	{
		canvas.Disable();
	}

	public GUIType
	GetGUIType()
	{
		return GUIType.HUD;
	}

	public void
	SetOverrideController(GUIController newController)
	{
		controller = newController;
		transform.SetParent (controller.transform);
	}
}
}