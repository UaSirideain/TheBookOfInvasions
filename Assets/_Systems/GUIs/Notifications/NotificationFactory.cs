﻿using UnityEngine;
using UnityEngine.UI;

namespace Notifications
{
    public static class NotificationFactory
    {
        public static RectTransform
        Build(RectTransform notificationPrefab, string text)
        {
            RectTransform notification = Object.Instantiate(notificationPrefab);
            notification.GetComponentInChildren<Text>().text = text;
            return notification;
        }
    }
}