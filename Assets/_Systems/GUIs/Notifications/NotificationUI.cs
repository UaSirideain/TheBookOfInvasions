﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Battle;

namespace Notifications
{
    public class NotificationUI : MonoBehaviour
    {
        [SerializeField] RectTransform notificationPrefab;
        [SerializeField] RectTransform notificationSlots;

        List<Notification> notificationsOnDisplay = new List<Notification>();
        Text notificationText;

        public void
        DisplayNotification(string text)
        {
            if (notificationText && notificationText.text.Equals(text)) return;

            RectTransform notification = NotificationFactory.Build(notificationPrefab, text);
            notification.SetParent(notificationSlots, false);
            Notification notifActions = notification.GetComponent<Notification>();

            if (notifActions)
            {
                notificationsOnDisplay.Insert(0, notifActions);
                EnableNotifications();
            }
            else
            {
                Debug.LogError("Failed to find component NotificationActions");
            }
        }

        void
        EnableNotifications()
        {
            notificationsOnDisplay[0].FirstRun();
            for (int index = 1; index < notificationsOnDisplay.Count; index++)
            {
                notificationsOnDisplay[index].Run();
            }
            notificationsOnDisplay.Sanitise();
        }
    }
}

/*public void
DisplayNotification(string text)
{
    if (notificationText == null || !notificationText.text.Equals(text))
    {
        CreateBanner(text);
    }
}

void 
CreateBanner(string text)
{
    RectTransform notification = Instantiate(notificationPrefab);
    notificationText = notification.GetComponentInChildren<Text>();
    notificationText.text = text;
    notification.SetParent(notificationSlots, false);
    AddToList(notification);
}

void
AddToList(RectTransform notification)
{
    notificationsOnDisplay.Insert(0, notification.GetComponent<NotificationActions>());
    ShiftNotifications();
}*/

/*void
ShiftNotifications()
{
    for (int index = 1; index < notificationsOnDisplay.Count; index++)
    {
        notificationsOnDisplay[index].Run();
    }
    CleanNotifications
}*/

/*void
CleanNotifications()
{
    List<RectTransform> _tempBanner = new List<RectTransform>();

    foreach (RectTransform rTrans in notificationsOnDisplay)
    {
        if (rTrans)
        {
            _tempBanner.Add(rTrans);
        }
    }
    notificationsOnDisplay = _tempBanner;
}*/