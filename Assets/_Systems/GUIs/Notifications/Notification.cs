﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Notifications
{
    public class Notification : MonoBehaviour
    {
        [SerializeField] private float lerpDuration;
        [SerializeField] private float lifeSpan;

        WaitForSeconds longevity;

        RectTransform thisTransform;
        Vector3 startPosition;
        Vector3 targetPosition;

        Text notificationText;
        Color startColour = new Color(1f, 1f, 1f, 0f);
        Color endColour = new Color(1f, 1f, 1f, 1f);

        bool lerpingPosition = false;

        void
        Awake()
        {
            notificationText = GetComponentInChildren<Text>();
            thisTransform = GetComponent<RectTransform>();
            longevity = new WaitForSeconds(lifeSpan);
        }

        public void
        FirstRun()
        {
            StartCoroutine(InitiateLifeTimeCounter());
            InitiateTransformLerp();
            StartCoroutine(FadeIn());
        }

        public void
        Run()
        {
            InitiateTransformLerp();
        }

        void
        InitiateTransformLerp()
        {
            startPosition = thisTransform.anchoredPosition;
            targetPosition = new Vector3(startPosition.x, startPosition.y - thisTransform.sizeDelta.y, 0);
            StartCoroutine(AnimateDownwards());
        }

        IEnumerator
        InitiateLifeTimeCounter()
        {
            yield return longevity;

            StartCoroutine(FadeOut());
        }

        int currentLerpTicket = 0;

        IEnumerator
        AnimateDownwards()
        {
            currentLerpTicket++;
            int myLerpTicket = currentLerpTicket;
            float t = 0f;
            while (t < 1f && myLerpTicket.Equals(currentLerpTicket))
            {
                lerpingPosition = true;
                thisTransform.anchoredPosition = Vector3.Lerp(startPosition, targetPosition, t);
                t += Time.deltaTime / 0.1f;
                yield return null;
            }
            thisTransform.anchoredPosition = targetPosition;
            lerpingPosition = false;
        }

        IEnumerator
        FadeIn()
        {
            float t = 0f;
            while (t < 1f)
            {
                notificationText.color = Color.Lerp(startColour, endColour, t);
                t += Time.deltaTime / lerpDuration;
                yield return null;
            }
        }

        IEnumerator
        FadeOut()
        {
            float t = 0f;
            while (t < 1f)
            {
                notificationText.color = Color.Lerp(endColour, startColour, t);
                t += Time.deltaTime / lerpDuration;
                yield return null;
            }

            while (lerpingPosition) yield return null;

            Destroy(gameObject);
        }
    }
}

/*private WaitForSeconds longevity;
[SerializeField] private float lerpDuration;
[SerializeField] private float notificationLifeSpan;

private Vector3 startPosition;
private Text notificationText;
private RectTransform thisTransform;

private Color startColour = new Color(1f, 1f, 1f, 0f);
private Color endColour = new Color(1f, 1f, 1f, 1f);

private int notificationPosition = 0;

private void 
Awake()
{
    notificationText = transform.GetChild(0).GetComponent<Text>();
    longevity = new WaitForSeconds(notificationLifeSpan);
}

public void
Run(int index)
{
    notificationPosition = index;
    StartCoroutine(StartLife());
}

private IEnumerator
StartLife()
{
    StartCoroutine(StartCountdown());
    while (isPlaying)
    {
        yield return null;
    }
    ActivateLerp();
    StartCoroutine(ColourLerp(startColour, endColour));
    while (!hasExpired)
    {
        yield return null;
    }
    StartCoroutine(ColourLerp(endColour, startColour, true));
}

bool hasExpired = false;
private IEnumerator
StartCountdown()
{
    hasExpired = false;
    yield return longevity;
    hasExpired = true;
}

Vector3 targetPosition;
bool isPlaying;
private void
ActivateLerp()
{
    thisTransform = GetComponent<RectTransform>();
    startPosition = thisTransform.anchoredPosition;
    targetPosition = new Vector3(thisTransform.anchoredPosition.x, thisTransform.anchoredPosition.y - (thisTransform.sizeDelta.y), 0);
    StartCoroutine(TransformLerp());
}

private IEnumerator
ColourLerp(Color startColour, Color endColour, bool destroy = false)
{
    float t = 0f;
    if (notificationPosition < 1 || destroy) //If first or last: Colour Lerp
    {
        while (t < 1)
        {
            notificationText.color = Color.Lerp(startColour, endColour, t);
            t += Time.deltaTime / lerpDuration;
            yield return null;
        }
    }
    if (destroy)
    {
        Destroy(gameObject);
    }
}

int currentLerpTicket = 0;
private IEnumerator
TransformLerp()
{
    isPlaying = true;
    currentLerpTicket++;
    int myLerpTicket = currentLerpTicket;
    float t = 0f;
    while (t < 1 && myLerpTicket.Equals(currentLerpTicket))
    {
        thisTransform.anchoredPosition = Vector3.Lerp(startPosition, targetPosition, t);
        t += Time.deltaTime / 0.1f;
        yield return null;
    }
    thisTransform.anchoredPosition = targetPosition;
    isPlaying = false;
}

public bool 
HasExpired()
{
    return hasExpired;
}*/
