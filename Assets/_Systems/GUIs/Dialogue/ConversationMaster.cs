﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

namespace Field.Sequences{

public class ConversationMaster : MonoBehaviour
{
	[SerializeField] string dialoguePath;

	[ExecuteInEditMode]
	void
	OnValidate()
	{
		WriteDialogue ();
	}
		
	public void
	WriteDialogue()
	{
		DialogueReader reader = new DialogueReader ();
		List<DialogueLine> chunks = reader.RetrieveDialogue (dialoguePath);

		int currentLineID = 0;

		foreach(OpenDialogue di in GetComponentsInChildren<OpenDialogue>())
		{
			if (chunks.Count > currentLineID)
			{
				di.GetDialogueData ().nameText = chunks [currentLineID].speaker;
				di.GetDialogueData ().dialogueText = chunks [currentLineID].speech;
				currentLineID++;
			}
			else
			{
				break;
			}
		}
	}
}
}