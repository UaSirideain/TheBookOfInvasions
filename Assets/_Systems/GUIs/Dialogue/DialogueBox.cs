﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Field.Dialogue{

public class DialogueBox : MonoBehaviour
{
	[SerializeField] KnotworkData knots;

	RectTransform t { get { return GetComponent<RectTransform> (); } }
	Image im { get { return GetComponent<Image> (); } }
	AudioSource audioSource { get { return GetComponent<AudioSource> (); } }

	[SerializeField] float sideMargin;
	[SerializeField] float topBottomMargin;

	[SerializeField] RectTransform graphicPrefab;

	[SerializeField] RectTransform speaker;
	[SerializeField] Text speech;

	//Transform followTarget;

	float desiredHeight;
	float desiredWidth;

	const float bleedMargin = 50f;

	public void
	WriteText(string speakerString, string dialogueString)
	{
		speaker.GetComponent<Text> ().text = speakerString;
		speech.text = DialogueFormatter (dialogueString);

		if (Language.GetLanguageOrientation () == LanguageOrientation.LeftToRight)
		{
			speech.alignment = TextAnchor.MiddleLeft;
		}
		else if (Language.GetLanguageOrientation () == LanguageOrientation.RightToLeft)
		{
			speech.alignment = TextAnchor.MiddleRight;
		}
	}

	public void
	Build()
	{
		t.sizeDelta = new Vector2 (speech.preferredWidth + (sideMargin * 2), speech.preferredHeight + (topBottomMargin * 2));
		DrawBorder ();
		Render ();
		audioSource.Play ();
	}

	public void
	SetAudioClip(AudioClip newAudioClip)
	{
		audioSource.clip = newAudioClip;
	}

	public void
	SetFollowTarget(Vector2 position, int layer)
	{
		float x = position.x;
		float y = position.y;

		if (x - (t.sizeDelta.x / 2) < 0 + bleedMargin)
		{
			x = 0 + bleedMargin + (t.sizeDelta.x / 2);
		}
		else if (x + (t.sizeDelta.x / 2) > 1920 - bleedMargin)
		{
			x = 1920 - bleedMargin - (t.sizeDelta.x / 2);
		}

		if(y + (t.sizeDelta.y / 2) > 0 - bleedMargin)
		{
			y = 0 -bleedMargin - (t.sizeDelta.y / 2);
		}
		else if(y - (t.sizeDelta.y / 2) < -1080 + bleedMargin)
		{
			y = -1080 + bleedMargin + (t.sizeDelta.y / 2);
		}

		t.anchoredPosition = new Vector2 (x, y);
	}

	public void
	SetFollowTarget(Transform newFollowTarget)
	{
		//followTarget = newFollowTarget;
	}

	void
	DrawBorder()
	{
		desiredHeight = GetComponent<RectTransform> ().sizeDelta.y;
		desiredWidth = GetComponent<RectTransform> ().sizeDelta.x;

		RectTransform endLeft_t = CreateBorderPiece (knots.endLeft, knots.colour);
		RectTransform cornerTopRight_t = CreateBorderPiece (knots.cornerTopRight, knots.colour);
		RectTransform cornerBottomLeft_t = CreateBorderPiece (knots.cornerBottomLeft, knots.colour);
		RectTransform cornerBottomRight_t = CreateBorderPiece (knots.cornerBottomRight, knots.colour);
		RectTransform endTop_t = CreateBorderPiece (knots.endTop, knots.colour);

		RectTransform tempHorizontalSection_t = CreateBorderPiece (knots.horizontalSection, knots.colour);
		RectTransform tempVerticalSection_t = CreateBorderPiece (knots.verticalSection, knots.colour);

		///Top: Namepiece///

		float runningWidth = 0f;

		float margin = cornerBottomLeft_t.sizeDelta.x;

		while (margin < (speaker.sizeDelta.x + speaker.anchoredPosition.x + 10 + endLeft_t.sizeDelta.x))
		{
			margin += tempHorizontalSection_t.sizeDelta.x;
		}

		float adjustedNameWidth = margin - endLeft_t.sizeDelta.x;

		speaker.sizeDelta = new Vector2 (adjustedNameWidth, speaker.sizeDelta.y);

		runningWidth += speaker.sizeDelta.x;

		///Top: Row///

		endLeft_t.anchoredPosition = new Vector2(runningWidth, 0);
		runningWidth += endLeft_t.sizeDelta.x;

		GenerateHorizontalSections (ref runningWidth, 0, 0);
	
		cornerTopRight_t.anchoredPosition = new Vector2(runningWidth, 0);
		runningWidth += cornerTopRight_t.sizeDelta.x;

		///Left Side: Buffer///

		float runningHeight = cornerTopRight_t.sizeDelta.y;

		while (runningHeight < speaker.sizeDelta.y + -speaker.anchoredPosition.y + endTop_t.sizeDelta.y)
		{
			runningHeight += tempVerticalSection_t.sizeDelta.y;
		}

		runningHeight -= endTop_t.sizeDelta.y;

		///Left Column///

		endTop_t.anchoredPosition = new Vector2 (0, -runningHeight);
		runningHeight += endTop_t.sizeDelta.y;

		GenerateVerticalSections (ref runningHeight, cornerBottomLeft_t.sizeDelta.y, 0);

		cornerBottomLeft_t.anchoredPosition = new Vector2 (0, -runningHeight);
		runningHeight += cornerBottomLeft_t.sizeDelta.y;

		///Bottom///
		desiredWidth = runningWidth;//here
		runningWidth = cornerBottomLeft_t.sizeDelta.x;

		GenerateHorizontalSections (ref runningWidth, cornerTopRight_t.sizeDelta.x, runningHeight - tempHorizontalSection_t.sizeDelta.y);

		///Right Column///

		desiredHeight = runningHeight;
		runningHeight = cornerTopRight_t.sizeDelta.y;

		GenerateVerticalSections (ref runningHeight, cornerTopRight_t.sizeDelta.y, runningWidth + cornerBottomRight_t.sizeDelta.x - tempVerticalSection_t.sizeDelta.x);

		////Bottom Right Corner///

		cornerBottomRight_t.anchoredPosition = new Vector2 (runningWidth, -runningHeight);

		runningWidth += cornerBottomRight_t.sizeDelta.x;
		runningHeight += cornerBottomRight_t.sizeDelta.y;

		//////Cleanup & Positioning

		Destroy (tempVerticalSection_t.gameObject);
		Destroy (tempHorizontalSection_t.gameObject);

		t.sizeDelta = new Vector2 (runningWidth, runningHeight);
	}

	void
	GenerateVerticalSections(ref float runningTotal, float adjustment, float xPos)
	{
		while (desiredHeight > runningTotal + adjustment)
		{
			RectTransform newVerSection = CreateBorderPiece (knots.verticalSection, knots.colour);
			newVerSection.anchoredPosition = new Vector2 (xPos, -runningTotal);
			runningTotal += newVerSection.sizeDelta.y;
		}
	}

	void
	GenerateHorizontalSections(ref float runningTotal, float adjustment, float yPos)
	{
		while (desiredWidth > runningTotal + adjustment)
		{
			RectTransform newHorSection = CreateBorderPiece (knots.horizontalSection, knots.colour);
			newHorSection.anchoredPosition = new Vector2 (runningTotal, -yPos);
			runningTotal += newHorSection.sizeDelta.x;
		}
	}

	void
	Render()
	{
		speech.color = Color.white;
		speaker.GetComponent<Text> ().color = Color.white;
		im.color = new Color(im.color.r, im.color.g, im.color.b, 1f);
	}
		
	string
	DialogueFormatter(string input)
	{
		int wordCount;

		List<string> wordList = ConvertToWordList (input, out wordCount);

		int lines = CalculateLineCount (wordCount);

		int wordsPerLine = wordCount / lines;

		string output = WriteToUIText (wordList, wordsPerLine);

		return output;
	}

	List<string>
	ConvertToWordList(string input, out int wordCount)
	{
		List<string> wordList = new List<string> ();

		string nextWord = "";

		wordCount = 0;

		foreach (char character in input)
		{
			if (character == " ".ToCharArray () [0])
			{
				wordList.Add (nextWord);
				nextWord = "";
				wordCount++;
			}
			else if (character == "-".ToCharArray () [0])
			{
				nextWord += character.ToString ();
				wordCount++;
			}
			else
			{
				nextWord += character.ToString ();
			}
		}
		wordList.Add (nextWord);
		wordCount++;
		return wordList;
	}

	int
	CalculateLineCount(int words)
	{
		int lines = 1;

		if (words >= 10 && words <= 22)
		{
			lines = 2;
		}
		else if (words >= 23 && words <= 35)
		{
			lines = 3;
		}
		else if (words >= 36 && words <= 50)
		{
			lines = 4;
		}
		else if (words >= 51 && words <= 68)
		{
			lines = 5;
		}
		else if (words >= 69 && words <= 90)
		{
			lines = 6;
		}
		return lines;
	}

	string
	WriteToUIText(List<string> wordList, int wordsPerLine)
	{
		string output = "";
		int wordsWrittenToThisLine = 0;

		foreach (string word in wordList)
		{
			if (wordsWrittenToThisLine >= wordsPerLine)
			{
				output += "\n";
				wordsWrittenToThisLine = 0;
			}
			else if (wordsWrittenToThisLine != 0)
			{
				output += " ";
			}

			output += word;
			wordsWrittenToThisLine++;

			if (word.Contains ("-"))
			{
				wordsWrittenToThisLine++;
			}
		}

		return output;
	}

	RectTransform
	CreateBorderPiece(Sprite sprite, Color colour)
	{
		RectTransform newPiece = Instantiate (graphicPrefab, transform);
		newPiece.GetComponent<Image> ().sprite = sprite;
		newPiece.GetComponent<Image> ().SetNativeSize ();
		newPiece.GetComponent<Image> ().color = colour;
		return newPiece;
	}
}
}