﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Dialogue{

public class DialogueUI : MonoBehaviour
{
	[SerializeField] DialogueBox dialogueBoxPrefab;

	public DialogueBox
	BuildDialogueBox(DialogueData data, int layer)
	{
		DialogueBox box = Instantiate (dialogueBoxPrefab);
		box.GetComponent<RectTransform> ().SetAsChildOf (GetComponent<RectTransform>());
		return box;
	}
}
}