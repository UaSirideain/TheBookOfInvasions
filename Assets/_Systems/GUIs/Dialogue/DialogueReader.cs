﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

public class DialogueReader
{
	const string idString = "id";
	string speakerString = "";
	string speechString = "";

	void
	GetLanguageSettings()
	{
		speakerString = Language.Localise (KeyWord.Cainteoir);
		speechString = Language.Localise (KeyWord.Caint);
	}

	public List<DialogueLine>
	RetrieveDialogue(string conversationName)
	{
		XmlDocument xmlDoc = new XmlDocument ();

		if (Application.platform == RuntimePlatform.WindowsEditor)
		{
			xmlDoc.Load (Application.dataPath + "/_Scenes/Dialogues/" + conversationName);
		}
		else
		{
			xmlDoc.Load (Application.dataPath + "/Dialogues/" + conversationName);
		}

		XmlNode root = xmlDoc.DocumentElement;

		XmlNodeList nodes = root.SelectNodes ("line");

		List<DialogueLine> lines = new List<DialogueLine> ();

		GetLanguageSettings ();

		foreach (XmlNode node in nodes)
		{
			DialogueLine newChunk = new DialogueLine (node[idString].InnerText, node[speakerString].InnerText, node[speechString].InnerText);
			lines.Add (newChunk);
		}

		return lines;
	}
}


public class DialogueLine
{
	public DialogueLine(string newId, string newSpeaker, string newSpeech)
	{
		id = newId.NumericStringToInt();
		speaker = newSpeaker;
		speech = newSpeech;
	}

	public readonly int id;
	public readonly string speaker;
	public readonly string speech;
}