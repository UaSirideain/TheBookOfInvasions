﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldToScreenUI : MonoBehaviour
{
	[SerializeField] Camera cam {get{return GameObject.FindObjectOfType<CameraManager>().GetActiveCamera();}}
	[SerializeField] Transform target;
	[SerializeField] RectTransform canvas;
	[SerializeField] float offset;

	void
	Update()
	{
		RectTransform t = GetComponent<RectTransform> ();
		Vector3 newTarget = new Vector3(target.position.x, target.position.y + offset, target.position.z);
		t.anchoredPosition = (cam.WorldToScreenPoint (newTarget) * 1920f / Screen.width);
	}
}