﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;using UnityEngine.UI;

public class SizeAdjuster : MonoBehaviour
{
	public void
	Fit()
	{
		float xMin = 0f;
		float xMax = 0f;
		bool xMinSet = false;
		bool xMaxSet = false;

		foreach (RectTransform rt in transform)
		{
			float min = rt.anchoredPosition.x - rt.sizeDelta.x * rt.pivot.x;
			float max = rt.anchoredPosition.x + rt.sizeDelta.x * (1 - rt.pivot.x);

			if (min < xMin || xMinSet == false)
			{
				xMin = min;
				xMinSet = true;
			}

			if (max > xMax || xMaxSet == false)
			{
				xMax = max;
				xMaxSet = true;
			}
		}

		GetComponent<RectTransform> ().sizeDelta = new Vector2 (xMax - xMin, 10f);
	}
}