﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void SongDelegate(NoteType note, OctaveType octave);

public class TuneLeader : MonoBehaviour
{
	[SerializeField] TuneData song;

	SongDelegate playMelody;
	SongDelegate playHarmony1;
	SongDelegate playHarmony2;
	SongDelegate playRhythm;

	public void
	RegisterTunedInstrument(SongDelegate instrumentNote)
	{
		if (playMelody == null)
		{
			playMelody += instrumentNote;
		}
		else if (playHarmony1 == null || song.GetAdditionalHarmony().Count == 0)
		{
			playHarmony1 += instrumentNote;
		}
		else
		{
			playHarmony2 += instrumentNote;
		}
	}

	public void
	DeregisterInstrument(SongDelegate instrumentNote)
	{
		playMelody -= instrumentNote;
		playHarmony1 -= instrumentNote;
		playHarmony2 -= instrumentNote;
	}

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<FieldInstrument> ())
		{
			other.GetComponent<FieldInstrument> ().HeyThere (this);
		}
	}

	void
	OnTriggerExit(Collider other)
	{
		if (other.GetComponent<FieldInstrument> ())
		{
			other.GetComponent<FieldInstrument> ().ByeNow (this);
		}
	}

	void
	Update()
	{
		if (Input.GetButtonDown("m"))
		{
			StartCoroutine (PlaySong ());
		}
	}

	//bool sessionStarted = false;
	int sessionTempo = 50;

	IEnumerator
	PlaySong()
	{
		//sessionStarted = true;
		sessionTempo = song.GetTempo ();

		for(int i = 0; i < song.GetHarmony().Count; i++)
		{
			if (playMelody != null)
			{
				//playMelody (song.GetMelody () [i], OctaveType.Mid);
			}
			if (playHarmony1 != null)
			{
				//playHarmony1 (song.GetHarmony () [i], OctaveType.Mid);
			}
			if (playHarmony2 != null)
			{
				//playHarmony2 (song.GetAdditionalHarmony () [i], OctaveType.Mid);
			}

			float timeToWait = SeshMaths.Rhythm.GetTimeUntilNextNote (song.GetGenre(), sessionTempo, i);

			yield return new WaitForSeconds (timeToWait);
		}

		yield return null;
	}
}