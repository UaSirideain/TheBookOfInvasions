﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Tune", menuName = "Field/Tunes/Create New Tune", order = 1)]
public class TuneData : ScriptableObject
{
	[SerializeField] TuneType genre;
	[SerializeField] int tempoLower;
	[SerializeField] int tempoHigher;

	[SerializeField] List<string> melody;
	[SerializeField] List<string> harmony1;
	[SerializeField] List<string> harmony2;
	[SerializeField] List<string> rhythm;

	public int
	GetTempo()
	{
		return Random.Range (tempoLower, tempoHigher);
	}

	public TuneType
	GetGenre()
	{
		return genre;
	}

	public List<string>
	GetMelody()
	{
		return melody;
	}

	public List<string>
	GetHarmony()
	{
		return harmony1;
	}

	public List<string>
	GetAdditionalHarmony()
	{
		return harmony2;
	}

	public List<string>
	GetRhythm()
	{
		return rhythm;
	}
}