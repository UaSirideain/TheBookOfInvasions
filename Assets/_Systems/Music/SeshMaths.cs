﻿using System.Collections.Generic;
using UnityEngine;

public enum TuneType{Jig, Reel}

public enum NoteType{First, Second, Third, Fourth, Fifth, Sixth, Seventh, Octave, Invalid}
public enum OctaveType{SuperLow, Low, Mid, High, SuperHigh}
public enum TuneMode{Major, Minor, FourthComplete, First, Second, Third, Fourth, Fifth}

public static class SeshMaths
{
	public static class Rhythm
	{
		static List<int> jigAccents = new List<int>(){1, 4};
		static List<int> reelAccents = new List<int>(){1, 5};
//		static List<int> hornpipeAccents = new List<int>(){1, 3};
//		static List<int> mazurkaAccents = new List<int>(){2};
//		static List<int> waltzAccents = new List<int>(){1};
//		static List<int> polkaAccents = new List<int>(){1};

		static List<float> jigRhythm = new List<float>(){1.25f, .75f, 1f, 1.25f, .75f, 1f};
		static List<float> reelRhythm = new List<float>(){1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};


		public static float
		GetTimeUntilNextNote(TuneType tuneType, int tempo, int currentNoteIndex)
		{
			float grossTime = 60f / NotesPerMinute (tuneType, tempo);
			return GetAdjustedTime(grossTime, tuneType, currentNoteIndex);
		}

		public static bool
		IsNoteAccented(TuneType tuneType, int currentNoteIndex)
		{
			bool isAccented = false;
			int noteOfCurrentMeasure = GetNoteOfCurrentMeasure (tuneType, currentNoteIndex);

			switch (tuneType)
			{
			case TuneType.Jig:
				isAccented = jigAccents.Contains (noteOfCurrentMeasure);
				break;
			
			case TuneType.Reel:
				isAccented = reelAccents.Contains (noteOfCurrentMeasure);
				break;
			}
			return isAccented;
		}

		static float
		GetAdjustedTime(float timeUntilNextNote, TuneType tuneType, int currentNoteIndex)
		{
			int measureNote = GetNoteOfCurrentMeasure (tuneType, currentNoteIndex);

			return timeUntilNextNote * jigRhythm [measureNote];
		}

		static float
		GetRhythmAdjuster(TuneType tuneType, int measureNote)
		{
			switch (tuneType)
			{
			case TuneType.Jig:
				return jigRhythm [measureNote];
			case TuneType.Reel:
				return reelRhythm [measureNote];
			}
			return 1f;
		}

		static float
		NotesPerMinute(TuneType tuneType, int tempo)
		{
			return (float)tempo * (float)NotesPerBeat (tuneType);
		}

		static int
		GetNoteOfCurrentMeasure(TuneType tuneType, int currentNoteIndex)
		{
			int notesPerMeasure = NotesPerMeasure (tuneType);
			int currentNote = currentNoteIndex;

			while (currentNote >= notesPerMeasure)
			{
				currentNote -= notesPerMeasure;
			}
				
			return currentNote;
		}

		static int
		BeatsPerMeasure(TuneType tuneType)
		{
			int beatsPerMeasure = 0;

			switch (tuneType)
			{
			case TuneType.Jig:
				beatsPerMeasure = 2;
				break;

			case TuneType.Reel:
				beatsPerMeasure = 4;
				break;
			}

			return beatsPerMeasure;
		}

		static int
		NotesPerMeasure(TuneType tuneType)
		{
			return BeatsPerMeasure (tuneType) * NotesPerBeat (tuneType);
		}

		static int
		NotesPerBeat(TuneType tuneType)
		{
			int notesPerBeat = 0;

			switch (tuneType)
			{
			case TuneType.Jig:
				notesPerBeat = 3;
				break;

			case TuneType.Reel:
				notesPerBeat = 2;
				break;
			}

			return notesPerBeat;
		}
	}

	public static class Melody
	{
		
	}

	public static class Mode //TODO: change the dictionaries here to hashsets or lookups. What we have here should only be written once.
	{
		static List<string> notes = new List<string> ()
		{ 	
			"C-2", "C#2", "D-2", "D#2", "E-2", "F-2", "F#2", "G-2", "G#2", "A-2", "A#2", "B-2", 
			"C-3", "C#3", "D-3", "D#3", "E-3", "F-3", "F#3", "G-3", "G#3", "A-3", "A#3", "B-3", 
			"C-4", "C#4", "D-4", "D#4", "E-4", "F-4", "F#4", "G-4", "G#4", "A-4", "A#4", "B-4",
			"C-5", "C#5", "D-5", "D#5", "E-5", "F-5", "F#5", "G-5", "G#5", "A-5", "A#5", "B-5", 
			"C-6", "C#6", "D-6", "D#6", "E-6", "F-6", "F#6", "G-6", "G#6", "A-6", "A#6", "B-6", 
			"C-7", "C#7", "D-7", "D#7", "E-7", "F-7", "F#7", "G-7", "G#7", "A-7", "A#7", "B-7"
		};

//		static Dictionary<TuneMode, List<int>> modeSteps = new Dictionary<TuneMode, List<int>> ()
//		{
//			{TuneMode.Major, 				new List<int>(){0, 2, 2, 1, 2, 2, 2, 1}},
//			{TuneMode.Minor, 				new List<int>(){0, 2, 1, 2, 2, 1, 2, 2}},
//			{TuneMode.FourthComplete, 		new List<int>(){0, 2, 2, 1, 2, 2, 1, 2}},
//
//			{TuneMode.First, 				new List<int>(){0, 2, 2, 3, 2, 3}},
//			{TuneMode.Second, 				new List<int>(){0, 2, 3, 2, 3, 2}},
//			{TuneMode.Third, 				new List<int>(){0, 1, 2, 3, 2, 2}},
//			{TuneMode.Fourth, 				new List<int>(){0, 2, 3, 2, 2, 3}},
//			{TuneMode.Fifth, 				new List<int>(){0, 1, 2, 2, 3, 2}}
//		};

		static Dictionary<TuneMode, List<int>> modeIntervals = new Dictionary<TuneMode, List<int>> ()
		{
			{TuneMode.Major, 				new List<int>(){0, 2, 4, 5, 7, 9, 11, 12}},
			{TuneMode.Minor, 				new List<int>(){0, 2, 3, 5, 7, 8, 10, 12}},
			{TuneMode.FourthComplete, 		new List<int>(){0, 2, 4, 5, 7, 9, 10, 12}},
		};

		static Dictionary<TuneMode, int> modeOffsets = new Dictionary<TuneMode, int>()
		{
			{TuneMode.Major, 				0},
			{TuneMode.Minor, 				5},
			{TuneMode.FourthComplete, 		3}
		};

		static Dictionary<NoteType, int> modeIndices = new Dictionary<NoteType, int>()
		{
			{NoteType.First, 	0},
			{NoteType.Second, 	1},
			{NoteType.Third, 	2},
			{NoteType.Fourth, 	3},
			{NoteType.Fifth, 	4},
			{NoteType.Sixth, 	5},
			{NoteType.Seventh, 	6},
		};

		static Dictionary<OctaveType, int> octaveIndices = new Dictionary<OctaveType, int>()
		{
			{OctaveType.SuperLow, 	0},
			{OctaveType.Low, 		12},
			{OctaveType.Mid, 		24},
			{OctaveType.High, 		36},
			{OctaveType.SuperHigh, 	48},
		};

		const string tonic = "D-";

		public static string
		GetNoteName(NoteType note, OctaveType octave, TuneMode mode)
		{
			int index = octaveIndices [octave];
			index += modeOffsets [mode];

			int noteInt = modeIndices [note];

			index += modeIntervals [mode] [noteInt];

			return notes [index];
		}
	}

	public static class Input
	{
		static Dictionary<string, NoteType> inputToNote = new Dictionary<string, NoteType>()
		{
			{"1",	NoteType.Sixth},
			{"2",	NoteType.Seventh},
			{"3",	NoteType.First},
			{"4",	NoteType.Second},
			{"5",	NoteType.Third},
			{"6",	NoteType.Fourth},
			{"7",	NoteType.Fifth},
			{"8",	NoteType.Sixth},
			{"9",	NoteType.Seventh},
			{"0",	NoteType.First},
			{"-",	NoteType.Second},
			{"=",	NoteType.Third},

		};

		public static NoteType
		GetNote(string input)
		{
			return inputToNote [input];
		}

		public static OctaveType
		GetAdjustedOctave(string input, OctaveType octave)
		{
			if (input == "1" || input == "2")
			{
				return LowerOctave (octave);
			}
			else if (input == "0" || input == "-" || input == "=")
			{
				return RaiseOctave (octave);
			}
			else
			{
				return octave;
			}
		}

		static OctaveType
		RaiseOctave(OctaveType octave)
		{
			switch (octave)
			{
			case OctaveType.Low:
				return OctaveType.Mid;

			case OctaveType.Mid:
				return OctaveType.High;

			case OctaveType.High:
				return OctaveType.SuperHigh;
			}

			Debug.Log ("WARNING: OctaveType did not fit profile. Whatever function called this, called it wrong.");
			return OctaveType.SuperLow;
		}

		static OctaveType
		LowerOctave(OctaveType octave)
		{
			switch (octave)
			{
			case OctaveType.Low:
				return OctaveType.SuperLow;

			case OctaveType.Mid:
				return OctaveType.Low;

			case OctaveType.High:
				return OctaveType.Mid;
			}

			Debug.Log ("WARNING: OctaveType did not fit profile. Whatever function called this, called it wrong.");
			return OctaveType.SuperLow;
		}
	}
}