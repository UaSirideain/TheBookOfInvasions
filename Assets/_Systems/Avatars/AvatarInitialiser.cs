﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Avatars;

public class AvatarInitialiser : MonoBehaviour
{
	AvatarDirectory directory { get { return GetComponent<AvatarDirectory> (); } }

	[SerializeField] FieldInstrument inst;

	void
	InitialiseInstrument()
	{
		if (inst != null)
		{
			directory.GetFieldMusic ().InitialiseInstrument (inst);
		}
	}

	public void
	InitialiseModelData(GameObject newModel)
	{
		newModel.transform.SetParent (transform, false);
		directory.SetPointAboveHead(newModel.transform.Find ("PointAboveHead"));
		newModel.AddComponent<HeadLookController> ();
		newModel.AddComponent<FieldAvatarAnimationEvents> ();
	}

	void
	Awake()
	{
		if (CheckIfNPC ())
		{
			InitialiseAsNPC ();
		}
		InitialiseInstrument ();
	}

	bool
	CheckIfNPC()
	{
		return GetComponentInChildren<Animator> () != null;
	}

	void
	InitialiseAsNPC()
	{
		GameObject newModel = GetComponentInChildren<Animator> ().gameObject;
		InitialiseModelData (newModel);
	}

	public void
	SetCharacter(Character newCharacter)
	{
		directory.SetCharacter(newCharacter);
	}
}