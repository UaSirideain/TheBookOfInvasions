﻿using UnityEngine;
using UnityEngine.UI;

public class InteractionIconUI : MonoBehaviour
{
	[SerializeField] Sprite interactionIcon;
	[SerializeField] Sprite inventoryInteractionIcon;

	public void
	RenderInteractionIcon()
	{
		GetComponent<Image> ().sprite = interactionIcon;
		gameObject.SetActive (true);
	}

	public void
	RenderInventoryInteractionIcon()
	{
		GetComponent<Image> ().sprite = inventoryInteractionIcon;
		gameObject.SetActive (true);
	}

	public void
	UnRender()
	{
		gameObject.SetActive (false);
	}
}