﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponCollider : MonoBehaviour
{
	public delegate void AttackContact();
	public AttackContact onContact;

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<AvatarFacade> () && other.GetComponent<AvatarFacade>().IsActiveCharacter())
		{
			onContact ();
		}
	}
}