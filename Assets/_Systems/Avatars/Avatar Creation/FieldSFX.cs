﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

[RequireComponent(typeof(AudioSource))]
public class FieldSFX : MonoBehaviour
{
	[SerializeField] List<AudioClip> footsteps = new List<AudioClip>();
	[SerializeField] AudioClip ropeSound;
	[SerializeField] AudioClip ropeDescentSound;
	[SerializeField] AudioClip grabSound;


	AudioSource audioSource{ get { return GetComponent<AudioSource> (); } }

	public void
	FootstepSFX()
	{
		audioSource.PlayOneShot (footsteps[Random.Range(0, footsteps.Count)]);
	}

	public void
	RopeSFX()
	{
		audioSource.PlayOneShot(ropeSound, .3f);
	}

	bool slidingDownRope = false;

	public void
	RopeDescend(bool status)
	{
		if (status && slidingDownRope == false)
		{
			audioSource.volume = .4f;
			slidingDownRope = true;
			audioSource.clip = ropeDescentSound;
			audioSource.loop = true;
			audioSource.Play ();
		}
		else if(!status && slidingDownRope == true)
		{
			slidingDownRope = false;
			audioSource.loop = false;
			audioSource.Stop ();
		}
	}

	public void
	GrabSFX()
	{
		int volume = Random.Range (5, 20);
		float fVolume = volume;
		fVolume = fVolume / 100;

		audioSource.PlayOneShot (grabSound, fVolume);
	}
}
}