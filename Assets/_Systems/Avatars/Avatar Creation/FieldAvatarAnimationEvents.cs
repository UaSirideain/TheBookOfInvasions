﻿using UnityEngine;

namespace Field.Avatars{

public class FieldAvatarAnimationEvents : MonoBehaviour
{
	FieldAnimations animationHandler{ get { return transform.parent.GetComponentInChildren<FieldAnimations> (); } }
	FieldSFX SFXHandler{ get { return transform.parent.GetComponentInChildren<FieldSFX> (); } }

	public void
	FootstepSFX()
	{
		//SFXHandler.FootstepSFX ();
	}

	public void
	RopeSFX()
	{
		SFXHandler.RopeSFX ();
	}

	public void
	GrabSFX()
	{
		SFXHandler.GrabSFX ();
	}

	public void
	PointOfContact()    //Called by Unity Animation Event
	{
		animationHandler.PointOfContact ();
	}

	public void
	EndOfAnimation()
	{
		animationHandler.AnimationEnd ();
	}

	public void
	AttackPointOfContact()
	{
		animationHandler.AttackPointOfContact ();
	}
}
}