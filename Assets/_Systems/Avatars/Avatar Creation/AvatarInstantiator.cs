﻿using Field.Avatars;
using System.Collections.Generic;
using UnityEngine;

public class AvatarInstantiator : MonoBehaviour
{
    [SerializeField] CameraPoint camPoint = null;
	[SerializeField] AvatarInitialiser fieldAvatarPrefab;

	public List<AvatarInitialiser>
	CreateFieldAvatars(List<AvatarInfo> serializedCharacterData)
	{
		List<AvatarInitialiser> avatars = new List<AvatarInitialiser> ();

		foreach (AvatarInfo characterData in serializedCharacterData)
		{
			avatars.Add (CreateAvatar(characterData));
		}

		return avatars;
	}
    
	AvatarInitialiser
	CreateAvatar(AvatarInfo characterData)
	{
		//GameObject newModel = Instantiate (characterData.modelData.avatarModel);

		AvatarInitialiser newAvatar = Instantiate (characterData.avatar);
		newAvatar.InitialiseModelData (newAvatar.GetComponentInChildren<Animator>().gameObject);
		newAvatar.SetCharacter (FindCharacter(characterData.characterName));

        if (camPoint != null)
        {
            newAvatar.GetComponent<AvatarFacade>().SetCurrentCameraPoint(camPoint);
        }

        return newAvatar;
	}

	Character
	FindCharacter(string characterName)
	{
		foreach (Character character in FindObjectsOfType<Character>())
		{
			if (character.name == characterName)
			{
				return character;
			}
		}

		print ("ERROR: Character of name " + characterName + " not found.");
		return null;
	}
}