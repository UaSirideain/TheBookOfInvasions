﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public class NPCState : MonoBehaviour
{
	protected NavAgentController navAgentCont { get { return transform.parent.GetComponentInChildren<NavAgentController> (); } }
	protected NPCStates stateMachine { get { return GetComponent<NPCStates> (); } }
	[HideInInspector] public Color colour;

	public virtual bool
	EntryRequirements()
	{
		return true;
	}
		
	public virtual void
	Routine()
	{

	}
}
}