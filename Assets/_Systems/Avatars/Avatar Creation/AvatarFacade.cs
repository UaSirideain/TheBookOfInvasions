﻿using Field.Abilities;
using Field.Sequences;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Field.Avatars;

public delegate void FinishDelegate();

public class AvatarFacade : MonoBehaviour
{
	AvatarDirectory direc{ get { return GetComponent<AvatarDirectory> (); } }

	public bool
	UseFromInventory(InventoryEntry entry)
	{
		if(direc.GetAbilityController().UseAbilityFromInventory(entry, this))
		{
			return true;
		}
		return false;
	}
		
	public bool
	GetSneaking()
	{
		return GetComponentInChildren<MovementSpeed> ().GetMoveType () == MoveType.Sneak;
	}

	public void
	LookAt(Transform target)
	{
		GetComponentInChildren<HeadLookController> ().SetTarget (target);
	}

	public void
	EndLookAt()
	{
		StartCoroutine(GetComponentInChildren<HeadLookController> ().EndLookAt ());
	}

	public void
	RemoveItem(InventoryEntry entry)
	{
		GetCharacter ().RemoveInventoryEntry (entry);
	}

	public Character
	GetCharacter()
	{
		return direc.GetCharacter ();//character;
	}

	public void
	EndSequenceMode()
	{
		GetComponent<AvatarComposition> ().EndSequenceMode ();
	}

	public void
	EnableSequenceMode()
	{
		GetComponent<AvatarComposition> ().EnableSequenceMode ();
	}

	public void
	AmbulateToPoint(Vector3 destination, FinishDelegate registerTargetHasReachedDestination)
	{
		StartCoroutine(direc.GetNavAgentController ().AmbulateToPoint (destination, registerTargetHasReachedDestination));
	}

	public void
	SetCurrentCameraPoint(AssignCameraPointToAvatar camPoint)
	{
		GetComponent<AvatarComposition>().SetCurrentCameraPoint(camPoint);
	}

	public void
	SetCurrentCameraPoint (CameraPoint camPoint)
	{
		GetComponent<AvatarComposition> ().SetCurrentCameraPoint (camPoint);
	}

	public bool
	IsActiveCharacter()
	{
		return GetComponent<AvatarComposition> ().IsActiveCharacter ();
	}

	public Transform
	GetSuitableLocationForFirstPersonCamera()
	{
		return direc.GetPointAboveHead ();
	}

	public MovementSpeed
	GetMovementSpeed()
	{
		return direc.GetMovementSpeed ();
	}

	public void
	SetAsControlledCharacter()
	{
		GetComponent<AvatarComposition> ().SetAsControlledCharacter ();
	}

	public void
	SetAsCompanionAI()
	{
		GetComponent<AvatarComposition> ().SetAsCompanionAI ();
	}

	public void
	UnregisterNewInteractionTrigger(InteractionTrigger trigger)
	{
		direc.GetInteractions ().UnregisterInteraction(trigger);
	}

	public void
	UnregisterInventoryInteractionTrigger(InventoryInteractionTrigger trigger)
	{
		direc.GetInteractions ().UnregisterInventoryInteraction(trigger);
	}

	public void
	BlockInteractions()
	{
		direc.GetInteractions ().BlockInteractions ();
	}

	public void
	UnblockInteractions()
	{
		direc.GetInteractions ().UnblockInteractions ();
	}

	//// Sticky, Gooey & Tricky: where do these methods go? ////

	public FieldAnimations
	GetFieldAnimations()
	{
		return direc.GetFieldAnimations ();
	}

	public Transform
	GetTransform()
	{
		return transform;
	}

	public PlayerAbilities
	GetFieldAbilities()
	{
		return direc.GetFieldAbilities ();
	}
}