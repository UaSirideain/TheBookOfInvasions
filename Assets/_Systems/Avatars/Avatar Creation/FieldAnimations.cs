﻿using Field.Sequences;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Field.Avatars{

public delegate void AttackDelegate();

public class FieldAnimations : MonoBehaviour
{
    Animator animator { get { return transform.parent.GetComponentInChildren<Animator>(); } }

	//Movement

	//bool onRope = false;

	Dictionary<string, int> animationLayers = new Dictionary<string, int>()
	{
		{"Base", 0},
		{"Harp", 1},
		{"Weapon", 2}
	};

	public void
	SetRopeDirection(int direction, bool sliding)
	{
		animator.SetInteger ("RopeDirection", direction);
		animator.SetBool ("RopeSlide", sliding);
	}

	public void
	SetAsOnRope(bool newOnRope)
	{
		//onRope = newOnRope;
		animator.SetBool ("OnRope", newOnRope);
	}
		
    void
    IdleTurning()
    {
		int turnDirection = 0;
        animator.SetInteger("TurnDirection", turnDirection);
    }

	public void
	SetMoving(bool moving)
	{
		animator.SetBool ("Moving", moving);
	}
		
	public void
	SetMovementType(MoveType moveType)
	{
		if (moveType == MoveType.Run)
		{
			StopSneaking ();
			animator.SetInteger ("Speed", 2);
		}
		else if (moveType == MoveType.Walk)
		{
			StopSneaking ();
			animator.SetInteger ("Speed", 1);
		}
		else if (moveType == MoveType.Sneak)
		{
			SetSneaking ();
		}
	}

	void
	SetSneaking()
	{
		animator.SetBool ("Sneaking", true);
	}

	void
	StopSneaking()
	{
		animator.SetBool ("Sneaking", false);
	}

	//Music

	public void
	SetAsPlayingInstrument()
	{
		animator.SetLayerWeight (animationLayers["Harp"], 1f);
		animator.SetBool ("PlayingInstrument", true);
	}

	public void
	SetAsNotPlayingInstrument()
	{
		animator.SetLayerWeight (animationLayers["Harp"], 0f);
		animator.SetBool ("PlayingInstrument", false);
	}

    public void
    PlayInstrumentNote()
    {
        animator.SetTrigger("PlayInstrumentNote");
        animator.SetLayerWeight(1, 1f);
    }

	//Sequences

	bool pointOfContactHit;
	bool animationEnded;

	public IEnumerator
	PlayEventAnimation(AnimationType anim, SequenceAnimationEvent fieldEvent)
	{
		pointOfContactHit = false;
		animationEnded = false;

		animator.SetTrigger(anim.ToString());

		yield return StartCoroutine(WaitForPointOfContact());

		fieldEvent.RegisterPointOfContact();

		yield return StartCoroutine(WaitForEndOfAnimation());
		fieldEvent.RegisterEndOfAnimation();
	}

	public void
	PointOfContact()
	{
		pointOfContactHit = true;
	}

	public void
	AnimationEnd()
	{
		animationEnded = true;
	}

	IEnumerator
	WaitForPointOfContact()
	{
		while (pointOfContactHit == false && animationEnded == false)
		{
			yield return null;
		}
	}

	IEnumerator
	WaitForEndOfAnimation()
	{
		while (animationEnded == false)
		{
			yield return null;
		}
	}

	public void
	DrawWeapon()
	{
		animator.SetLayerWeight (animationLayers["Weapon"], 1f);
		animator.SetTrigger ("DrawWeapon");
	}

	public void
	SheatheWeapon()
	{
		StartCoroutine (SheatheWeaponDisableLayer ());
	}

	IEnumerator
	SheatheWeaponDisableLayer()
	{
		animator.SetTrigger ("SheatheWeapon");
		yield return null;
		int currentAnimation = animator.GetCurrentAnimatorStateInfo (animationLayers["Weapon"]).shortNameHash;
		while (animator.GetCurrentAnimatorStateInfo (animationLayers["Weapon"]).shortNameHash == currentAnimation)
		{
			yield return null;
		}

		animator.SetLayerWeight (animationLayers["Weapon"], 0f);
	}

	public IEnumerator
	Attack(AttackDelegate poc, AttackDelegate end)
	{
		
		attackPointOfContact += poc;

		animator.SetTrigger ("Attack");

		yield return null;

		int currentAnimation = animator.GetCurrentAnimatorStateInfo (animationLayers["Weapon"]).shortNameHash;
		while (animator.GetCurrentAnimatorStateInfo (animationLayers["Weapon"]).shortNameHash == currentAnimation)
		{
			yield return null;
		}
	
		end ();
		attackPointOfContact -= poc;
	}

	AttackDelegate attackPointOfContact;

	public void
	AttackPointOfContact()
	{
		attackPointOfContact ();
	}

	public IEnumerator
	PlayAnimation(AnimationType anim, FinishDelegate end)
	{
		animator.SetTrigger(anim.ToString());

		yield return null;
		yield return new WaitForSeconds (animator.GetCurrentAnimatorStateInfo (animationLayers ["Base"]).length + animator.GetCurrentAnimatorStateInfo (animationLayers ["Base"]).normalizedTime);

		end ();
	}
}
}