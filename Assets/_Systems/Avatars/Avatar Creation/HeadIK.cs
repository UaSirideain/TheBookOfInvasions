﻿using UnityEngine;
using System.Collections;

public class HeadIK: MonoBehaviour
{
	Transform lookAtTarget;

	void Update ()
	{
		if (lookAtTarget != null)
		{
			transform.LookAt (lookAtTarget);
		}
	}

	public void
	LookAt(Transform newLookAtTarget)
	{
		lookAtTarget = newLookAtTarget;
	}
}
