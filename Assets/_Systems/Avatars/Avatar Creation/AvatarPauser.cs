﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Avatars;

[RequireComponent(typeof(PauseCompanion))]
public class AvatarPauser : MonoBehaviour, IPausable
{
	bool animatorWasEnabled;
	bool navMeshAgentWasEnabled;
	bool rigidbodyWasKinematic;
	Vector3 rigidbodyVelocity;

	public void
	Pause()
	{
		StoreOldValues ();

		GetComponentInChildren<Animator> ().enabled = false;
		GetComponent<AvatarMediator>().GetNavAgentController().Pause ();
		GetComponent<Rigidbody> ().isKinematic = true;
		GetComponent<Rigidbody> ().velocity = Vector3.zero;
		GetComponent<AvatarMediator> ().GetFieldAnimations ().enabled = false;
	}

	public void
	Unpause()
	{
		GetComponentInChildren<Animator> ().enabled = animatorWasEnabled;
		GetComponent<AvatarMediator>().GetNavAgentController().Unpause ();
		GetComponent<Rigidbody> ().isKinematic = rigidbodyWasKinematic;
		GetComponent<Rigidbody> ().velocity = rigidbodyVelocity;
		GetComponent<AvatarMediator> ().GetFieldAnimations ().enabled = true;
	}

	void
	StoreOldValues()
	{
		animatorWasEnabled = GetComponentInChildren<Animator> ().enabled;
		rigidbodyWasKinematic = GetComponent<Rigidbody> ().isKinematic;
		rigidbodyVelocity = GetComponent<Rigidbody> ().velocity;
	}
}