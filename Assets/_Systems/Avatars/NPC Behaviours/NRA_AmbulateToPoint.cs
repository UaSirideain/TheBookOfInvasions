﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Avatars;

namespace Field{

public class NRA_AmbulateToPoint : NPCRoutineAction
{
	public override void
	PerformAction(AvatarFacade avatar, FinishDelegate del)
	{
		avatar.GetComponentInChildren<NavAgentController> ().Activate ();
		avatar.AmbulateToPoint(transform.position, del);
	}
}
}