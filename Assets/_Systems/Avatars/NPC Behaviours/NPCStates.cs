﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public delegate void VisionDelegate(List<AvatarFacade> avatars);
public delegate void HearingDelegate();

public class NPCStates : MonoBehaviour, IPausable
{
	Hearing hearing { get { return transform.parent.GetComponentInChildren<Hearing> (); } }
	Vision vision { get { return transform.parent.GetComponentInChildren<Vision> (); } }

	[SerializeField] float checkHearingFrequency = 0.1f;
	[SerializeField] float checkVisionFrequency = 0.1f;

	float hearingTimeStamp = 0f;
	float visionTimeStamp = 0f;

	VisionDelegate sightDetected;
	HearingDelegate soundDetected;

	NPCState activeState;

	bool paused = false;

	public void
	Pause()
	{
		paused = true;
	}

	public void
	Unpause()
	{
		paused = false;
	}

	public void
	SubscribeToVision(VisionDelegate del)
	{
		sightDetected += del;
	}

	public void
	SubscribeToHearing(HearingDelegate del)
	{
		soundDetected += del;
	}

	void
	CheckStates()
	{
		foreach (NPCState state in GetComponents<NPCState>())
		{
			if (state.EntryRequirements ())
			{
				activeState = state;
				stateMarker.GetComponent<MeshRenderer> ().material.color = activeState.colour;
				break;
			}
		}
	}

	void
	Update()
	{
		if (!paused && GetComponents<NPCState>().Length > 0)
		{
			CheckStates ();
			activeState.Routine ();

			if (Time.time - hearingTimeStamp > checkHearingFrequency)
			{

			}

			if (Time.time - visionTimeStamp > checkVisionFrequency)
			{
				sightDetected (vision.GetSeenAvatars ());
			}
		}
	}

	//DEBUG

	Transform stateMarker;

	void
	Start()
	{
		stateMarker = Instantiate (GameObject.CreatePrimitive (PrimitiveType.Sphere), transform).transform;
		stateMarker.localPosition = new Vector3 (0f, 2f, 0f);
		stateMarker.localScale = new Vector3 (.1f, .1f, .1f);
	}
}
}