﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Field.Avatars{

public class NPCStateSearch : NPCState
{
	int avatarsInSight = 0;
	//int avatarsInEarshot = 0;

	Vector3 lastSeenLocation = Vector3.zero;

	Transform closestAvatar;

	void
	Start()
	{
		stateMachine.SubscribeToVision (Vision);
		colour = Color.yellow;
	}

	void
	Vision(List<AvatarFacade> avatars)
	{
		avatarsInSight = avatars.Count;
		if (avatarsInSight > 0)
		{
			closestAvatar = avatars.ConvertToListOfTransforms ().GetNavMeshClosestTo (transform);
			lastSeenLocation = closestAvatar.position;
		}
	}

	public override bool
	EntryRequirements()
	{
		return (avatarsInSight == 0 && lastSeenLocation != Vector3.zero);
	}

	public override void
	Routine()
	{
		navAgentCont.Activate ();
		navAgentCont.MoveToPoint (lastSeenLocation);

		if (Vector3.Distance (lastSeenLocation, transform.position) < 3f)
		{
			lastSeenLocation = Vector3.zero;
		}
	}
}
}