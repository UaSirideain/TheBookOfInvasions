﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public class NPCIdleState : NPCState
{
	int avatarsInSight = 0;
	int avatarsInEarshot = 0;

	NPCRoutine routine{ get { return GetComponent<NPCRoutine> (); } }

	void
	Start()
	{
		stateMachine.SubscribeToVision (Vision);
		colour = Color.green;
	}

	void
	Vision(List<AvatarFacade> avatars)
	{				
		avatarsInSight = avatars.Count;
	}

	public override bool
	EntryRequirements()
	{		
		return (avatarsInSight == 0 && avatarsInEarshot == 0);
	}

	public override void
	Routine()
	{
		routine.Run ();
	}
}
}