﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Field.Avatars{

public class NPCStateChase : NPCState
{
	int avatarsInSight = 0;
	int avatarsInEarshot = 0;

	Transform closestAvatar;

	void
	Start()
	{
		stateMachine.SubscribeToVision (Vision);
		colour = Color.red;
	}

	void
	Vision(List<AvatarFacade> avatars)
	{
		avatarsInSight = avatars.Count;
		if (avatarsInSight > 0)
		{
			closestAvatar = avatars.ConvertToListOfTransforms ().GetNavMeshClosestTo (transform);
		}
	}

	public override bool
	EntryRequirements()
	{
		return (avatarsInSight > 0 || avatarsInEarshot > 0);
	}

	public override void
	Routine()
	{
		navAgentCont.Activate ();
		navAgentCont.MoveToPoint (closestAvatar.position);
	}
}
}