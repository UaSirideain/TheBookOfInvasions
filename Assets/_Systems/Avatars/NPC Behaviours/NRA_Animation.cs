﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Avatars;

namespace Field{

public class NRA_Animation : NPCRoutineAction
{
	[SerializeField] AnimationType targetAnimation;
	public override void
	PerformAction(AvatarFacade avatar, FinishDelegate del)
	{
		StartCoroutine(avatar.GetFieldAnimations ().PlayAnimation (targetAnimation, del));
	}
}
}