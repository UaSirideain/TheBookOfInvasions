﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hearing : MonoBehaviour
{
	[SerializeField] float weakEarshot = 120f;
	[SerializeField] float strongEarshot = 60f;

	public void
	RegisterWeakSound(Transform sound)
	{
		if (Vector3.Distance (transform.position, sound.position) < strongEarshot)
		{
			PiqueHearing ();
		}
	}

	public void
	RegisterStrongSound(Transform sound)
	{
		if (Vector3.Distance (transform.position, sound.position) < weakEarshot)
		{
			PiqueHearing ();
		}
	}

	void
	PiqueHearing()
	{

	}
}