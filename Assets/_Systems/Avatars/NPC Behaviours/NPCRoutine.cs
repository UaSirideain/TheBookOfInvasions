﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCRoutine : MonoBehaviour
{
	[SerializeField] List<NPCRoutineAction> actions = new List<NPCRoutineAction>();

	int currentAction = 0;

	public enum ListTraversal{Stop, Loop, Bounce}
	[SerializeField] ListTraversal onListTraversed;

	bool currentActionFinished = true;

	public void
	Run()
	{
		if (currentActionFinished == true && actions.Count > 1)
		{
			//print ("Doing next Action");
			currentActionFinished = false;
			actions [currentAction].PerformAction (transform.parent.GetComponent<AvatarFacade> (), OnActionFinished);
			PrepareNextAction ();
		}
	}

	void
	OnActionFinished()
	{
		currentActionFinished = true;
	}

	void
	PrepareNextAction()
	{
		if (actions [currentAction] == actions.GetLast ())
		{
			EndOfActionChain ();
		}
		else
		{
			currentAction++;
		}
	}

	void
	EndOfActionChain()
	{
		if (onListTraversed == ListTraversal.Loop)
		{
			currentAction = 0;
		}
		else if (onListTraversed == ListTraversal.Bounce)
		{
			currentAction = 1;
			actions.Reverse ();
		}
		else
		{
			currentActionFinished = false;
		}
	}
}