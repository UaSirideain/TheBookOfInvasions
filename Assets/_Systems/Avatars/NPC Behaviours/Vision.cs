﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour
{
	[SerializeField] float weakHorizontalFOV = 200f;
	[SerializeField] float weakVerticalFOV = 135f;
	[SerializeField] float strongHorizontalFOV = 110f;
	[SerializeField] float strongVerticalFOV = 70f;
	[SerializeField] float weakRange = 35f;
	[SerializeField] float strongRange = 15f;

	[SerializeField] LayerMask mask;
    
	List<AvatarFacade> avatars = new List<AvatarFacade>();

	float acceptableWeakHorizontalAngle = 0f;
	float acceptableWeakVerticalAngle = 0f;

	float acceptableStrongHorizontalAngle = 0f;
	float acceptableStrongVerticalAngle = 0f;

	void
	Start()
	{
		InitialiseAcceptableAngles();
	}

	void
	InitialiseAcceptableAngles()
	{
		acceptableWeakHorizontalAngle = weakHorizontalFOV / 2f;
		acceptableWeakVerticalAngle = weakVerticalFOV / 2f;

		acceptableStrongHorizontalAngle = strongHorizontalFOV / 2f;
		acceptableStrongVerticalAngle = strongVerticalFOV / 2f;
	}

	public List<AvatarFacade>
	GetSeenAvatars()
	{
		FindSeenAvatars ();
		return avatars;
	}

	void
	FindSeenAvatars()
	{
		foreach (AvatarFacade avatar in FindObjectsOfType<AvatarFacade>())
		{
			if (CanSeeAvatar(avatar.transform, avatar.GetSneaking()))
			{
				Add (avatar);
			}
			else
			{
				Remove(avatar);
			}
		}
	}

	void
	Add(AvatarFacade avatar)
	{
		if (!avatars.Contains (avatar))
		{
			avatars.Add (avatar);
		}
	}

	void
	Remove(AvatarFacade avatar)
	{
		if (avatars.Contains (avatar))
		{
			avatars.Remove (avatar);
		}
	}

	bool
	CanSeeAvatar(Transform avatar, bool sneaking)
	{
		Vector3 avatarPosition = avatar.GetComponent<Collider> ().bounds.center;
		Vector3 avatarHead = avatarPosition + new Vector3(0f, avatar.GetComponent<Collider> ().bounds.extents.y, 0f);

		return (
			DistanceIsAcceptable(avatarPosition, sneaking)
			&& (HasLineOfSight(avatarPosition, avatar) || HasLineOfSight(avatarHead, avatar))
			&& AnglesAreAcceptable(avatarPosition, sneaking)
		);
	}	

	bool
	DistanceIsAcceptable(Vector3 avatarPosition, bool sneaking)
	{
		if (Vector3.Distance (transform.position, avatarPosition) >= weakRange && !sneaking)
		{
			return false;
		}
		else if (Vector3.Distance (transform.position, avatarPosition) >= strongRange)
		{
			return false;
		}

		return true;
	}

	bool
	HasLineOfSight(Vector3 targetPosition, Transform avatar)
	{
		RaycastHit[] hits = Physics.RaycastAll (transform.position, targetPosition - transform.position, Vector3.Distance(transform.position, targetPosition), Physics.AllLayers, QueryTriggerInteraction.Ignore);

		foreach(RaycastHit hit in hits)
		{
			if (!hit.transform.IsChildOf (transform) && !hit.transform.IsChildOf (avatar))
			{
				return false;
			}
		}

		return true;
	}

	bool
	AnglesAreAcceptable(Vector3 avatarPosition, bool sneaking)
	{
		float forwardDistance = transform.InverseTransformPoint (avatarPosition).z;
		float sideDistance = Mathf.Abs (transform.InverseTransformPoint(avatarPosition).x);
		float upDistance = Mathf.Abs (transform.InverseTransformPoint (avatarPosition).y);

		float horAngle = Mathf.Rad2Deg * Mathf.Atan (sideDistance / forwardDistance);
		float vertAngle = Mathf.Rad2Deg * Mathf.Atan (upDistance / forwardDistance);

		Vector3 targetPos = new Vector3 (sideDistance, 0f, forwardDistance);
		Vector3 targetPos2 = new Vector3 (0f, upDistance, Mathf.Abs(forwardDistance));
		Vector3 forwardPos = new Vector3 (0f, 0f, 1f);

		horAngle = Vector3.Angle (forwardPos, targetPos);
		vertAngle = Vector3.Angle (forwardPos, targetPos2);

		return (
			((horAngle < acceptableWeakHorizontalAngle && horAngle >= 0f) && (vertAngle < acceptableWeakVerticalAngle && vertAngle >= 0f) && !sneaking)
			||
			((horAngle < acceptableStrongHorizontalAngle && horAngle >= 0f) && (vertAngle < acceptableStrongVerticalAngle && vertAngle >= 0f))
		);
	}
}