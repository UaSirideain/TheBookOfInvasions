﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public class Weapon : MonoBehaviour, IInputtable, IStateChanger
{
	AvatarMediator mediator { get { return transform.parent.GetComponent<AvatarMediator> (); } }
	FieldAnimations animations { get { return transform.parent.GetComponentInChildren<FieldAnimations> (); } }
	bool weaponDrawn = false;

	ActiveStates stateMachine;
	const float cooldownLength = .2f;
	bool inCooldown = false;

	GameObject weapon;

	[SerializeField] StartBattleTrigger battleTrigger;

	void
	Start()
	{
		weapon = mediator.GetWeapon ().gameObject;
	}

	public void
	SetStateMachine(ActiveStates newStateMachine)
	{
		stateMachine = newStateMachine;
	}

	public void
	SubscribeTo(InputValidator newInput)
	{
		newInput.SubscribeToStringInput (InputMode.Down, InputButton.DrawWeapon, new List<InputState> (), Input_DrawWeapon);
		newInput.SubscribeToStringInput (InputMode.Down, InputButton.LeftMouse, new List<InputState> (), Attack);
	}

	void
	Input_DrawWeapon()
	{
		if (weaponDrawn)
		{
			SheatheWeapon ();
		}
		else if(weapon != null)
		{
			DrawWeapon ();
		}
	}

	void
	Attack()
	{
		if (weaponDrawn && !inCooldown)
		{
			print ("ATTACKED");
			inCooldown = true;
			StartCoroutine(animations.Attack (PointOfContact, AnimationEnded));
		}
	}

	void
	PointOfContact()
	{
		weapon.AddComponent<Rigidbody> ();
		weapon.GetComponent<Rigidbody> ().isKinematic = true;
		weapon.AddComponent<WeaponCollider> ();
		weapon.GetComponent<WeaponCollider> ().onContact += battleTrigger.EnemyAttacked;
		weapon.AddComponent<MeshCollider> ();
		weapon.GetComponent<MeshCollider> ().isTrigger = true;
	}

	void
	AnimationEnded()
	{
		Destroy(weapon.GetComponent<WeaponCollider> ());
		Destroy(weapon.GetComponent<MeshCollider> ());
		StartCoroutine(CooldownTimer ());
	}

	void
	SheatheWeapon()
	{
		print("Weapon Sheathed");
		weaponDrawn = false;
		animations.SheatheWeapon ();
		stateMachine.RemoveState (InputState.WeaponDrawn);
	}

	void
	DrawWeapon()
	{
		print ("Weapon Drawn");
		weaponDrawn = true;
		animations.DrawWeapon ();
		stateMachine.WriteState (InputState.WeaponDrawn);
	}

	IEnumerator
	CooldownTimer()
	{
		float timer = cooldownLength;
		while (timer > 0f)
		{
			timer -= Time.deltaTime;
			yield return null;
		}
		inCooldown = false;
	}
}
}