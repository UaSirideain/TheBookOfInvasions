﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationReference : MonoBehaviour
{
	void
	OnValidate()
	{
		transform.rotation = Quaternion.Euler (0f, transform.rotation.eulerAngles.y, 0f);
	}
}