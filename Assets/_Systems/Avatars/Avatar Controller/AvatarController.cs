﻿using System.Collections.Generic;
using UnityEngine;

public class AvatarController : MonoBehaviour, IInputtable
{
	List<AvatarFacade> playerAvatars = new List<AvatarFacade>();
    AvatarFacade activeCharacterComposition;
    int currentAvatarIndex = 0;

	public List<AvatarFacade>
	GetAvatarsInParty()
	{
		return playerAvatars;
	}

	public AvatarFacade
	GetActiveCharacter()
	{
		return activeCharacterComposition;
	}

	public AvatarFacade
	GetCharacterComposition()
	{
		return activeCharacterComposition;
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.SwitchCharacter, new List<InputState> (){InputState.Battle}, SwitchCharacter_Input);
	}

	public void
	SetAsActiveCharacters(List<AvatarFacade> avatars)
	{
		playerAvatars = avatars;

		foreach(AvatarFacade avatar in playerAvatars)
		{
			avatar.GetTransform ().SetParent (transform);
		}

		currentAvatarIndex = 0;
		SetPlayerCharacter (currentAvatarIndex);
	}

	void
	OnEnable()
	{
		GetComponent<InputListener> ().SetListening ();
	}

    void
    Initialise()
    {
        foreach (AvatarFacade avatar in playerAvatars)
        {
            avatar.GetTransform().SetParent(transform);
        }
        currentAvatarIndex = 0;
        SetPlayerCharacter(currentAvatarIndex);
    }

	void
	SwitchCharacter_Input()
	{
		SwitchCharacter ();
	}
		
    void
    SwitchCharacter()
    {
        currentAvatarIndex++;
        if (currentAvatarIndex >= playerAvatars.Count)
        {
            currentAvatarIndex = 0;
        }

        DeactivateAvatars();
        SetPlayerCharacter(currentAvatarIndex);
    }

    void
    SetPlayerCharacter(int index)
	{
		activeCharacterComposition = playerAvatars[index].GetComponent<AvatarFacade>();
		activeCharacterComposition.SetAsControlledCharacter();
    }

    void
    DeactivateAvatars()//set as ai avatar
    {
        for (int i = 0; i < playerAvatars.Count; i++)
        {
            if (i != currentAvatarIndex)
            {
				playerAvatars[i].SetAsCompanionAI();
            }
        }
    }
}