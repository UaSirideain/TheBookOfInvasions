﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public class AvatarMediator : MonoBehaviour
{
	AvatarDirectory direc{ get { return GetComponent<AvatarDirectory> (); } }

	public PlayerMovement
	GetFieldMovement()
	{
		return direc.GetFieldMovement ();
	}

	public NavAgentController
	GetNavAgentController()
	{
		return direc.GetNavAgentController ();
	}

	public FieldAnimations
	GetFieldAnimations()
	{
		return direc.GetFieldAnimations ();
	}

	public Interactions
	GetInteractions()
	{
		return direc.GetInteractions ();
	}

	public Transform
	GetWeapon()
	{
		return GetComponentInChildren<Transform> ();
	}
}
}