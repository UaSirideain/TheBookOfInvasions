using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeadLookController : MonoBehaviour
{
	[SerializeField] Transform rootNode;
	[SerializeField] List<BendingSegment> segments = new List<BendingSegment>();
	[SerializeField] NonAffectedJoints[] nonAffectedJoints;
	[SerializeField] Vector3 headLookVector = Vector3.forward;
	[SerializeField] Vector3 headUpVector = Vector3.up;
	public Vector3 target = Vector3.zero;
	[SerializeField] float effect = 1;
	[SerializeField] bool overrideAnimation = false;

	Transform targetObject;

	public void
	SetTarget(Transform t)
	{
		targetObject = t;
		target = t.position;
	}

	public IEnumerator
	EndLookAt()
	{
		while (effect > 0f)
		{
			effect = Mathf.Lerp (effect, 0f, .06f);
			yield return null;
		}

		targetObject = null;
		effect = 1f;
	}

	void
	InitialiseSegments()
	{
		BendingSegment neckSegment = new BendingSegment ();
		neckSegment.firstTransform = transform.FindDeepChild ("Neck");
		neckSegment.lastTransform = transform.FindDeepChild ("Head");
		neckSegment.thresholdAngleDifference = 0f;
		neckSegment.bendingMultiplier = .7f;
		neckSegment.maxAngleDifference = 30f;
		neckSegment.maxBendingAngle = 80f;
		neckSegment.responsiveness = 4f;

		segments.Add (neckSegment);

		BendingSegment spineSegment = new BendingSegment ();
		spineSegment.firstTransform = transform.FindDeepChild ("Spine");
		spineSegment.lastTransform = transform.FindDeepChild ("Neck");
		spineSegment.thresholdAngleDifference = 30f;
		spineSegment.bendingMultiplier = .6f;
		spineSegment.maxAngleDifference = 90f;
		spineSegment.maxBendingAngle = 40f;
		spineSegment.responsiveness = 2.5f;

		segments.Add (spineSegment);
	}

	void
	Start ()
	{
		InitialiseSegments ();

		if (rootNode == null)
		{
			rootNode = transform;
		}
		
		// Setup segments
		foreach (BendingSegment segment in segments)
		{
			Quaternion parentRotation = segment.firstTransform.parent.rotation;
			Quaternion parentRotationInverse = Quaternion.Inverse(parentRotation);
			segment.referenceLookDirection = parentRotationInverse * rootNode.rotation * headLookVector.normalized;
			segment.referenceUpDirection = parentRotationInverse * rootNode.rotation * headUpVector.normalized;
			segment.angleHorizontal = 0;
			segment.angleVertical = 0;
			segment.directionUp = segment.referenceUpDirection;
			
			segment.chainLength = 1;
			Transform t = segment.lastTransform;

			while (t != segment.firstTransform && t != t.root)
			{
				segment.chainLength++;
				t = t.parent;
			}
			
			segment.originalRotations = new Quaternion[segment.chainLength];
			t = segment.lastTransform;

			for (int i = segment.chainLength-1; i >= 0; i--)
			{
				segment.originalRotations[i] = t.localRotation;
				t = t.parent;
			}
		}
	}
	
	void
	LateUpdate ()
	{
		if(targetObject != null)
		{
		if (Time.deltaTime == 0)
		{
			return;
		}

//		// Remember initial directions of joints that should not be affected
//		Vector3[] jointDirections = new Vector3[nonAffectedJoints.Length];
//		for (int i = 0; i < nonAffectedJoints.Length; i++)
//		{
//			foreach (Transform child in nonAffectedJoints[i].joint)
//			{
//				jointDirections[i] = child.position - nonAffectedJoints[i].joint.position;
//				break;
//			}
//		}
		
		// Handle each segment
		foreach (BendingSegment segment in segments)
		{
			Transform t = segment.lastTransform;
			if (overrideAnimation)
			{
				for (int i=segment.chainLength-1; i>=0; i--)
				{
					t.localRotation = segment.originalRotations[i];
					t = t.parent;
				}
			}
			
			Quaternion parentRot = segment.firstTransform.parent.rotation;
			Quaternion parentRotInv = Quaternion.Inverse(parentRot);
			
			// Desired look direction in world space
			Vector3 lookDirWorld = (target - segment.lastTransform.position).normalized;
			
			// Desired look directions in neck parent space
			Vector3 lookDirectionGoal = (parentRotInv * lookDirWorld);
			
			// Get the horizontal and vertical rotation angle to look at the target
			float hAngle = AngleAroundAxis(
				segment.referenceLookDirection, lookDirectionGoal, segment.referenceUpDirection
			);
			
			Vector3 rightOfTarget = Vector3.Cross(segment.referenceUpDirection, lookDirectionGoal);
			
			Vector3 lookDirGoalinHPlane =
				lookDirectionGoal - Vector3.Project(lookDirectionGoal, segment.referenceUpDirection);
			
			float vAngle = AngleAroundAxis(lookDirGoalinHPlane, lookDirectionGoal, rightOfTarget);
			
			// Handle threshold angle difference, bending multiplier,
			// and max angle difference here
			float hAngleThr = Mathf.Max(0, Mathf.Abs(hAngle) - segment.thresholdAngleDifference) * Mathf.Sign(hAngle);
			
			float vAngleThr = Mathf.Max(0, Mathf.Abs(vAngle) - segment.thresholdAngleDifference) * Mathf.Sign(vAngle);
			
			hAngle = Mathf.Max(
				Mathf.Abs(hAngleThr) * Mathf.Abs(segment.bendingMultiplier),
				Mathf.Abs(hAngle) - segment.maxAngleDifference
			) * Mathf.Sign(hAngle) * Mathf.Sign(segment.bendingMultiplier);
			
			vAngle = Mathf.Max(
				Mathf.Abs(vAngleThr) * Mathf.Abs(segment.bendingMultiplier),
				Mathf.Abs(vAngle) - segment.maxAngleDifference
			) * Mathf.Sign(vAngle) * Mathf.Sign(segment.bendingMultiplier);
			
			// Handle max bending angle here
			hAngle = Mathf.Clamp(hAngle, -segment.maxBendingAngle, segment.maxBendingAngle);
			vAngle = Mathf.Clamp(vAngle, -segment.maxBendingAngle, segment.maxBendingAngle);
			
			Vector3 referenceRightDir =	Vector3.Cross(segment.referenceUpDirection, segment.referenceLookDirection);
			
			// Lerp angles
			segment.angleHorizontal = Mathf.Lerp(segment.angleHorizontal, hAngle, Time.deltaTime * segment.responsiveness);
			segment.angleVertical = Mathf.Lerp(segment.angleVertical, vAngle, Time.deltaTime * segment.responsiveness);
			
			// Get direction
			lookDirectionGoal = Quaternion.AngleAxis(segment.angleHorizontal, segment.referenceUpDirection)
				* Quaternion.AngleAxis(segment.angleVertical, referenceRightDir)
				* segment.referenceLookDirection;
			
			// Make look and up perpendicular
			Vector3 upDirGoal = segment.referenceUpDirection;
			Vector3.OrthoNormalize(ref lookDirectionGoal, ref upDirGoal);
			
			// Interpolated look and up directions in neck parent space
			Vector3 lookDirection = lookDirectionGoal;
			segment.directionUp = Vector3.Slerp(segment.directionUp, upDirGoal, Time.deltaTime*5);
			Vector3.OrthoNormalize(ref lookDirection, ref segment.directionUp);
			
			// Look rotation in world space
			Quaternion lookRotation = (
				(parentRot * Quaternion.LookRotation(lookDirection, segment.directionUp))
				* Quaternion.Inverse(
					parentRot * Quaternion.LookRotation(
						segment.referenceLookDirection, segment.referenceUpDirection
					)
				)
			);
			
			// Distribute rotation over all joints in segment
			Quaternion dividedRotation = Quaternion.Slerp(Quaternion.identity, lookRotation, effect / segment.chainLength);
			t = segment.lastTransform;

			for (int i = 0; i < segment.chainLength; i++)
			{
				t.rotation = dividedRotation * t.rotation;
				t = t.parent;
			}
		}
		
//		// Handle non affected joints
//		for (int i=0; i<nonAffectedJoints.Length; i++)
//		{
//			Vector3 newJointDirection = Vector3.zero;
//			
//			foreach (Transform child in nonAffectedJoints[i].joint)
//			{
//				newJointDirection = child.position - nonAffectedJoints[i].joint.position;
//				break;
//			}
//			
//			Vector3 combinedJointDirection = Vector3.Slerp(jointDirections[i], newJointDirection, nonAffectedJoints[i].effect);
//			
//			nonAffectedJoints[i].joint.rotation = Quaternion.FromToRotation(newJointDirection, combinedJointDirection) * nonAffectedJoints[i].joint.rotation;
//		}
		}
	}
	
	// The angle between dirA and dirB around axis
	public static float
	AngleAroundAxis (Vector3 directionA, Vector3 directionB, Vector3 axis)
	{
		// Project A and B onto the plane orthogonal target axis
		directionA = directionA - Vector3.Project(directionA, axis);
		directionB = directionB - Vector3.Project(directionB, axis);
		
		// Find (positive) angle between A and B
		float angle = Vector3.Angle(directionA, directionB);
		
		// Return angle multiplied with 1 or -1
		return angle * (Vector3.Dot(axis, Vector3.Cross(directionA, directionB)) < 0 ? -1 : 1);
	}
}