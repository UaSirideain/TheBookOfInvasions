using UnityEngine;
using System.Collections;

public class TestHeadLook : MonoBehaviour
{
	[SerializeField] HeadLookController headLook;

	void
	LateUpdate ()
	{
		if (headLook != null)
		{
			headLook.target = transform.position;
		}
	}
}