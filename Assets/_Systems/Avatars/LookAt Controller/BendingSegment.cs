﻿using UnityEngine;

[System.Serializable]
public class BendingSegment
{
	public Transform firstTransform;
	public Transform lastTransform;
	public float thresholdAngleDifference = 0;
	public float bendingMultiplier = 0.6f;
	public float maxAngleDifference = 30;
	public float maxBendingAngle = 80;
	public float responsiveness = 5;
	internal float angleHorizontal;
	internal float angleVertical;
	internal Vector3 directionUp;
	internal Vector3 referenceLookDirection;
	internal Vector3 referenceUpDirection;
	internal int chainLength;
	internal Quaternion[] originalRotations;
}