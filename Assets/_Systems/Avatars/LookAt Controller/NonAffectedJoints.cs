﻿using UnityEngine;

[System.Serializable]
public class NonAffectedJoints
{
	public Transform joint;
	public float effect = 0;
}