﻿using Field.Abilities;
using UnityEngine;
using UnityEngine.AI;

namespace Field.Avatars{

public class AvatarDirectory : MonoBehaviour
{
	Character character;
	Transform pointAboveHead;

	[SerializeField] Transform interactionUI;
	[SerializeField] Transform characterIndicator;

	FieldMusic playerMusic;
	PlayerMovement playerMovement;
	PlayerAbilities playerAbilities;

	MovementSpeed movementSpeed;

	FieldAnimations animations;
//	FieldInteractions interactions;
	Interactions newInteractions;

	CompanionAI companionMovement;
    SenseController senseController;

	AbilityController abilityController;

	NavAgentController navAgentController;
	NavMeshAgent navMeshAgent;

	void
	Awake()
	{
		playerMusic = GetComponentInChildren<FieldMusic> ();
		playerMovement = GetComponentInChildren<PlayerMovement> ();
		playerAbilities = GetComponentInChildren<PlayerAbilities> ();
		animations = GetComponentInChildren<FieldAnimations> ();
//		interactions = GetComponentInChildren<FieldInteractions> ();
		newInteractions = GetComponentInChildren<Interactions> ();
		abilityController = GetComponentInChildren<AbilityController> ();

		companionMovement = GetComponentInChildren<CompanionAI> ();
        senseController = GetComponentInChildren<SenseController>();

        navAgentController = GetComponentInChildren<NavAgentController> ();
		navMeshAgent = GetComponentInChildren<NavMeshAgent> ();

		movementSpeed = GetComponentInChildren<MovementSpeed> ();
	}

	public MovementSpeed
	GetMovementSpeed()
	{
		return movementSpeed;
	}

	public void
	SetCharacter(Character newCharacter)
	{
		character = newCharacter;
		gameObject.name = AvatarOperations.GetCharacterDisplayName (character.GetCharacterName ());
	}

	public Character
	GetCharacter()
	{
		return character;
	}
		
	public void
	SetPointAboveHead(Transform newPoint)
	{
		pointAboveHead = newPoint;
		characterIndicator.transform.position = newPoint.position;
		interactionUI.position = pointAboveHead.position;
	}

	public FieldMusic
	GetFieldMusic()
	{
		return playerMusic;
	}

	public Transform
	GetPointAboveHead()
	{
		return pointAboveHead;
	}

	public PlayerAbilities
	GetFieldAbilities()
	{
		return playerAbilities;
	}

	public CompanionAI
	GetCompanionAI()
	{
		return companionMovement;
	}

    public SenseController
    GetSenseController()
    {
        return senseController;
    }

	public NavMeshAgent
	GetNavAgent()
	{
		return navMeshAgent;
	}

	public NavAgentController
	GetNavAgentController()
	{
		return navAgentController;
	}

	public PlayerMovement
	GetFieldMovement()
	{
		return playerMovement;
	}

	public GameObject
	GetActiveCharacterIndicator()
	{
		return characterIndicator.gameObject;
	}

//	public FieldInteractions
//	GetFieldInteractions()
//	{
//		return interactions;
//	}
//
	public FieldAnimations
	GetFieldAnimations()
	{
		return animations;
	}

	public Interactions
	GetInteractions()
	{
		return newInteractions;
	}

	public AbilityController
	GetAbilityController()
	{
		return abilityController;
	}
}
}