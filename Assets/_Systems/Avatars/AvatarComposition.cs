﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Sequences;

namespace Field.Avatars{

public class AvatarComposition : MonoBehaviour, IInputtable, IStateChanger
{
	ActiveStates stateMachine;
	AvatarDirectory directory { get { return GetComponent<AvatarDirectory> (); } }
	AvatarFacade composition { get { return GetComponent<AvatarFacade> (); } }
	InputListener inputListener {get{return GetComponent<InputListener>();}}
	//AssignCameraPointToAvatar currentCameraPoint2;
	CameraPoint currentCameraPoint;

	bool activeCharacter;//we can probably replace this variable with the CurrentControlState
	enum AvatarControlState{Controlled, Companion, NPC}
	AvatarControlState currentControlState = AvatarControlState.NPC;

	public void
	SetStateMachine(ActiveStates newStateMachine)
	{
		stateMachine = newStateMachine;
		stateMachine.WriteState (InputState.ReadyingAbilities);
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ToggleNumericMap, new List<InputState> (){InputState.PausingUI, InputState.FullscreenUI, InputState.UI, InputState.Battle}, ToggleNumericMap_Input);
	}

	enum CurrentState{Music, Abilities}
	CurrentState currentState = CurrentState.Abilities;

	void
	ToggleNumericMap_Input()
	{
		if (currentState == CurrentState.Abilities)
		{
			directory.GetFieldAbilities().DisableAbilitiesState ();
			directory.GetFieldMusic().EnableMusicState ();
			currentState = CurrentState.Music;
		}
		else if(currentState == CurrentState.Music)
		{
			directory.GetFieldMusic().DisableMusicState ();
			directory.GetFieldAbilities().EnableAbilitiesState ();
			currentState = CurrentState.Abilities;
		}
	}

	public void
	SetAsControlledCharacter()
	{
		currentControlState = AvatarControlState.Controlled;

		activeCharacter = true;

		directory.GetCompanionAI().enabled = false;
		directory.GetNavAgentController().enabled = false;
		directory.GetNavAgent().enabled = false;

		directory.GetMovementSpeed().SetWalking ();

		StartCoroutine(ShowActiveCharacterMarker());

		if (currentCameraPoint != null)
		{
			currentCameraPoint.ActivateCameraPoint ();// (GetComponent<AvatarFacade> ());//currentCameraPoint.OnTriggerEnterOverride(GetComponent<AvatarFacade>(), 300f);
		}

		inputListener.SetListening ();
	}

	public void
	SetAsCompanionAI()
	{
		currentControlState = AvatarControlState.Companion;

		activeCharacter = false;
		directory.GetActiveCharacterIndicator().SetActive (false);

		directory.GetNavAgent().enabled = true;
		directory.GetNavAgentController().enabled = true;
		directory.GetCompanionAI().enabled = true;
		directory.GetFieldMovement().DisableMovement();

//		directory.GetFieldInteractions().NoLongerActiveCharacter();

		inputListener.SetNotListening ();
	}

	public void
	EnableSequenceMode()
	{
		directory.GetNavAgent ().enabled = true;
		directory.GetNavAgentController ().enabled = true;
		directory.GetCompanionAI ().enabled = false;
		directory.GetFieldMovement ().DisableMovement ();

		inputListener.SetNotListening ();
	}

	public void
	EndSequenceMode()
	{
		if (currentControlState == AvatarControlState.Companion)
		{
			SetAsCompanionAI ();
		}
		else if(currentControlState == AvatarControlState.Controlled)
		{
			SetAsControlledCharacter ();
		}
	}

	IEnumerator
	ShowActiveCharacterMarker()
	{
		directory.GetActiveCharacterIndicator().SetActive(true);
		yield return new WaitForSeconds(1);
		directory.GetActiveCharacterIndicator().SetActive(false);
	}

	public void
	SetCurrentCameraPoint(AssignCameraPointToAvatar camPoint)
	{
		//currentCameraPoint2 = camPoint;
	}

	public void
	SetCurrentCameraPoint(CameraPoint camPoint)
	{
		currentCameraPoint = camPoint;
		GetComponentInChildren<OrientationOffset> ().SetNextOffset (camPoint.GetOrientationOffset ());

		if (activeCharacter)
		{
			currentCameraPoint.ActivateCameraPoint ();
		}
	}

	public bool
	IsActiveCharacter()
	{
		return activeCharacter;
	}
}
}