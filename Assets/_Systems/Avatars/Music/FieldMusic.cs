﻿using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public class FieldMusic : MonoBehaviour, IInputtable, IStateChanger
{
    [SerializeField] GameObject instrumentPrefab;
	[SerializeField] GameObject instrumentMeshPrefab;

    FieldInstrument instrument;
	GameObject instrumentMesh;

	AvatarFacade avatarComposition { get { return transform.parent.GetComponent<AvatarFacade> (); } }
    FieldMusicUI UI { get { return GameObject.FindObjectOfType<FieldMusicUI>(); } }
    FieldAnimations animations { get { return transform.parent.GetComponentInChildren<FieldAnimations>(); } }

	ActiveStates stateMachine;

	public void
	SetStateMachine(ActiveStates newStateMachine)
	{
		stateMachine = newStateMachine;
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down,InputButton.ChangeMusicalStrain, new List<InputState> (){ InputState.Battle, InputState.FullscreenUI, InputState.ReadyingAbilities, InputState.PausingUI }, ChangeMusicalStrain_Input);
		input.SubscribeToNumericInput (InputMode.Down, new List<InputState> (){ InputState.Battle, InputState.FullscreenUI, InputState.ReadyingAbilities, InputState.PausingUI }, NumericInput);

		input.SubscribeToStringInput (InputMode.Down, InputButton.LowerOctave, new List<InputState> (){InputState.Battle, InputState.ReadyingAbilities}, RaiseOctave_Input);
		input.SubscribeToStringInput (InputMode.Up, InputButton.RaiseOctave, new List<InputState> (){InputState.Battle, InputState.ReadyingAbilities}, RaiseOctave_InputRelease);
		input.SubscribeToStringInput (InputMode.Down, InputButton.LowerOctave, new List<InputState> (){InputState.Battle, InputState.ReadyingAbilities}, LowerOctave_Input);
		input.SubscribeToStringInput (InputMode.Up, InputButton.LowerOctave, new List<InputState> (){InputState.Battle, InputState.ReadyingAbilities}, LowerOctave_InputRelease);
	}

	bool rightControlHeld;
	bool rightShiftHeld;

	OctaveType activeOctave = OctaveType.Mid;

	void
	RaiseOctave_Input()
	{
		activeOctave = instrument.RaiseOctave ();
	}

	void
	RaiseOctave_InputRelease()
	{
		activeOctave = instrument.ResetOctave ();
	}

	void
	LowerOctave_Input()
	{
		activeOctave = instrument.LowerOctave ();
	}

	void
	LowerOctave_InputRelease()
	{
		activeOctave = instrument.ResetOctave ();
	}

	void
	ChangeMusicalStrain_Input()
	{
		
	}

	void
	NumericInput(string numeric)
	{
		NoteType note = SeshMaths.Input.GetNote (numeric);
		OctaveType noteOctave = SeshMaths.Input.GetAdjustedOctave (numeric, activeOctave);

		instrument.PlayNote (note, noteOctave);

		instrumentMesh.SetActive (true);
		animations.PlayInstrumentNote ();
	}

	//TODO: This is a temporary way to have the harp begin in the correct position for while Shane is here. These variables and all lines that reference them can be safely deleted.
	Vector3 hPosition = new Vector3(1.002f, 0.124f, 0.008f);
	Vector3 hRotation = new Vector3(72.52901f, 9.645f, 102.775f);

	public void
	InitialiseInstrument(FieldInstrument instrumentPrefab)
	{
		FieldInstrument newInstrument = Instantiate (instrumentPrefab);
		instrument = newInstrument;
		instrument.transform.SetParent (transform);

		instrumentMesh = instrument.GetMesh ().gameObject;
		instrumentMesh.transform.SetParent (FindMusicBone (), false);
		instrumentMesh.transform.localPosition = hPosition;
		instrumentMesh.transform.localRotation = Quaternion.Euler(hRotation);
		instrumentMesh.SetActive (false);
	}

    public bool
    HasInstrument()
    {
		return instrument != null;
    }

    public void
    RenderStrain(MusicalStrain strain)
    {
    }

	public void
	EnableMusicState()
	{		
		stateMachine.WriteState (InputState.PlayingMusic);
		ActivateHarpMesh(true);
		//animations.PlayingInstrument(true);
		animations.SetAsPlayingInstrument ();
	}

    public void
    DisableMusicState()
    {
        ActivateHarpMesh(false);
        //animations.PlayingInstrument(false);
		animations.SetAsNotPlayingInstrument ();
        UI.TurnOffUI();
		stateMachine.RemoveState (InputState.PlayingMusic);
    }

    void
    ActivateHarpMesh(bool status)
    {
        if (instrumentMesh != null)
        {
            instrumentMesh.SetActive(status);
        }
    }

    Transform
    FindMusicBone()
    {
		return avatarComposition.transform.FindDeepChild("Spine");
    }
}
}