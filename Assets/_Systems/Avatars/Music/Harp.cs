﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Harp : FieldInstrument
{
	Dictionary<string, AudioSource> notes = new Dictionary<string, AudioSource>(); 

	void
	Awake()
	{
		foreach (AudioSource note in GetComponentsInChildren<AudioSource>())
		{
			notes.Add (note.name, note);
		}
	}
		
	public override void
	PlayNote(NoteType note, OctaveType octave)
	{
		string nameOfNote = SeshMaths.Mode.GetNoteName (note, octave, TuneMode.Major);

		if (notes.ContainsKey (nameOfNote))
		{
			notes [nameOfNote].Play ();
		}
	}

	public override OctaveType
	RaiseOctave()
	{
		return OctaveType.High;
	}

	public override OctaveType
	LowerOctave()
	{
		return OctaveType.Low;
	}

	public override OctaveType
	ResetOctave()
	{
		return OctaveType.Mid;
	}
}