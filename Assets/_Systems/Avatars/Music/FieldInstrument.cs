﻿using UnityEngine;

public enum MusicalStrain{Standard, Geantrai, Goltrai, Suantrai}

public abstract class FieldInstrument : MonoBehaviour
{
	[SerializeField] protected Transform mesh;

	public Transform
	GetMesh()
	{
		return mesh;
	}

	protected AvatarFacade fieldAvatar;

	public void
	HeyThere(TuneLeader leader)
	{
		leader.RegisterTunedInstrument (PlayNote);
	}

	public void
	ByeNow(TuneLeader leader)
	{
		leader.DeregisterInstrument (PlayNote);
	}

	public abstract void
	PlayNote (NoteType note, OctaveType octave);

	public abstract OctaveType
	RaiseOctave ();

	public abstract OctaveType
	LowerOctave();

	public abstract OctaveType
	ResetOctave ();
}