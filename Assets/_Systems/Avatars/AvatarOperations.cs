﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NamedCharacter{Aon, AonghasOg}

public static class AvatarOperations
{
	static List<NamedCharacter> playerCharacters = new List<NamedCharacter>()
	{
		NamedCharacter.Aon,
		NamedCharacter.AonghasOg
	};

	public static AvatarFacade
	FindCharacter(NamedCharacter characterName)
	{
		List<AvatarFacade> avatarsInScene = new List<AvatarFacade> ();
		avatarsInScene.AddRange(GameObject.FindObjectsOfType<AvatarFacade> ());

		foreach (AvatarFacade avatar in avatarsInScene)
		{
			if (avatar.GetCharacter ().GetCharacterName () == characterName)
			{
				return avatar;
			}
		}

		return null;
	}

	static Dictionary<NamedCharacter, string> characterNames = new Dictionary<NamedCharacter, string>()
	{
		{NamedCharacter.Aon, "Aon"},
		{NamedCharacter.AonghasOg, "Aonghas Óg"}
	};

	public static string
	GetCharacterDisplayName(NamedCharacter characterName)
	{
		return characterNames [characterName];
	}

	public static List<AvatarFacade>
	GetAllAvatarsInParty()
	{
		List<AvatarFacade> avatarsInParty = new List<AvatarFacade> ();

		List<AvatarFacade> avatarsInScene = new List<AvatarFacade> ();
		avatarsInScene.AddRange(GameObject.FindObjectsOfType<AvatarFacade> ());

		foreach (AvatarFacade avatar in avatarsInScene)
		{
			if(playerCharacters.Contains(avatar.GetCharacter().GetCharacterName()))
			{
				avatarsInParty.Add(avatar);
			}
		}

		return avatarsInParty;
	}
}