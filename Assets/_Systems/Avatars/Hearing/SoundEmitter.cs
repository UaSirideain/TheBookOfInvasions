﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum SoundType { AllyFootsteps, EnemyFootsteps, AllyAbility, EnemyAbility, Environment }
public enum SoundVolume { Quiet, Loud }
public class SoundEmitter : MonoBehaviour
{
    [SerializeField] private SoundVolume noiseLevel = SoundVolume.Quiet;
    [SerializeField] private SoundType noiseType = SoundType.AllyFootsteps;
    [SerializeField] private float defaultStrength = 1f;

    public SoundVolume NoiseLevel
    {
        get { return noiseLevel; }
        set { noiseLevel = value; }
    }

    public void
    Emit(float strengthOverride = 0f)
    {
        print("Emitting");
        DrawSoundSphere(strengthOverride);
    }

    private void
    DrawSoundSphere(float strengthOverride = 0f)
    {
        print("Drawing sphere");
        List<Collider> allObjects = new List<Collider>();
        List<SoundListener> listeners = new List<SoundListener>();
        
        allObjects = Physics.OverlapSphere(transform.position, ActiveSoundStrength(strengthOverride)).ToList();

        print(allObjects.Count);

        listeners = FindListeners(allObjects);

        foreach (SoundListener listener in listeners)
        {
            print("Registering Sound");
            listener.RegisterSound(new SoundObject(this, noiseType, NoiseLevel, transform.position));
        }
    }

    private List<SoundListener>
    FindListeners(List<Collider> allObjects)
    {
        List<SoundListener> _tempListeners = new List<SoundListener>();
        foreach (Collider singleObject in allObjects)
        {
            print(singleObject);
            if (singleObject.GetComponentInChildren<SoundListener>())
            {
                print("Has a Sound Listener");
                _tempListeners.Add(singleObject.GetComponentInChildren<SoundListener>());
            }
        }
        return _tempListeners;
    }

    private float
    ActiveSoundStrength(float strengthOverride)
    {
        if (strengthOverride > 0f)
        {
            return strengthOverride;
        }
        return defaultStrength;
    }
}
