﻿using UnityEngine;

public class SoundTrigger : MonoBehaviour
{
    private void
    OnTriggerEnter(Collider collider)
    {
        if (collider.GetComponent<SoundEmitter>())
        {
            print("Collided");
            collider.GetComponent<SoundEmitter>().Emit();
        }
    }
}
