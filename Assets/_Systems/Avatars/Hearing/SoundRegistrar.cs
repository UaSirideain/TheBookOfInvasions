﻿using System.Collections.Generic;
using UnityEngine;

public class SoundRegistrar : MonoBehaviour
{
    private SenseController senseController { get { return GetComponent<SenseController>(); } }

    [SerializeField] private List<SoundType> soundsToIgnore = new List<SoundType>();
    private List<SoundObject> soundToInvestigate = new List<SoundObject>();
    private List<SoundObject> dangerousSounds = new List<SoundObject>();

    public void
    RegisterSound(SoundObject sound)
    {
        if (soundsToIgnore.Contains(sound.NoiseType))
        {
            print("Ignoring");
            return;
        }

        if (!soundToInvestigate.Contains(sound))
        {
            soundToInvestigate.Add(sound);
            senseController.UpdateHearing(sound, false);
            print("Unknown Sound");
            //InvestigateRoutine
        }
        else
        {
            if (dangerousSounds.Contains(sound))
            {
                senseController.UpdateHearing(sound, true);
                print("Dangerous Sound");
                //DangerousRoutine
            }
            if (soundToInvestigate.Contains(sound))
            {
                senseController.UpdateHearing(sound, false);
                print("Similar Unknown Sound");
                //InvestigateRoutine
            }
        }
    }
}
