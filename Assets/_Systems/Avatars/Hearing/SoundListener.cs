﻿using System.Collections;
using UnityEngine;

public class SoundListener : MonoBehaviour
{
    private const float SPEED_OF_SOUND = 331.2f;

    [SerializeField] private float hearingRadiusClose = 5f;
    [SerializeField] private float hearingRadiusFar = 10f;

    private SoundRegistrar registrar = null;
    
    private void
    Start()
    {
        registrar = GetComponentInParent<SoundRegistrar>();
        GetComponent<SphereCollider>().radius = hearingRadiusFar;
    }

    public void
    RegisterSound(SoundObject sound)
    {
        float distanceFromSource = Vector3.Distance(sound.Source, transform.position);
        StartCoroutine(WaitToHear(sound, distanceFromSource));        
    }

    private IEnumerator
    WaitToHear(SoundObject sound, float distance)
    {
        if (sound.NoiseLevel.Equals(SoundVolume.Loud) || distance <= hearingRadiusClose)
        {
            float timeToHear = 0f;

            while (timeToHear < distance)
            {
                timeToHear += SPEED_OF_SOUND * Time.deltaTime;
                print("Waiting");
                yield return null;
            }
            registrar.RegisterSound(sound);
        }
    }

    private void
    OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, hearingRadiusFar);
        
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, hearingRadiusClose);
    }
}
