﻿using UnityEngine;

public class SoundObject : System.Object
{
    public Vector3 Source { get; set; }
    public SoundEmitter Emitter { get; set; }
    public SoundType NoiseType { get; set; }
    public SoundVolume NoiseLevel { get; set; }
    public bool HasBeenInvestigated { get; set; }

    public SoundObject(SoundEmitter emitter, SoundType noiseType, SoundVolume noiseLevel, Vector3 source, bool investigated = false)
    {
        Emitter = emitter;
        NoiseType = noiseType;
        NoiseLevel = noiseLevel;
        Source = source;
        HasBeenInvestigated = investigated;
    }
}
