﻿using UnityEngine;

public class SenseController : MonoBehaviour
{
    public SoundObject ActiveSound { get; private set; }
    public bool isDangerous { get; private set; }

    public void
    UpdateHearing(SoundObject sound, bool isDangerous)
    {
        if (ActiveSound == null)
        {
            print("Assigning active sound");
            ActiveSound = sound;
            this.isDangerous = isDangerous;
        }
        else
        {
            if (ActiveSound.NoiseLevel != sound.NoiseLevel)
            {
                if (sound.NoiseLevel == SoundVolume.Loud)
                {
                    print("assigning active sound 2");
                    ActiveSound = sound;
                    this.isDangerous = isDangerous;
                }
                return;
            }
            else if (ActiveSound.NoiseType != sound.NoiseType)
            {
                if (sound.NoiseType == SoundType.AllyAbility)
                {
                    print("assigning active sound 3");
                    ActiveSound = sound;
                    this.isDangerous = isDangerous;
                }
                return;
            }
            else
            {
                DetermineMostPressingSound(sound, isDangerous);
            }
        }
    }

    private void
    DetermineMostPressingSound(SoundObject sound, bool isDangerous)
    {
        if (this.isDangerous == isDangerous)
        {
            float distanceToSource = Vector3.Distance(sound.Source, transform.position);
            float distanceBetweenSounds = Vector3.Distance(sound.Source, ActiveSound.Source);
            float distanceToActiveSound = Vector3.Distance(ActiveSound.Source, transform.position);
            if (distanceBetweenSounds > 5f)
            {
                print("assigning active sound 4");
                ActiveSound = sound;
                this.isDangerous = isDangerous;
                return;
            }
            else if (distanceToActiveSound > distanceToSource)
            {
                print("assigning active sound 5");
                ActiveSound = sound;
                this.isDangerous = isDangerous;
                return;
            }
        }
        else if (isDangerous == true)
        {
            print("assigning active sound 6");
            ActiveSound = sound;
            this.isDangerous = isDangerous;
        }
    }

    public void
    SetAsInvestigated()
    {
        ActiveSound.HasBeenInvestigated = true;
        ActiveSound = null;
        isDangerous = false;
    }
}
