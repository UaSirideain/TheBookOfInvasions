﻿using UnityEngine;

public class AvatarGuideLink : MonoBehaviour
{
    private PathGuide guide;

    public Vector3
    ConfineMovementToDirection(Vector3 moveVector)
    {
        if (guide != null)
        {
            return guide.CalculatePath(moveVector);
        }
        return moveVector;
    }

    public PathGuide	//TODO: This shouldn't be a method. The tests should reflect the variable if they want to access it.
    GetGuide()
    {
        return guide;
    }

    public void
    SetGuide(PathGuide newGuide)
    {
        guide = newGuide;
    }
}