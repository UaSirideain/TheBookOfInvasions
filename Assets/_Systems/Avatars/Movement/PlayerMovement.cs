﻿using UnityEngine;
using System.Collections.Generic;

namespace Field.Avatars{

public enum MovementState{Regular, Rope, Disabled};

public class PlayerMovement : MonoBehaviour, IInputtable
{
	MovementState movementState = MovementState.Regular;

    CharacterController controller { get { return transform.parent.GetComponent<CharacterController>(); } }
	MovementSpeed moveSpeed { get { return transform.parent.GetComponentInChildren<MovementSpeed>(); } }
	GroundMovement groundMove {get{return transform.parent.GetComponentInChildren<GroundMovement>();}}
	RopeMovement ropeMove{get{ return transform.parent.GetComponentInChildren<RopeMovement>(); }}

    Vector3 falling = new Vector3(0, -9.8f, 0);

    float axisWS = 0f;
    float axisAD = 0f;

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.Run, new List<InputState> (){InputState.PausingUI, InputState.Battle}, LeftShift);
		input.SubscribeToStringInput (InputMode.Up, InputButton.Run, new List<InputState> (){InputState.PausingUI, InputState.Battle}, LeftShiftUp);
		input.SubscribeToStringInput (InputMode.Down, InputButton.Sneak, new List<InputState> (){InputState.PausingUI, InputState.Battle}, LeftControl);
		input.SubscribeToStringInput (InputMode.Up, InputButton.Sneak, new List<InputState> (){InputState.PausingUI, InputState.Battle}, LeftControlUp);
		input.SubscribeToAxesInput (new List<InputState> (){InputState.FullscreenUI, InputState.PausingUI, InputState.Battle}, AxesInput);
	}
		
	public void
	SetKeymap(MovementState newMap)
	{
		if (newMap == MovementState.Regular || newMap == MovementState.Rope || newMap == MovementState.Disabled)
		{
			movementState = newMap;
		}
		else
		{
			throw new System.NotSupportedException();
		}
	}

	public void
	DisableMovement()
	{
		ResetAxes ();
	}

	void
	LeftControl()
	{
		moveSpeed.SetSneaking ();
	}

	void
	LeftControlUp()
	{
		moveSpeed.StopSneaking ();
	}

	void
	LeftShift()
	{
		moveSpeed.SetRunning ();
	}

	void
	LeftShiftUp()
	{
		moveSpeed.StopRunning ();
	}

	void
	AxesInput (float x, float y)
	{
		axisAD = x;
		axisWS = y;
		Move ();
	}
		
    void
    Move()
    {
		if (movementState == MovementState.Regular)
		{
			groundMove.Move (axisAD, axisWS);

			controller.Move (falling * Time.deltaTime);
		}
		else if (movementState == MovementState.Rope)
		{
			ropeMove.Move (axisAD, axisWS);
		}
    }

    void
    ResetAxes()
    {
        axisWS = 0f;
        axisAD = 0f;
    }
}
}