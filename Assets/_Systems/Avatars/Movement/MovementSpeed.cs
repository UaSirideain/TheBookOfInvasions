﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Avatars{

public enum MoveType{Walk, Run, Sneak, Crawl}

public class MovementSpeed : MonoBehaviour
{
	MoveType moveType = MoveType.Walk;

	[SerializeField] float walkSpeed;
	[SerializeField] float runSpeed;
	[SerializeField] float crawlSpeed;
	[SerializeField] float sneakSpeed;
	float currentSpeed;

	FieldAnimations animations {get{return transform.parent.GetComponentInChildren<FieldAnimations>();}}

	bool runKeyPressed = false;
	bool sneakKeyPressed = false;

	public void
	SetWalking()
	{
		moveType = MoveType.Walk;
		currentSpeed = walkSpeed;

		animations.SetMovementType (MoveType.Walk);
	}

	public void
	SetRunning()
	{
		runKeyPressed = true;

		moveType = MoveType.Run;
		currentSpeed = runSpeed;

		animations.SetMovementType (MoveType.Run);
	}

	public void
	SetCrawling()
	{
		moveType = MoveType.Crawl;
		currentSpeed = crawlSpeed;

		animations.SetMovementType (MoveType.Crawl);
	}

	public void
	SetSneaking()
	{
		sneakKeyPressed = true;

		moveType = MoveType.Sneak;
		currentSpeed = sneakSpeed;

		animations.SetMovementType (MoveType.Sneak);
	}

	public void
	StopSneaking()
	{
		if (sneakKeyPressed)
		{
			sneakKeyPressed = false;
			Revert ();
		}
	}

	public void
	StopRunning()
	{
		if (runKeyPressed)
		{
			runKeyPressed = false;
			Revert ();
		}
	}

	void
	Start()
	{
		SetWalking ();
	}

	void
	Revert()
	{
		if (sneakKeyPressed)
		{
			SetSneaking ();
		}
		else if (runKeyPressed)
		{
			SetRunning ();
		}
		else
		{
			SetWalking ();
		}
	}

	public float
	GetSpeed()
	{
		return currentSpeed;
	}

	public MoveType
	GetMoveType()
	{
		return moveType;
	}
}
}