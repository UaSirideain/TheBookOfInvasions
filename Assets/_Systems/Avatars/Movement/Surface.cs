﻿using UnityEngine;

public static class Surface
{
    public static bool
    IsWalkable(this CharacterController controller, Transform movementFeeler, LayerMask layerMask, string tag = "Walkable")
    {
        RaycastHit ground;

        //needs to ignore certain layers
        if (Physics.Raycast(movementFeeler.position, movementFeeler.forward, out ground, GetMaxDistanceToGround(controller, movementFeeler), layerMask))
        {
            if (ground.collider.gameObject.tag == tag && ground.distance > GetMinDistanceToGround(controller, movementFeeler))
            {
                return true;
            }
        }
        return false;
    }

    private static float maxWaterLevel = 0.3f;

    public static bool //measures distance between ground and water
    IsWaterShallow(this CharacterController controller, Transform movementFeeler, LayerMask layerMask, string tag = "Walkable")
    {
        RaycastHit ground;
        float distanceToGround = GetMaxDistanceToGround(controller, movementFeeler);

        //needs to ignore certain layers
        if (Physics.Raycast(movementFeeler.position, movementFeeler.forward, out ground, 20f, layerMask))
        {
            if (TagsAreEqual(ground, tag))
            {
                distanceToGround = ground.distance;
            }
        }

        RaycastHit water;
        float distanceToWater = GetMaxDistanceToGround(controller, movementFeeler);

        //needs to ignore all layers that aren't water
        if (Physics.Raycast(movementFeeler.position, movementFeeler.forward, out water, 20f))
        {
            if (TagsAreEqual(water, tag = "Water"))
            {
                distanceToWater = water.distance;
            }
        }

        float waterLevel = Mathf.Clamp(distanceToGround - distanceToWater, 0f, maxWaterLevel + 1f);

        return (waterLevel >= maxWaterLevel);
    }

    private static bool
    TagsAreEqual(RaycastHit hitInfo, string tagToCompare)
    {
        return hitInfo.collider.gameObject.tag == tagToCompare;
    }

    //TODO: Unit Test
    public static float
    GetMaxDistanceToGround(CharacterController controller, Transform movementFeeler)
    {
        float distance = SetupDistance(controller, movementFeeler);
        return distance + movementFeeler.localPosition.y + controller.radius;  
    }
    
    //TODO: Unit Test
    public static float
    GetMinDistanceToGround(CharacterController controller, Transform movementFeeler)
    {
        float distance = SetupDistance(controller, movementFeeler);
        distance = movementFeeler.localPosition.y - distance;
        return distance;// - widthOfCharacter;
    }

    private static float
    SetupDistance(CharacterController controller, Transform movementFeeler)
    {
        float distance;
        float tanOfSlopeLimit = Mathf.Tan(controller.slopeLimit * Mathf.Deg2Rad);

        distance = tanOfSlopeLimit * movementFeeler.localPosition.z;
        return distance;
    }
}