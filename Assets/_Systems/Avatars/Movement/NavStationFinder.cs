﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavStationFinder : MonoBehaviour
{
	public NavStation
	GetNavStationClosestTo(Transform target)
	{
		List<NavStation> navStations = GetAllNavStations ();
		navStations = RemoveInvalidStations(navStations);
		return GetClosestStationFrom (navStations, target);
	}

	List<NavStation>
	GetAllNavStations()
	{
		List<NavStation> navStations = new List<NavStation> ();
		navStations.AddRange(FindObjectsOfType<NavStation>());
		return navStations;
	}

	List<NavStation>
	RemoveInvalidStations(List<NavStation> navStations)
	{
		List<NavStation> validStations = new List<NavStation> ();

		foreach (NavStation station in navStations)
		{
			if (station.occupied == false)
			{
				validStations.Add (station);
			}
		}

		return validStations;
	}

	NavStation
	GetClosestStationFrom(List<NavStation> navStations, Transform target)
	{
		float distance = 0f;
		bool distanceSet = false;
		NavStation closestNavStation = null;

		foreach (NavStation station in navStations)
		{
			if (distanceSet == false)
			{
				distance = CalculateDistanceFromTarget (station.transform.position, target.position);
				closestNavStation = station;
			}
			else if (CalculateDistanceFromTarget(station.transform.position, target.position) < distance)
			{
				distance = CalculateDistanceFromTarget (station.transform.position, target.position);
				closestNavStation = station;
			}
		}
		return closestNavStation;
	}

	float
	CalculateDistanceFromTarget(Vector3 station, Vector3 target)
	{
		NavMeshPath path = new NavMeshPath();
		NavMesh.CalculatePath (station, target, NavMesh.AllAreas, path);

		float distance = 0f;
		Vector3 lastPoint = transform.position;

		foreach (Vector3 corner in path.corners)
		{
			distance += Vector3.Distance (corner, lastPoint);
			lastPoint = corner;
		}

		return distance;
	}
}