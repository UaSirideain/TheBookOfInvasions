﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationOffset : MonoBehaviour
{
	Transform currentOffset;
	Transform nextOffset;

	public void
	SetNextOffset(Transform targetOffset)
	{
		nextOffset = targetOffset;
	}

	public void
	UpdateOffset()
	{
		if (nextOffset)
		{
			currentOffset = nextOffset;
			nextOffset = null;
		}
	}

	void
	ZeroXZRotation()
	{
		float yRotation = currentOffset.rotation.eulerAngles.y;

		currentOffset.eulerAngles = new Vector3 (0, yRotation, 0);
	}

	public Vector3
	ApplyOffsetTo(Vector3 direction)
	{
		if (currentOffset != null)
		{
			ZeroXZRotation ();
			return currentOffset.TransformDirection (direction);
		}
		else
		{
			return direction;
		}
	}
}