﻿using UnityEngine;

public class MovementFeeler : MonoBehaviour
{
	[SerializeField] LayerMask walkMask;
	[SerializeField] LayerMask waterMask;

	float acceptableWaterHeight = 0.3f;
	float acceptableSlope = 75f;//in degrees (Previously 53)

	float feelerUp = .95f;
	float feelerOut = .5f;

	public bool
	CanWalk()
	{
		HitInfo hit = CreateRaycastToGround ();

		if (hit.successful)
		{
			if (SlopeIsAcceptable (hit) && WaterHeightIsAcceptable (hit))
			{
				return true;
			}
		}

		return false;
	}
		
	bool
	SlopeIsAcceptable(HitInfo hit)
	{
		if(GetSlopeOfPath(hit) < acceptableSlope)
		{
			return true;
		}
		return false;
	}

	float
	GetSlopeOfPath(HitInfo hit)
	{
		float changeInPathHeight = Mathf.Abs(hit.raycast.point.y - GetCurrentPositionOnGround().y);
		float angleOfSlope = Mathf.Tan (changeInPathHeight / feelerOut) * Mathf.Rad2Deg;

		Debug.DrawLine(hit.raycast.point, GetCurrentPositionOnGround());

		return Mathf.Abs(angleOfSlope);
	}

	bool
	WaterHeightIsAcceptable(HitInfo groundHit)
	{
		HitInfo waterHit = CreateRaycastToWater ();

		if (waterHit.successful == false)
		{
			return true;
		}
		else if (waterHit.successful)
		{
			if (groundHit.raycast.distance - waterHit.raycast.distance < acceptableWaterHeight)
			{
				return true;
			}
		}

		return false;
	}

	HitInfo
	CreateRaycastToGround()
	{
		HitInfo newHit = new HitInfo ();

		foreach(RaycastHit hit in Physics.RaycastAll(GetReferencePoint(), -transform.up))
		{
			if (hit.collider.tag == "Walkable" && (hit.distance < newHit.raycast.distance || newHit.successful == false))
			{
				newHit.successful = true;
				newHit.raycast = hit;
			}
		}
			
		return newHit;
	}
		
	void
	OffsetFeeler()
	{
		transform.localPosition += new Vector3(0, feelerUp, feelerOut);
	}

	Vector3
	GetCurrentPositionOnGround()
	{
		HitInfo newHit = new HitInfo ();

		foreach(RaycastHit hit in Physics.RaycastAll(transform.position, -transform.up))
		{
			if (hit.collider.tag == "Walkable" && (hit.distance < newHit.raycast.distance || newHit.successful == false))
			{
				newHit.successful = true;
				newHit.raycast = hit;
			}
		}
			
		if (newHit.successful == true)
		{
			return newHit.raycast.point;
		}
		else
		{
			return transform.position;
		}
	}

	Vector3
	GetReferencePoint()
	{
		Vector3 vec = transform.localPosition + new Vector3 (0, feelerUp, feelerOut);
		return transform.TransformPoint (vec);
	}

	HitInfo
	CreateRaycastToWater()
	{
		HitInfo newHit = new HitInfo ();

		foreach(RaycastHit hit in Physics.RaycastAll(GetReferencePoint(), -transform.up))
		{
			if (hit.collider.tag == "Water")
			{
				newHit.successful = true;
				newHit.raycast = hit;
			}
		}
			
		return newHit;
	}
}

public class HitInfo
{
	public bool successful = false;
	public RaycastHit raycast;
}