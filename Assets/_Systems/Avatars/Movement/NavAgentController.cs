﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

namespace Field.Avatars{

public class NavAgentController : MonoBehaviour
{
	NavMeshAgent navAgent {get{return transform.parent.GetComponent<NavMeshAgent> ();}}
	FieldAnimations animations {get{ return transform.parent.GetComponentInChildren<FieldAnimations>(); }}

	bool paused = false;
	bool isStoppedBuffer;

	bool turning = false;

	public void
	Activate()
	{
		navAgent.enabled = true;
	}

	public void
	MoveToPoint(Vector3 target)
	{
		if (navAgent.enabled && !paused)
		{
			navAgent.SetDestination (target);
			navAgent.speed = GetComponentInChildren<MovementSpeed> ().GetSpeed ();
			navAgent.isStopped = false;
		}
	}

	public void
	PauseMovement()
	{
		if (navAgent.enabled)
		{
			navAgent.isStopped = true;
		}
	}

	public void
	LookAt(Transform target)
	{

	}

	public IEnumerator
	AmbulateToPoint(Vector3 destination, FinishDelegate registerTargetHasReachedDestination)
	{
		MoveToPoint (destination);

		while (TargetHasReachedDestination() == false)
		{
			yield return null;
		}
		registerTargetHasReachedDestination ();
	}

	bool
	TargetHasReachedDestination()
	{
		return Vector3.Distance(transform.position, navAgent.destination) < 0.2f;
	}

	bool
	IsMoving()
	{
		if (navAgent.enabled)
		{
			return !navAgent.isStopped && !turning;
		}
		return (navAgent.velocity.x != 0 || navAgent.velocity.z != 0);
	}

	void
	Update()
	{
		if (paused)
		{
			navAgent.velocity = Vector3.zero;
		}
		if (navAgent.enabled)
		{
			EnsureStationaryWhileTurning ();
			animations.SetMoving (IsMoving());
		}
	}

	void
	EnsureStationaryWhileTurning()
	{
		if (Vector3.Dot (navAgent.desiredVelocity, transform.parent.GetComponentInChildren<Animator> ().transform.forward) < 1.4f && navAgent.isStopped == false && navAgent.desiredVelocity != Vector3.zero)
		{
			turning = true;
			navAgent.velocity = Vector3.zero;
		}
		else
		{
			turning = false;
		}
	}
		
	public void
	Pause()
	{
		paused = true;

		if (navAgent.enabled)
		{
			isStoppedBuffer = navAgent.isStopped;
			navAgent.isStopped = true;
		}
	}

	public void
	Unpause()
	{
		paused = false;

		if (navAgent.enabled)
		{
			navAgent.isStopped = isStoppedBuffer;
		}
	}
}
}