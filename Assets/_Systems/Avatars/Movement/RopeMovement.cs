﻿using UnityEngine;

namespace Field.Avatars{

public class RopeMovement : MonoBehaviour
{
	float ropeT = 0f;
	float axisWS;

	Rope rope;

	float ropeSpeed = .2f;
	const float ropeUpSpeed = .2f;
	const float ropeSlideSpeed = 1.7f;
	const float ropeDownSpeed = .6f;

	bool ropeSlide = false;

	MovementSpeed moveSpeed{ get { return GetComponent<MovementSpeed> (); } }
	FieldAnimations animations { get { return transform.parent.GetComponent<FieldAnimations> (); } }

	public void
	Move(float horizontal, float vertical)
	{
		axisWS = vertical;
	}

	void
	RopeMove()
	{
		transform.position = Vector3.Lerp(rope.GetStartPosition(), rope.GetEndPosition(), ropeT);

		PlayRopeSFX();

		ropeT -= axisWS * Time.deltaTime / rope.GetLengthOfRope() * ropeSpeed;
		ropeT = Mathf.Clamp01(ropeT);

		animations.SetRopeDirection ((int)axisWS, ropeSlide);
	}

	public void
	SetRope(Rope newRope, bool startOfRope)
	{
		rope = newRope;

		if (startOfRope)
		{
			ropeT = 0f;
		}
		else
		{
			ropeT = 1f;
		}
	}

	public void
	LeaveRope(Transform exitPlace)
	{
		transform.position = exitPlace.position;
	}

	void
	PlayRopeSFX()
	{
		if (axisWS >= 0)
		{
			GetComponent<FieldSFX>().RopeDescend(false);

			ropeSpeed = ropeUpSpeed;
		}
		if (axisWS < 0 && ropeT < .99f)
		{
			GetComponent<FieldSFX>().RopeDescend(true);

			if(moveSpeed.GetMoveType() == MoveType.Run)
			{
				ropeSpeed = ropeSlideSpeed;
			}
			else
			{
				ropeSpeed = ropeDownSpeed;
			}
		}
	}
}
}