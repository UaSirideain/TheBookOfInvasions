﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Field.Commands;

namespace Field.Avatars{

public class CompanionCommands : MonoBehaviour, ICommandable
{
	List<Command> commands = new List<Command>();

	public List<Command>
	GetCommands()
	{
		if (commands.Count == 0 || commands == null)
		{
			BuildCommands ();
		}

		return commands;
	}

	void
	BuildCommands()
	{
		commands.Clear ();

		CompanionAI comp = GetComponent<CompanionAI> ();

		Command com_nameLabel = new Command ();

		com_nameLabel.label = name;
		com_nameLabel.labelOnly = true;
		commands.Add (com_nameLabel);

		Command com_keepClose = new Command ();

		com_keepClose.label = "Stay near me";
		com_keepClose.myCommand = comp.StayNear;
		commands.Add (com_keepClose);


		Command com_keepDistance = new Command ();

		com_keepDistance.label = "Don't follow too closely";
		com_keepDistance.myCommand = comp.StayFar;
		commands.Add (com_keepDistance);


		Command com_stayHere = new Command ();

		com_stayHere.label = "Stay put for a moment";
		com_stayHere.myCommand = comp.StayHere;
		commands.Add (com_stayHere);


		Command com_doYaThing = new Command ();

		com_doYaThing.label = "Do Ya Thing";
		com_doYaThing.myCommand = comp.DoYaThing;
		commands.Add (com_doYaThing);


//		Command com_disableCollider = new Command ();
//
//		com_disableCollider.label = "You're getting in the way";
//		com_disableCollider.myCommand += COM_DisableCollider;
//		com_disableCollider.active = true;
//		GetComponent<FieldContextMenuCommands> ().commands.Add (com_disableCollider);
	}
}
}