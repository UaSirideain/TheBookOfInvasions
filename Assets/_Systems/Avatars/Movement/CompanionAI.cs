﻿using UnityEngine;
using UnityEngine.AI;

namespace Field.Avatars{

public class CompanionAI : MonoBehaviour
{
	enum FollowMode{StayNear, StayFar, StayHere, DoYaThing}
	FollowMode currentFollowMode;

	const float closeEnoughDistance = 5f; //distance at which the companion decides it doesn't need to get closer

	const float stayNearDistance = 7f;
	const float stayFarDistance = 16f;
	const float doYaThingDistance = 7f;
	float desiredDistance;

	const float runIfDistanceAbove = 15f;
	const float walkIfDistanceBelow = 12f;

	MovementSpeed moveSpeed { get { return transform.parent.GetComponentInChildren<MovementSpeed> (); } }

	Transform currentLeader;
	Transform currentNavStation;
	Transform currentlyFollowing;


	NavAgentController navAgentCont { get { return transform.parent.GetComponentInChildren<NavAgentController> (); } }
	AvatarController avatarController {get{return GameObject.FindObjectOfType<AvatarController>();}}

	public void
	SetTarget(Transform target)
	{
		currentlyFollowing = target;
	}

	public void
	StayFar()
	{
		currentFollowMode = FollowMode.StayFar;
		desiredDistance = stayFarDistance;
	}

	public void
	StayNear()
	{
		currentFollowMode = FollowMode.StayNear;
		desiredDistance = stayNearDistance;
	}

	public void
	StayHere()
	{
		currentFollowMode = FollowMode.StayHere;
		currentlyFollowing = null;
	}

	public void
	DoYaThing()
	{
		currentFollowMode = FollowMode.DoYaThing;
	}

	void
	Start()
	{
		StayNear ();
	}

	void//placeholder
	SetLeader()
	{
		currentLeader = avatarController.GetCharacterComposition().GetTransform();
	}

	void
	SetLeaderAsTarget()
	{
		currentlyFollowing = currentLeader;
	}

	void
	SetNavStationAsTarget()
	{
		currentNavStation = FindObjectOfType<NavStationFinder> ().GetNavStationClosestTo (currentLeader).transform;
		currentlyFollowing = currentNavStation;
	}

	void
	Update()
	{
		if (this.enabled)
		{
			UpdateLeader ();

			CheckProximityToTarget ();
		}
	}

	void
	CheckProximityToTarget()
	{
		if (currentFollowMode == FollowMode.StayFar || currentFollowMode == FollowMode.StayNear)
		{
			DoStayFarStayNear ();
		}
		else if (currentFollowMode == FollowMode.DoYaThing)
		{
			DoDoYaThing ();
		}
		else if (currentFollowMode == FollowMode.StayHere)
		{
			DoStayHere ();
		}
	}

	void
	DoStayHere()
	{
		navAgentCont.PauseMovement ();
	}

	void
	DoDoYaThing()
	{
		UpdateTargetNavStation ();

		if (CalculateDistanceFromTarget () < 0.5f)
		{
			navAgentCont.PauseMovement ();
		}
		else
		{
			UpdateMovementSpeed ();
			navAgentCont.MoveToPoint (currentlyFollowing.position);
		}
	}

	void
	UpdateLeader()
	{
		if (currentLeader == null)
		{
			SetLeader ();
		}
	}

	void
	UpdateTargetNavStation()
	{
		Transform closestNavPoint = FindObjectOfType<NavStationFinder> ().GetNavStationClosestTo (currentLeader).transform;

		if (currentNavStation == null)
		{
				currentNavStation = closestNavPoint;
		}
		if (currentNavStation == closestNavPoint)
		{
			//do nothing
		}
		else if (CalculatePathDistance (currentNavStation.position, currentLeader.position) < doYaThingDistance)
		{
			//do nothing
		}
		else
		{
			currentNavStation = closestNavPoint;
		}
	}

	void
	DoStayFarStayNear()
	{
		SetLeaderAsTarget ();

		if (CalculateDistanceFromTarget () < closeEnoughDistance)
		{
			navAgentCont.PauseMovement ();
		}
		else if (CalculateDistanceFromTarget () >= desiredDistance)
		{
			UpdateMovementSpeed ();
			navAgentCont.MoveToPoint (currentlyFollowing.position);
		}
	}

	void
	UpdateMovementSpeed()
	{
		if (CalculateDistanceFromTarget() >= runIfDistanceAbove)
		{
			moveSpeed.SetRunning ();
		}
		else if (CalculateDistanceFromTarget() <= walkIfDistanceBelow)
		{
			moveSpeed.SetWalking ();
		}
	}
		
	float
	CalculateDistanceFromTarget()
	{
		return CalculatePathDistance(transform.position, currentlyFollowing.position);
	}

	float
	CalculatePathDistance(Vector3 pos1, Vector3 pos2)
	{
		NavMeshPath path = new NavMeshPath ();
		NavMesh.CalculatePath (pos1, pos2, NavMesh.AllAreas, path);

		float distance = 0f;
		Vector3 lastPoint = pos1;

		foreach (Vector3 corner in path.corners)
		{
			distance += Vector3.Distance (corner, lastPoint);
			lastPoint = corner;
		}

		return distance;
	}
}
}