﻿using UnityEngine;

namespace Field.Avatars{

public class GroundMovement : MonoBehaviour, IStateChanger
{
	CharacterController controller { get { return transform.parent.GetComponent<CharacterController>(); } }
	MovementSpeed moveSpeed { get { return GetComponent<MovementSpeed>(); } }
	OrientationOffset offset { get { return GetComponent<OrientationOffset> (); } }
	AvatarGuideLink guideLink { get { return GetComponent<AvatarGuideLink>(); } }
	FieldAnimations animations { get { return transform.parent.GetComponentInChildren<FieldAnimations> (); } }

	[SerializeField] MovementFeeler feeler;

	float lastWS = 0f;
	float lastAD = 0f;

	[SerializeField] ActiveStates stateMachine;

	public void
	SetStateMachine(ActiveStates newStateMachine)
	{
		stateMachine = newStateMachine;
	}

    public void
    Move(float axisAD, float axisWS)
	{
		Vector3 movement = offset.ApplyOffsetTo (new Vector3(axisAD, 0, axisWS));

		if (axisAD != lastAD || axisWS != lastWS)
		{
			offset.UpdateOffset ();
		}

		lastWS = axisWS;
		lastAD = axisAD;

        if (movement.x != 0 || movement.z != 0)
        {
            movement = guideLink.ConfineMovementToDirection(movement);
            controller.transform.rotation = Quaternion.LookRotation(movement);
        }

        movement = new Vector3(movement.x, 0, movement.z);

		if (feeler.CanWalk ())
		{
			controller.Move (movement * moveSpeed.GetSpeed () * Time.deltaTime);
		}
		else
		{
			movement = Vector3.zero;
		}

		animations.SetMoving (movement != Vector3.zero);

		UpdateState (movement);
    }

	void
	UpdateState(Vector3 movement)
	{
		if (movement.x == 0 && movement.z == 0)
		{
			stateMachine.RemoveState (InputState.Running);
			stateMachine.RemoveState (InputState.Walking);
		}
		else if (moveSpeed.GetMoveType () == MoveType.Run)
		{
			stateMachine.WriteState (InputState.Running);
			stateMachine.RemoveState (InputState.Walking);
		}
		else//if(moveSpeed.GetMoveType() == MoveType.Walk)
		{
			stateMachine.WriteState (InputState.Walking);
			stateMachine.RemoveState (InputState.Running);
		}
	}
}
}