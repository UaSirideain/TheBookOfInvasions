﻿using System.Collections.Generic;
using UnityEngine;

public delegate void InteractionDelegate(InteractionTrigger trigger);

public class Interactions : MonoBehaviour
{
	[SerializeField]List<InteractionTrigger> triggers = new List<InteractionTrigger> ();
	List<InventoryInteractionTrigger> inventoryTriggers = new List<InventoryInteractionTrigger>();

	[SerializeField] GameObject interactionIcon;
	[SerializeField] InteractionIconUI interactionIconUI;
	InteractionUI interactionUI { get { return FindObjectOfType<InteractionUI>(); } }

	bool interactionsBlocked = false;

	public void
	RegisterInteraction(InteractionTrigger trigger)
	{
//		if (triggers.Contains (trigger) == false) //there are rare issues where a trigger would otherwise be added twice
//		{
			triggers.Add (trigger);
//		}

		//RemoveInactiveTriggers ();

		RenderInteractionIcon ();
	}

	public void
	UnregisterInteraction(InteractionTrigger trigger)
	{
		triggers.Remove (trigger);

		//RemoveInactiveTriggers ();

		RenderInteractionIcon ();
		RenderUI ();
	}

	public void
	RegisterInventoryInteraction(InventoryInteractionTrigger trigger)
	{
		inventoryTriggers.Add (trigger);

		RenderInteractionIcon ();
	}

	public void
	UnregisterInventoryInteraction(InventoryInteractionTrigger trigger)
	{
		inventoryTriggers.Remove (trigger);

		RenderInteractionIcon ();

		RenderUI ();
	}
		
	public void
	Interact()
	{
		if (!interactionsBlocked)
		{
			if (triggers.Count == 1)
			{
				InteractWith (triggers [0]);
			}
			else if (triggers.Count > 1)
			{
				interactionUI.Activate (triggers, InteractWith);
			}
			else if (inventoryTriggers.Count > 0)
			{
				//Make the character say something like "Maybe I need something to use this."
				print("You need an item to interact with this.");
			}
		}
	}

	public bool
	UseItem(InventoryEntry entry)
	{
		if (!interactionsBlocked)
		{
			if (inventoryTriggers.Count > 0)
			{
				inventoryTriggers [0].Trigger (GetComponentInParent<AvatarFacade> (), entry);
                print("This isn't the correct entry");
				return true;
			}
		}
		return false;
	}

	public void
	BlockInteractions()
	{
		interactionsBlocked = true;
		RenderInteractionIcon ();
	}

	public void
	UnblockInteractions()
	{
        RemoveInactiveTriggers();
		interactionsBlocked = false;
		RenderInteractionIcon ();
	}

    private void
    RemoveInactiveTriggers()
    {
		if (triggers.Count > 0)
		{
			for (int triggerIndex = Mathf.Clamp (triggers.Count - 1, 0, 1000); triggerIndex >= 0; triggerIndex--)
			{
				if (!triggers [triggerIndex].isActiveAndEnabled)
				{
					triggers.Remove (triggers [triggerIndex]);
				}
			}
		}
    }

	void
	InteractWith(InteractionTrigger trigger)
	{
		trigger.Trigger(GetComponentInParent<AvatarFacade>());
	}

	void
	RenderInteractionIcon()
	{
		bool hasInventoryInteractions = false;

		foreach (InventoryInteractionTrigger trigger in inventoryTriggers)
		{
			if (trigger.displayInteractionIcon)
			{
				hasInventoryInteractions = true;
			}
		}

		if (!interactionsBlocked)
		{
			if (triggers.Count > 0)
			{
				interactionIconUI.RenderInteractionIcon ();
			}
			else if (hasInventoryInteractions)
			{
				interactionIconUI.RenderInventoryInteractionIcon ();
			}
			else
			{
				interactionIconUI.UnRender ();
			}
		}
		else
		{
			interactionIconUI.UnRender ();
		}
	}

	void
	RenderUI()
	{
		if (inventoryTriggers.Count == 0 && triggers.Count == 0)
		{
			interactionUI.Deactivate ();
		}
	}
}