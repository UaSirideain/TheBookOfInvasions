﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractions : MonoBehaviour, IInputtable
{
	[SerializeField] Interactions interactions;

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.Interact, new List<InputState> (){InputState.Battle}, Interaction_Input);
	}

	void
	Interaction_Input()
	{
		interactions.Interact ();
	}
}