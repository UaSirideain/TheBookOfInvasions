﻿using UnityEngine;

public class LineOfInteraction : MonoBehaviour
{
	[SerializeField] Interactions interactions;

    public void
    RegisterInteraction(InteractionTrigger trigger)
    {
		interactions.RegisterInteraction(trigger);
    }

    public void
    UnregisterInteraction(InteractionTrigger trigger)
    {
		interactions.UnregisterInteraction(trigger);
    }

	public void
	RegisterInteraction(InventoryInteractionTrigger trigger)
	{
		interactions.RegisterInventoryInteraction (trigger);
	}

	public void
	UnregisterInteraction(InventoryInteractionTrigger trigger)
	{
		interactions.UnregisterInventoryInteraction (trigger);
	}
}