﻿using UnityEngine;

public class Pooler : MonoBehaviour
{
    [SerializeField] private PoolTag poolType;
    
    public GameObject
    InstanceWithPooler(AbilityInfo ability)
    {
        return AbilityPooler.instance.Create(poolType, ability.instancePosition.position, Quaternion.identity);
    }
}
