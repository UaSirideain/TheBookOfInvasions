﻿using System.Collections;
using UnityEngine;

namespace Field.Abilities{

public class FindEnchanter : MonoBehaviour
{
    private float heightAdjustment = 2f;

    public IEnumerator
    Run(/*FieldAbility enchantment, */Transform enchantmentSpawn, float heightOffset)
    {
        Vector3 enchantmentSource = enchantmentSpawn.position;
        Vector3 spawnPosition = transform.position;
        Vector3 destination = new Vector3(transform.position.x, transform.position.y + heightAdjustment, transform.position.z);

        float distance = Vector3.Distance(transform.position, destination);
        int metresPerSecond = 10;
        float duration = distance / metresPerSecond;

        float percentage = 0f;
        while (percentage < 1f)
        {
            Vector3 direction = (enchantmentSource - transform.position);
            Debug.DrawLine(enchantmentSource, transform.position, Color.red);

            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, direction, out hitInfo, 18))
            {
                Vector3 newPosition;
                if (hitInfo.transform.gameObject.layer == 14)
                {
                    newPosition = Vector3.Lerp(spawnPosition, destination, percentage);
                    percentage += Time.deltaTime / duration;
                }
                else
                {
                    destination = newPosition = new Vector3(transform.position.x, transform.position.y + heightOffset, transform.position.z);
                }
                transform.position = newPosition;
                if (transform.position.Equals(destination))
                {
                    break;
                }
            }
            else
            {
                break;
            }
            yield return null;
        }
        //StartCoroutine(enchantment.LifespanCountdown());
    }
}
}