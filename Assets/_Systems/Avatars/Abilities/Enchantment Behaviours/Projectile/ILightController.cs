﻿using UnityEngine;

public abstract class ILightController : MonoBehaviour
{
    public abstract void Activate();
    public abstract void Deactivate();
}
