﻿using System.Collections;
using UnityEngine;

namespace Field.Enchantments{

public class SprayProjectile : MonoBehaviour, IAbilityBehaviour
{
    private AudioSource audioSource { get { return GetComponent<AudioSource>(); } }
    [SerializeField] ParticleSystem primaryParticles;
    [SerializeField] ParticleSystem secondaryParticles;
    [SerializeField] float audioFadeOutInSeconds = 1f; 

    public void
    Run(AbilityInfo ability)
    {
        primaryParticles.Play();
        if (secondaryParticles)
        {
            secondaryParticles.Play();
        }
        StartCoroutine(AudioFadeIn());
        audioSource.Play();
    }

    public void
    Disable(AbilityInfo ability)
    {
        StartCoroutine(DestroyParticles());
    }

    private IEnumerator
    DestroyParticles()
    {
        primaryParticles.Stop();
        if (secondaryParticles)
        {
            secondaryParticles.Stop();
        }
        yield return StartCoroutine(AudioFadeOut());
        audioSource.Stop();
        Destroy(gameObject);
    }

    private IEnumerator
    AudioFadeOut()
    {
        float t = 0f;
        float startVolume = audioSource.volume;
        while (t < 1.2f)
        {
            audioSource.volume = Mathf.Lerp(startVolume, 0f, t);
            t += Time.deltaTime / audioFadeOutInSeconds;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator
    AudioFadeIn()
    {
        float t = 0f;
        float startVolume = audioSource.volume;
        while (t < 1.2f)
        {
            audioSource.volume = Mathf.Lerp(0f, startVolume, t);
            t += Time.deltaTime / audioFadeOutInSeconds;
            yield return null;
        }
        yield return null;
    }
}
}