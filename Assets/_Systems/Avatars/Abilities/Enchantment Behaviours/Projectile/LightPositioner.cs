﻿using System.Collections;
using UnityEngine;

public class LightPositioner : ILightController
{
    [SerializeField] private GameObject objectToFloatAbove;
    [SerializeField] private Vector3 floatOffset = new Vector3(0, 0.2f, 0);

    public override void 
    Activate()
    {
        StartCoroutine(Float());
    }

    public override void Deactivate()
    {
        throw new System.NotImplementedException();
    }

    private IEnumerator
    Float()
    {
        while (gameObject.activeSelf)
        {
            transform.position = objectToFloatAbove.transform.position + floatOffset;
            transform.rotation = objectToFloatAbove.transform.rotation;
            yield return null;
        }
        yield return null;
    }
}
