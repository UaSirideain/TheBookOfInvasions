﻿using UnityEngine;

namespace Field.Enchantments{

public class SelfProjectile : MonoBehaviour, IAbilityBehaviour
{
    public void 
    Disable(AbilityInfo ability)
    {
        Destroy(gameObject.transform.parent);
    }

    public void
    Run(AbilityInfo ability)
    {
        gameObject.transform.parent.SetParent(ability.instancePosition, true);
    }
}
}