﻿using UnityEngine;

namespace Field.Enchantments{

public class ThrowProjectile : MonoBehaviour, IAbilityBehaviour
{
    [SerializeField] private float force = 10f;
    [SerializeField] private float throwAngle = 60f;

    public void 
    Disable(AbilityInfo ability)
    {
        throw new System.NotImplementedException();
    }

    public void
    Run(AbilityInfo info)
    {
        ThrowInDirection(info.creator, info.mouseClick);
    }

    private void
    ThrowInDirection(AvatarFacade avatar, Vector3 targetObject)
    {
        Transform source = avatar.GetTransform().FindDeepChild("RightHand");
        transform.position = source.position + source.right;    //TODO: Remove "+ source.right" after animation is created
        transform.rotation = source.rotation;

        Rigidbody rb = GetComponent<Rigidbody>();

        rb.AddForce(rb.ThrowArc(targetObject, throwAngle) * rb.mass, ForceMode.Impulse);
        rb.AddTorque(rb.ThrowArc(transform.position - source.position, throwAngle) * force, ForceMode.Impulse);
    }
}
}