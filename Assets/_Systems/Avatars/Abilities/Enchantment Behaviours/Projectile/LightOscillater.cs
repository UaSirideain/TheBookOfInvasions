﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(PauseCompanion))]
public class LightOscillater : ILightController, IPausable
{
    Light objectLight { get { return GetComponent<Light>(); } }

    [SerializeField] bool playOnSceneStart = false;

    [SerializeField] float max = 6f;
    [SerializeField] float min = 3f;
    [SerializeField] float oscillationSpeed = 0.5f;

    private bool dim = false;

    public override void
    Activate()
    {
        StartCoroutine(Oscillate());
    }

    public override void 
    Deactivate()
    {
        StartCoroutine(EndOscillate());
    }

    int lerpTicket = 0;

    private IEnumerator
    Oscillate()
    {
        lerpTicket++;
        int currentTicket = lerpTicket;
        while (objectLight.enabled && currentTicket == lerpTicket)
        {
            float t = 0f;
            float currentIntensity = objectLight.intensity;
            while (t < 1.2f)
            {
                if (!dim)
                {                    
                    Brighten(t, currentIntensity);
                }
                else
                {
                    Dim(t, currentIntensity);
                }
                t += Time.deltaTime / oscillationSpeed * pauseFactor;
                yield return null;
            }
            dim = !dim;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator
    EndOscillate()
    {
        float startIntensity = objectLight.intensity;
        float t = 0f;
        while (t < 1.2f)
        {
            objectLight.intensity = Mathf.Lerp(startIntensity, 0, t);
            t += Time.deltaTime / oscillationSpeed;
            yield return null;
        }
        yield return null;
    }

    private void
    Brighten(float t, float currentIntensity)
    {
        objectLight.intensity = Mathf.Lerp(currentIntensity, max, t);
    }

    private void
    Dim(float t, float currentIntensity)
    {
        objectLight.intensity = Mathf.Lerp(currentIntensity, min, t);
    }

    private void
    Awake()
    {
        if (playOnSceneStart)
        {
            Activate();
        }
    }

	float pauseFactor = 1f;

	public void
	Pause()
	{
		pauseFactor = 0f;
	}

	public void
	Unpause()
	{
		pauseFactor = 1f;
	}
}
