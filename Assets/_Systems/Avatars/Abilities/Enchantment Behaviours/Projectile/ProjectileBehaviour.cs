﻿using UnityEngine;

namespace Field.Enchantments{

public interface ProjectileBehaviour
{
    void PerformCast(AvatarFacade avatar, Transform targetPosition);
}
}