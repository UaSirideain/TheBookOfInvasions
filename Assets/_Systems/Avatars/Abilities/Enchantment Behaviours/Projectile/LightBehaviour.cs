﻿using UnityEngine;

public class LightBehaviour : MonoBehaviour, IAbilityBehaviour
{
    [SerializeField] GameObject lightObject;

    public void 
    Disable(AbilityInfo ability)
    {
        foreach (ILightController controller in lightObject.GetComponents<ILightController>())
        {
            controller.Deactivate();
        }
    }

    public void
    Run(AbilityInfo ability)
    {
        foreach (ILightController controller in lightObject.GetComponents<ILightController>())
        {
            controller.Activate();
        }
    }
}
