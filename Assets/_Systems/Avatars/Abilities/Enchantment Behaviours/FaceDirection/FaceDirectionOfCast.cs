﻿using UnityEngine;

public class FaceDirectionOfCast : MonoBehaviour, IAbilityBehaviour
{
    public void 
    Disable(AbilityInfo ability)
    {
        //throw new System.NotImplementedException();
    }

    public void 
    Run(AbilityInfo ability)
    {
        var orientation = Quaternion.LookRotation(ability.mouseClick - ability.creator.transform.position);
        orientation.x = 0f;
        orientation.z = 0f;
        ability.creator.transform.rotation = orientation;
    }
}
