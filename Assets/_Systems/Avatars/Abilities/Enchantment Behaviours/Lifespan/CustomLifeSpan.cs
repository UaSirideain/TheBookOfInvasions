﻿using System.Collections;
using UnityEngine;

namespace Field.Enchantments{

public class CustomLifeSpan : MonoBehaviour, IAbilityBehaviour
{
    [SerializeField] private float lifeSpan;

    public void Disable(AbilityInfo ability)
    {
        throw new System.NotImplementedException();
    }

    public void
    Run(AbilityInfo ability)
    {
        StartCoroutine(Countdown());
    }

    private IEnumerator
    Countdown()
    {
        float startTime = Time.time;
        while ((Time.time - startTime) < lifeSpan)
        {
            yield return null;
        }
        Destroy(gameObject);
    }
}
}