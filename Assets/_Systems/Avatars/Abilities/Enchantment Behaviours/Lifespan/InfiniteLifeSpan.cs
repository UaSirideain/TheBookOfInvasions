﻿using UnityEngine;

namespace Field.Enchantments{

public class InfiniteLifeSpan : MonoBehaviour, IAbilityBehaviour
{
    public void Disable(AbilityInfo ability)
    {
        //throw new System.NotImplementedException();
    }

    public void
    Run(AbilityInfo ability)
    {
        //Does not expire
    }
}
}