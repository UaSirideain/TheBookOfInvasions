﻿using Field.Abilities;
using UnityEngine;

namespace Field.Abilities{

public class BulrushEffect : FieldAbilityEffect
{
    private GameObject bulrush;
    [SerializeField] private GameObject bulrushPrefab;

    private bool isEquipped = false;

    private void OnValidate()
    {
        isEquipped = false;
    }

    public override void 
    Execute(AbilityInfo ability)
    {
        if (!isEquipped)
        {
            isEquipped = true;
            ability.instancePosition = ability.creator.GetTransform().FindDeepChild("RightHand");
            bulrush = CreateInstance(ability, bulrushPrefab);

            bulrush.transform.SetParent(ability.instancePosition, true);
        }
        else
        {
            isEquipped = false;
            Disable(ability);
        }
    }

    public override void 
    Disable(AbilityInfo ability)
    {
        Destroy(bulrush);
    }
}
}