﻿using System.Collections.Generic;
using UnityEngine;

namespace Field.Abilities{

[RequireComponent(typeof(QuickItems))]
[RequireComponent(typeof(AbilityController))]
public class PlayerAbilities : MonoBehaviour, IInputtable, IStateChanger
{
    QuickItems quickItems { get { return GetComponent<QuickItems>(); } }
    AbilityController abilityController { get { return GetComponent<AbilityController>(); } }
    AvatarFacade avatarFacade { get { return transform.parent.GetComponent<AvatarFacade>(); } }//TODO2: This probably should be sending an avatar facade. The Facade is for use only by scripts outside the avatar sub system
	QuickItemsUIController quickItemsUI { get { return FindObjectOfType<QuickItemsUIController>(); } }

	ActiveStates stateMachine;

	private InventoryEntry leftHand;
	private InventoryEntry rightHand;

    public QuickItems
    GetAvatarHotbar()
    {
        return quickItems;
    }

	public void
	SetStateMachine(ActiveStates newStateMachine)
	{
		stateMachine = newStateMachine;
	}

    public void 
    SubscribeTo(InputValidator newInput)
    {
		newInput.SubscribeToStringInput(InputMode.Down, InputButton.LeftMouse, new List<InputState>(){InputState.PlayingMusic, InputState.PausingUI, InputState.FullscreenUI, InputState.UI, InputState.Battle}, Left);
		newInput.SubscribeToStringInput(InputMode.Down, InputButton.RightMouse, new List<InputState>(){InputState.PlayingMusic, InputState.PausingUI, InputState.FullscreenUI, InputState.UI, InputState.Battle}, Right);
		newInput.SubscribeToStringInput(InputMode.Up, InputButton.RightMouse, new List<InputState>(){InputState.PlayingMusic, InputState.PausingUI, InputState.FullscreenUI, InputState.UI, InputState.Battle}, RightUp);
		newInput.SubscribeToNumericInput(InputMode.Down, new List<InputState>(){InputState.PlayingMusic, InputState.PausingUI, InputState.FullscreenUI, InputState.UI, InputState.Battle}, NumericInputDown);
		newInput.SubscribeToNumericInput(InputMode.Up, new List<InputState>(){InputState.PlayingMusic, InputState.PausingUI, InputState.FullscreenUI, InputState.UI, InputState.Battle}, NumericInputUp);
    }

	public void
	EnableAbilitiesState()
	{
		stateMachine.WriteState (InputState.ReadyingAbilities);
		quickItemsUI.SetIndicesOfReadiedAbilities (quickItems.GetSlots (), new List<InventoryEntry> (){ leftHand, rightHand });
		//turn ui on
	}

	public void
	DisableAbilitiesState()
	{
		stateMachine.RemoveState (InputState.ReadyingAbilities);
		//turn ui off
	}

    private void
    Left()
    {
		abilityController.Use(leftHand, avatarFacade);
    }

    private void
    Right()
    {
		abilityController.Cast(rightHand, avatarFacade);
    }

    private void
    RightUp()
    {
		abilityController.EndCast(rightHand, avatarFacade);
    }
    
    private void
    NumericInputDown(string numeric)
    {
		int arrayIndex;
		if (numeric.NumericStringToArrayIndex (out arrayIndex))
		{
			ReadyAbility (quickItems.GetNonAutoUseSlotForIndex (arrayIndex));
			quickItemsUI.SetIndicesOfReadiedAbilities (quickItems.GetSlots (), new List<InventoryEntry> (){ leftHand, rightHand });
			abilityController.AutoCastDown (quickItems.GetAutoUseSlotForIndex (arrayIndex), avatarFacade);
		}
	}

    private void
    NumericInputUp(string numeric)
    {
		int arrayIndex;
		if (numeric.NumericStringToArrayIndex (out arrayIndex))
		{
			abilityController.AutoCastUp (quickItems.GetAutoUseSlotForIndex (arrayIndex), avatarFacade);
		}
    }

	void
	ReadyAbility(InventoryEntry ability)
	{
		if (ability != null)
		{
			if (ability.GetAbility ().MustBeReadied())
			{
				if (ability.GetEntryType () == InventoryCategory.Skill)
				{
					rightHand = ability;
				}
				else//if (ability.GetEntryType () == InventoryCategory.Item)
				{
					leftHand = ability;
				}
			}
		}
	}
}
}