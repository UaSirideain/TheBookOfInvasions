﻿namespace Field.Abilities{

public abstract class FieldItemEffect : FieldAbilityEffect
{
	public override abstract void
	Execute(AbilityInfo ability);
}
}