﻿using GUIManagement;
using UnityEngine;

namespace Field.Abilities{

public class EliminatorEffect : FieldAbilityEffect
{
	[SerializeField] EliminatorUI eliminatorUIPrefab;
	[SerializeField] EliminatorUI ui;
	[SerializeField] bool inUse = false;

	void
	OnValidate()
	{
		inUse = false;
		ui = null;
	}

	public override void
	Execute(AbilityInfo ability)
	{
		if (inUse)  //TODO: Switch to if (!inUse) to prioritise the most likely scenario
		{
			//do nothing
		}
		else
		{
			inUse = true;
			if (ui != null)
			{
				ui.gameObject.SetActive (true);
			}
			else
			{
				ui = Instantiate (eliminatorUIPrefab);
			}

			FindObjectOfType<GUIController>().InductNewUI(ui.GetComponent<IGUI>());
			ui.Activate (this);
		}
	}

	public override void
	Disable(AbilityInfo ability)
	{
		inUse = false;
		ui.gameObject.SetActive (false);
	}
}
}