﻿public interface IAbilityBehaviour
{
    void Run(AbilityInfo ability);
    void Disable(AbilityInfo ability);
}
