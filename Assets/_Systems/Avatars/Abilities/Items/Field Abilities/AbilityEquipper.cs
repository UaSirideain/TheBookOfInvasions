﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// I wonder if this script is warranted. It seems to be nothing more than a very large container for 2 variables, neither of which are ever accessed by more than one script.
/// </summary>
//
//public class AbilityEquipper : MonoBehaviour
//{
//	private InventoryEntry leftHand;
//	private InventoryEntry rightHand;
//
//    public void
//	ReadyAbility(InventoryEntry ability)
//    {
//        if (ability != null)
//        {
//			if (ability.GetAbility ().GetFieldAbility ().Equippable)
//			{
//				if (ability.GetEntryType () == InventoryCategory.Skill)
//				{
//					rightHand = ability;
//				}
//				else//if (ability.GetEntryType () == InventoryCategory.Item)
//				{
//					leftHand = ability;
//				}
//			}
//        }
//    }
//
//	public InventoryEntry
//    GetLeftHand()
//    {
//        return leftHand;
//    }
//
//	public InventoryEntry
//    GetRightHand()
//    {
//        return rightHand;
//    }
//
//    public List<InventoryEntry>
//    GetEquippedAbilities()
//    {
//		return new List<InventoryEntry>(){leftHand, rightHand};		//seems to do the job of the function below.
//		//return EquipAbilities(leftHand, rightHand);
//    }
//		
//
////	private InventoryEntry[]
////	EquipAbilities(InventoryEntry item, InventoryEntry enchantment)
////    {
////		InventoryEntry[] abilities = new InventoryEntry[2];
////        if (item != null && item != null)
////        {
////            abilities[0] = item;
////        }
////        if (enchantment != null && enchantment != null)
////        {
////            abilities[1] = enchantment;
////        }
////        return abilities;
////    }
//}