﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Field.Abilities{

public abstract class FieldAbilityEffect : MonoBehaviour
{
    protected AbilityInstancer abilityInstancer = new AbilityInstancer();
    List<IAbilityBehaviour> behaviours = new List<IAbilityBehaviour>(); 

    public abstract void 
    Execute(AbilityInfo ability);

    public abstract void
    Disable(AbilityInfo ability);

    protected void
    RunBehaviours(AbilityInfo ability)
    {
        foreach (IAbilityBehaviour behaviour in behaviours)
        {
            behaviour.Run(ability);
        }
    }

    protected virtual GameObject
    CreateInstance(AbilityInfo ability, GameObject prefabToInstance)
    {
        return abilityInstancer.CreateInstance(prefabToInstance, ability.instancePosition);
    }

    protected void
    SetBehaviours(IAbilityBehaviour[] behaviours)
    {
        if (behaviours.Length > 0)
        {
            this.behaviours.Clear();
            this.behaviours.AddRange(behaviours.ToList());
        }
    }
}
}