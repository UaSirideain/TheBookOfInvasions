﻿using UnityEngine;

namespace Field.Abilities{

public enum SkillMovementType { Projectile, Spray, Instance, Self }

public class FieldAbility : MonoBehaviour
{
    [SerializeField] private SkillMovementType abilitySubType = SkillMovementType.Instance;

    [SerializeField] private FieldAbilityEffect effect;

    private AbilityInfo abilityInfo;
	    
    public void
    Execute(AvatarFacade avatar)
    {
        Vector3 mouseHit;
        if (effect && RaycastMouseHitPoint(out mouseHit))
        {
            effect.Execute(CreateAbilityInfo(avatar, mouseHit));
        }
    }

    public void
    Disable(AvatarFacade avatar)
    {
        if (effect)
        {
            effect.Disable(CreateAbilityInfo(avatar, Vector3.zero));
        }
    }

    private AbilityInfo
    CreateAbilityInfo(AvatarFacade avatar, Vector3 mouseHit)
    {
        abilityInfo = new AbilityInfo();
        abilityInfo.creator = avatar;
        abilityInfo.mouseClick = mouseHit;
        return abilityInfo;
    }

    private bool
    RaycastMouseHitPoint(out Vector3 mouseHit)
    {
		Ray ray = FindObjectOfType<CameraManager>().GetActiveCamera().ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            mouseHit = hitInfo.point;
            return true;
        }
        else
        {
            mouseHit = Vector3.zero;
            return false;
        }
    }

    public SkillMovementType
    GetAbilitySubType()
    {
        return abilitySubType;
    }
}
}