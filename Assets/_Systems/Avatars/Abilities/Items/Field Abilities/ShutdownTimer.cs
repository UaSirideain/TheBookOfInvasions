﻿using System.Collections;
using UnityEngine;

//TODO: Not sure what system this is a part of
public class ShutdownTimer
{
    private readonly float idleTime;
    private readonly ICountDown renderer;
    public ShutdownTimer(ICountDown renderer, float idleTime)
    {
        this.idleTime = idleTime;
        this.renderer = renderer;
    }

    private float endTime;
    private int currentLerpTicket = 0;

    public IEnumerator
    RunCountdown()
    {
        currentLerpTicket++;
        int activeLerpTicket = currentLerpTicket;

        endTime = Time.time + idleTime;
        while (Time.time < endTime)
        {
            yield return null;
        }

        if (activeLerpTicket.Equals(currentLerpTicket))
        {
            renderer.TurnOffUI();
        }
    }
}