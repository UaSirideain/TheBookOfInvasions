﻿using Notifications;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Abilities{

public class QuickItems : MonoBehaviour
{
	[SerializeField] List<InventoryEntry> slots = new List<InventoryEntry>();
    QuickItemsUIController uiController { get { return FindObjectOfType<QuickItemsUIController>(); } }
    NotificationUI Notification { get { return FindObjectOfType<NotificationUI>(); } }

    public List<InventoryEntry>
    GetSlots()
    {
        return slots;
    }

    public void
    UnassignHotbarButton(InventoryEntry abilityToRemove)
    {
		foreach (InventoryEntry slot in slots)
        {
            if (slot == abilityToRemove)
            {
                slots[slots.IndexOf(slot)] = null;
            }
        }
        uiController.AssignTooltip(slots);
    }

    public void
	AssignHotbarButton(InventoryEntry newAbility, int slot)
    {
		foreach (InventoryEntry currentSlot in slots)
        {
            if (currentSlot == newAbility)
            {
                slots[slots.IndexOf(currentSlot)] = null;
            }
        }
        slots[slot] = newAbility;
		uiController.AssignTooltip(slots);
    }

	public InventoryEntry
    GetNonAutoUseSlotForIndex(int index)
    {
		if (slots [index].GetAbility() != null)
		{
			if (slots [index].GetAbility ())
			{
				return (slots [index]);
			}
		}
        return null;
    }

    public InventoryEntry
    GetAutoUseSlotForIndex(int index)
    {
		if (slots [index].GetAbility() != null)
		{
			if (slots [index].GetAbility ().GetFieldAbility () && !slots[index].GetAbility().MustBeReadied())
			{
				return slots [index];
			}
		}
        return null;
    }

    private bool
    IsAssigned(int index)
    {
        if (slots[index] == null)
        {
            NotifyIfSlotsAreNull();
            return false;
        }
        return true;
    }

    private void
    NotifyIfSlotsAreNull()
    {
		foreach (InventoryEntry slot in slots)
        {
            if (slot != null)
            {
                return;
            }
        }

        Notification.DisplayNotification("No ability slots are assigned - press backspace to assign abilities.");
    }
}
}