﻿using UnityEngine;

public class AbilityInfo
{
    public AvatarFacade creator;
    public Vector3 mouseClick;
    public Transform target;
    public Transform instancePosition;
}