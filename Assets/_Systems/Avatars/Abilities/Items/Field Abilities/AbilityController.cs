﻿using System.Collections;
using UnityEngine;
using Field.Avatars;

public enum UseResult{UsedOnTrigger, UsedForOwnSake, NotUsed}

namespace Field.Abilities{

public class AbilityController : MonoBehaviour
{
    bool inCooldown = false;
    WaitForSeconds cooldown = new WaitForSeconds(0.5f);

    public void
    Use(InventoryEntry equippedItem, AvatarFacade avatar)
    {
        if (equippedItem != null)
        {
            UseAbility(equippedItem, avatar);
        }
    }

    public void
    AutoCastDown(InventoryEntry slotAbility, AvatarFacade avatarComposition)
    {
        if (slotAbility != null)
        {
			if(slotAbility.GetAbility().GetUICategory() == InventoryCategory.Skill)
            {
                UseAbility(slotAbility, avatarComposition);
				if (slotAbility.GetAbility().GetFieldAbility().GetAbilitySubType() != SkillMovementType.Spray)
                {
                    StartCoroutine(CoolingDown());
                }
            }
        }
    }

    public void
	AutoCastUp(InventoryEntry slotAbility, AvatarFacade avatarComposition)
    {
        if (slotAbility != null)
        {
			if(slotAbility.GetAbility().GetUICategory() == InventoryCategory.Skill)
            {
                DisableAbility(slotAbility, avatarComposition);
				if (slotAbility.GetAbility().GetFieldAbility().GetAbilitySubType() == SkillMovementType.Spray)
                {
                    StartCoroutine(CoolingDown());
                }
            }
        }
    }

    public void
	Cast(InventoryEntry equippedEnchantment, AvatarFacade avatar)
    {
        if (equippedEnchantment != null)
        {
            UseAbility(equippedEnchantment, avatar);
			if (equippedEnchantment.GetAbility().GetFieldAbility().GetAbilitySubType() != SkillMovementType.Spray)
            {
                StartCoroutine(CoolingDown());
            }
        }
    }

    public void
	EndCast(InventoryEntry equippedEnchantment, AvatarFacade avatarComposition)
    {
		if (equippedEnchantment != null && equippedEnchantment.GetAbility().GetFieldAbility().GetAbilitySubType() == SkillMovementType.Spray)
        {
            DisableAbility(equippedEnchantment, avatarComposition);
        }
    }

    IEnumerator
    CoolingDown()
    {
        inCooldown = true;
        yield return cooldown;
        inCooldown = false;
    }

	private void
	UseAbility(InventoryEntry entry, AvatarFacade avatar)
	{
		if (!inCooldown)
		{
			if (avatar.GetComponent<AvatarMediator>().GetInteractions().UseItem (entry) == false)
			{
				entry.GetAbility ().GetFieldAbility ().Execute (avatar);
			}
		}
	}

	public bool
	UseAbilityFromInventory(InventoryEntry ability, AvatarFacade avatar)
	{
		if (!inCooldown)
		{
			bool usedOnTrigger = avatar.GetComponent<AvatarMediator> ().GetInteractions ().UseItem (ability);

			if (usedOnTrigger)
			{
				return true;
			}
			else if(ability.GetAbility().UsableFromInventory())
			{
				ability.GetAbility ().GetFieldAbility ().Execute (avatar);
				return true;
			}
		}
		return false;
	}

	private void
	DisableAbility(InventoryEntry ability, AvatarFacade avatar)
	{
		ability.GetAbility().GetFieldAbility().Disable(avatar);
	}
}
}