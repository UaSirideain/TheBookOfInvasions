﻿using UnityEngine;

public enum FieldItemType { Flora, Item, KeyItem, Poison, Potion, Relic }
public enum Tag { First, Second, Third, Fourth, Hidden }

public class FieldItem : MonoBehaviour
{
    /*
	[SerializeField] public FieldItemType itemType;
	[SerializeField] Sprite UIIcon;
	[SerializeField] string UIName;
	[SerializeField] string UIDescription;

	[SerializeField] bool canEquip;
	public bool equippable { get { return canEquip; } }

	[SerializeField] FieldItemEffect effect;

	public Sprite
	GetUIIcon()
	{
		return UIIcon;
	}

	public string
	GetUIName()
	{
		return UIName;
	}

	public string
	GetUIDescription()
	{
		return UIDescription;
	}

	public void
	UseItem(AvatarComposition avatar)
	{
		CreateAction (avatar);
		if (effect)
		{
			effect.Execute (avatar);
		}
	}
		
	void
	CreateAction(AvatarComposition avatar)
	{
		FieldAction action = new FieldAction ();
		action.actionType = FieldActionType.UseItem;
		action.avatar = avatar;
		action.item = this;
		Broadcast.Action (action);
	}

	public int
	GetItemAmount()
	{
		return GetComponent<Item> ().GetItemAmount ();
	}

	public bool
	IsFinite()
	{
		return GetComponent<Item> ().IsFinite ();
	}
    */
}