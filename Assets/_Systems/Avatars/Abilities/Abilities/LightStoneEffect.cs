﻿using UnityEngine;

namespace Field.Abilities{

public class LightStoneEffect : FieldAbilityEffect
{
    private Pooler pooler { get { return GetComponent<Pooler>(); } }
    private GameObject _lightStone;
    private bool isActive = false;

    [SerializeField] private GameObject lightStonePrefab;

    public override void
    Execute(AbilityInfo ability)
    {
        ability.instancePosition = ability.creator.GetTransform();
        if (pooler)
        {
            _lightStone = pooler.InstanceWithPooler(ability);
            SetBehaviours(_lightStone.GetComponentsInChildren<IAbilityBehaviour>());
            RunBehaviours(ability);
        }
        else
        {
            if (!isActive)
            {
                isActive = true;
                _lightStone = CreateInstance(ability, lightStonePrefab);
                SetBehaviours(_lightStone.GetComponentsInChildren<IAbilityBehaviour>());
                RunBehaviours(ability);
            }
            else
            {
                Disable(ability);
                isActive = false;
            }
        }
    }
    
    public override void
    Disable(AbilityInfo ability)
    {
        Destroy(_lightStone);
    }
}
}