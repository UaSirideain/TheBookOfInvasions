﻿using UnityEngine;

namespace Field.Abilities{

public class AbilityInstancer
{
    public GameObject
    CreateInstance(GameObject prefab, Transform targetTransform)
    {
        GameObject instance = Object.Instantiate(prefab) as GameObject;
        OrientInstance(ref instance, targetTransform);
        return instance;
    }

    private void
    OrientInstance(ref GameObject instance, Transform targetTransform)
    {
        if (targetTransform.FindDeepChild("RightHand"))
        {
            instance.transform.position = targetTransform.FindDeepChild("RightHand").position;
        }
        else
        {
            instance.transform.position = targetTransform.position;
        }
        instance.transform.rotation = targetTransform.rotation;
    }
}
}