﻿using UnityEngine;

namespace Field.Abilities{

public class FireSprayEffect : FieldAbilityEffect
{
    private bool isActive = false;

    private GameObject _fireSpray;
    [SerializeField] private GameObject _fireSprayPrefab;

    private void 
    OnValidate()
    {
        isActive = false;
    }

    public override void
    Execute(AbilityInfo ability)
    {
        if (!isActive)
        {
            isActive = true;
            ability.instancePosition = ability.creator.GetTransform();
            _fireSpray = CreateInstance(ability, _fireSprayPrefab);

            SetBehaviours(_fireSpray.GetComponentsInChildren<IAbilityBehaviour>());
            RunBehaviours(ability);
        }
    }

    public override void
    Disable(AbilityInfo ability)
    {
        isActive = false;
        DisableBehaviours(ability, _fireSpray.GetComponents<IAbilityBehaviour>());
    }

    private void
    DisableBehaviours(AbilityInfo ability, IAbilityBehaviour[] behaviours)
    {
        foreach (IAbilityBehaviour behaviour in behaviours)
        {
            behaviour.Disable(ability);
        }
    }
}
}