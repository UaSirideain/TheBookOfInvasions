﻿using UnityEngine;

public static class ArcCalculator
{
    public static Vector3
    ThrowArc(this Rigidbody rb, Vector3 targetPosition, float angle)
    {
        float gravity = Physics.gravity.magnitude;

        angle *= Mathf.Deg2Rad;

        Vector3 planarTarget = new Vector3(targetPosition.x, 0, targetPosition.z);
        Vector3 planarPosition = new Vector3(rb.transform.position.x, 0, rb.transform.position.z);

        float distance = Mathf.Clamp(Vector3.Distance(planarTarget, planarPosition), 0f, 10f);
        float yOffset = rb.transform.position.y - targetPosition.y;

        float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));

        Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

        float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPosition) * (targetPosition.x > rb.transform.position.x ? 1 : -1);
        Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;

        return finalVelocity;
    }
}
