﻿using Field.Enchantments;

namespace Field.Abilities{

public abstract class FieldEnchantmentEffect : FieldAbilityEffect
{
    private ProjectileBehaviour projectileBehaviour;
    private LifeSpanBehaviour lifeSpanBehaviour;

    public override abstract void
    Execute(AbilityInfo ability);

    public override abstract void 
    Disable(AbilityInfo ability);

    protected void
    PerformCast(AbilityInfo ability)
    {
        projectileBehaviour.PerformCast(ability.creator, ability.target);
        lifeSpanBehaviour.StartLife();
    }

    protected void
    SetProjectileBehaviour(ProjectileBehaviour pb)
    {
        projectileBehaviour = pb;
    }

    protected void
    SetLifeSpanBehaviour(LifeSpanBehaviour lsb)
    {
        lifeSpanBehaviour = lsb;
    }
}
}