﻿using System.Collections.Generic;
using UnityEngine;

public enum LanguageType{Gaeilge, English}
public enum KeyWord
{
	NonLocalised,

	//ui keywords
	Resolution, Brightness, Settings, Language, EnableSeanchló, Create,

	//inventory items
	Bulrush, BulrushDescription, Eliminator, EliminatorDescription, EliminatorUnknown, EliminatorUnknownDescription, Poo,
	KeyToPulley, KeyToPulleyDescription, Maul, MaulDescription, Rope, RopeDescription, WoodenPlank, WoodenPlankDescription,

	//abilities
	FireSpray, FireSprayDescription, LightStone, LightStoneDescription,

	//flora
	CaveFungus, LogFungus,

	//Dialogue XML Files
	Cainteoir, Caint
}

public static class Language
{
	public static LanguageType activeLanguage = LanguageType.Gaeilge;

	static Dictionary<KeyWord, string> irishKeywords = new Dictionary<KeyWord, string>()
	{
		//ui keywords
		{KeyWord.Resolution, "Taifeach"},
		{KeyWord.Brightness, "Gile"},
		{KeyWord.Language, "Teanga"},
		{KeyWord.EnableSeanchló, "Cuir Seanchló ar Chumas"},
		{KeyWord.Settings, "Socraithe"},
		{KeyWord.Create, "Cruthú"},

		//inventory objects
		{KeyWord.Rope, "Téad"},
		{KeyWord.RopeDescription, "..."},
		{KeyWord.Bulrush, "..."},
		{KeyWord.BulrushDescription, "..."},
		{KeyWord.KeyToPulley, ""},
		{KeyWord.KeyToPulleyDescription, "..."},
		{KeyWord.Maul, "..."},
		{KeyWord.MaulDescription, "..."},
		{KeyWord.Eliminator, "Díothóir"},
		{KeyWord.EliminatorDescription, "..."},
		{KeyWord.EliminatorUnknown, "Réad Aisteach"},
		{KeyWord.EliminatorUnknownDescription, "A mysterious artefact stolen from a magpie's nest."},
		{KeyWord.WoodenPlank, "..."},
		{KeyWord.WoodenPlankDescription, "..."},

		//flora

		//abilities
		{KeyWord.FireSpray, "..."},
		{KeyWord.FireSprayDescription, "..."},
		{KeyWord.LightStone, "..."},
		{KeyWord.LightStoneDescription, "..."},

		//Dialogue XML Files
		{KeyWord.Cainteoir, "cainteoir"},
		{KeyWord.Caint, "caint"}
	};

	static Dictionary<KeyWord, string> englishKeywords = new Dictionary<KeyWord, string>()
	{
		//ui keywords
		{KeyWord.Resolution, "Resolution"},
		{KeyWord.Brightness, "Brightness"}, 
		{KeyWord.Language, "Language"},
		{KeyWord.EnableSeanchló, "Enable Seanchló"},
		{KeyWord.Settings, "Settings"},
		{KeyWord.Create, "Create"},

		//inventory items
		{KeyWord.Rope, "Rope"},
		{KeyWord.RopeDescription, "Multi-purpose tool."},
		{KeyWord.Bulrush, "Bulrush"},
		{KeyWord.BulrushDescription, "Can be used as a torch if soaked with fat."},
		{KeyWord.KeyToPulley, "Pulley Key"},
		{KeyWord.KeyToPulleyDescription, "A key used to enable a pulley system."},
		{KeyWord.Maul, "Maul"},
		{KeyWord.MaulDescription, "Used as a mining tool to break large rocks."},
		{KeyWord.EliminatorUnknown, "Strange Artefact"},
		{KeyWord.EliminatorUnknownDescription, "A mysterious artefact stolen from a magpie's nest."},
		{KeyWord.Eliminator, "The Eliminator"},
		{KeyWord.EliminatorDescription, "Reveals the most dangerous option when presented with a dilemma."},
		{KeyWord.WoodenPlank, "Wooden Plank"},
		{KeyWord.WoodenPlankDescription, "Has many uses in construction. Quite sturdy."},

		//flora
		{KeyWord.LogFungus, "Log Fungus"},
		{KeyWord.CaveFungus, "Cave Fungus"},

		//abilities
		{KeyWord.FireSpray, "Fire Spray"},
		{KeyWord.FireSprayDescription, "Unleash massive flames in a wide area."},
		{KeyWord.LightStone, "Light Stone"},
		{KeyWord.LightStoneDescription, "A stone which has been enchanted to project light."},

		//Dialogue XML Files
		{KeyWord.Cainteoir, "speaker"},
		{KeyWord.Caint, "speech"}
	};
		
	public static string
	Localise(KeyWord keyWord)
	{
		string attempt;
		if (GetActiveLanguage ().TryGetValue (keyWord, out attempt))
		{
			return GetActiveLanguage () [keyWord];
		}
		else
		{
			Debug.Log ("Failed to localise: " + keyWord.ToString () + " to " + activeLanguage.ToString ());
			return "(" + activeLanguage.ToString() + ": " + keyWord.ToString() + ")";
		}
	}

	static Dictionary<LanguageType, LanguageOrientation> langOrients = new Dictionary<LanguageType, LanguageOrientation>()
	{
		{LanguageType.Gaeilge, LanguageOrientation.LeftToRight},
		{LanguageType.English, LanguageOrientation.RightToLeft}
	};

	public static LanguageOrientation
	GetLanguageOrientation()
	{
		return langOrients [activeLanguage];
	}

	static Dictionary<KeyWord, string>
	GetActiveLanguage()
	{
		if (activeLanguage == LanguageType.Gaeilge)
		{
			return irishKeywords;
		}
		else
		{
			return englishKeywords;
		}
	}

	public static List<LanguageType> supportedLanguages = new List<LanguageType> ()
	{
		LanguageType.Gaeilge,
		LanguageType.English
	};

	public static List<LanguageType>
	GetSupportedLanguages()
	{
		return supportedLanguages;
	}
}

public enum LanguageOrientation{LeftToRight, RightToLeft}

public class Teanga
{
	public LanguageOrientation orientation;
	public LanguageType languageName;
}