﻿using UnityEngine;
using UnityEngine.UI;

public class TextCompanion : MonoBehaviour
{
	Text textComponent { get { return GetComponent<Text> (); } }

	[SerializeField] KeyWord keyWord;

	public void
	Assign(KeyWord newKeyWord)
	{
		keyWord = newKeyWord;
		Refresh ();
	}

	void
	OnEnable()
	{
		Refresh();
	}

	void
	OnValidate()
	{
		Refresh ();
	}

	public void
	Refresh()
	{
		if (keyWord != KeyWord.NonLocalised)
		{
			textComponent.text = Language.Localise (keyWord);
		}
	}
}