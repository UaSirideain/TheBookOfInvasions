﻿using System;
using UnityEngine;
using Field.Sequences;

[RequireComponent(typeof(DrawCameraGizmos))]
public class CameraPoint : MonoBehaviour
{
    private CameraManager manager { get { return FindObjectOfType<CameraManager>(); } }

    [Tooltip("The direction the movement axes face when this is the active camera")]
    [SerializeField] Transform orientationReference;
    [Range(50f, 80f), Tooltip("The camera zoom when this is the active point")]
    [SerializeField] private float fieldOfView = 79f;


    [SerializeField] private bool enableModifiers = false;
    [SerializeField] private CameraModifiers m_Modifiers = CameraModifiers.defaultSettings;

    [Tooltip("The rail upon which this camera is guided (If assigned, add component \"Camera Rail\").")]
    [SerializeField] private BezierCurveLink rail;

    public CameraModifiers cameraModifiers
    {
        get { return m_Modifiers; }
        set { m_Modifiers = value; }
    }

	public float
	GetFOV()
	{
		return fieldOfView;
	}

    public void
    ActivateCameraPoint()
    {
        manager.SnapFieldCameraTo(transform, true);
        manager.SetFieldOfView(fieldOfView);
        if (enableModifiers)
        {
            if (cameraModifiers.overrideStack != null)
            {
                manager.AssignNewStack(cameraModifiers.overrideStack);
            }
            else
            {
                manager.SetSaturation(cameraModifiers.saturation);
                manager.SetContrast(cameraModifiers.contrast);
            }
        }
        else
        {
            manager.ResetProfile();
        }

        if (rail != null)
        {
            if (!GetComponent<CameraRail>())
            {
                throw new NullReferenceException("Camera rail must be added as a component to " + transform.name + " if a rail is assigned.");
            }
            else
            {
                StartCoroutine(GetComponent<CameraRail>().StickToRail(manager, rail));
            }
        }
    }

	public Transform
	GetOrientationOffset()
	{
		return orientationReference;
	}
}