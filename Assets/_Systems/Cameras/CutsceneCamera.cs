﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CutsceneCamera : MonoBehaviour
{
	public IEnumerator Move(Transform start, Transform end, float secondsToLerp)
	{
		float lerpTime = 0.0f;

		while (transform.position != end.position)
		{
			transform.position = Vector3.Lerp (start.position, end.position, lerpTime);
			transform.rotation = Quaternion.Lerp (start.rotation, end.rotation, lerpTime);

			lerpTime += Time.deltaTime / secondsToLerp;

			yield return null;
		}
	}
}
