﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Cameras{

public class BattleCamera : MonoBehaviour
{
	[SerializeField] CanvasGroup fadeGraphic;

	int fadeTicket = 0;
	const float duration = 1f;

	public void
	Enable()
	{
		GetComponent<Camera> ().enabled = true;
	}

	public void
	Disable()
	{
		GetComponent<Camera> ().enabled = false;
		fadeGraphic.alpha = 0f;
	}
		
	public void
	LookAt(Transform avatar)
	{
		transform.LookAt (avatar);
	}

	public IEnumerator
	FadeOut()
	{
		yield return StartCoroutine (Fade (0f, 1f));
	}

	public IEnumerator
	FadeIn()
	{
		yield return StartCoroutine (Fade (1f, 0f));
	}
		
	IEnumerator
	Fade(float start, float end)
	{
		fadeTicket++;
		int myFadeTicket = fadeTicket;

		float t = 0.0f;

		fadeGraphic.alpha = start;

		while (t < 1.2f && myFadeTicket == fadeTicket)
		{
			fadeGraphic.alpha = Mathf.Lerp (start, end, t);
			t += Time.deltaTime / duration;
			yield return null;
		}

		fadeGraphic.alpha = end;
	}
}
}