﻿using System;
using UnityEngine;
using UnityEngine.PostProcessing;

[Serializable]
public struct CameraModifiers
{
    [Range(0f, 2f), Tooltip("Intensifies all colours")]
    public float saturation;

    [Range(0f, 2f), Tooltip("Shrinks the differences between the darkest dark and the brightest bright")]
    public float contrast;

    [Tooltip("Passes the stack to the camera behaviour profile when this is the active camera (leave blank to ignore)")]
    public PostProcessingProfile overrideStack;

    public static CameraModifiers defaultSettings
    {
        get
        {
            return new CameraModifiers
            {
                saturation = 1f,
                contrast = 1f
            };
        }
    }
}