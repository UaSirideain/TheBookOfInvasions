﻿using UnityEngine;
using System.Collections;

namespace Field.Sequences{

public class AssignCameraPointToAvatar : FieldEvent
{
	[SerializeField] CameraPoint cameraPoint;

	public override IEnumerator
	LocalRun()
	{
		AvatarFacade avatar = sequenceInfo.triggeringAvatar;

		avatar.SetCurrentCameraPoint(cameraPoint);

		yield return null;
	}
}
}