﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

namespace Cameras{

public class FieldCamera : MonoBehaviour, ICamera
{
	Camera cam { get { return GetComponent<Camera> (); } }

	public void
	SnapTo(Transform newCameraPoint, bool motionBlur)
	{
		if (transform.position != newCameraPoint.position || transform.rotation != newCameraPoint.rotation)
		{
			if (motionBlur)
			{
				StartCoroutine (SetMotionBlur ());
			}

			transform.position = newCameraPoint.position;
			transform.rotation = newCameraPoint.rotation;
		}
	}
		
	public void
	FreezeFrame()
	{
		gameObject.AddComponent<FreezeFrame> ();
	}

	public void
	ResumeRegularFramerate()
	{
		Destroy(GetComponent<FreezeFrame>());
	}
		
	int motionBlurTicket = 0;

	IEnumerator
	SetMotionBlur()
	{
		motionBlurTicket++;
		int myMotionBlurTicket = motionBlurTicket;

		MotionBlur mb = GetComponent<MotionBlur> ();
		mb.blurAmount = 0.92f;
		mb.enabled = true;

		float lerpTime = 0f;

		while (lerpTime < 1f && myMotionBlurTicket == motionBlurTicket)
		{
			mb.blurAmount = Mathf.Lerp (0.92f, mb.blurAmount - 0.5f, lerpTime);
			lerpTime += Time.deltaTime / 3f;
			yield return null;
		}
			
		if (myMotionBlurTicket == motionBlurTicket)
		{
			mb.enabled = false;
		}
	}

		
	/// <summary>
	/// Fade Class
	/// We want to move this to an external script. That script will be called by the Camera Manager and will be able to apply itself onto any ICamera.
	/// </summary>

	[SerializeField] Image fadeGraphic;

	int fadeTicket = 0;

	public IEnumerator
	Fade(float opacity, float duration)
	{
		fadeTicket++;
		int myFadeTicket = fadeTicket;

		float lerpTime = 0.0f;
		Color endColour = new Color (0, 0, 0, opacity);

		fadeGraphic.gameObject.SetActive (true);

        while (fadeGraphic.color.a != endColour.a && myFadeTicket == fadeTicket)
	    {
            fadeGraphic.color = Color.Lerp(fadeGraphic.color, endColour, Mathf.SmoothStep (0.0f, 1.0f, lerpTime));
			lerpTime += Time.deltaTime / duration;

			if (Mathf.Abs (fadeGraphic.color.a - endColour.a) < 0.1)
			{
				fadeGraphic.color = endColour;
			}

			yield return null;
		}

		if(opacity == 0f)
		fadeGraphic.gameObject.SetActive (false);
	}

	public void
	Enable()
	{
		this.enabled = true;
		cam.enabled = true;
	}

	public void
	Disable()
	{
		this.enabled = false;
		cam.enabled = false;
	}
}
}