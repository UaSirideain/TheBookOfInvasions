﻿using System.Collections;
using UnityEngine;

//TODO: Rename and Refactor - This is impossible to read

public class CameraRailPoint : MonoBehaviour
{
	public void OnValidate()
	{

	}

	public IEnumerator
	MoveAlongRail (Transform currentCameraPoint, Transform nextCameraPoint, Transform currentRailCollider, Transform nextRailCollider, Transform player)
	{
		while(currentRailCollider.CalculateLocalDistanceTo(player, "x") > -50.1 && nextRailCollider.CalculateLocalDistanceTo(player, "x") < 50.1)
		{
			float lerpPosition = 0f;

			while (currentRailCollider.CalculateLocalDistanceTo(player, "x") > -0.1 && nextRailCollider.CalculateLocalDistanceTo(player, "x") < 0.1)
			{
				lerpPosition = currentRailCollider.CalculateLocalDistanceTo (player, "x") / currentRailCollider.CalculateLocalDistanceTo (nextRailCollider, "x");

				//normalise value and make lerpposition equal it
				transform.position = Vector3.Lerp (currentCameraPoint.position, nextCameraPoint.position, lerpPosition);
				transform.rotation = Quaternion.Lerp (currentCameraPoint.rotation, nextCameraPoint.rotation, lerpPosition);

				yield return null;
			}

			yield return null;
		}
	}
}