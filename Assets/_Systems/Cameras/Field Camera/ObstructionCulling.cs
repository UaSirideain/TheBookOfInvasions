﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstructionCulling : MonoBehaviour 
{
	[SerializeField]
	List<Renderer> cameraObstructions;

	void OnTriggerEnter ()
	{
		foreach (Renderer obstruction in cameraObstructions)
		{
			//obstruction.SetActive (false);
			StartCoroutine(Cull (obstruction, opaque, transparent));
		}
	}

	void OnTriggerExit ()
	{
		foreach (Renderer obstruction in cameraObstructions)
		{
			//obstruction.SetActive (true);
			StartCoroutine(Cull (obstruction, transparent, opaque));
		}
	}

	Color opaque = new Color (1, 1, 1, 1);
	Color transparent = new Color(0, 0, 0, 0);

	[SerializeField]
	[Range(0.1f, 1.5f)]
	float seconds = 0.68f;

	IEnumerator Cull(Renderer obstruction, Color start, Color end)
	{
		float lerpTime = 0.0f;

		if (end.a == 0){
		obstruction.material.shader = Shader.Find ("Legacy Shaders/Transparent/Diffuse");}

		while(lerpTime < 1.0f)
		{
			obstruction.material.color = Color.Lerp (start, end, lerpTime);
			lerpTime += Time.deltaTime / seconds;

			yield return null;
		}

		if (end.a == 1){
		obstruction.GetComponent<Renderer> ().material.shader = Shader.Find ("Standard");}

		yield return null;
	}
}