﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Cameras;
//
//public enum OrientationKeymap { Manual, Auto }
//
//namespace Field.Sequences{
//
//public class CameraRailEvent : FieldEvent
//{
//    [Header("Camera Point Data")]
//    [SerializeField] FieldCamera cam;
//    [SerializeField] public Transform cameraPoint;
//    [SerializeField] Transform initialCameraPoint;
//    [SerializeField] Transform orientationReference;
//    
//    [Header("Pan Data")]
//    [SerializeField] List<Transform> thresholds;
//    [SerializeField] List<Transform> points;
//
//    GizmoRenderer gizmo { get { return GetComponent<GizmoRenderer>(); } }
//
//    bool inTrigger;
//
//    Transform currentStartThreshold;
//    Transform currentEndThreshold;
//    Transform currentStartPoint;
//    Transform currentEndPoint;
//
//    Transform player;
//
//    void
//    OnValidate()
//    {
//        cam = FindObjectOfType<FieldCamera>();
//        foreach (Transform threshold in thresholds)
//        {
//            if (threshold == null)
//            {
//                thresholds.RemoveAt(thresholds.IndexOf(threshold));
//            }
//        }
//
//        foreach (Transform point in points)
//        {
//            if (point == null)
//            {
//                points.RemoveAt(points.IndexOf(point));
//            }
//        }
//        
//        if (thresholds.Count > 0)
//        {
//            Orient();
//        }
//    }
//		
//    void
//    Orient()
//    {
//        thresholds[0].LookAt(thresholds[1]);
//        thresholds[1].rotation = thresholds[0].rotation;
//    }
//		    
//    public void
//    OnTriggerEnterOverride(AvatarFacade avatar, float newOverrideMovementSpeed)
//    {
//        //overrideMovementSpeed = newOverrideMovementSpeed;
//        StartCoroutine(Execute(avatar));
//    }
//
//    public override IEnumerator
//    LocalRun()
//    {
//		StartCoroutine(Execute(stage.RequestSequenceInfo().triggeringAvatar));
//        yield return null;
//    }
//		
//    IEnumerator
//    Execute(AvatarFacade avatar)
//    {
//        avatar.SetCurrentCameraPoint(this);
//
//		if (orientationReference != null)
//		{
//			avatar.GetComponentInChildren<OrientationOffset>().SetNextOffset(orientationReference);
//		}
//
//        if (avatar.IsPlayerCharacter())
//        {
//            inTrigger = true;
//            player = avatar.GetTransform();
//
//            if (initialCameraPoint != null)
//            {
//                cameraPoint.position = initialCameraPoint.position;
//                cameraPoint.rotation = initialCameraPoint.rotation;
//            }
//
//            
//
//            StartCoroutine(UpdateCameraPointPosition());
//            cam.SnapTo(cameraPoint);
//        }
//        else
//        {
//            inTrigger = false;
//        }
//
//        yield return null;
//    }
//		
//    IEnumerator
//    UpdateCameraPointPosition()
//    {
//        while (inTrigger)
//        {
//            SetStartThreshold();
//            SetCameraPointPosition();
//
//            yield return null;
//        }
//    }
//
//    void
//    SetCameraPointPosition()
//    {
//        if (currentStartThreshold != null)
//        {
//            float maxDistance = currentStartThreshold.CalculateLocalDistanceTo(currentEndThreshold, "z");
//            float currentDistance = currentStartThreshold.CalculateLocalDistanceTo(player, "z");
//            float lerpPosition = currentDistance / maxDistance;
//
//            cameraPoint.position = Vector3.Lerp(currentStartPoint.position, currentEndPoint.position, lerpPosition);
//            cameraPoint.rotation = Quaternion.Lerp(currentStartPoint.rotation, currentEndPoint.rotation, lerpPosition);
//        }
//    }
//
//    void
//    SetStartThreshold()
//    {
//        for (int i = thresholds.Count - 1; i > -1; i--)
//        {
//            if (thresholds[i].CalculateLocalDistanceTo(player, "x") > 0f)
//            {
//                if (i + 1 < thresholds.Count)
//                {
//                    SetValues(i, i + 1);
//                }
//
//                else
//                {
//                    SetValues(i - 1, i);
//                }
//
//                return;
//            }
//
//            if (i == 0)
//            {
//                SetValues(i, i + 1);
//                return;
//            }
//        }
//    }
//
//    void
//    SetValues(int indexOfStart, int indexOfEnd)
//    {
//        currentStartThreshold = thresholds[indexOfStart];
//        currentEndThreshold = thresholds[indexOfEnd];
//
//        currentStartPoint = points[indexOfStart];
//        currentEndPoint = points[indexOfEnd];
//    }
//}
//}