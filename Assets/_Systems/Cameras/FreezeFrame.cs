﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeFrame : MonoBehaviour
{
	RenderTexture frozenFrame;
	bool freezeFrame = false;

	void
	OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (frozenFrame == null)
		{
			frozenFrame = Instantiate (source) as RenderTexture;
		}

		if (freezeFrame)
		{
			Graphics.Blit (frozenFrame, destination);
		}
		else
		{
			Graphics.Blit (source, frozenFrame);
		}
	}
}