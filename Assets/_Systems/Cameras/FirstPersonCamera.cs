﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cameras{

public class FirstPersonCamera : MonoBehaviour, IInputtable, IPausable
{
	[SerializeField] Transform startPosition;
	[SerializeField] Transform endPosition;
	[SerializeField] AvatarController avatarController;

	[SerializeField] float seconds;
	bool lerpFinished;

	public IEnumerator
	Activate(Transform startPoint)
	{
		startPosition = startPoint;
		transform.position = startPosition.position;
		transform.rotation = startPosition.rotation;
		endPosition = avatarController.GetActiveCharacter().GetSuitableLocationForFirstPersonCamera ();

		GetComponent<Camera> ().enabled = true;

		StartCoroutine (Lerp (startPoint, endPosition));

		yield return null;
	}

	public IEnumerator
	Deactivate()
	{
		
		yield return StartCoroutine(Lerp(transform, startPosition));

		GetComponent<Camera>().enabled = false;

		yield return null;
	}

	IEnumerator
	Lerp(Transform startPoint, Transform endPoint)
	{
		float lerpTime = 0f;
		lerpFinished = false;

		Vector3 startPosition = startPoint.position;
		Quaternion startRotation = startPoint.rotation;

		while (lerpTime < 1f)
		{
			transform.position = Vector3.Lerp (startPosition, endPoint.position, lerpTime);
			transform.rotation = Quaternion.Lerp (startRotation, endPoint.rotation, lerpTime);

			lerpTime += Time.deltaTime / Mathf.Clamp(seconds, 0.1f, .5f) * pauseFactor;

			yield return null;
		}

        if (transform.position != endPoint.position)
        {
            transform.position = endPoint.position;
        }


		yaw = endPosition.eulerAngles.y;
		pitch = endPosition.eulerAngles.x;

		lerpFinished = true;
	}
		
	const float PITCH_MIN = -80f;
	const float PITCH_MAX = 80f;

	float yaw = -90.0f;
	float pitch = 0.0f;

	float yawSensitivity = 20000f;
	float pitchSensitivity = 20000f;

    bool moveCam = false;

	void
	Right()
	{
		moveCam = true;
		yaw = transform.eulerAngles.y;
		pitch = transform.eulerAngles.x;
	}

	void
	RightUp()
	{
		moveCam = false;
	}

	void
	AxesInput(float x, float y)
	{
		yaw += x / yawSensitivity;
		pitch -= y / pitchSensitivity;
		pitch = Mathf.Clamp(pitch, PITCH_MIN, PITCH_MAX);
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToMouseAxesInput (new List<InputState> (), AxesInput);
		input.SubscribeToStringInput (InputMode.Down, InputButton.RightMouse, new List<InputState> (), Right);
		input.SubscribeToStringInput (InputMode.Up, InputButton.RightMouse, new List<InputState> (), RightUp);
	}

	void
	Update()
	{
		if (lerpFinished && moveCam)
		{
			transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
		}
	}

	float pauseFactor = 1f;

	public void
	Pause()
	{
		pauseFactor = 0f;
	}

	public void
	Unpause()
	{
		pauseFactor = 1f;
	}
}
}