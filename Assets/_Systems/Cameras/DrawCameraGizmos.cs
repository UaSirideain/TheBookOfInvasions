﻿using UnityEngine;

public class DrawCameraGizmos : MonoBehaviour
{
    [SerializeField] private Color gizmoColour = Color.blue;
    private void
    OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = gizmoColour;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
    }
}