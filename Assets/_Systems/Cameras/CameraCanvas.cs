﻿using UnityEngine;
using System.Collections;

public class CameraCanvas : MonoBehaviour
{
	GameObject ruleOfThirds;
	
	[SerializeField] bool showRuleOfThirds;


	void
	OnValidate()
	{
		ruleOfThirds = transform.Find ("Rule of Thirds").gameObject;
		ruleOfThirds.SetActive (showRuleOfThirds);
	}
}
