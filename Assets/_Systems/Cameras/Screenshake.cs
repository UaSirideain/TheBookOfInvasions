﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screenshake : MonoBehaviour
{
	Transform tr { get { return GetComponent<Transform> (); } }

	const float shakeAmount = .03f;

	Vector3 oldPosition;// = tr.position;


	public IEnumerator
	Shake(float duration)
	{
		oldPosition = tr.localPosition;

		float t = 0f;

		while (t < duration)
		{
			Shake2 ();
			t += Time.deltaTime;
			yield return null;
		}

		t = 0f;

		while (t < 1.2f)
		{
			tr.localPosition = Vector3.Lerp (tr.localPosition, oldPosition, t);
			t += Time.deltaTime * 2f;
			yield return null;
		}
	}

	void
	Shake2()
	{
		tr.localPosition = oldPosition + Random.insideUnitSphere * shakeAmount;
		//tr.localPosition.y = Random.insideUnitSphere.y * shakeAmount;
	}
}