﻿using BezierCurves;
using System.Collections;
using UnityEngine;
using Field.Sequences;

public class CameraRail : MonoBehaviour
{
	BezierPoint3D pointA;
	BezierPoint3D pointB;

	Quaternion rotationTargetA = Quaternion.identity;
	Quaternion rotationTargetB = Quaternion.identity;

	float rotationT = 0f;

    public IEnumerator
    StickToRail(CameraManager manager, BezierCurveLink curve)
    {
        while (curve.OnCurve)
        {
            manager.SnapFieldCameraTo(transform, false);
            yield return null;
        }
    }

    public void
    MoveAlongRail(BezierCubicCurve rail, float normalDistance)
    {
        transform.position = rail.GetRealPoint(normalDistance);

		UpdateRotationTargets (rail, normalDistance);
		transform.rotation = Quaternion.Lerp (rotationTargetA, rotationTargetB, rotationT);
    }

	void
	UpdateRotationTargets(BezierCubicCurve rail, float normalDistance)
	{
		rail.GetCubicSegment (normalDistance , out pointA, out pointB, out rotationT);
		rotationTargetA = pointA.RotationTarget.rotation;
		rotationTargetB = pointB.RotationTarget.rotation;
	}
}