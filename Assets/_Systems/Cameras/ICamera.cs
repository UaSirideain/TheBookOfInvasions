﻿using System.Collections;
using UnityEngine;

namespace Cameras{

public interface ICamera
{
	IEnumerator
	Fade (float opacity, float duration);
}
}