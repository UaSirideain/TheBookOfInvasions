﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cameras{

public class ScreenReader : MonoBehaviour
{
	Transform myCamera { get { return GetComponent<Transform> (); } }
	[SerializeField] RenderTexture renderTexture;

	float r;
	float g;
	float b;

	public Color
	GetAverageColourForCamera(Transform targetCamera)
	{
		myCamera.SetParent (targetCamera);
		myCamera.localScale = new Vector3 (1, 1, 1);
		myCamera.localPosition = new Vector3 (0, 0, 0);
		myCamera.localRotation = Quaternion.identity;

		return GetAverageColourOfScreen ();
	}

	public float
	GetBrightnessForCamera(Transform targetCamera)
	{
		myCamera.SetParent (targetCamera);
		myCamera.localScale = new Vector3 (1, 1, 1);
		myCamera.localPosition = new Vector3 (0, 0, 0);
		myCamera.localRotation = Quaternion.identity;


		Color colourOfScreen = GetAverageColourOfScreen ();
		float brightness = 0f;

		brightness += colourOfScreen.r;
		brightness += colourOfScreen.g;
		brightness += colourOfScreen.b;

		brightness /= 3;

		return brightness;
	}

	Color
	GetAverageColourOfScreen()
	{
		Color[] colours = CastAsTexture2D(renderTexture).GetPixels ();

		r = 0f;
		g = 0f;
		b = 0f;

		foreach (Color colour in colours)
		{
			r += colour.r / colours.Length;
			g += colour.g / colours.Length;
			b += colour.b / colours.Length;
		}

		return new Color (r, g, b);
	}

	Texture2D
	CastAsTexture2D(RenderTexture rTexture)
	{
		Texture2D texture = new Texture2D (16, 16, TextureFormat.RGB24, false);
		RenderTexture.active = rTexture;
		texture.ReadPixels (new Rect (0, 0, rTexture.width, rTexture.height), 0, 0);
		texture.Apply ();
		return texture;
	}
}
}