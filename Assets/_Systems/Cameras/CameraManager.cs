﻿using Cameras;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public enum ActiveCamera{Field, Battle, FirstPerson, None}

public class CameraManager : MonoBehaviour, IInputtable
{
    [SerializeField] FieldCamera fieldCamera { get { return GetComponentInChildren<FieldCamera>(); } }
    [SerializeField] BattleCamera battleCamera { get { return GetComponentInChildren<BattleCamera>(); } }
    [SerializeField] FirstPersonCamera firstPersonCamera { get { return GetComponentInChildren<FirstPersonCamera>(); } }
	ScreenReader screenReader { get { return GetComponentInChildren<ScreenReader> (); } }

    List<PostProcessingBehaviour> postFXBehaviours = new List<PostProcessingBehaviour>();
    List<ColorGradingModel> postFXSettings = new List<ColorGradingModel>();

    [SerializeField] private PostProcessingProfile defaultProfile;
    [SerializeField] Transform audioListener;
    [SerializeField] ActiveCamera activeCamera = ActiveCamera.Field;

	Dictionary<ActiveCamera, Transform> camerasList = new Dictionary<ActiveCamera, Transform>();

    public Camera
	GetActiveCamera()
    {
		return camerasList [activeCamera].GetComponent<Camera>();
    }
		
	public void
	SnapFieldCameraTo(Transform point, bool motionBlur)
	{
		fieldCamera.SnapTo (point, motionBlur);
	}

	public float
	GetBrightnessOfScreen()
	{
		return screenReader.GetBrightnessForCamera (fieldCamera.transform);
	}
		
	public void
	ActivateBattleCamera()
	{
		if (activeCamera != ActiveCamera.Battle)
		{
			fieldCamera.FreezeFrame ();

			activeCamera = ActiveCamera.Battle;
			battleCamera.Enable();

			fieldCamera.Disable ();
		}
	}

	public void
	DeactivateBattleCamera()
	{
		if (activeCamera == ActiveCamera.Battle)
		{
			activeCamera = ActiveCamera.Field;
			fieldCamera.ResumeRegularFramerate ();
			fieldCamera.Enable();

			battleCamera.Disable();
		}
	}

	public void
	SubscribeTo(InputValidator input)
	{
		input.SubscribeToStringInput (InputMode.Down, InputButton.ToggleFirstPersonCamera, new List<InputState> (){InputState.Battle }, ToggleFirstPersonCamera_Input);
		input.SubscribeToAxesInput (new List<InputState> (){InputState.Battle}, AxesInput);
	}

    void
    Awake()
    {
        camerasList = new Dictionary<ActiveCamera, Transform>()
        {
            {ActiveCamera.Battle, battleCamera.transform},
            {ActiveCamera.Field, fieldCamera.transform},
            {ActiveCamera.FirstPerson, firstPersonCamera.transform}
        };

        GetPostProcessingBehaviours();
        AssignNewStack(defaultProfile);
        GetPostProcessingColourGrades();
    }

    private void 
    GetPostProcessingColourGrades()
    {
        foreach (PostProcessingBehaviour postFXBehaviour in postFXBehaviours)
        {
            postFXSettings.Add(postFXBehaviour.profile.colorGrading);
        }
    }

    private void 
    GetPostProcessingBehaviours()
    {
        foreach (Transform childCamera in transform)
        {
            if (childCamera.GetComponent<PostProcessingBehaviour>())
            {
                postFXBehaviours.Add(childCamera.GetComponent<PostProcessingBehaviour>());
            }
        }
    }

    void
    ActivateFirstPersonCamera()
    {
        if (activeCamera == ActiveCamera.Field)
        {
            activeCamera = ActiveCamera.FirstPerson;

            StartCoroutine(firstPersonCamera.Activate(fieldCamera.transform));
        }
        else if (activeCamera == ActiveCamera.FirstPerson)
        {
            activeCamera = ActiveCamera.Field;
            StartCoroutine(firstPersonCamera.Deactivate());
        }
    }

    void
    Update()
    {
        SetAudioPosition();
    }
		
    void
    SetAudioPosition()
    {
		audioListener.position = camerasList [activeCamera].position;
		audioListener.rotation = camerasList [activeCamera].rotation;
    }

    void 
    OnEnable()
    {
		GetComponent<InputListener> ().SetListening ();
    }

	void
	ToggleFirstPersonCamera_Input()
	{
		ActivateFirstPersonCamera ();
	}

	void
	AxesInput(float x, float y)
	{
		if (activeCamera == ActiveCamera.FirstPerson)
		{
			if (x != 0 || y != 0)
			{
				ActivateFirstPersonCamera ();
			}
		}
	}

	public IEnumerator
	FadeOut(float duration)
	{
		yield return camerasList [activeCamera].GetComponent<ICamera> ().Fade (1f, duration);
	}

	public IEnumerator
	FadeIn(float duration)
	{
		yield return camerasList [activeCamera].GetComponent<ICamera> ().Fade (0f, duration);
	}
		
	public void
	SetBrightness(float value)
	{
		foreach (BrightnessEffect brightness in GetComponentsInChildren<BrightnessEffect>())
		{
			brightness.SetBrightness (value);
		}
	}

    public void
    AssignNewStack(PostProcessingProfile newProfile)
    {
        defaultProfile = newProfile;
        foreach (PostProcessingBehaviour postFXBehaviour in postFXBehaviours)
        {
            postFXBehaviour.profile = newProfile;
        }
    }

    public void
    SetSaturation(float saturationOverride)
    {
        for (int index = 0; index < postFXSettings.Count; index++)
        {
            ColorGradingModel.Settings settings = postFXSettings[index].settings;
            settings.basic.saturation = saturationOverride;
            postFXSettings[index].settings = settings;
        }
    }

    public void
    SetContrast(float contrastOverride)
    {
        for (int index = 0; index < postFXSettings.Count; index++)
        {
            ColorGradingModel.Settings settings = postFXSettings[index].settings;
            settings.basic.contrast = contrastOverride;
            postFXSettings[index].settings = settings;
        }
    }

    public void
    SetFieldOfView(float fov)
    {
        foreach (PostProcessingBehaviour postFXBehaviour in postFXBehaviours)
        {
            if (postFXBehaviour.transform.GetComponent<Camera>())
            {
                postFXBehaviour.transform.GetComponent<Camera>().fieldOfView = fov;
            }
        }
    }

    public void
    ResetProfile()
    {
        AssignNewStack(defaultProfile);
    }
}