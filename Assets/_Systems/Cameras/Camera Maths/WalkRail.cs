﻿using System.Collections.Generic;
using UnityEngine;

namespace BezierCurves{

public static class WalkRail
{
	//A note about this function:
	//We want these calculations to be restricted to the XZ plane.
	//Currently, they are not done so. This has not been a problem yet, but there is nothing in place to stop it from becoming a problem.
    public static List<BezierPoint3D>
    GetActiveSegmentBetweenPoints(BezierPoint3D closestPoint, BezierPoint3D previousPoint, BezierPoint3D nextPoint, Vector3 playerPosition)
    {
        // Use the law of Cosines.
        List<BezierPoint3D> activeSegment = new List<BezierPoint3D>();

        Vector3 pointA = closestPoint.Position;
        Vector3 pointB = previousPoint.Position;
        Vector3 altPointB = nextPoint.Position;
        Vector3 pointC = playerPosition;

		Vector3 vA = new Vector3 (pointA.x, playerPosition.y, pointA.z);
		Vector3 vB = new Vector3 (pointB.x, playerPosition.y, pointB.z);
		Vector3 vAltB = new Vector3 (altPointB.x, playerPosition.y, altPointB.z);
		Vector3 vC = new Vector3 (pointC.x, playerPosition.y, pointC.z);


		float a = Vector3.Distance(vA, vC);
		float b = Vector3.Distance(vB, vC);
		float altB = Vector3.Distance(vAltB, vC);
		float c = Vector3.Distance(vA, vB);
		float altC = Vector3.Distance(vA, vAltB);

        float angleB = Mathf.Acos((Mathf.Pow(a, 2) + Mathf.Pow(c, 2) - Mathf.Pow(b, 2)) / (2 * c * a)) * Mathf.Rad2Deg;
        float alternateAngleA = Mathf.Acos((Mathf.Pow(a, 2) + Mathf.Pow(altC, 2) - Mathf.Pow(altB, 2)) / (2 * altC * a)) * Mathf.Rad2Deg;

        if (angleB < alternateAngleA)
        {
            activeSegment.Add(previousPoint);
            activeSegment.Add(closestPoint);
        }
        else
        {
            activeSegment.Add(closestPoint);
            activeSegment.Add(nextPoint);
        }

        return activeSegment;
    }
		
    public static List<BezierPoint3D>
    GetActiveSegments(Vector3 playerPosition, List<BezierPoint3D> KeyPoints)
    {
        List<BezierPoint3D> activeSegment = new List<BezierPoint3D>();
        if (KeyPoints.Count == 2)
        {
            activeSegment.AddRange(KeyPoints);
            return activeSegment;
        }
        else if (KeyPoints.Count > 2)
        {
            int indexOfClosestPoint = KeyPoints.IndexOf(GetClosestPoint(playerPosition, KeyPoints));
            if (indexOfClosestPoint == 0)
            {
                activeSegment.Add(KeyPoints[0]);
                activeSegment.Add(KeyPoints[1]);
            }
            else if (indexOfClosestPoint > 0 && indexOfClosestPoint + 1 < KeyPoints.Count)
            {
                activeSegment.AddRange(GetActiveSegmentBetweenPoints(KeyPoints[indexOfClosestPoint], KeyPoints[indexOfClosestPoint - 1], KeyPoints[indexOfClosestPoint + 1], playerPosition));
            }
            else
            {
                activeSegment.Add(KeyPoints[KeyPoints.Count - 2]);
                activeSegment.Add(KeyPoints[KeyPoints.Count - 1]);
            }

        }
        return activeSegment;
    }

    public static BezierPoint3D
    GetClosestPoint(Vector3 playerPosition, List<BezierPoint3D> KeyPoints)
    {
        float shortestDistance = 1000f;
        BezierPoint3D closestPoint = null;
        foreach (BezierPoint3D point in KeyPoints)
        {
			Vector3 v1 = new Vector3(point.Position.x, playerPosition.y, point.Position.z);

            if (Vector3.Distance(v1, playerPosition) < shortestDistance)
            {
                shortestDistance = Vector3.Distance(point.Position, playerPosition);
                closestPoint = point;
            }
        }
        return closestPoint;
    }
}
}