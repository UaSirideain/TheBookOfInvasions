﻿using System.Collections;
using UnityEngine;

namespace Field.Sequences{

public class DisableCurveLink : FieldEvent
{
    [SerializeField] private BezierCurveLink railToDisable;

    public override IEnumerator 
    LocalRun()
    {
        railToDisable.Deactivate();
        yield return null;
    }
}
}