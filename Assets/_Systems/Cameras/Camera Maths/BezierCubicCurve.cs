﻿using System.Collections.Generic;
using UnityEngine;

namespace BezierCurves{

public class BezierCubicCurve : MonoBehaviour
{
    // Serializable Fields
    [Tooltip("The colour used to render the curve")]
    [SerializeField] private Color curveColour = Color.green;

    [Tooltip("The colour used to render the start point of the curve")]
    [SerializeField] private Color startPointColour = Color.red;

    [Tooltip("The colour used to render the end point of the curve")]
    [SerializeField] private Color endPointColour = Color.blue;

    [Range(1, 100)]
    [Tooltip("The number of segments that the curve has. Affects calculations and performance")]
    [SerializeField] private int sampling = 25;

    [HideInInspector]
    [SerializeField] private List<BezierPoint3D> keyPoints = new List<BezierPoint3D>();

    [Tooltip("If assigned, will lerp the target transform from the start to end of the curve.")]
    [SerializeField] private Transform objectToLerp;

    [Range(0f, 1f)]
    [SerializeField] private float normalisedTime = 0.5f;

    // Properties        
    public int Sampling
    {
        get { return sampling; }
        set { sampling = value; }
    }

    public List<BezierPoint3D> KeyPoints { get { return keyPoints; } }
    public int KeyPointsCount { get { return KeyPoints.Count; } }
    
    private List<Vector3> samplePoints = new List<Vector3>();

    private void 
    Awake()
    {
        samplePoints.Add(KeyPoints[0].Position);
        for (int i = 0; i < Sampling; i++)
        {
            float distance = (i + 1) / (float)Sampling;
            samplePoints.Add(GetPoint(distance));
        }
        samplePoints.Add(KeyPoints[KeyPointsCount - 1].Position);
    }

    // Public Methods

    /// <summary>
    /// Adds a key point at the end of the curve
    /// </summary>
    /// <returns>The new key point</returns>
    public BezierPoint3D
    AddKeyPoint()
    {
        return AddKeyPointAt(KeyPointsCount);
    }

    /// <summary>
    /// Add a key point at a specified index
    /// </summary>
    /// <param name="index">The index at which the key point will be added</param>
    /// <returns>The new key point</returns>
    public BezierPoint3D
    AddKeyPointAt(int index)
    {
        BezierPoint3D newPoint = new GameObject("Point " + KeyPoints.Count, typeof(BezierPoint3D)).GetComponent<BezierPoint3D>();
        newPoint.Curve = this;
        newPoint.transform.SetParent(transform);
        newPoint.transform.localRotation = Quaternion.identity;

        if (KeyPointsCount == 0 || KeyPointsCount == 1)
        {
            newPoint.LocalPosition = Vector3.zero;
        }
        else
        {
            if (index == 0)
            {
                newPoint.Position = (KeyPoints[0].Position - KeyPoints[1].Position).normalized + KeyPoints[0].Position;
            }
            else if (index == KeyPointsCount)
            {
                newPoint.Position = (KeyPoints[index - 1].Position - KeyPoints[index - 2].Position).normalized + KeyPoints[index - 1].Position;
            }
            else
            {
                newPoint.Position = BezierCurve3D.GetPointOnCubicCurve(0.5f, KeyPoints[index - 1], KeyPoints[index]);
            }
        }

        KeyPoints.Insert(index, newPoint);

        return newPoint;
    }

    /// <summary>
    /// Removes a key point at a specified index
    /// </summary>
    /// <param name="index">The index of the key point that will be removed</param>
    /// <returns>true - if the point was removed, false - otherwise</returns>
    public bool
    RemoveKeyPointAt(int index)
    {
        if (KeyPointsCount < 2)
        {
            return false;
        }

        BezierPoint3D point = KeyPoints[index];
        KeyPoints.RemoveAt(index);

        Destroy(point.gameObject);

        return true;
    }

    /// <summary>
    /// Evaluates a position along the curve at a specified normalised distance [0, 1]
    /// </summary>
    /// <param name="distance">The normalised length at which we want to get a position [0, 1]</param>
    /// <returns>The evaluated Vector3 position</returns>
    public Vector3
    GetPoint(float distance)
    {
        // The evaluated points is between these two points
        BezierPoint3D startPoint;
        BezierPoint3D endPoint;
        float distanceRelativeToSegment;

        GetCubicSegment(distance, out startPoint, out endPoint, out distanceRelativeToSegment);

        return BezierCurve3D.GetPointOnCubicCurve(distanceRelativeToSegment, startPoint, endPoint);
    }

    Vector3 lastGoodPosition;

    public Vector3
    GetRealPoint(float distance)
    {
        float desiredDistance = distance * GetApproximateLength();

        for (int i = 0; i < samplePoints.Count - 1; i++)
        {
            if (i < samplePoints.Count - 1)
            {
                if (desiredDistance - Vector3.Distance(samplePoints[i], samplePoints[i + 1]) >= 0)
                {
                    desiredDistance -= Vector3.Distance(samplePoints[i], samplePoints[i + 1]);
                }
                else
                {
                    lastGoodPosition = (samplePoints[i] + (samplePoints[i + 1] - samplePoints[i]).normalized * desiredDistance);
                    return lastGoodPosition;
                }
            }
        }
        return lastGoodPosition;
    }

    public Quaternion
    GetRotation(float distance, Vector3 up)
    {
        BezierPoint3D startPoint;
        BezierPoint3D endPoint;
        float distanceRelativeToSegment;

        GetCubicSegment(distance, out startPoint, out endPoint, out distanceRelativeToSegment);

        return BezierCurve3D.GetRotationOnCubicCurve(distanceRelativeToSegment, up, startPoint, endPoint);
    }

    public Vector3
    GetTangent(float distance)
    {
        BezierPoint3D startPoint;
        BezierPoint3D endPoint;
        float distanceRelativeToSegment;

        GetCubicSegment(distance, out startPoint, out endPoint, out distanceRelativeToSegment);

        return BezierCurve3D.GetTangentOnCubicCurve(distanceRelativeToSegment, startPoint, endPoint);
    }

    public Vector3
    GetBinormal(float distance, Vector3 up)
    {
        BezierPoint3D startPoint;
        BezierPoint3D endPoint;
        float distanceRelativeToSegment;

        GetCubicSegment(distance, out startPoint, out endPoint, out distanceRelativeToSegment);

        return BezierCurve3D.GetBinormalOnCubicCurve(distanceRelativeToSegment, up, startPoint, endPoint);
    }

    public Vector3
    GetNormal(float distance, Vector3 up)
    {
        BezierPoint3D startPoint;
        BezierPoint3D endPoint;
        float distanceRelativeToSegment;

        GetCubicSegment(distance, out startPoint, out endPoint, out distanceRelativeToSegment);

        return BezierCurve3D.GetNormalOnCubicCurve(distanceRelativeToSegment, up, startPoint, endPoint);
    }

    public float
    GetApproximateLength()
    {
        float length = 0;
        int subCurveSampling = (Sampling / (KeyPointsCount - 1)) + 1;
        for (int i = 0; i < KeyPointsCount - 1; i++)
        {
            length += BezierCurve3D.GetApproximateLengthOfCubicCurve(KeyPoints[i], KeyPoints[i + 1], subCurveSampling);
        }

        return length;
    }

    public void
    GetCubicSegment(float distance, out BezierPoint3D startPoint, out BezierPoint3D endPoint, out float distanceRelativeToSegment)
    {
        startPoint = null;
        endPoint = null;
        distanceRelativeToSegment = 0f;

        float subCurvePercent = 0f;
        float totalPercent = 0f;
        float approximateLength = GetApproximateLength();
        int subCurveSampling = (Sampling / (KeyPointsCount - 1)) + 1;

        for (int i = 0; i < KeyPointsCount - 1; i++)
        {
            subCurvePercent = BezierCurve3D.GetApproximateLengthOfCubicCurve(KeyPoints[i], KeyPoints[i + 1], subCurveSampling) / approximateLength;
            if (subCurvePercent + totalPercent > distance)
            {
                startPoint = KeyPoints[i];
                endPoint = KeyPoints[i + 1];

                break;
            }

            totalPercent += subCurvePercent;
        }

        if (endPoint == null)
        {
            // If the evaluated point is very near to the end of the curve we are in the last segment
            startPoint = KeyPoints[KeyPointsCount - 2];
            endPoint = KeyPoints[KeyPointsCount - 1];

            // We remove the percentage of the last sub-curve
            totalPercent -= subCurvePercent;
        }

        distanceRelativeToSegment = (distance - totalPercent) / subCurvePercent;
    }

    // Protected Methods

    protected virtual void
    OnDrawGizmos()
    {
        if (KeyPointsCount > 1)
        {
            // Draw the curve
            Vector3 fromPoint = GetPoint(0f);

            for (int i = 0; i < Sampling; i++)
            {
                float distance = (i + 1) / (float)Sampling;
                Vector3 toPoint = GetPoint(distance);
                
                // Draw segment
                Gizmos.color = curveColour;
                Gizmos.DrawLine(fromPoint, toPoint);

                fromPoint = toPoint;
            }

            // Draw the start and the end of the curve indicators
            Gizmos.color = startPointColour;
            Gizmos.DrawSphere(KeyPoints[0].Position, 0.05f);

            Gizmos.color = endPointColour;
            Gizmos.DrawSphere(KeyPoints[KeyPointsCount - 1].Position, 0.05f);

            // Draw the point at the normalised distance
            Vector3 point = GetPoint(normalisedTime);
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(point, 0.1f);

            Vector3 tangent = GetTangent(normalisedTime);
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(point, point + tangent / 2f);

            Vector3 binormal = GetBinormal(normalisedTime, Vector3.up);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(point, point + binormal / 2f);

            Vector3 normal = GetNormal(normalisedTime, Vector3.up);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(point, point + normal / 2f);
        }
    }
}
}