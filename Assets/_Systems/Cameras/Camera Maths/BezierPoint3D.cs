﻿using UnityEngine;

namespace BezierCurves{

public class BezierPoint3D : MonoBehaviour
{
    public enum HandleType
    {
        Connected,
        Broken
    }

    // Serializable Fields
    [SerializeField]
    [Tooltip("The curve that the point belongs to")]
    private BezierCubicCurve curve = null;

    [SerializeField]
    private HandleType handleType = HandleType.Connected;

    [SerializeField]
    private Vector3 leftHandleLocalPosition = new Vector3(-0.5f, 0f, 0f);

    [SerializeField]
    private Vector3 rightHandleLocalPosition = new Vector3(0.5f, 0f, 0f);

    // Properties

    /// <summary>
    /// Gets or sets the curve that the point belongs to.
    /// </summary>
    public BezierCubicCurve Curve
    {
        get
        {
            return curve;
        }
        set
        {
            curve = value;
        }
    }

    /// <summary>
    /// Gets or sets the type/style of the handle.
    /// </summary>
    public HandleType HandleStyle
    {
        get
        {
            return handleType;
        }
        set
        {
            handleType = value;
        }
    }

    /// <summary>
    /// Gets or sets the position of the transform.
    /// </summary>
    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    /// <summary>
    /// Gets or sets the position of the transform.
    /// </summary>
    public Vector3 LocalPosition
    {
        get
        {
            return transform.localPosition;
        }
        set
        {
            transform.localPosition = value;
        }
    }

    /// <summary>
    /// Gets or sets the local position of the left handle.
    /// If the HandleStyle is Connected, the local position of the right handle is automaticaly set.
    /// </summary>
    public Vector3 LeftHandleLocalPosition //This is the handle by which the shape of the curve from this point to the previous point is edited
    {
        get
        {
            return leftHandleLocalPosition;
        }
        set
        {
            leftHandleLocalPosition = value;
            if (handleType == HandleType.Connected)
            {
                rightHandleLocalPosition = -value;
            }
        }
    }

    /// <summary>
    /// Gets or sets the local position of the right handle.
    /// If the HandleType is Connected, the local position of the left handle is automaticaly set.
    /// </summary>
    public Vector3 RightHandleLocalPosition //This is the handle by which the shape of the curve from this point to the following point is edited
    {
        get
        {
            return rightHandleLocalPosition;
        }
        set
        {
            rightHandleLocalPosition = value;
            if (handleType == HandleType.Connected)
            {
                leftHandleLocalPosition = -value;
            }
        }
    }

    /// <summary>
    /// Gets or sets the position of the left handle.
    /// If the HandleStyle is Connected, the position of the right handle is automaticaly set.
    /// </summary>
    public Vector3 LeftHandlePosition 
    {
        get
        {
            return transform.TransformPoint(LeftHandleLocalPosition);
        }
        set
        {
            LeftHandleLocalPosition = transform.InverseTransformPoint(value);
        }
    }

    /// <summary>
    /// Gets or sets the position of the right handle.
    /// If the HandleType is Connected, the position of the left handle is automaticaly set.
    /// </summary>
    public Vector3 RightHandlePosition
    {
        get
        {
            return transform.TransformPoint(RightHandleLocalPosition);
        }
        set
        {
            RightHandleLocalPosition = transform.InverseTransformPoint(value);
        }
    }

	[SerializeField] Transform rotationTarget;
	[SerializeField] GameObject rotationTargetPrefab;

	public Transform RotationTarget
	{
		get
		{
			return rotationTarget;
		}
	}

	public void
	AddRotationTarget()
	{
		GameObject newRotationTarget = Instantiate (rotationTargetPrefab, transform.position, transform.rotation, transform);
		newRotationTarget.name = "Rotation Target";
		rotationTarget = newRotationTarget.transform;
	}

	public void
	RemoveRotationTarget()
	{
		DestroyImmediate (rotationTarget.gameObject);
		rotationTarget = null;
	}
}
}