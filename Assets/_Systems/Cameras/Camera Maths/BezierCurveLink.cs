﻿using BezierCurves;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Field.Sequences{

public class BezierCurveLink : FieldEvent
{
	AvatarController avatarController { get { return FindObjectOfType<AvatarController>(); } }
	List<BezierPoint3D> KeyPoints { get { return avatarWalkRail.KeyPoints; } }
	Vector3 PlayerPosition { get { return avatarController.GetActiveCharacter().GetTransform().position; } }
	public bool OnCurve { get { return onCurve; } }

	[Tooltip("The rail used to calculate the position of the player.")]
	[SerializeField] BezierCubicCurve avatarWalkRail = null;

	[Tooltip("The rail the camera will follow.")]
	[SerializeField] BezierCubicCurve cameraRail = null;

	[Tooltip("The camera script that will manipulate the rail")]
	[SerializeField] CameraPoint activeCamera;

	List<BezierPoint3D> activePoints = new List<BezierPoint3D>();
	bool onCurve = false;

	public override IEnumerator
	LocalRun()
	{
		onCurve = true;
		StartCoroutine(UpdatePlayerPositionOnRail());
		yield return null;
	}

	public void
	Deactivate()
	{
		onCurve = false;
		activePoints.Clear();
	}

	private IEnumerator
	UpdatePlayerPositionOnRail()
	{
		while (onCurve)
		{
			activePoints.Clear();
			activePoints.AddRange(WalkRail.GetActiveSegments(PlayerPosition, KeyPoints));
			activeCamera.transform.GetComponent<CameraRail>().MoveAlongRail(cameraRail, GetDistanceAlongRail(PlayerPosition));

			yield return null;
		}
	}

	Vector3 quadranglePointX = Vector3.zero;
	Vector3 quadranglePointY = Vector3.zero;

	void
	UpdateOuterPointsOfQuadrangle(Vector3 playerPosition)
	{
		Vector3 pointB = new Vector3(activePoints [0].Position.x, 0f, activePoints [0].Position.z);
		Vector3 pointC = new Vector3(activePoints [1].Position.x, 0f, activePoints [1].Position.z);

		Vector3 directionB = GetAngleDirectionOfABC (activePoints [0]);
		directionB.y = 0f;

		Vector3 directionC = GetAngleDirectionOfABC (activePoints [1]);
		directionC.y = 0f;

		Vector3 adjustedPlayerPosition = new Vector3(playerPosition.x, 0f, playerPosition.z);

		Vector3 directionOfPlayerLine = pointB - pointC;

		Math3d.LineLineIntersection (out quadranglePointX, pointB, directionB, adjustedPlayerPosition, directionOfPlayerLine);
		Math3d.LineLineIntersection (out quadranglePointY, pointC, directionC, adjustedPlayerPosition, -directionOfPlayerLine);

		quadranglePointX.y = playerPosition.y;
		quadranglePointY.y = playerPosition.y;
	}

	Vector3
	GetAngleDirectionOfABC(BezierPoint3D pointB)
	{
		int index = KeyPoints.IndexOf (pointB);
		Vector3 adjustedPointB = new Vector3 (pointB.Position.x, 0f, pointB.Position.z);

		if (index == 0)//if index is first point
		{
			Vector3 adjustedPointC = new Vector3 (KeyPoints [index + 1].Position.x, 0f, KeyPoints [index + 1].Position.z);
			return Quaternion.AngleAxis (-90f, Vector3.up) * (adjustedPointB - adjustedPointC);
		}
		else if (index == KeyPoints.Count - 1)//if index is last point
		{
			Vector3 adjustedPointA = new Vector3 (KeyPoints [index - 1].Position.x, 0f, KeyPoints [index - 1].Position.z);
			return Quaternion.AngleAxis (90f, Vector3.up) * (adjustedPointB - adjustedPointA);
		}
		else
		{
			Vector3 adjustedPointC = new Vector3 (KeyPoints [index + 1].Position.x, 0f, KeyPoints [index + 1].Position.z);
			Vector3 adjustedPointA = new Vector3 (KeyPoints [index - 1].Position.x, 0f, KeyPoints [index - 1].Position.z);

			Vector3 ab = (adjustedPointA - adjustedPointB).normalized;
			Vector3 bc = (adjustedPointC - adjustedPointB).normalized;

			return (ab + bc).normalized;
		}
	}

	private float
	GetDistanceAlongRail(Vector3 playerPosition)
	{
		UpdateOuterPointsOfQuadrangle (playerPosition);

		float localDistance = CalculatePercentageOfProgressOnSegment (playerPosition);
		float playerDistance = localDistance * Vector3.Distance (activePoints [0].Position, activePoints [1].Position);

		float totalDistance = GetDistanceTravelled() + playerDistance;
		float relativeNormal = totalDistance / avatarWalkRail.GetApproximateLength();

		Debug.DrawLine(playerPosition, activePoints[0].Position);
		Debug.DrawLine(playerPosition, activePoints[1].Position);

		return Mathf.Clamp01(relativeNormal);
	}

	float
	CalculatePercentageOfProgressOnSegment(Vector3 playerPosition)
	{
		float distanceBetweenPlayerAndPointX = Vector3.Distance (playerPosition, quadranglePointX);
		float distanceBetweenPlayerAndPointY = Vector3.Distance (playerPosition, quadranglePointY);
		float length = Vector3.Distance (quadranglePointX, quadranglePointY);

		if (distanceBetweenPlayerAndPointX > length)
		{
			return 1f;
		}
		else if (distanceBetweenPlayerAndPointY > length)
		{
			return 0f;
		}
		else
		{
			return distanceBetweenPlayerAndPointX / length;
		}
	}

	private float
	GetDistanceTravelled()
	{
		float totalDistance = 0f;
		for (int index = KeyPoints.IndexOf(activePoints[GetEarliestPoint()]); index > 0; index--)
		{
			totalDistance += Vector3.Distance(KeyPoints[index].Position, KeyPoints[index - 1].Position);
		}

		return totalDistance;
	}

	private int
	GetEarliestPoint()
	{
		int firstPoint = int.Parse(activePoints[0].name.Replace("Point ", ""));
		int secondPoint = int.Parse(activePoints[1].name.Replace("Point ", ""));

		if (firstPoint < secondPoint)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	protected virtual void
	OnDrawGizmos()
	{
		if (activePoints.Count == 2)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(activePoints[0].Position, 0.1f);

			Gizmos.color = Color.red;
			Gizmos.DrawSphere(activePoints[1].Position, 0.1f);

			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(avatarWalkRail.GetRealPoint(GetDistanceAlongRail(PlayerPosition)), 0.1f);

			Gizmos.color = Color.green;
			Gizmos.DrawSphere (quadranglePointX, .1f);

			Gizmos.color = Color.green;
			Gizmos.DrawSphere (quadranglePointY, .1f);
		}
	}
}
}