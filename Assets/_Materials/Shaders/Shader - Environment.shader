﻿// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'
// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'
// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "TheBookOfInvasions/Environment"
{
	Properties
	{
		_MainTex("Albedo", 2D) = "white"{}
		_Colour ("Albedo Tint", Color) = (1, 1, 1, 1)
		_SpecColour ("Specular Tint", Color) = (1, 1, 1, 1)
		_RimColour ("Rim Colour", Color) = (1, 1, 1, 1)

		_RimBrilliance ("Rim Brilliance", Float) = 1

		_Ramp ("Ramp", 2D) = "gray"{}

		[HideInInspector] _LightStrengthMax ("", Float) = 1
		[HideInInspector] _LightStrengthMin ("", Float) = .3
		[HideInInspector] _ShadowStrengthMax ("", Float) = .15
		[HideInInspector] _ShadowStrengthMin ("", Float) = .02
	}

	SubShader
	{
		Pass
		{
			Tags {"LightMode" = "ForwardBase"}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile_fwdadd_fullshadows
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			uniform fixed4 _Colour;
			uniform fixed4 _SpecColour;
			uniform fixed4 _RimColour;
			uniform half _RimPower;
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform half _RimThickness;
			uniform half _RimBrilliance;

			fixed _LightStrengthMax;
			fixed _LightStrengthMin;
			fixed _ShadowStrengthMax;
			fixed _ShadowStrengthMin;

			sampler2D _Ramp;

			uniform half3 _LightColor0;

			struct vertexInput
			{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				float2 uv : TEXCOORD0;
				SHADOW_COORDS(1)
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD2;
				fixed4 lightDirection : TEXCOORD3;
				half3 viewDirection : TEXCOORD4;
				half3 normalDir : TEXCOORD5;


			};

			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;

				o.normalDir = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex = v.texcoord;

				half4 posWorld = mul(unity_ObjectToWorld, v.vertex);

				half3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld);

				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;

				half distance = length(fragmentToLightSource);

				fixed atten = lerp(1, 1 / distance, _WorldSpaceLightPos0.w);
				half3 lightDirection = lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w);

				o.lightDirection = fixed4(lightDirection, atten);
				o.viewDirection = viewDirection;

				//TRANSFER_VERTEX_TO_FRAGMENT(o);
				TRANSFER_SHADOW(o);

				return o;
			}

			fixed4 frag(vertexOutput i) : COLOR
			{
				//should be calculated elsewhere
				_LightStrengthMax = 1;
				_LightStrengthMin = .3;
				_ShadowStrengthMax = .45;
				_ShadowStrengthMin = .02;

				fixed shadow = SHADOW_ATTENUATION(i);

				fixed3 normalDirection = i.normalDir;

				//diffuse reflection
				half3 nDotL = dot(normalDirection, i.lightDirection);
				float3 lightColour = clamp(_LightColor0.rgb, 0.1, 1);

				fixed shadowStrength = lightColour * (_ShadowStrengthMax - _ShadowStrengthMin) + _ShadowStrengthMin;
				fixed lightStrength = lightColour * (_LightStrengthMax - _LightStrengthMin) + _LightStrengthMin;

				fixed3 diffuseReflection = clamp(nDotL, shadowStrength, lightStrength );

				fixed3 invertedLightColour = normalize(clamp(fixed3(1.0 - lightColour), 0.1, 1));
				invertedLightColour = lightColour; //previous line is not working as intended: at close ranges, light is inverted when it shouldn't be

				fixed fragmentIsLit = (diffuseReflection - shadowStrength) / ( lightStrength - shadowStrength );
				diffuseReflection = lerp(diffuseReflection * invertedLightColour, diffuseReflection * lightColour, round(fragmentIsLit));

				diffuseReflection *= i.lightDirection.w;


				//rim lighting
				half3 vDotN = dot(normalize(i.viewDirection), normalDirection);
				fixed rim = 1 - saturate(floor(vDotN * 2));
				fixed3 rimLighting = rim * _RimColour.rgb * _RimBrilliance / 3;

				//final lighting pass
				half3 lightFinal = diffuseReflection + rimLighting;

				//texture
				fixed4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);

				return fixed4(tex.rgb * lightFinal * _Colour.rgb, 1);
			}

			ENDCG
		}

		Pass
		{
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One 
			CGPROGRAM

			//uniform float4x4 unity_WorldToLight;


			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile_fwdadd
			#pragma multi_compile_fwdadd_fullshadows
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Lighting.cginc"

			uniform fixed4 _Colour;
			uniform fixed4 _SpecColour;
			uniform fixed4 _RimColour;
			uniform half _RimPower;
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform half _RimThickness;
			uniform half _RimBrilliance;

			fixed _LightStrengthMax;
			fixed _LightStrengthMin;
			fixed _ShadowStrengthMax;
			fixed _ShadowStrengthMin;

			//uniform float4 _LightColor0;
			//uniform sampler2D _LightTexture0;

			uniform sampler2D _Ramp;

			struct vertexInput
			{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD0;
				fixed3 lightDirection : TEXCOORD1;
				half3 viewDirection : TEXCOORD2;
				half3 normalDir : TEXCOORD3;

				float3 _ShadowCoord : TEXCOORD5;
				float3 _LightCoord : TEXCOORD4;
			};

			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;

				o.normalDir = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex = v.texcoord;

				half4 posWorld = mul(unity_ObjectToWorld, v.vertex);
				half3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld);
				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
			
				o.lightDirection = lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w);
				o.viewDirection = viewDirection;

				TRANSFER_VERTEX_TO_FRAGMENT(o);
				TRANSFER_SHADOW(o);

				return o;
			}

			fixed4 frag(vertexOutput i) : COLOR
			{
				//should be calculated elsewhere
				_LightStrengthMax = 1;
				_LightStrengthMin = .3;
				_ShadowStrengthMax = .15;
				_ShadowStrengthMin = .02;

				fixed shadow = SHADOW_ATTENUATION(i);

				fixed3 normalDirection = i.normalDir;

				//diffuse reflection
				half3 nDotL = dot(normalDirection, i.lightDirection);
				float3 lightColour = clamp(_LightColor0.rgb, 0.1, 1);

				fixed shadowStrength = lightColour * (_ShadowStrengthMax - _ShadowStrengthMin) + _ShadowStrengthMin;
				fixed lightStrength = lightColour * (_LightStrengthMax - _LightStrengthMin) + _LightStrengthMin;

				fixed3 diffuseReflection = clamp( nDotL, shadowStrength, lightStrength );
				fixed3 invertedLightColour = normalize(clamp(fixed3(1.0 - lightColour), 0.1, 1));
				fixed fragmentIsLit = (diffuseReflection - shadowStrength) / ( lightStrength - shadowStrength );

				diffuseReflection = lerp( ( diffuseReflection * invertedLightColour), (diffuseReflection * lightColour ), ( round(fragmentIsLit))) * ceil(LIGHT_ATTENUATION(i)) * clamp(_LightColor0.a, 0, 4) ;

				//final lighting pass
				half3 lightFinal = diffuseReflection * shadow;

				//texture
				fixed4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);

				return fixed4(tex.rgb * lightFinal * _Colour.rgb, 1);
			}

			ENDCG
		}
	}
	Fallback "Diffuse"
}