﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "TheBookOfInvasions/Character"
{
	Properties
	{
		_MainTex("Albedo", 2D) = "white"{}
		_Colour ("Albedo Tint", Color) = (1, 1, 1, 1)
		_RimColour ("Rim Colour", Color) = (1, 1, 1, 1)

		_RimBrilliance ("Rim Brilliance", Float) = 1

		[HideInInspector] _LightStrengthMax ("", Float) = 1
		[HideInInspector] _LightStrengthMin ("", Float) = .3
		[HideInInspector] _ShadowStrengthMax ("", Float) = .15
		[HideInInspector] _ShadowStrengthMin ("", Float) = .02
	}

	SubShader
	{
		Pass
		{
			Tags {"LightMode" = "ForwardBase"}

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			uniform fixed4 _Colour;
			uniform fixed4 _RimColour;
			uniform half _RimPower;
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform half _RimThickness;
			uniform half _RimBrilliance;

			fixed _LightStrengthMax;
			fixed _LightStrengthMin;
			fixed _ShadowStrengthMax;
			fixed _ShadowStrengthMin;

			uniform fixed4 _LightColor0;

			struct vertexInput
			{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				float2 uv : TEXCOORD0;

				half3 normalDir : TEXCOORD1;
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD2;

				half3 viewDirection : TEXCOORD3;
				fixed3 lightDirection : TEXCOORD4;

				SHADOW_COORDS(5)
			};

			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;

				o.normalDir = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex = v.texcoord;

				half4 posWorld = mul(unity_ObjectToWorld, v.vertex);
				o.viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld);

				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
				o.lightDirection = lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w);
	
				TRANSFER_SHADOW(o);

				return o;
			}

			fixed4 frag(vertexOutput i) : COLOR
			{
				//should be calculated elsewhere
				_LightStrengthMax = 1;
				_LightStrengthMin = .3;
				_ShadowStrengthMax = .45;
				_ShadowStrengthMin = .02;

				fixed3 normalDirection = i.normalDir;

				//diffuse reflection
				half3 nDotL = dot(normalDirection, i.lightDirection);
				float3 lightColour = clamp(_LightColor0.rgb, 0.1, 1);

				fixed shadowStrength = lightColour * (_ShadowStrengthMax - _ShadowStrengthMin) + _ShadowStrengthMin;
				fixed lightStrength = lightColour * (_LightStrengthMax - _LightStrengthMin) + _LightStrengthMin;
				fixed shadow = SHADOW_ATTENUATION(i);

				fixed3 diffuseReflection = clamp(floor(nDotL * LIGHT_ATTENUATION(i) * SHADOW_ATTENUATION(i) * 2), shadowStrength, lightStrength );

				fixed3 invertedLightColour = normalize(clamp(fixed3(1.0 - lightColour), 0.1, 1));
				fixed fragmentIsLit = (diffuseReflection - shadowStrength) / ( lightStrength - shadowStrength );
				diffuseReflection = lerp(diffuseReflection * invertedLightColour, diffuseReflection * lightColour, round(fragmentIsLit));

				//rim lighting
				half3 vDotN = dot(normalize(i.viewDirection), normalDirection);
				fixed rim = 1 - saturate(floor(vDotN * 2));
				fixed3 rimLighting = rim * _RimColour.rgb * _RimBrilliance / 3;

				//final lighting pass
				half3 lightFinal = diffuseReflection;// + rimLighting;

				//texture
				fixed4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);

				//return
				return fixed4(tex.rgb * lightFinal * _Colour.rgb, 1);
			}
			ENDCG
		}

		Pass
		{
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile_fwdbase
			#pragma multi_compile_fwdadd_fullshadows

			#include "UnityCG.cginc"
			#include "AutoLight.cginc"

			uniform fixed4 _Colour;
			uniform fixed4 _RimColour;
			uniform half _RimPower;
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform half _RimThickness;
			uniform half _RimBrilliance;

			fixed _LightStrengthMax;
			fixed _LightStrengthMin;
			fixed _ShadowStrengthMax;
			fixed _ShadowStrengthMin;

			uniform fixed4 _LightColor0;

			struct vertexInput
			{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				float2 uv : TEXCOORD0;
				float3 _LightCoord : TEXCOORD1;

				half3 normalDir : TEXCOORD2;
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD3;

				half3 viewDirection : TEXCOORD4;
				fixed3 lightDirection : TEXCOORD5;

				SHADOW_COORDS(6)
			};

			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;

				o.normalDir = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.tex = v.texcoord;

				half4 posWorld = mul(unity_ObjectToWorld, v.vertex);
				o.viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld);

				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
				o.lightDirection = lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w);

				TRANSFER_VERTEX_TO_FRAGMENT(o);
				TRANSFER_SHADOW(o);

				return o;
			}

			fixed4 frag(vertexOutput i) : COLOR
			{
				//should be calculated elsewhere
				_LightStrengthMax = 1;
				_LightStrengthMin = .3;
				_ShadowStrengthMax = .15;
				_ShadowStrengthMin = .02;

				fixed shadow = SHADOW_ATTENUATION(i);

				fixed3 normalDirection = i.normalDir;

				//diffuse reflection
				half3 nDotL = dot(normalDirection, i.lightDirection);
				fixed3 lightColour = clamp(_LightColor0.rgb, 0.1, 1);

				fixed shadowStrength = 1;//lightColour * (_ShadowStrengthMax - _ShadowStrengthMin) + _ShadowStrengthMin;
				fixed lightStrength = lightColour * (_LightStrengthMax - _LightStrengthMin) + _LightStrengthMin;

				fixed3 diffuseReflection = floor(nDotL * SHADOW_ATTENUATION(i) * 2);

				fixed3 invertedLightColour = normalize(clamp(fixed3(1.0 - lightColour), 0.1, 1));
				invertedLightColour = lightColour; //previous line is not working as intended: at close ranges, light is inverted when it shouldn't be
				fixed fragmentIsLit = (diffuseReflection - shadowStrength) / ( lightStrength - shadowStrength );

				fixed3 inverto = diffuseReflection * invertedLightColour;
				fixed3 nonverto = diffuseReflection * lightColour;
				diffuseReflection = lerp( inverto, nonverto,  round(fragmentIsLit)) * ceil(LIGHT_ATTENUATION(i)) * clamp(_LightColor0.a, 0, 4) ;
				diffuseReflection = clamp(diffuseReflection , shadowStrength, lightStrength );

				//texture
				fixed4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);

				return fixed4(tex.rgb * diffuseReflection * _Colour.rgb, 1);
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
}