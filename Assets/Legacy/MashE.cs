﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MashE : MonoBehaviour
{
	[SerializeField] Image progressBar;
	[SerializeField] Text text;
	float progress = 0f;

	void
	Start()
	{
		//disable movement
	}

	void
	Update()
	{
		if (Input.GetButtonDown ("e"))
		{
			progress += 0.1f;
		}

		if (progress >= 1f)
		{
			StartCoroutine(Complete());
		}

		progressBar.fillAmount = progress;
	}

	IEnumerator
	Complete()
	{
		text.text = "Success";

		//wait for animation to complete

		//enable movement

		yield return new WaitForSeconds (1);

		Destroy (gameObject);
	}
}