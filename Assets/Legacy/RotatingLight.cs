﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingLight : MonoBehaviour
{
	[SerializeField] float rotateX = .1f;
	[SerializeField] float rotateY = .1f;
	[SerializeField] float rotateZ = .1f;
	void
	Update ()
	{
		transform.Rotate (rotateX, rotateY, rotateZ);
	}
}