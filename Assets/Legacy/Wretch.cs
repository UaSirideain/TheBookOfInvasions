﻿using UnityEngine;

public class Wretch : MonoBehaviour
{
	[SerializeField] GameObject mashEPrefab;

	void
	OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<AvatarFacade> ())
		{
			if (other.GetComponent<AvatarFacade> ().IsActiveCharacter())
			{
				Instantiate (mashEPrefab);
			}
		}
	}
}


//A cooldown should be implemented across all wretch triggers. So when you trigger one, you can't trigger any of them for another 10 seconds, say.
//Perhaps the mashE isn't a prefab, but a system present in the level, and it has a cooldown by itself, and simply does nothing if triggered inside the cooldown period.