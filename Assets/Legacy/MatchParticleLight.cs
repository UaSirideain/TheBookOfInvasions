﻿using UnityEngine;

[RequireComponent(typeof(PauseCompanion))]
public class MatchParticleLight : MonoBehaviour, IPausable
{
    ParticleSystem particles { get { return transform.parent.GetComponentInChildren<ParticleSystem>(); } }
    Light lights { get { return GetComponent<Light>(); } }

    [Range(0f, 1f)]
    [SerializeField] float lowerFlickerLimit = 1f;
    [Range(1f, 2f)]
    [SerializeField] float upperFlickerLimit = 1.5f;
    
    float particleIntensity;
    float maxLightIntensity;
    float maxRange;
	bool paused = false;

    private void 
    Awake()
    {
        maxLightIntensity = lights.intensity;
        maxRange = lights.range;
        lights.intensity = 0;
        lights.range = 0;
    }

    private void 
    Update()
    {
		if (!paused)
		{
            particleIntensity = ((float)particles.particleCount / (float)particles.main.maxParticles);
            CalculateLightIntensity ();
			CalculateFlicker ();
		}
    }

    private void
    CalculateLightIntensity()
    {
        lights.intensity = maxLightIntensity * particleIntensity;
    }

    private void
    CalculateFlicker()
    {
        lights.range = maxRange * (particleIntensity * Random.Range(lowerFlickerLimit, upperFlickerLimit));
    }

	public void
	Pause()
	{
		paused = true;
	}

	public void
	Unpause()
	{
		paused = false;
	}
}