﻿using UnityEngine;
using System.Collections;

public class ToggleHelpScreen : MonoBehaviour
{
	[SerializeField]
	GameObject helpScreen;

	[Header("Keymap")]
	[SerializeField] string toggleHelp;

	void Update ()
	{
		if (Input.GetButtonUp (toggleHelp))
		{
			print (0);
			if (helpScreen.activeInHierarchy)
			{
				helpScreen.SetActive (false);
			}
			else
			{
				helpScreen.SetActive (true);
			}
		}		
	}
}
