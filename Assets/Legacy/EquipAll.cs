﻿using Field.Sequences;
using UnityEngine;

public class EquipAll : MonoBehaviour
{
	void 
	Update ()
	{
		if (Input.GetKeyDown(KeyCode.K))
		{
			foreach (AddInventoryItemEvent item in FindObjectsOfType<AddInventoryItemEvent>())
			{
				if (item.GetItems().Count > 0)
				{
					StartCoroutine(item.LocalRun());
				}
			}
		}
	}
}