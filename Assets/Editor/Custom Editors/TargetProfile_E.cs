﻿using Battle;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TargetCollector))]
public class TargetCollector_E : Editor
{
	private SerializedObject script;

	private SerializedProperty _targets;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_targets = script.FindProperty ("targetProfiles");

	}

	public override void
	OnInspectorGUI()
	{
		script.Update ();
		{
			EdFunctions.ShowSO (_targets, "Profile", true, true);
		}
		script.ApplyModifiedProperties ();
	}
}