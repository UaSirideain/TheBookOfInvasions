﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Field.Sequences;


[CustomEditor(typeof(AmbulateEvent), true)]
public class AmbulateEvent_E : Editor
{
	SerializedObject script;

	SerializedProperty _destination;
	SerializedProperty _run;


	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_destination = script.FindProperty ("destination");
		_run = script.FindProperty ("run");

	}

	public override void
	OnInspectorGUI()
	{
		script.Update ();

		RenderSubClassSettings ();
		RenderDestinationOptions ();

		script.ApplyModifiedProperties ();
	}


	void
	RenderSubClassSettings()
	{
        DrawPropertiesExcluding(script, new string[] { "m_Script" });
    }


	void
	RenderDestinationOptions()
	{
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Destination", GUILayout.Width (75));

		if (_run.boolValue)
		{
			if (GUILayout.Button ("Run to", GUILayout.Width (105)))
			{
				_run.boolValue = false;
			}
		}
		else
		{
			if (GUILayout.Button ("Walk to", GUILayout.Width (105)))
			{
				_run.boolValue = true;
			}
		}

		_destination.objectReferenceValue = EditorGUILayout.ObjectField (_destination.objectReferenceValue, typeof(Transform), true);

		EditorGUILayout.EndHorizontal ();
	}
}