﻿using UnityEngine;
using UnityEditor;
using Battle;

[CustomPropertyDrawer(typeof(EffectValueIndex))]
public class EffectValueIndex_P : PropertyDrawer
{
	public override void
	OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty (position, label, property);
		{
			if (property.FindPropertyRelative ("present").boolValue == true)
			{
				EditorGUILayout.BeginHorizontal ();
				{
					EditorGUILayout.LabelField (property.FindPropertyRelative ("valueReference").stringValue, GUILayout.Width (100));
					property.FindPropertyRelative ("effect").objectReferenceValue = EditorGUILayout.ObjectField (property.FindPropertyRelative ("effect").objectReferenceValue, typeof(Effect), false, GUILayout.Width (230));
					property.FindPropertyRelative ("valueIndex").intValue = EditorGUILayout.IntField (property.FindPropertyRelative ("valueIndex").intValue, GUILayout.Width (25));
				}
				EditorGUILayout.EndHorizontal ();
			}
		}
		EditorGUI.EndProperty ();
	}
}