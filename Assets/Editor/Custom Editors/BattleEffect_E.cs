﻿using Battle;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Effect), true)]
public class BattleEffect_E : Editor
{
	private SerializedObject script;

	private SerializedProperty _values;
	private SerializedProperty _helperText;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_values = script.FindProperty ("values");
	}

	public override void OnInspectorGUI()
	{
		script.Update ();
		{
			if (_values.arraySize > 0)
			{
				EditorGUILayout.LabelField("Values:", GUILayout.Width(190));
				EdFunctions.ShowSO(_values, "Value", false, false);
				EdFunctions.Space(3);
			}
			{
				EditorGUILayout.EndHorizontal();
				RenderSubClassSettings ();
			}
		}
		script.ApplyModifiedProperties ();
	}

	void
	RenderSubClassSettings()
	{
		DrawPropertiesExcluding (script, new string[]{"m_Script"});
	}
}