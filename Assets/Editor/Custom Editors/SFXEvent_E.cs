﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Field.Sequences;

[CustomEditor(typeof(SFXEvent), true)]
public class SFXEvent_E : Editor
{
	SerializedObject script;

	SerializedProperty _audioSource;
	SerializedProperty _stop;
	SerializedProperty _audioClip;
	SerializedProperty _volume;
	SerializedProperty _loop;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_audioSource = script.FindProperty ("audioSource");
		_stop = script.FindProperty ("stop");
		_audioClip = script.FindProperty ("audioClip");
		_volume = script.FindProperty ("volume");
		_loop = script.FindProperty ("loop");
	}
	
	public override void
	OnInspectorGUI()
	{
		script.Update ();

		RenderAudioSource ();
		if (_stop.boolValue)
		{
			if (GUILayout.Button ("Stop SFX"))
			{
				_stop.boolValue = false;
			}
			EditorGUILayout.EndHorizontal ();
		}
		else
		{
			if (GUILayout.Button ("Start SFX"))
			{
				_stop.boolValue = true;
			}
			EditorGUILayout.EndHorizontal ();

			RenderClip ();
		}

		//RenderNextEvent ();

		script.ApplyModifiedProperties ();
	}

	void
	RenderClip()
	{
		EdFunctions.Space (2);

		EditorGUILayout.LabelField ("Clip settings", EditorStyles.boldLabel);

		EditorGUILayout.BeginHorizontal ();
		_audioClip.objectReferenceValue = EditorGUILayout.ObjectField (_audioClip.objectReferenceValue, typeof(AudioClip), true, GUILayout.Width(300));
		_loop.boolValue = EditorGUILayout.Toggle (_loop.boolValue, GUILayout.Width(15));
		EditorGUILayout.LabelField ("Loop", GUILayout.Width(30));
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		_volume.floatValue = EditorGUILayout.Slider (_volume.floatValue, 0f, 1f);
		EditorGUILayout.EndHorizontal ();
	}

	void
	RenderAudioSource()
	{
		EditorGUILayout.LabelField ("Audio Source", EditorStyles.boldLabel);
		EditorGUILayout.BeginHorizontal ();
		_audioSource.objectReferenceValue = EditorGUILayout.ObjectField (_audioSource.objectReferenceValue, typeof(AudioSource), true, GUILayout.Width(300));
	}

//	void
//	RenderNextEvent()
//	{
//		EdFunctions.Space (3);
//
//		EditorGUILayout.LabelField ("Next Event", EditorStyles.boldLabel);
//		_nextEvent.objectReferenceValue = EditorGUILayout.ObjectField (_nextEvent.objectReferenceValue, typeof(FieldEvent), true);
//	}
}