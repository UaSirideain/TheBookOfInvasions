﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Battle;

[CustomPropertyDrawer(typeof(BattleEffectValue))]
public class BattleEffectValue_P : PropertyDrawer
{
	public override void
	OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty (position, label, property);
		{
			EditorGUILayout.BeginHorizontal ();
			{
				//EditorGUILayout.LabelField ("", GUILayout.Width (25));
				//EditorGUILayout.LabelField (property.FindPropertyRelative ("inspectorName").stringValue + ":", GUILayout.Width (90));

				//EditorGUILayout.EndHorizontal ();

				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("", GUILayout.Width (25));

				EditorGUILayout.LabelField ("Use value from", GUILayout.Width (90));
				{
					property.FindPropertyRelative ("setBy").enumValueIndex = (int)(SetByType)EditorGUILayout.EnumPopup ((SetByType)property.FindPropertyRelative ("setBy").enumValueIndex, GUILayout.Width (70));

					if (property.FindPropertyRelative ("setBy").enumValueIndex == (int)SetByType.Persistency)
					{
						property.FindPropertyRelative ("newReplacementValue").enumValueIndex = (int)(NewReplacementType)EditorGUILayout.EnumPopup ((NewReplacementType)property.FindPropertyRelative ("newReplacementValue").enumValueIndex, GUILayout.Width (110));
						EditorGUILayout.EndHorizontal ();
					}
					else
					{
						//EditorGUILayout.EndHorizontal ();

						//EditorGUILayout.BeginHorizontal ();
						{
						//	property.FindPropertyRelative ("actionType").enumValueIndex = (int)(ActionType)EditorGUILayout.EnumPopup ((ActionType)property.FindPropertyRelative ("actionType").enumValueIndex, GUILayout.Width (60));
							property.FindPropertyRelative ("type").enumValueIndex = (int)(ModificationType)EditorGUILayout.EnumPopup ((ModificationType)property.FindPropertyRelative ("type").enumValueIndex, GUILayout.Width (110));
							if (property.FindPropertyRelative ("type").enumValueIndex != (int)ModificationType.DisableSkill && property.FindPropertyRelative ("type").enumValueIndex != (int)ModificationType.EnableSkill)
							{
								EditorGUILayout.LabelField("by", GUILayout.Width(20));
								property.FindPropertyRelative ("value").intValue = EditorGUILayout.IntField (property.FindPropertyRelative ("value").intValue, GUILayout.Width (25));
							}
						}
						EditorGUILayout.EndHorizontal ();
					}
				}
			}
		}
		EditorGUI.EndProperty ();
	}
}