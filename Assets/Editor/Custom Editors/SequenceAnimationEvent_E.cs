﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Field.Sequences;


[CustomEditor(typeof(SequenceAnimationEvent), true)]
public class SequenceAnimationEvent_E : Editor
{
	SerializedObject script;
    	
	SerializedProperty _animation;
	SerializedProperty _waitForEndOfAnimation;
    
    
	public void
	OnEnable()
	{
		script = new SerializedObject (target);
        
		_animation = script.FindProperty ("animationType");
		_waitForEndOfAnimation = script.FindProperty ("waitForEndOfAnimation");
        
	}


	public override void
	OnInspectorGUI()
	{
		script.Update ();

        RenderSubClassSettings();
		RenderAnimationOptions ();
        
		//RenderPOCSettings ();

		//RenderNextEvent ();

		script.ApplyModifiedProperties ();
	}

    void
    RenderSubClassSettings()
    {
        DrawPropertiesExcluding(script, new string[] { "m_Script" });
    }


    void
	RenderAnimationOptions()
	{
		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField ("Animation", GUILayout.Width (75));
		_animation.enumValueIndex = (int)(AnimationType)EditorGUILayout.EnumPopup ((AnimationType)_animation.enumValueIndex, GUILayout.Width(200));

        if (_waitForEndOfAnimation.boolValue)
        {
            if (GUILayout.Button("Wait for animation"))
            {
                _waitForEndOfAnimation.boolValue = false;
            }
        }
        else if (_waitForEndOfAnimation.boolValue == false)
        {
            if (GUILayout.Button("Run next event"))
            {
                _waitForEndOfAnimation.boolValue = true;
            }
        }

        EditorGUILayout.EndHorizontal ();
	}
    

	void
	RenderPOCSettings()
	{
//		EdFunctions.Space (2);
//
//		/*if(_contactEvents.arraySize == 0)
//		{
//			_runEventsOnPointOfContact.boolValue = false;
//			if (GUILayout.Button ("Don't run events on Point of Contact"))
//			{
//				_contactEvents.arraySize += 1;
//			}
//		}
//		else if(_contactEvents.arraySize > 0)
//		{*/
//			EdFunctions.Show (_contactEvents, "POC Event");
//		//}
	}


	void
	RenderNextEvent()
	{
//		EdFunctions.Space (3);
//		EditorGUILayout.BeginHorizontal ();
//
//		EditorGUILayout.LabelField ("Next Event", GUILayout.Width (75));
//		//_nextEvent.objectReferenceValue = EditorGUILayout.ObjectField (_nextEvent.objectReferenceValue, typeof(FieldEvent), true);
//		_nextEvent.boolValue = EditorGUILayout.Toggle (_nextEvent.boolValue, GUILayout.Width(50));//s
//
//		EditorGUILayout.EndHorizontal ();
	}
}