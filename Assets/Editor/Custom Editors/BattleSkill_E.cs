﻿using Battle;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BattleSkill))]
public class BattleSkill_E : Editor
{
	private SerializedObject script;

	private SerializedProperty _uiName;
	private SerializedProperty _manaCost;
	private SerializedProperty _uiDescription;

	private SerializedProperty _effectValueIndices;

	private SerializedProperty _action;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_uiName = script.FindProperty ("uiName");
		_manaCost = script.FindProperty ("manaCost");
		_uiDescription = script.FindProperty ("uiDescription");

		_effectValueIndices = script.FindProperty ("effectValueIndices");

		_action = script.FindProperty ("action");
	}

	public override void OnInspectorGUI()
	{
		script.Update ();
		{
			RenderCardFaceInfo ();
			RenderValues ();
			RenderActionField ();
		}
		script.ApplyModifiedProperties ();
	}

	void
	RenderCardFaceInfo()
	{
		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField ("Title & Cost", GUILayout.Width (100));
			EditorStyles.textField.wordWrap = true;
			_uiName.stringValue = EditorGUILayout.TextField (_uiName.stringValue, GUILayout.Width (140));
			_manaCost.intValue = EditorGUILayout.IntField (_manaCost.intValue, GUILayout.Width (25));
		}
		EditorGUILayout.EndHorizontal ();


		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField ("Description", GUILayout.Width (100));
			_uiDescription.stringValue = EditorGUILayout.TextArea (_uiDescription.stringValue, GUILayout.Height (34));
		}
		EditorGUILayout.EndHorizontal ();
	}

	void
	RenderValues()
	{
		if (_uiDescription.stringValue.Contains ("<value"))
		{
			EdFunctions.ShowSO (_effectValueIndices, "Values", false, false);
		}
		EdFunctions.Space (2);
	}

	void
	RenderActionField()
	{
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Action", GUILayout.Width (90));
		EditorGUILayout.PropertyField (_action, GUIContent.none);
		EditorGUILayout.EndHorizontal ();
	}
}