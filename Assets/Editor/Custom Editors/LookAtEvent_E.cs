﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Field.Sequences;

[CustomEditor(typeof(LookAtEvent), true)]
public class LookAtEvent_E : Editor
{
    SerializedObject script;

    SerializedProperty _lookAtTarget;
    SerializedProperty _keepFollowing;

    SerializedProperty _nextEvent;

    public void 
    OnEnable()
    {
        script = new SerializedObject(target);

        _lookAtTarget = script.FindProperty("target");

    }

    public override void
    OnInspectorGUI()
    {
        script.Update();

        RenderSubClassSettings();
        RenderLookAtOptions();

        script.ApplyModifiedProperties();
    }

    void
    RenderSubClassSettings()
    {
        DrawPropertiesExcluding(script, new string[] { "m_Script" });
    }

    void
    RenderLookAtOptions()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Look At", GUILayout.Width(75));

        _lookAtTarget.objectReferenceValue = EditorGUILayout.ObjectField(_lookAtTarget.objectReferenceValue, typeof(Transform), true);
        
        EditorGUILayout.EndHorizontal();
    }
}
