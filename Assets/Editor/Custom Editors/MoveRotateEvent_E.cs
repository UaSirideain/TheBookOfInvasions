﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Field.Sequences;

[CustomEditor(typeof(MoveRotateEvent), true)]
public class MoveRotateEvent_E : Editor
{
	private SerializedObject script;

	private SerializedProperty _objectToMove;
	private SerializedProperty _durationOfMovement;

	private SerializedProperty _move;
	//private SerializedProperty _startPosition;
	//private SerializedProperty _endPosition;

	private SerializedProperty _rotate;
	//private SerializedProperty _startRotation;
	//private SerializedProperty _endRotation;

	//private SerializedProperty _nextEvent;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_objectToMove = script.FindProperty ("objectToMove");
		_durationOfMovement = script.FindProperty ("durationOfMovement");

		_move = script.FindProperty ("move");
		//_startPosition = script.FindProperty ("startPosition");
		//_endPosition = script.FindProperty ("endPosition");

		_rotate = script.FindProperty ("rotate");
		//_startRotation = script.FindProperty ("startRotation");
		//_endRotation = script.FindProperty ("endRotation");

		//_nextEvent = script.FindProperty ("nextEvent");
	}

	public override void
	OnInspectorGUI()
	{
		script.Update ();

		RenderTargetSettings ();

		PositionSettings ();
		RotationSettings ();

		EdFunctions.Space (1);

		DurationSettings ();

		//EdFunctions.Space (3);

		RenderNextEvent ();

		script.ApplyModifiedProperties ();
	}


	void
	RenderTargetSettings()
	{
		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField ("Target", GUILayout.Width (80));
		_objectToMove.objectReferenceValue = EditorGUILayout.ObjectField (_objectToMove.objectReferenceValue, typeof(Transform), true);

		EditorGUILayout.EndHorizontal ();
	}


	void
	DurationSettings()
	{
		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField ("Duration", GUILayout.Width(80));
		_durationOfMovement.floatValue = EditorGUILayout.FloatField(_durationOfMovement.floatValue, GUILayout.Width(45));

		EditorGUILayout.EndHorizontal ();
	}

	void
	PositionSettings ()
	{
		MoveRotateEvent methodAccess = (MoveRotateEvent)target;

		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField ("Position", GUILayout.Width(60));
		_move.boolValue = EditorGUILayout.Toggle (_move.boolValue, GUILayout.Width (15));

		if (_move.boolValue)
		{
			if (GUILayout.Button ("Set as start"))
			{
				methodAccess.SetStartPosition ();
			}
			if (GUILayout.Button ("Set as end"))
			{
				methodAccess.SetEndPosition ();
			}
			if (GUILayout.Button ("Go to start"))
			{
				methodAccess.GoToStartPosition ();
			}
			if (GUILayout.Button ("Go to end"))
			{
				methodAccess.GoToEndPosition ();
			}
		}

		EditorGUILayout.EndHorizontal ();
	}

	void
	RotationSettings()
	{
		MoveRotateEvent methodAccess = (MoveRotateEvent)target;

		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField ("Rotation", GUILayout.Width(60));
		_rotate.boolValue = EditorGUILayout.Toggle (_rotate.boolValue, GUILayout.Width (15));

		if (_rotate.boolValue)
		{

			if (GUILayout.Button ("Set as start"))
			{
				methodAccess.SetStartRotation ();
			}
			if (GUILayout.Button ("Set as end"))
			{
				methodAccess.SetEndRotation ();
			}
			if (GUILayout.Button ("Go to start"))
			{
				methodAccess.GoToStartRotation ();
			}
			if (GUILayout.Button ("Go to end"))
			{
				methodAccess.GoToEndRotation ();
			}
		}

		EditorGUILayout.EndHorizontal ();
	}

	void
	RenderNextEvent()
	{
//		EditorGUILayout.BeginHorizontal ();
//
//		EditorGUILayout.LabelField ("Next Event", GUILayout.Width (80));
//		_nextEvent.objectReferenceValue = EditorGUILayout.ObjectField (_nextEvent.objectReferenceValue, typeof(FieldEvent), true);
//
//		EditorGUILayout.EndHorizontal ();
	}

}