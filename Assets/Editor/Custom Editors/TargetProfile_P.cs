﻿using Battle;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(TargetProfile))]
public class TargetProfile_P : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty (position, label, property);

		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField (property.FindPropertyRelative ("instructions").stringValue);
		}
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField ("Get target from", GUILayout.Width (105));
			property.FindPropertyRelative ("autoTarget").enumValueIndex = (int)(AutoTarget)EditorGUILayout.EnumPopup ((AutoTarget)property.FindPropertyRelative ("autoTarget").enumValueIndex, GUILayout.Width(130));

			if (property.FindPropertyRelative ("autoTarget").enumValueIndex == (int)AutoTarget.Persistency)
			{
				property.FindPropertyRelative ("persistencyAutoTarget").enumValueIndex = (int)(PersistencyTargets)EditorGUILayout.EnumPopup ((PersistencyTargets)property.FindPropertyRelative ("persistencyAutoTarget").enumValueIndex, GUILayout.Width(110));
			}

			if (property.FindPropertyRelative ("autoTarget").enumValueIndex == (int)AutoTarget.OtherTargetProfile)
			{
				property.FindPropertyRelative ("copyTargetsFrom").intValue = EditorGUILayout.IntField (property.FindPropertyRelative ("copyTargetsFrom").intValue, GUILayout.Width (50));
			}
		}
		EditorGUILayout.EndHorizontal ();

		if ((int)(AutoTarget)property.FindPropertyRelative ("autoTarget").enumValueIndex == 0)
		{
			EdFunctions.Space (1);

			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.LabelField ("Target Info", GUILayout.Width (105));
			property.FindPropertyRelative ("maxTargets").intValue = EditorGUILayout.IntField (property.FindPropertyRelative ("maxTargets").intValue, GUILayout.Width (25));
			property.FindPropertyRelative ("targetAlignment").intValue = (int)(Disposition)EditorGUILayout.EnumPopup ((Disposition)property.FindPropertyRelative ("targetAlignment").intValue);
			property.FindPropertyRelative ("targetType").intValue = (int)(TargetType)EditorGUILayout.EnumPopup ((TargetType)property.FindPropertyRelative ("targetType").intValue);

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.LabelField ("Instructions", GUILayout.Width (105));
			property.FindPropertyRelative ("displayInstructions").stringValue = EditorGUILayout.TextArea (property.FindPropertyRelative ("displayInstructions").stringValue, GUILayout.Height (34));

			EditorGUILayout.EndHorizontal ();
		}
			
		ShowConditionsList (property.FindPropertyRelative ("conditions"));

		EditorGUI.EndProperty ();
	}

	void
	ShowConditionsList(SerializedProperty list)
	{
		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField("Conditions", GUILayout.Width(63));

		if (GUILayout.Button("+", GUILayout.Width(17)))
		{
			list.arraySize += 1;
		}

		if (list.arraySize > 0)
		{
			if (GUILayout.Button ("-", GUILayout.Width (17)))
			{
				list.arraySize -= 1;
			}
		}

		if (list.arraySize > 0)
		{
			for (int i = 0; i < list.arraySize; i++)
			{
				if (i.EvenlyDivisibleBy(3))
				{
					EditorGUILayout.EndHorizontal ();
					EditorGUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField("", GUILayout.Width(105));
				}
				list.GetArrayElementAtIndex(i).enumValueIndex = (int)(TPCondition)EditorGUILayout.EnumPopup ((TPCondition)list.GetArrayElementAtIndex(i).intValue, GUILayout.Width(96));
			}
		}

		EditorGUILayout.EndHorizontal ();
	}
}