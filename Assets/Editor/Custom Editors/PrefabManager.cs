﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class PrefabManager : MonoBehaviour
{
	[SerializeField] List<GameObject> prefabs;

	void
	OnValidate()
	{
		if (gameObject.activeInHierarchy)
		{
			foreach (GameObject prefab in prefabs)
			{
				GameObject newPrefab = (GameObject)PrefabUtility.InstantiatePrefab (prefab);
				newPrefab.transform.SetParent (transform.parent);
				//newPrefab.name = prefab.name;
			}

			Destroy (gameObject);
		}
	}
}