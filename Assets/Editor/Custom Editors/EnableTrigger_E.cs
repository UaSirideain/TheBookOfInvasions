﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Field.Sequences;

[CustomEditor(typeof(EnableTrigger), true)]
public class EnableTrigger_E : Editor
{
	SerializedObject script;
	SerializedProperty _nextEvent;

	SerializedProperty _triggersToDisable;
	SerializedProperty _triggersToEnable;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);
		_nextEvent = script.FindProperty ("nextEvent");

		_triggersToDisable = script.FindProperty ("triggersToDisable");
		_triggersToEnable = script.FindProperty ("triggersToEnable");
	}

	public override void
	OnInspectorGUI()
	{
		script.Update ();

		EdFunctions.Show (_triggersToEnable, "Enablement");
		EdFunctions.Show (_triggersToDisable, "Disablement");

		RenderNextEvent ();

		script.ApplyModifiedProperties ();
	}



	void
	RenderNextEvent()
	{
		EdFunctions.Space (3);

		EditorGUILayout.LabelField ("Next Event", EditorStyles.boldLabel);
		_nextEvent.objectReferenceValue = EditorGUILayout.ObjectField (_nextEvent.objectReferenceValue, typeof(FieldEvent), true);
	}
}