﻿using Battle;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TargetList))]
public class TargetList_E : Editor
{
	private SerializedObject script;

	private SerializedProperty _profileIndex;
	private SerializedProperty _targetIndex;
	private SerializedProperty _capture;

	private SerializedProperty _effectValueIndices;

	private SerializedProperty _action;

	public void
	OnEnable()
	{
		script = new SerializedObject (target);

		_profileIndex = script.FindProperty ("profileIndex");
		_targetIndex = script.FindProperty ("targetIndex");
		_capture = script.FindProperty ("capture");
	}

	public override void OnInspectorGUI()
	{
		script.Update ();
		{
			RenderScript ();
		}
		script.ApplyModifiedProperties ();
	}

	void
	RenderScript()
	{
		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField ("Capture", GUILayout.Width (65));
			_capture.intValue = (int)(TargetCapturing)EditorGUILayout.EnumPopup((TargetCapturing)_capture.intValue, GUILayout.Width (140));
		}
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField ("Profile Index", GUILayout.Width (80));
			_profileIndex.intValue = EditorGUILayout.IntField(_profileIndex.intValue, GUILayout.Width (30));

			if (_capture.intValue == (int)TargetCapturing.SingleTargetAtIndex)
			{
				EditorGUILayout.LabelField ("Target Index", GUILayout.Width (80));
				_targetIndex.intValue = EditorGUILayout.IntField (_targetIndex.intValue, GUILayout.Width (30));
			}
		}
		EditorGUILayout.EndHorizontal ();
	}
}