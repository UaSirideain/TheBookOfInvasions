﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PointsWindow : EditorWindow
{
    AvatarInstantiator instantiator { get { return FindObjectOfType<AvatarInstantiator>(); } }

    [SerializeField] static List<CameraPoint> cameraPoints = new List<CameraPoint>();
    [SerializeField] static GameObject pointRoot;
	[SerializeField] static List<CameraPoint> pointRootChildren;
    [SerializeField] static Transform camTrans;

    [MenuItem("Window/Camera Points")]
    static void 
    Init()
    {
        PointsWindow window = GetWindow<PointsWindow>();
        Texture icon = AssetDatabase.LoadAssetAtPath<Texture>("Assets/_Sprites/Editor/camera.png");
        GUIContent titleContent = new GUIContent("Points", icon);
        window.titleContent = titleContent;
        window.Show();
    }

    [ExecuteInEditMode]
    static void
    Initialise()
    {
		if (GameObject.Find ("Camera Points"))
		{
			pointRoot = GameObject.Find ("Camera Points");
			pointRootChildren = pointRoot.GetComponentsInChildren<CameraPoint> ().ToList ();
			camTrans = GameObject.Find ("Field Camera").GetComponent<Camera> ().transform;
			Populate ();
		}
	}
    
    static Transform
    GetCorrectParent(int camera)
    {
        Transform thisParent;
        if (cameraPoints[camera] != null)
        {
            thisParent = cameraPoints[camera].transform.parent;
            
            return thisParent;
        }
        else
        {
            return null;
        }
    }

    static void 
    Populate()
    {
		HashSet<CameraPoint> camPointList = new HashSet<CameraPoint>();
		foreach (CameraPoint camPoint in pointRootChildren)
        {
            if (!camPointList.Contains(camPoint))
            {
                camPointList.Add(camPoint);
            }
        }
        cameraPoints = camPointList.ToList();
    }
		    
    private void 
    SwitchTo(int cameraPoint)
    {
        camTrans.position = cameraPoints[cameraPoint].transform.position;
        camTrans.rotation = cameraPoints[cameraPoint].transform.rotation;
		camTrans.GetComponent<Camera> ().fieldOfView = cameraPoints [cameraPoint].GetFOV ();
    }
    
    private void
    SetAsStart(int cameraPoint)
    {
        GameObject spawners = GameObject.Find("Stations");
        Transform orientationReference = cameraPoints[cameraPoint].GetOrientationOffset();
        SetCameraData(cameraPoints[cameraPoint]);
        OrientStations(spawners, orientationReference);
    }

    private void
    SetCameraData(CameraPoint point)
    {
        SerializedObject script = new SerializedObject(instantiator);
        SerializedProperty camPoint = script.FindProperty("camPoint");

        script.Update();

        camPoint.objectReferenceValue = point;

        script.ApplyModifiedProperties();
    }

    private void
    OrientStations(GameObject stations, Transform orientationReference)
    {
        stations.transform.position = orientationReference.position;
        stations.transform.rotation = orientationReference.rotation;
    }

    [SerializeField] Vector2 scrollPosition = Vector2.zero;
    [SerializeField] Transform thisParent;

    [ExecuteInEditMode]
    private void 
    OnGUI()
    {
        Initialise();
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false);

        for (int camera = 0; camera < cameraPoints.Count; camera++)
        {
            GUILayout.BeginHorizontal();

            thisParent = GetCorrectParent(camera);
            
            if (GUILayout.Button(thisParent.name + cameraPoints[camera].name.Replace("Point", ""), GUILayout.Width(340)))
            {
                SwitchTo(camera);
            }

            if (GUILayout.Button("Spawn", GUILayout.Width(100)))
            {
                SetAsStart(camera);
                SwitchTo(camera);
            }

            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();
    }
}