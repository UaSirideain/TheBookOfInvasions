﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Points : EditorWindow
{
//    [SerializeField] static List<CameraPoint> cameraPoints = new List<CameraPoint>();
//    [SerializeField] static GameObject pointRoot;
//	[SerializeField] static List<CameraPoint> pointRootChildren;
//    [SerializeField] static Transform camTrans;
//
//    [MenuItem("Window/Camera Points")]
//    static void 
//    Init()
//    {
//        Points window = GetWindow<Points>();
//        Texture icon = AssetDatabase.LoadAssetAtPath<Texture>("Assets/_Sprites/Editor/camera.png");
//        GUIContent titleContent = new GUIContent("Points", icon);
//        window.titleContent = titleContent;
//        window.Show();
//    }
//
//    [ExecuteInEditMode]
//    static void
//    Initialise()
//    {
//		if (GameObject.Find ("Camera Points"))
//		{
//			pointRoot = GameObject.Find ("Camera Points");
//			pointRootChildren = pointRoot.GetComponentsInChildren<CameraPoint> ().ToList ();
//			camTrans = Camera.main.transform;
//			Populate ();
//		}
//    }
//    
//    static Transform
//    GetCorrectParent(int camera)
//    {
//        Transform thisParent;
//        if (cameraPoints[camera] != null)
//        {
//            if (cameraPoints[camera].transform.parent.name.Contains("Spawn Point"))
//            {
//                thisParent = cameraPoints[camera].transform.parent.parent;
//            }
//            else
//            {
//                thisParent = cameraPoints[camera].transform.parent;
//            }
//            return thisParent;
//        }
//        else
//        {
//            return null;
//        }
//    }
//
//    static void 
//    Populate()
//    {
//		HashSet<CameraPoint> gizmoSet = new HashSet<CameraPoint>();
//		foreach (CameraPoint gizmo in pointRootChildren)
//        {
//            if (!gizmoSet.Contains(gizmo) && !gizmo.name.Contains("Camera Point"))
//            {
//                gizmoSet.Add(gizmo);
//            }
//        }
//        cameraPoints = gizmoSet.ToList();
//        cameraPoints = cameraPoints.OrderBy(x => GetCorrectParent(cameraPoints.IndexOf(x)).name).ThenBy(x => x.name).ToList();
//    }
//		    
//    private void 
//    SwitchTo(int cameraPoint)
//    {
//        camTrans.position = cameraPoints[cameraPoint].transform.position;
//        camTrans.rotation = cameraPoints[cameraPoint].transform.rotation;
//		camTrans.GetComponent<Camera> ().fieldOfView = cameraPoints [cameraPoint].GetFOV ();
//    }
//    
//    private void
//    SetAsStart(int cameraPoint)
//    {
//        GameObject spawners = GameObject.Find("Stations");
//        Transform spawnPoint = cameraPoints[cameraPoint].transform.parent.GetComponentInChildren<MovementTriggerCollider>().transform;
//        spawners.transform.position = spawnPoint.position;
//        spawners.transform.rotation = spawnPoint.rotation;
//    }
//
//    [SerializeField] Vector2 scrollPosition = Vector2.zero;
//    [SerializeField] Transform thisParent;
//
//    [ExecuteInEditMode]
//    private void 
//    OnGUI()
//    {
//        Initialise();
//        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false);
//
//        for (int camera = 0; camera < cameraPoints.Count; camera++)
//        {
//            GUILayout.BeginHorizontal();
//
//            thisParent = GetCorrectParent(camera);
//            
//            if (GUILayout.Button(thisParent.name + cameraPoints[camera].name.Replace("Point", ""), GUILayout.Width(340)))
//            {
//                SwitchTo(camera);
//            }
//
//            if (GUILayout.Button("...", GUILayout.Width(25)))
//            {
//				SwitchTo (camera);
//                SetAsStart(camera);
//            }
//
//            GUILayout.EndHorizontal();
//        }
//
//        GUILayout.EndScrollView();
//    }
}