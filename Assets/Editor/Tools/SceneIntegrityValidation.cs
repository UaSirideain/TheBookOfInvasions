﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class SceneIntegrityValidation : EditorWindow
{
	[MenuItem("Window/SceneIntegrity")]
	static void
	Initialise()
	{
		SceneIntegrityValidation window = GetWindow<SceneIntegrityValidation> ();
		//Texture icon = AssetDatabase.LoadAssetAtPath<Texture> ("");
		GUIContent titleContent = new GUIContent("Scene Integrity");
		window.titleContent = titleContent;
		window.Show ();
	}

	static List<Rigidbody> rigidbodiesInScene = new List<Rigidbody>();

	[ExecuteInEditMode]
	static void
	VerifyRigidbodies()
	{
		rigidbodiesInScene.Clear ();
		rigidbodiesInScene.AddRange(FindObjectsOfType<Rigidbody> ());

		foreach (Rigidbody rb in rigidbodiesInScene)
		{
			if (rb.GetComponent<RigidbodyPauser> () == null)
			{
				Debug.Log (rb.name + " has not been set up for pausing.");
			}
		}
	}

	static List<ParticleSystem> pfxInScene = new List<ParticleSystem>();

	[ExecuteInEditMode]
	static void
	VerifyPFX()
	{
		pfxInScene.Clear ();
		pfxInScene.AddRange(FindObjectsOfType<ParticleSystem> ());

		foreach (ParticleSystem pfx in pfxInScene)
		{
			if (pfx.GetComponent<PFXPauser> () == null)
			{
				Debug.Log (pfx.name + " has not been set up for pausing.");
			}
		}
	}

	[ExecuteInEditMode]
	void
	OnGUI()
	{
		if(GUILayout.Button ("Verify Rigidbodies are Pausable for this scene."))
		{
			VerifyRigidbodies ();
		}
		if (GUILayout.Button ("Verify PFX in scene are Pausable."))
		{
			VerifyPFX ();
		}
	}
}