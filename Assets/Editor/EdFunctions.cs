﻿using UnityEngine;
using UnityEditor;
using Battle;

public static class EdFunctions
{
	public static void
	Show(SerializedProperty list, string listType)
	{
		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField(listType + "s");

		if (GUILayout.Button ("Add " + listType, GUILayout.Width (140)))
			list.arraySize += 1;


		EditorGUILayout.EndHorizontal ();

		for (int i = 0; i < list.arraySize; i++)
		{
			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i), GUIContent.none);

			if (GUILayout.Button ("Remove", GUILayout.Width (140)))
			{
				int oldListSize = list.arraySize;
				list.DeleteArrayElementAtIndex (i);
				if (oldListSize == list.arraySize)
					list.DeleteArrayElementAtIndex (i);
			}

			EditorGUILayout.EndHorizontal ();
		}
	}

	public static void
	ShowSO(SerializedProperty list, string listType, bool canSetQuantity, bool showNumber)
	{
		EditorGUILayout.BeginHorizontal ();
		//		if (list.arraySize != 0)
		//		{
		//			EditorGUILayout.LabelField (listType + "s", GUILayout.Width (195));
		//		}
		//		else
		//		{
		//		EditorGUILayout.LabelField (listType, GUILayout.Width (300));
		//		EditorGUILayout.LabelField ("", GUILayout.Width (299));
		//		}
		if (canSetQuantity)
		{
			if (list.arraySize > 0)
			{
				EditorGUILayout.LabelField(listType, GUILayout.Width(195));
				//                for (int i = 0; i < list.arraySize; i++)
				//                {
				////                    if (GUILayout.Button("Remove", GUILayout.Width(100)))
				////                    {
				////                        list.DeleteArrayElementAtIndex(i);
				////                    }
				//                }
				if (GUILayout.Button ("Remove", GUILayout.Width (100)))
				{
					list.arraySize -= 1;
				}
			}
			else
			{

				EditorGUILayout.LabelField(listType, GUILayout.Width(300));
			}

			if (GUILayout.Button("Add", GUILayout.Width(100)))
			{
				list.arraySize += 1;
			}
		}


		EditorGUILayout.EndHorizontal ();

		if (list.arraySize > 0)
		{
			for (int i = 0; i < list.arraySize; i++)
			{
				if(showNumber)EditorGUILayout.LabelField ("#" + i);

				EditorGUI.PropertyField (new Rect (0, 0, 140, 140), list.GetArrayElementAtIndex (i), GUIContent.none);
			}
		}
	}

	public static void
	ShowSONew(SerializedProperty list)
	{
		if (list.arraySize > 0)
		{
			for (int i = 0; i < list.arraySize; i++)
			{
				EditorGUILayout.BeginHorizontal ();
				{
					EditorGUI.PropertyField (new Rect (0, 0, 140, 140), list.GetArrayElementAtIndex (i), GUIContent.none);
				}
				EditorGUILayout.EndHorizontal ();
			}
		}

		EditorGUILayout.BeginHorizontal ();
		{
			EditorGUILayout.LabelField ("");
			if (GUILayout.Button ("+", GUILayout.Width (18)))
			{
				list.arraySize += 1;
			}
			if (list.arraySize > 0)
			{
				if (GUILayout.Button ("-", GUILayout.Width (18)))
				{
					list.arraySize -= 1;
				}
			}
		}
		EditorGUILayout.EndHorizontal ();
	}

	public static void
	Show2D(SerializedProperty list, SerializedProperty list2, string listType, string listType2)
	{
		EdFunctions.Space (3);

		EditorGUILayout.BeginHorizontal ();

		EditorGUILayout.LabelField(listType + "s");
		EditorGUILayout.LabelField(listType2, GUILayout.Width(40));

		if (GUILayout.Button ("Add " + listType, GUILayout.Width (140)))
		{
			list.arraySize += 1;
			list2.arraySize += 1;
		}
		EditorGUILayout.EndHorizontal ();

		for (int i = 0; i < list.arraySize; i++)
		{
			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i), GUIContent.none);
			EditorGUILayout.PropertyField (list2.GetArrayElementAtIndex (i), GUIContent.none, GUILayout.Width(40));

			if (GUILayout.Button ("Remove " + listType, GUILayout.Width (140)))
			{
				int oldListSize = list.arraySize;
				int oldList2Size = list2.arraySize;

				list.DeleteArrayElementAtIndex (i);
				list2.DeleteArrayElementAtIndex (i);

				if (oldListSize == list.arraySize)
					list.DeleteArrayElementAtIndex (i);

				if(oldList2Size == list2.arraySize)
					list2.DeleteArrayElementAtIndex (i);
			}

			EditorGUILayout.EndHorizontal ();
		}
	}

	public static void
	Space(int times)
	{
		for (int i = 0; i < times; i++)
		{
			EditorGUILayout.Space ();
		}
	}
}