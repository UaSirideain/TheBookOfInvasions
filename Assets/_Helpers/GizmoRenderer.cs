﻿using UnityEngine;

public class GizmoRenderer : MonoBehaviour
{
    bool inTrigger = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AvatarFacade>())
        {
            if (other.GetComponent<AvatarFacade>().IsActiveCharacter())
            {
                inTrigger = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<AvatarFacade>())
        {
            if (other.GetComponent<AvatarFacade>().IsActiveCharacter())
            {
                inTrigger = false;
            }
        }
    }

    [Header("Size")]
    [SerializeField] Vector3 boxSize = new Vector3(1f, 1f, 1f);
    [Header("Colours")]
    [SerializeField] Color colourNormal = new Color(0.5f, 0.5f, 0.5f, 0.25f);
    [SerializeField] Color colourInCollider = new Color(1f, 0f, 0f, 0.25f);

    private void 
    OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        if (inTrigger)
        {
            Gizmos.color = colourInCollider;
        }
        else
        {
            Gizmos.color = colourNormal;
        }
        Gizmos.DrawCube(Vector3.zero, boxSize);
    }
}
