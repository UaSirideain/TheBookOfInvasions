﻿using UnityEngine;
using System.Collections;

public class ZeroTransform : MonoBehaviour
{
	void
	OnValidate()
	{
		transform.localPosition = new Vector3 (0, 0, 0);
		transform.localRotation = new Quaternion (0, 0, 0, 0);
		transform.localScale = new Vector3 (1, 1, 1);
	}
}