﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MonoBehaviourExtensions
{
	public static List<Transform>
	ConvertToListOfTransforms<T>(this List<T> pts) where T : MonoBehaviour
	{
		List<Transform> ts = new List<Transform>();

		foreach (T pt in pts)
		{
			ts.Add(pt.transform);
		}

		return ts;
	}
}