﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class TransformExtensions
{
	public static float 
	CalculateLocalDistanceTo(this Transform trans1, Transform trans2, string axis)
	{
		Vector3 distance = trans1.InverseTransformPoint(trans2.position);

		if (axis == "x")
			return distance.x;
		else
		if (axis == "y")
			return distance.y;
		else
		if (axis == "z")
			return distance.z;
		else
			return 0.0f;
	}

	public static IEnumerator
	RotateTowards(this Transform trans1, Vector3 trans2, float duration)
	{
		Vector3 targetDirection = trans2 - trans1.position;
		float lerpTime = 0.0f;
		while (lerpTime < 1.0f)
		{
			Vector3 newDirection = Vector3.RotateTowards(trans1.forward, targetDirection, lerpTime, 0.0f);
			lerpTime += Time.deltaTime / duration;

			trans1.rotation = Quaternion.LookRotation(newDirection);

			yield return null;
		}
	}

	public static void
	SetAsChildOf(this Transform t1, Transform t2)
	{
		t1.SetParent(t2);
		t1.Rescale();
	}

	/// <summary>
	/// Sets the transform's local scale to (1f, 1f, 1f).
	/// </summary>
	/// <param name="t">Transform.</param>
	public static void
	Rescale(this Transform t)
	{
		t.localScale = new Vector3(1f, 1f, 1f);
	}

	/// <summary>
	/// Makes the given game objects children of the transform.
	/// </summary>
	/// <param name="this">Parent transform.</param>
	/// <param name="children">Game objects to make children</param>
	public static void
	AddChildren(this Transform @this, params GameObject[] children)
	{
		Array.ForEach(children, child => child.transform.parent = @this);
	}

	/// <summary>
	/// Makes a given game objects of given components children of the transform.
	/// </summary>
	/// <param name="this">Parent transform.</param>
	/// <param name="children">Components of game objects to make children.</param>
	public static void 
	AddChildren(this Transform @this, params Component[] children)
	{
		Array.ForEach(children, child => child.transform.parent = @this);
	}

	/// <summary>
	/// Sets the position of a transform's children to zero.
	/// </summary>
	/// <param name="this">Transform.</param>
	/// <param name="recursive">bool.</param>
	public static void
	ResetChildPositions(this Transform @this, bool recursive = false)
	{
		foreach (Transform child in @this)
		{
			child.position = Vector3.zero;

			if (recursive)
			{
				child.ResetChildPositions(recursive);
			}
		}
	}

	public static Transform
	FindDeepChild(this Transform aParent, string aName)
	{
		foreach (Transform child in aParent)
		{
			if (child.name == aName)
			{
				return child;
			}

			Transform result = child.FindDeepChild(aName);
			if (result != null)
			{
				return result;
			}
		}
		return null;
	}

	public static Transform
	GetClosestTo(this List<Transform> ts, Transform me)
	{
		Transform closest = null;
		float currentDistance = 0f;

		foreach (Transform t in ts)
		{
			float distance = Vector3.Distance(me.position, t.position);
			if (closest == null || distance < currentDistance)
			{
				currentDistance = distance;
				closest = t;
			}
		}

		return closest;
	}

	public static Transform
	GetNavMeshClosestTo(this List<Transform> ts, Transform me)
	{
		Transform closest = null;
		float currentDistance = 0f;

		foreach (Transform t in ts)
		{
			NavMeshPath path = new NavMeshPath();
			NavMesh.CalculatePath(me.position, t.position, NavMesh.AllAreas, path);
			float distance = path.GetDistance();

			if (closest == null || distance < currentDistance)
			{
				currentDistance = distance;
				closest = t;
			}
		}

		return closest;
	}

	/// <summary>
	/// Searches this game object's hierarchy of objects for a component of type T.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="this">This transform.</param>
	/// <returns>A component of type T if it exists on any object in this game object's hierarchy.</returns>
	public static T
	FindComponentInHierarchy<T>(this Transform @this) where T : Component
	{
		if (@this == null) return null;

		T component = @this.GetComponent<T>();
		if (component == null)
		{
			component = @this.FindComponentInChildren<T>();
			if (component == null)
			{
				component = @this.FindComponentInParents<T>();
			}
		}

		return component;
	}

	/// <summary>
	/// Searches this game object's parent objects for a component of type T.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="this">This transform.</param>
	/// <param name="recursive">If all parents to the root object should be searched.</param>
	/// <returns>A component of type T if it exists on any of this game object's parents.</returns>
	public static T
	FindComponentInParents<T>(this Transform @this, bool recursive = true) where T : Component
	{
		if (@this == null) return null;

		@this = @this.parent;

		T component = @this.GetComponent<T>();
		if (component) return component;

		if (recursive) return @this.FindComponentInParents<T>();
		else return null;
	}

	/// <summary>
	/// Searches this game object's child objects for a component of type T.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="this">This transform</param>
	/// <param name="recursive">If all children in the hierarchy should be searched.</param>
	/// <returns>A component of type T if it exists on any of this game object's children.</returns>
	public static T
	FindComponentInChildren<T>(this Transform @this, bool recursive = true) where T : Component
	{
		if (@this == null) return null;

		foreach (Transform child in @this)
		{
			@this = child;

			T component = @this.GetComponent<T>();
			if (component) return component;

			if (recursive) return @this.FindComponentInChildren<T>();
		}
		return null;
	}
}