﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Currently, Unity only supports assigning of layers via setting an integer. As they are defined in the UnityEditor through string, the whole thing is very fragile.
/// This script allows us to use human readable names when assigning layers.
/// 
/// For example: When assinging the layer of an object through code, you would normally have to write:
/// 
/// 			gameObject.layer = 19;
/// 
/// This script allows us to write:
/// 
/// 			gameObject.layer = UnityLayers.LayerOfType(UnityLayer.UIModels);
/// 
/// </summary>

public enum UnityLayer{UIModels}

public static class UnityLayers
{
	static Dictionary<UnityLayer, int> layerIndex = new Dictionary<UnityLayer, int>()
	{
		{UnityLayer.UIModels, 20}
	};

	static int
	LayerOfType(UnityLayer targetType)
	{
		return layerIndex [targetType];
	}

	public static void
	SetAs(this GameObject go, UnityLayer layer, bool affectChildren = true)
	{
		go.layer = UnityLayers.LayerOfType (layer);

		foreach (Transform child in go.transform)
		{
			child.gameObject.layer = layerIndex[layer];
		}
	}

	public static void
	SetAs(this Transform t, UnityLayer layer, bool affectChildren = true)
	{
		t.gameObject.layer = UnityLayers.LayerOfType (layer);

		foreach (Transform child in t.gameObject.transform)
		{
			child.gameObject.layer = layerIndex[layer];
		}
	}
}