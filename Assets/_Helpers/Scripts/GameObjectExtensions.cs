﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
    /// <summary>
    /// Gets a component attached to the given game object.
    /// If one isn't found, a new one is attached and returned.
    /// </summary>
    /// <param name="this">GameObject.</param>
    /// <returns>Previously or newly attached component.</returns>
    public static T
    GetOrAddComponent<T>(this GameObject @this) where T : Component
    {
        T component = @this.GetComponent<T>();

        if (component == null) return @this.AddComponent<T>();
        else return component;
    }

    /// <summary>
    /// Checks whether a game object has a component of type T attached.
    /// </summary>
    /// <param name="this">GameObject.</param>
    /// <returns>True when component is attached</returns>
    public static bool
    HasComponent<T>(this GameObject @this) where T : Component
    {
        return @this.GetComponent<T>() != null;
    }
}