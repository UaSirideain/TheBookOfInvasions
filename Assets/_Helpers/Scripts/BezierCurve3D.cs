﻿using UnityEngine;

namespace BezierCurves
{
    public static class BezierCurve3D
    {
        public static Vector3 
        GetPointOnCubicCurve(float distance, BezierPoint3D startPoint, BezierPoint3D endPoint)
        {
            return GetPointOnCubicCurve(distance, startPoint.Position, endPoint.Position, startPoint.RightHandlePosition, endPoint.LeftHandlePosition);
        }

        public static Quaternion 
        GetRotationOnCubicCurve(float distance, Vector3 up, BezierPoint3D startPoint, BezierPoint3D endPoint)
        {
            return GetRotationOnCubicCurve(distance, up, startPoint.Position, endPoint.Position, startPoint.RightHandlePosition, endPoint.LeftHandlePosition);
        }

        public static Vector3 
        GetTangentOnCubicCurve(float distance, BezierPoint3D startPoint, BezierPoint3D endPoint)
        {
            return GetTangentOnCubicCurve(distance, startPoint.Position, endPoint.Position, startPoint.RightHandlePosition, endPoint.LeftHandlePosition);
        }

        public static Vector3 
        GetBinormalOnCubicCurve(float distance, Vector3 up, BezierPoint3D startPoint, BezierPoint3D endPoint)
        {
            return GetBinormalOnCubicCurve(distance, up, startPoint.Position, endPoint.Position, startPoint.RightHandlePosition, endPoint.LeftHandlePosition);
        }

        public static Vector3 
        GetNormalOnCubicCurve(float distance, Vector3 up, BezierPoint3D startPoint, BezierPoint3D endPoint)
        {
            return GetNormalOnCubicCurve(distance, up, startPoint.Position, endPoint.Position, startPoint.RightHandlePosition, endPoint.LeftHandlePosition);
        }

        public static float 
        GetApproximateLengthOfCubicCurve(BezierPoint3D startPoint, BezierPoint3D endPoint, int sampling)
        {
            return GetApproximateLengthOfCubicCurve(startPoint.Position, endPoint.Position, startPoint.RightHandlePosition, endPoint.LeftHandlePosition, sampling);
        }

        private static Vector3
        GetPointOnCubicCurve(float distance, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            float progress = distance;
            float progressRemaining = 1f - progress;
            float progressSquared = progress * progress;
            float progressRemainingSquared = progressRemaining * progressRemaining;
            float progressRemainingCubed = progressRemainingSquared * progressRemaining;
            float progressCubed = progressSquared * progress;

            Vector3 result =
                (progressRemainingCubed) * startPosition +
                (3f * progressRemainingSquared * progress) * startTangent +
                (3f * progressRemaining * progressSquared) * endTangent +
                (progressCubed) * endPosition;

            return result;
        }

        private static Quaternion
        GetRotationOnCubicCurve(float distance, Vector3 up, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            Vector3 tangent = GetTangentOnCubicCurve(distance, startPosition, endPosition, startTangent, endTangent);
            Vector3 normal = GetNormalOnCubicCurve(distance, up, startPosition, endPosition, startTangent, endTangent);

            return Quaternion.LookRotation(tangent, normal);
        }

        private static Vector3
        GetTangentOnCubicCurve(float distance, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            float progress = distance;
            float progressRemaining = 1f - progress;
            float progressRemainingSquared = progressRemaining * progressRemaining;
            float progressSquared = progress * progress;

            Vector3 tangent =
                (-progressRemainingSquared) * startPosition +
                (progressRemaining * (progressRemaining - 2f * progress)) * startTangent -
                (progress * (progress - 2f * progressRemaining)) * endTangent +
                (progressSquared) * endPosition;

            return tangent.normalized;
        }

        private static Vector3
        GetBinormalOnCubicCurve(float distance, Vector3 up, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            Vector3 tangent = GetTangentOnCubicCurve(distance, startPosition, endPosition, startTangent, endTangent);
            Vector3 binormal = Vector3.Cross(up, tangent);

            return binormal.normalized;
        }

        private static Vector3
        GetNormalOnCubicCurve(float distance, Vector3 up, Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent)
        {
            Vector3 tangent = GetTangentOnCubicCurve(distance, startPosition, endPosition, startTangent, endTangent);
            Vector3 binormal = GetBinormalOnCubicCurve(distance, up, startPosition, endPosition, startTangent, endTangent);
            Vector3 normal = Vector3.Cross(tangent, binormal);

            return normal.normalized;
        }

        private static float 
        GetApproximateLengthOfCubicCurve(Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent, int sampling)
        {
            float length = 0f;
            Vector3 fromPoint = GetPointOnCubicCurve(0f, startPosition, endPosition, startTangent, endTangent);

            for (int i = 0; i < sampling; i++)
            {
                float time = (i + 1) / (float)sampling;
                Vector3 toPoint = GetPointOnCubicCurve(time, startPosition, endPosition, startTangent, endTangent);
                length += Vector3.Distance(fromPoint, toPoint);
                fromPoint = toPoint;
            }

            return length;
        }
    }
}