﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class of extension methods for Vector3s
/// </summary>
public static class Vector3Extensions
{
    /// <summary>
    /// Sets the X, Y and Z coordinates of a Vector3 to those given.
    /// </summary>
    /// <param name="this">Vector3.</param>
    /// <param name="x">X-coordinate.</param>
    /// <param name="y">Y-coordinate.</param>
    /// <param name="z">Z-coordinate.</param>
    /// <returns>A Vector3 with the given X, Y and Z coordinates.</returns>
    public static Vector3
    SetAll(this Vector3 @this, float x, float y, float z)
    {
        @this.x = x;
        @this.y = y;
        @this.z = z;
        return @this;
    }

    /// <summary>
    /// Sets the X and Y coordinates of a Vector3 to those given.
    /// </summary>
    /// <param name="this">Vector3.</param>
    /// <param name="x">X-coordinate.</param>
    /// <param name="y">Y-coordinate.</param>
    /// <returns>A Vector3 with the given X and Y coordinates.</returns>
    public static Vector3
    SetXY(this Vector3 @this, float x, float y)
    {
        @this.x = x;
        @this.y = y;
        return @this;
    }

    /// <summary>
    /// Sets the X and Z coordinates of a Vector3 to those given.
    /// </summary>
    /// <param name="this">Vector3.</param>
    /// <param name="x">X-coordinate.</param>
    /// <param name="z">Z-coordinate.</param>
    /// <returns>A Vector3 with the given X and Z coordinates.</returns>
    public static Vector3
    SetXZ(this Vector3 @this, float x, float z)
    {
        @this.x = x;
        @this.z = z;
        return @this;
    }

    /// <summary>
    /// Sets the Y and Z coordinates of a Vector3 to those given.
    /// </summary>
    /// <param name="this">Vector3.</param>
    /// <param name="y">Y-coordinate.</param>
    /// <param name="z">Z-coordinate.</param>
    /// <returns>A Vector3 with the given Y and Z coordinates.</returns>
    public static Vector3
    SetYZ(this Vector3 @this, float y, float z)
    {
        @this.y = y;
        @this.z = z;
        return @this;
    }
}