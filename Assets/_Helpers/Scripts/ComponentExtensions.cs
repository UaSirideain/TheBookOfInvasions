﻿using Battle;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


public static class ComponentExtensions 
{
	/// <summary>
	/// 
	/// </summary>
	/// <param name="copy">Component.</param>
	/// <param name="original">Component to copy.</param>
	/// <returns></returns>
	public static T CopyComponent<T>(this Component copy, T original) where T : Component
	{
		System.Type type = copy.GetType();

		if (type != original.GetType())
			return null;

		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] properties = type.GetProperties(flags);

		foreach (PropertyInfo property in properties)
		{
			if (property.IsDefined(typeof(System.ObsoleteAttribute), true))
			{
				//if property is obsolete, don't copy it
			}
			else if (property.CanWrite)
			{
				try
				{
					property.SetValue(copy, property.GetValue(original, null), null);
				}
				catch
				{
				}
			}
		}


		FieldInfo[] fields = type.GetFields(flags);

		foreach (FieldInfo field in fields)
		{
			field.SetValue(copy, field.GetValue(original));
		}

		return copy as T;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="comp">Component.</param>
	/// <param name="other">T.</param>
	/// <returns></returns>
	public static T
	GetCopyOf<T>(this Component comp, T other) where T : Component
	{
		System.Type type = comp.GetType();

		if (type != other.GetType())
		{
			return null;
		}

		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;

		PropertyInfo[] pinfos = type.GetProperties(flags);

		foreach (PropertyInfo pinfo in pinfos)
		{
			if (pinfo.CanWrite)
			{
				try
				{
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { }
			}
		}

		FieldInfo[] finfos = type.GetFields(flags);
		foreach (FieldInfo finfo in finfos)
		{
			finfo.SetValue(comp, finfo.GetValue(other));
		}

		return comp as T;
	}

	/// <summary>
	/// Attaches a component to the given component's game object.
	/// </summary>
	/// <param name="this">Component.</param>
	/// <returns>Newly attached component.</returns>
	public static T 
	AddComponent<T>(this Component @this) where T : Component
	{
		return @this.gameObject.AddComponent<T>();
	}

	/// <summary>
	/// Gets a component attached to the given component's game object.
	/// If one isn't found, a new one is attached and returned.
	/// </summary>
	/// <param name="this">Component.</param>
	/// <returns>Previously or newly attached component.</returns>
	public static T
	GetOrAddComponent<T>(this Component @this) where T : Component
	{
		T component = @this.GetComponent<T>();

		if (component == null) return @this.AddComponent<T>();
		else return component;
	}

	/// <summary>
	/// Checks whether a component's game object has a component of type T attached.
	/// </summary>
	/// <param name="this">Component.</param>
	/// <returns>True when component is attached.</returns>
	public static bool
	HasComponent<T>(this Component @this) where T : Component
	{
		return @this.GetComponent<T>() != null;
	}
}