﻿using Battle;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using UnityEditor;

public static class ExtensionMethods
{
	//Calculate distance between trans1 and trans2 on a single axis local to trans1
	/*public static float CalculateLocalDistanceTo(this Transform trans1, Transform trans2, string axis)
	{
		Vector3 distance = trans1.InverseTransformPoint (trans2.position);

		if (axis == "x")
			return distance.x;
		else
		if (axis == "y")
			return distance.y;
		else
		if (axis == "z")
			return distance.z;
		else
			return 0.0f;
	}
		
	public static IEnumerator
	RotateTowards(this Transform trans1, Vector3 trans2, float duration)
	{
		Vector3 targetDirection = trans2 - trans1.position;
		float lerpTime = 0.0f;
		while (lerpTime < 1.0f)
		{
			Vector3 newDirection = Vector3.RotateTowards(trans1.forward, targetDirection, lerpTime, 0.0f);
			lerpTime += Time.deltaTime / duration;

			trans1.rotation = Quaternion.LookRotation (newDirection);

			yield return null;
		}
	}

	public static void
	SetAsChildOf(this Transform t1, Transform t2)
	{
		t1.SetParent (t2);
		t1.Rescale ();
	}

	public static void
	Rescale(this Transform t)
	{
		t.localScale = new Vector3 (1f, 1f, 1f);
	}*/


	/// /// /// 


	/*public static T	CopyComponent<T>(this Component copy, T original) where T : Component
	{
		System.Type type = copy.GetType ();

		if (type != original.GetType ())
			return null;

		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] properties = type.GetProperties (flags);

		foreach (PropertyInfo property in properties)
		{
			if (property.IsDefined (typeof(System.ObsoleteAttribute), true))
			{
				//if property is obsolete, don't copy it
			}
			else if (property.CanWrite)
			{
				try
				{
					property.SetValue(copy, property.GetValue(original, null), null);
				}
				catch
				{
				}
			}
		}


		FieldInfo[] fields = type.GetFields (flags);

		foreach (FieldInfo field in fields)
		{
			field.SetValue (copy, field.GetValue(original));
		}

		return copy as T;
	}*/
		
	/*public static Transform
	FindDeepChild(this Transform aParent, string aName)
	{
		foreach (Transform child in aParent)
		{
			if (child.name == aName)
			{
				return child;
			}

			Transform result = child.FindDeepChild (aName);
			if (result != null)
			{
				return result;
			}
		}
		return null;
	}*/
		
	public static bool
	IsEven(this int i)
	{
		if (i == 0)
		{
			return true;
		}
		else if (i % 2 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    public static bool
    IsTypeOf<T>(this List<T> list, T type)
    {
		if (list.GetType () == type.GetType ())
		{
			return true;
		}
		else
		{
			return false;
		}
    }

	public static void
	Enable(this CanvasGroup canvas)
	{
		canvas.alpha = 1;
		canvas.blocksRaycasts = true;
	}

	public static void
	EnableTransparent(this CanvasGroup canvas)
	{
		canvas.alpha = 0;
		canvas.blocksRaycasts = true;
	}

	public static void
	Disable(this CanvasGroup canvas)
	{
		canvas.alpha = 0;
		canvas.blocksRaycasts = false;
	}

	public static IEnumerator
	Fade(this CanvasGroup canvas, float start, float end, float duration, bool blockRaycasts = false)
	{
		float t = 0f;
		while (t < 1.2f)
		{
			canvas.alpha = Mathf.Lerp (start, end, t);
			t += Time.deltaTime / duration;

			yield return null;
		}

		canvas.blocksRaycasts = blockRaycasts;
		canvas.alpha = end;
	}

	public static int
	NumericStringToInt(this string numericString)
	{
		int numeric;
		if (int.TryParse (numericString, out numeric))
		{
			return numeric;
		}
		else
		{
			Debug.Log ("Parsing failed. Return value is invalid.");
			return 0;
		}
	}
		
	public static bool
	NumericStringToArrayIndex(this string numericKey, out int result)
	{
		return numericToArrayInt.TryGetValue(numericKey, out result);
	}

	static Dictionary <string, int> numericToArrayInt = new Dictionary<string, int>()
	{
		{"1",	0},
		{"2", 	1},
		{"3", 	2},
		{"4", 	3},
		{"5", 	4},
		{"6", 	5},
		{"7", 	6},
		{"8", 	7},
		{"9", 	8},
		{"0", 	9},
		{"-", 	10},
		{"=", 	11}
	};

	public static string
	ArrayIndexToNumericString(this int arrayIndex)
	{
		Debug.Assert (arrayIndex >= 0 && arrayIndex <= 11);
		return arrayToString[arrayIndex];
	}

	static Dictionary <int, string> arrayToString = new Dictionary<int, string>()
	{
		{0, "1"},
		{1, "2"},
		{2, "3"},
		{3, "4"},
		{4, "5"},
		{5, "6"},
		{6, "7"},
		{7, "8"},
		{8, "9"},
		{9, "0"},
		{10, "-"},
		{11, "="}
	};

	public static void
	SetAsChildOf(this RectTransform rt, RectTransform newParent)
	{
		rt.SetParent (newParent);
		rt.localScale = new Vector3 (1, 1, 1);
		rt.transform.localPosition = new Vector3 (rt.transform.localPosition.x, rt.transform.localPosition.y, 0f);
		rt.Rescale ();
	}

	public static List<T>
	Excluding<T>(this List<T> list, int itemToExclude)
	{
		List<T> newList = new List<T> ();

		for (int i = 0; i < list.Count; i++)
		{
			if (i != itemToExclude)
			{
				newList.Add (list [i]);
			}
		}

		return newList;
	}

	public static List<T>
	Excluding<T>(this List<T> list, T itemToExclude)
	{
		List<T> newList = new List<T> ();

		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].Equals(itemToExclude) == false)
			{
				newList.Add (list [i]);
			}
		}

		return newList;
	}

	/*public static T
	GetCopyOf<T>(this Component comp, T other) where T : Component
	{
		Type type = comp.GetType ();

		if (type != other.GetType ())
		{
			return null;
		}

		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;

		PropertyInfo[] pinfos = type.GetProperties (flags);

		foreach (PropertyInfo pinfo in pinfos)
		{
			if (pinfo.CanWrite)
			{
				try
				{
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch{}
			}
		}

		FieldInfo[] finfos = type.GetFields (flags);
		foreach (FieldInfo finfo in finfos)
		{
			finfo.SetValue (comp, finfo.GetValue (other));
		}

		return comp as T;
	}*/

	public static void
	CopyValues(this Effect toModify, Effect toCopy)
	{
		toModify.SetValues (toCopy.GetValues ());
	}

	public static void
	WriteSource(this List<BattleEffectValue> values, Battle.Avatar source)
	{
		foreach (BattleEffectValue value in values)
		{
			value.WriteSource (source);
		}
	}

	public static void
	RemoveLast<T>(this List<T> list)
	{
		if (list.Count > 0)
		{
			list.RemoveAt (list.Count - 1);
		}
	}

	public static T
	GetLast<T>(this List<T> list)
	{
		if (list.Count > 0)
		{
			return list [list.Count - 1];
		}
		return default(T);
	}

	public static void
	EnsureSize<T>(this List<T> list, int targetSize) where T : new ()
	{
		while (list.Count < targetSize)
		{
			list.Add (new T ());
		}
	}

	public static void
	Sanitise<T>(this List<T> list)
	{
		int i = 0;
		while (i < list.Count)
		{
			if (list [i] == null)
			{
				list.RemoveAt (i);
			}
			else
			{
				i++;
			}
		}
	}

	public static bool
	Between(this int @this, int lower, int upper, bool inclusive = true)
	{
		if (inclusive) return @this >= lower || @this <= upper;
		else return @this > lower || @this < upper;
	}

	public static bool
	NotBetween(this int @this, int lower, int upper, bool inclusive = true)
	{
		return !@this.Between(lower, upper, inclusive);
	}

	public static void
	With<T>(this T @this, Action<T> act)
	{
		act(@this);
	}

	public static bool
	Is<T>(this object item) where T : class
	{
		return item is T;
	}

	public static bool
	IsNot<T>(this object item) where T : class
	{
		return !(item.Is<T>());
	}

	public static T
	As<T>(this object item) where T : class
	{
		return item as T;
	}

	public static bool
	In<T>(this T source, params T[] list)
	{
		if (source == null) throw new ArgumentNullException("source");
		return list.Contains(source);
	}

	public static bool
	EvenlyDivisibleBy(this int num, int target)
	{
		int newNum = num;
		newNum -= target;
		while (newNum >= target)
		{
			newNum -= target;
		}

		return newNum == 0;
	}

	public static float
	ToCanvasSpace(this float num)
	{
		num = num / Screen.width * 1920f;
		return num;
	}

	/*public static Transform
	GetClosestTo(this List<Transform> ts, Transform me)
	{
		Transform closest = null;
		float currentDistance = 0f;

		foreach (Transform t in ts)
		{
			float distance = Vector3.Distance (me.position, t.position);
			if (closest == null || distance < currentDistance)
			{
				currentDistance = distance;
				closest = t;
			}
		}

		return closest;
	}*/

	/*public static Transform
	GetNavMeshClosestTo(this List<Transform> ts, Transform me)
	{
		Transform closest = null;
		float currentDistance = 0f;

		foreach (Transform t in ts)
		{
			NavMeshPath path = new NavMeshPath ();
			NavMesh.CalculatePath(me.position, t.position, NavMesh.AllAreas, path);
			float distance = path.GetDistance ();

			if (closest == null || distance < currentDistance)
			{
				currentDistance = distance;
				closest = t;
			}
		}

		return closest;
	}*/

	public static float
	GetDistance(this NavMeshPath path)
	{
		float runningDistance = 0f;

		for (int i = 1; i < path.corners.Length; i++)
		{
			runningDistance += Vector3.Distance (path.corners[i], path.corners[i - 1]);
		}

		return runningDistance;
	}

	/*public static List<Transform>
	ConvertToListOfTransforms<T>(this List<T> pts) where T : MonoBehaviour
	{
		List<Transform> ts = new List<Transform> ();

		foreach (T pt in pts)
		{
			ts.Add (pt.transform);
		}

		return ts;
	}*/
}