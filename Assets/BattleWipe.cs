﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class BattleWipe : MonoBehaviour
{
    const bool testing = false;
	void
	Update()
    {
		if (Input.GetMouseButtonDown (1) && testing)
		{
			StartCoroutine(Do ());
		}
    }

	Camera cam { get { return GetComponent<Camera> (); } }
	MotionBlur motionBlur {get{return GetComponent<MotionBlur>();}}

	float startFOV;
	Vector3 startRotationEuler;
	float originalBlurAmount;

	const float targetFOV = 10f;
	const float targetRotation = 50f;

	IEnumerator
	Do()
	{
		motionBlur.enabled = true;
		yield return null;
		motionBlur.enabled = true;
		motionBlur.blurAmount = 100f;

		GetInitialSettings ();

		float t = 0f;

		while(t < 1f)
		{
			cam.fieldOfView = Mathfx.Sinerp (startFOV, targetFOV, t);
			transform.rotation = Quaternion.Euler(Mathfx.Sinerp(startRotationEuler, new Vector3(startRotationEuler.x, startRotationEuler.y, startRotationEuler.z + 50f), t));
			t += Time.deltaTime / 2.3f;
			yield return null;
		}
	}

	void
	GetInitialSettings()
	{
		startFOV = cam.fieldOfView;
		startRotationEuler = transform.rotation.eulerAngles;
		originalBlurAmount = motionBlur.blurAmount;
	}

	void
	ResetCamera()
	{
		cam.fieldOfView = startFOV;
		transform.rotation = Quaternion.Euler (startRotationEuler);
		motionBlur.blurAmount = originalBlurAmount;
		motionBlur.enabled = false;
	}
}