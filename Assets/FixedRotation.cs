﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedRotation : MonoBehaviour
{
	Quaternion rotation;

	[SerializeField] float xRotation;
	[SerializeField] float yRotation;
	[SerializeField] float zRotation;

	void
	Start()
    {
		rotation = Quaternion.Euler(new Vector3 (xRotation, yRotation, zRotation));
    }

    void
	Update()
    {
		print (1);
		transform.rotation = rotation;
    }
}